@echo off

del ..\Dist\Release\CsAlCore*.nupkg

REM Set version
dotnet msbuild ..\SetVersion\SetVersion.csproj /p:Configuration=Release /t:Clean,Build
dotnet ..\Dist\Release\netcoreapp2.0\SetVersion.dll ..\SolutionInfo.proj

REM Clean
dotnet msbuild ..\CsAl.Common\CsAlCore.Common.csproj /p:Configuration=Release /t:Clean
dotnet msbuild ..\CsAl\CsAlCore.csproj /p:Configuration=Release /t:Clean

REM Build and package
dotnet msbuild ..\CsAl.Common\CsAlCore.Common.csproj /p:Configuration=Release /t:Build
IF %ERRORLEVEL% NEQ 0 (
    echo Error building CsAlCore.Common
    pause
    exit /b -1
)
dotnet msbuild ..\CsAl\CsAlCore.csproj /p:Configuration=Release /t:Build
IF %ERRORLEVEL% NEQ 0 (
    echo Error building CsAlCore
    pause
    exit /b -1
)

REM Publish nuget package
set SOURCE="paoloiommarini.pkgs.visualstudio.com"
set NUGETPATH=\Development\Software\NuGet

for /f %%i in ('dir /b/a-d/od/t:c ..\Dist\Release\CsAlCore.?.*.nupkg') do set LAST=%%i
echo Publishing %LAST% to %SOURCE%

"%NUGETPATH%\nuget.exe" push -Source %SOURCE% -ApiKey VSTS ..\Dist\Release\%LAST%
IF %ERRORLEVEL% NEQ 0 (
    echo Error publishing CsAlCore.Common
    pause
    exit /b -1
)

for /f %%i in ('dir /b/a-d/od/t:c ..\Dist\Release\CsAlCore.Common.?.*.nupkg') do set LAST=%%i
echo Publishing %LAST% to %SOURCE%

"%NUGETPATH%\nuget.exe" push -Source %SOURCE% -ApiKey VSTS ..\Dist\Release\%LAST%
IF %ERRORLEVEL% NEQ 0 (
    echo Error publishing CsAlCore
    pause
    exit /b -1
)

pause
exit /b 0