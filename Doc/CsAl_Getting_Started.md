# Getting started

[TOC]

## Creating the data model

The data model is defined in a XML file.

Here we create a schema with two object types: subjectObj and addressObj. AddressObj is a child of subjectObj.

```xml
<?xml version = "1.0" encoding = "utf-8"?>
<Schema Name="TestSchema" Revision="1">
  <Description>
  	This is a test schema.
  </Description>
  <ObjectTypes>
      <ObjectType Name="addressObj" Inherits="object">
        <Attributes>
            <Attribute Name="address" Type="t:name" />
            <Attribute Name="city" Type="t:name" />
            <Attribute Name="zipCode" Type="String" MaxLength="10"/>
        </Attributes>
		<Indexes>
          <Index Name="idx_address_zipCode" >
            <Attributes>
                <Attribute Name="zipCode" />
            </Attributes>
          </Index>
        </Indexes>
      </ObjectType> 
          <ObjectType Name="subjectObj" Inherits="object">
        <Attributes>
            <Attribute Name="name" Type="t:name" Required="true" />
            <Attribute Name="surname" Type="t:name" />
            <Attribute Name="email" Type="t:name" />
            <Attribute Name="addresses" Type="MultiReference" RefObjectType="addressObj" 
                       ChildrenReference="true" />
        </Attributes>
        <Indexes>
          <Index Name="idx_subject_name" >
            <Attributes>
                <Attribute Name="name" />
                <Attribute Name="surname" />
            </Attributes>
          </Index>
        </Indexes>
      </ObjectType> 
  </ObjectTypes>
</Schema>
```

Save the schema to the file TestSchema.xml

The simplest way to create the database tables is to use the CsAlManager utility

```bash
CsAlManager.exe schemaupgrade --in TestSchema.xml --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

To create the database tables from code

```c#
CsAl.Configuration config = new CsAl.Configuration()
{
	DbType = "SQLite",
    DbConnectionString = "Data Source=test.sqlite;"
};

Schema newSchema = Schema.Load("TestSchema.xml");
using (CsAl.CsAl csal = new CsAl.CsAl(config)) {
  if (csal.Open()) {
    csal.UpgradeSchema(newSchema);
  }
}
```

## Creating schema C# classes

It is possible to create C# classes reflecting the object types in the schema.

```bash
CsAlManager.exe schematoclass --in TestSchema.xml --out project\SchemaClasses
```

## Importing/creating objects

## Reading/querying objects

To read objects CsAlCore exposes different methods.

Here's an example using a query to select objects to load:

```c#
foreach (GenericObject obj in csal.LoadObjects("select orderobj", null, DeletedObjectsStrategy.OnlyLive, -1)) {
  ...
}
```

## Deleting objects

