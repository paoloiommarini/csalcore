# CsAlCore Concepts

[TOC]

## Supported database engines

CsAlCore supports as its backend database:

- MariaDb from version 10.1
- MySQL from version 5.7
- Oracle from version 12c
- PostgreSQL from version 5.7
- SQLite
- Sql Server from version 2012

## The schema

Object types and their relations are defined in a XML schema. 

The XML schema contains a list of object types. A schema can include other schemas and all schemas includes the system schema containing system objects.

```xml
<?xml version = "1.0" encoding = "utf-8"?>
<Schema Name="TestSchema" Revision="1">
  <Description>
  	The schema description
  </Description>  
  <Includes>
    <File Path="base.xml" />
  </Includes>   
  <ObjectTypes>
    ...
  </ObjectTypes>
</Schma>
```

A schema can define named data types. The name must be prefixed by *t:*

The named data types can be used to define attributes.

```xml
<Schema Name="SystemSchema" Revision="1">
  <Description>
    System schema
  </Description>
  <!-- System types -->
  <Types>
    <Attribute Name="t:name" Type="String" MaxLength="255" />
    <Attribute Name="t:description" Type="String" MaxLength="2048" />
  </Types>
  ...
</Schema>
```

To create the database from the schema it is possible to use the [CsAlManager](#The-CsAlManager-utility) utility.

Example definition of *userGroupObj* and *userObj*:

```xml
<!-- User group -->
<ObjectType Name="userGroupObj" Inherits="object">
  <Description>Application user group</Description>
  <Attributes>
      <Attribute Name="name" Type="t:name" Required="true" />
      <Attribute Name="description" Type="t:description" />
  </Attributes>
  <Indexes>
    <Index Name="idx_userGroup_name" Unique="true">
      <Attributes>
          <Attribute Name="name" />
      </Attributes>
    </Index>
  </Indexes>
</ObjectType>   

<!-- User -->
<ObjectType Name="userObj" Inherits="object">
  <Description>Application user</Description>
  <Attributes>
    <Attribute Name="password" Type="Password" MaxLength="128" Required="true">
    	<Description>The user password</Description>
  	</Attribute>
  	<Attribute Name="userName" Type="String" MaxLength="128" Required="true">
    	<Description>The username</Description>
  	</Attribute>
    <Attribute Name="name" Type="t:name" />
    <Attribute Name="surname" Type="t:name" />
    <Attribute Name="email" Type="t:name" />
    <Attribute Name="birthday" Type="Date" />
    <Attribute Name="groups" Type="MultiReference" Sortable="true" 
               RefObjectType="userGroupObj">
      <SubAttributes>
        <Attribute Name="notes" Type="String" MaxLength="512" />
      </SubAttributes>
	</Attribute>
  </Attributes>
  <Indexes>
    <Index Name="idx_user_userName" Unique="true">
      <Attributes>
          <Attribute Name="userName" />
      </Attributes>
    </Index>
  </Indexes>
</ObjectType>
```

## The object type

The object type defines an object structure and its relation with other object types (using [Reference](#reference) and [MultiReference](#multireference) attributes).

Any object should inherit from the standard *object*. Object type not inheriting from *object* cannot be used in [Reference](#reference) and [Multireference](#multireference) attributes and cannot be [soft deleted](#soft_delete_and_hard_delete).

It is possible to define objects not inheriting from object but this opportunity should be used only for special reasons: we don't want for example our log object interferes with the other schema objects.

```xml
<ObjectType Name="testObject" Inherits="object">
  <Description>The object description</Description>
  <Attributes>
	...
  </Attributes>
</ObjectType>
```

An object can be defined *abstract*. An abstract object cannot be created and is used to define inheriting object types.

```xml
<ObjectType Name="testAbstractObject" Abstract="true" Inherits="object">
  <Description>The abstract object description</Description>
  <Attributes>
	...
  </Attributes>
</ObjectType>

<ObjectType Name="testObject" Inherits="testAbstractObject">
  <Description>The object description</Description>
  <Attributes>
	...
  </Attributes>
</ObjectType>
```

An object type can have one or more indexes.

```xml
<Indexes>
  <Index Name="indexName" Unique="true">
    <Attributes>
        <Attribute Name="firstAttributeName" />
        <Attribute Name="secondAttributeName" />      
    </Attributes>
  </Index>
</Indexes>
```

### Objects validation

Objects are validated when are created or updated.

Required attributes must be set (not null), attributes minimum value and maximum value must be respected.

It is also possible to add object validation to the schema using XSLT.

The *ValidationXslt* is applied to the object. If the XSLT returns a non empty collection of *ValidationErrors* the object validation has failed.

Example validation on *articleObj*:

```xml
<ObjectType Name="articleObj" Inherits="object">
  <Attributes>
      <Attribute Name="name" Type="t:name" Required="true" />
      <Attribute Name="description" Type="t:description" />
      <Attribute Name="weight" Type="Integer" />
      <Attribute Name="volume" Type="Integer" />
      <Attribute Name="image" Type="BinaryData" 
                 AllowedMimeTypes="image/jpeg;image/png" />
      <Attribute Name="documentation" Type="BinaryData" 
                 AllowedMimeTypes="application/pdf" />
  </Attributes>
    <ValidationXslt>
      <![CDATA[
      <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        <xsl:template match="/">
          <ValidationErrors>
            <xsl:if test="@weight &lt;= 0">
              <ValidationError Message="Invalid weight" 
                                        AttributeName="weight"/>
            </xsl:if>
            <xsl:if test="not(contains(name, 'Article'))">
              <ValidationError Message="Name does non contain the word 'Article'" 
							AttributeName="name"/>
            </xsl:if>
            <xsl:if test="@volume &lt;= 0">
              <ValidationError Message="Invalid volume" AttributeName="volume"/>
            </xsl:if>
            <xsl:if test="image/binaryData/@size &lt;= 1000000">
              <ValidationError Message="Image size must be greater than 1000000" 
							AttributeName="volume"/>
            </xsl:if>
          </ValidationErrors>
        </xsl:template>
      </xsl:stylesheet>
      ]]>
    </ValidationXslt>
</ObjectType>
```

### Objects serialization

Objects can be serialized in XML or JSON. Most of the attributes are rendered as XML attributes. Examples of  special serialization (like [Reference](#reference) and [MultiReference](#multireference)) can be found in the attribute's type documentation.

Null values won't be serialized (the XML won't contain empty attributes or elements).

Example of XML serialized object:

```xml
<subjectObj
  id="8605"
  created="2018-01-22T10:32:07.005Z">
  <name>Subject number 1530</name>
  <addresses>
    <ref
      idRef="8606">
      <addressObj
        id="8606"
        created="2018-01-22T10:32:07.007Z">
        <address>Address number 0</address>
        <city>Some City</city>
        <zipCode>12345</zipCode>
        <parentObjRef
          idRef="8605" />
      </addressObj>
    </ref>
    <ref
      idRef="8607">
      <addressObj
        id="8607"
        created="2018-01-22T10:32:07.008Z">
        <address>Address number 1</address>
        <city>Some City</city>
        <zipCode>12345</zipCode>
        <parentObjRef
          idRef="8605" />
      </addressObj>
    </ref>
  </addresses>
</subjectObj>
```

## The attributes

An attribute's name cannot contain spaces or special characters. Any attribute can be defined as *required*. Required attributes cannot be null.

```xml
<Attribute Name="myAttribute" Type="Integer" Required="true">
  <Description>Attribute's description</Description>
</Attribute>
```

An attribute's type can be one of the following:

###BinaryData

BinaryData attributes hold binary data (files). Binary data are saved in the file system.

Files in the file system are organized by attribute. The data root path is set in an [environment variable](#the-environment-variables).

Options:

- *AllowedMimeTypes*: a semicolon separated list of allowed mime types
- *MaxLength*: the file maximum size in bytes
- *MinLength*: the file minimum size in bytes

```xml
<Attribute Name="image" Type="BinaryData" 
           AllowedMimeTypes="image/jpeg;image/png" MinLength="0" MaxLength="16777216"/>
```

BinaryData attributes are serialized as an element.

Example of a serialized binary data 

```xml
<articleObj id="..." ...>
  ...
  <image>
    <binaryData id="..." fileName="article.jpg" mimeType="image/jpeg" size="73614"/>
  </image>
</articleObj>
```

### Blob

Blob attributes hold blob values (bytes). Blob values are stored in the database.

Options:

- *MaxLength*: the blob maximum length in bytes
- *MinLength*: the blob minimum length in bytes

```xml
<Attribute Name="myBlob" Type="Blob" MinLength="128" MaxLength="1024" />
```

Blob attributes are serialized as an element containing the Base64 encoded bytes.

```xml
<myBlob>/9j/4AAQSkZJRgABAQEA8ADwAAD/4QxBRXhpZgAASUkqAAgAAAAJAA8BAgASAAAAegAAAB...0pTSlNKU0pTSlNKU0pTSlNKU0pTSlNKU0pTSlNKU0pX//Z</myBlob>
```

### Boolean

Boolean attributes can hold the values *True*, *False* and *null* (only if the attribute is not defined as *required*).

Options:

- *DefaultValue*: the attribute's default value (true or false)

```xml
<Attribute Name="myBoolean" Type="Boolean" DefaultValue="true" />
```

Boolean attributes are serialized as an attribute.

### Date

Date attributes hold date values.

Options:

- *DefaultValue*: the attribute's default value (format *yyyy-MM-dd*)
- *MaxValue*: the date maximum value
- *MinValue*: the date minimum value

```xml
<Attribute Name="myDate" Type="Date" DefaultValue="2018-01-22" 
           MinValue="2010-01-01" MaxValue="2099-12-31" />
```

Date attributes are serialized as an attribute.

### DateTime

DateTime attributes hold date time values.

Options:

- *DefaultValue*: the attribute's default value (format *yyyy-MM-ddTHH:mm:ss.fffZ*)
- *MaxValue*: the date time maximum value
- *MinValue*: the date time minimum value

```xml
<Attribute Name="myDateTime" Type="DateTime" DefaultValue="2018-01-22T13:44:00.000Z"
           MinValue="2010-01-01T00:00:00.000Z" MaxValue="2099-12-31T23:59:59.999Z"/>
```

DateTime attributes are serialized as an attribute.

### Enum

Enum attributes hold a single value from a list of valid values.

Options:

- *DefaultValue*: the attribute's default value (the name of a valid value)
- *Values*: the list of valid values

```xml
<Attribute Name="priority" Type="Enum" DefaultValue="Normal">
    <Values>
        <Value Value="0" Name="VeryLow" />
        <Value Value="1" Name="Low" />
        <Value Value="2" Name="Normal" />
        <Value Value="3" Name="High" />
        <Value Value="4" Name="VeryHigh" />
        <Value Value="5" Name="Critical" />
    </Values>
</Attribute>
```

Enum attributes are serialized as an attribute.

### Flags

Flags attribute holds one or more values from a list of valid values.

Options:

- *DefaultValue*: the attribute's default value (a pipe separated list of valid value's names)
- *Values*: the list of valid values

```xml
<Attribute Name="categories" Type="Flags" DefaultValue="News|Local">
    <Values>
        <Value Name="News" />
        <Value Name="Sport" />
        <Value Name="Economy" />
        <Value Name="Local" />    
    </Values>
</Attribute>
```

Flags attributes are serialized as an attribute.

### Identity

The identity is a unique number identifying the object. Identity attributes are *required* by default. An object type can have only one Identity attribute.

Identity attributes are managed by CsAlCore.

```xml
<Attribute Name="myIdentity" Type="Identity" />
```

Identity attributes are serialized as an attribute.

### Integer

Integer attributes hold integer values.

Options:

- *DefaultValue*: the attribute's default value
- *MaxValue*: the number maximum value
- *MinValue*: the number maximum value

```xml
<Attribute Name="myInteger" Type="Integer" DefaultValue="1" 
           MinValue="1" MaxValue="10" />
```

Integer attributes are serialized as an attribute.

### MultiReference

MultiReference attributes defines a multiple reference to other objects.

Options:

- *RefObjectType*: the object type name of the referenced objects
- *Sortable*: if set to true the referenced objects can be sorted (i.e. you can move the referenced objects up and down), else the list is unsorted
- *SubAttributes*: additional attributes for the reference

```xml
<Attribute Name="muMultiRef" Type="MultiReference" Sortable="true"         
           RefObjectType="objectTypeName">
  <SubAttributes>
    <Attribute Name="notes" Type="String" MaxLength="512" />
  </SubAttributes>  
</Attribute>
```

MultiReference attributes are serialized as an element. Each reference, its sub attributes and the referenced object are enclosed in a *ref* element.

Example of a serialized MultiReference attribute

```xml
<userObj
  id="..."
  created="...">
  <userName>Administrator</userName>
  <name>Administrator</name>
  <groups>
    <ref
      idRef="...">
      <notes>additional notes</notes>
      <userGroupObj
        id="..."
        created="...">
        <additionalDescription />
        <name>Administrators</name>
      </userGroupObj>
    </ref>
  </groups>
</userObj>
```

#### ChildrenReference

ChildrenReference attributes are a special type of MultiReference. Children objects cannot exist without a parent.

```xml
<Attribute Name="muMultiRef" Type="MultiReference" Sortable="true"       
           RefObjectType="objectTypeName" ChildrenReference="true" />
```

Serialized children objects have an additional attribute *parentObjRef* (the reference to their parent object).

```xml
<subjectObj id="..." created="...">
  ...	
  <addresses>
    <ref idRef="...">
      <addressObj
        id="..."
        created="...">
        <address>Address number 0</address>
        <city>Some City</city>
        <zipCode>12345</zipCode>
        <parentObjRef
          idRef="..." />
      </addressObj>
    </ref>
	...    
  </addresses>
</subjectObj>
```

### Password

Password attributes hold passwords and won't be serialized when reading/exporting objects.

Options:

- *DefaultValue*: the attribute's default value
- *MaxLength*: the password maximum length
- *MinLength*: the password minimum length
- *ValidationRegEx*: a regular expression used to validate the attribute's value

```xml
<Attribute Name="myPassword" Type="Password" Required="true" 
           MinLength="8" MaxLength="255" ValidationRegEx="^[^/\\]+$" />
```

### Real

Real attributes hold real values.

Options:

- *DefaultValue*: the attribute's default value
- *MaxValue*: the number maximum value
- *MinValue*: the number maximum value

```xml
<Attribute Name="myInteger" Type="Real" DefaultValue="1.5" 
           MinValue="1.5" MaxValue="10.5" />
```

Real attributes are serialized as an attribute.

### Reference

Reference attributes hold a reference to another object.

Options:

- *RefObjectType*: the object type name of the referenced object

```xml
<Attribute Name="myReference" Type="Reference" RefObjectType="objectTypeName" />
```

Reference attributes are serialized in an element.

Example of a serialized Reference attribute

```xml
<orderDetailObj
  id="..."
  created="..."
  quantity="150">
  <articleRef
    idRef="...">
    <articleObj
      id="..."
      created="..."
      weight="1417"
      volume="1229">
      <name>Article number 1459</name>
    </articleObj>
  </articleRef>
  ...
</orderDetailObj>
```

### String

String attributes hold string values.

Options:

- *DefaultValue*: the attribute's default value
- *MaxLength*: the string maximum length
- *MinLength*: the string minimum length
- *ValidationRegEx*: a regular expression used to validate the attribute's value

```xml
<Attribute Name="myString" Type="String" DefaultValue="default" 
           MinLength="5" MaxLength="255" ValidationRegEx="^[^/\\]+$" />
```

String attributes are serialized as an element.

### TimeSpan

TimeSpan attributes hold time span values.

Options:

- *DefaultValue*: the attribute's default value (format *days.HH:mm:ss.fff*)
- *MaxValue*: the time span  maximum value
- *MinValue*: the time span minimum value

```xml
<Attribute Name="myTimeSpan1" Type="TimeSpan" DefaultValue="1.12:30:12:00"
           MinValue="00:01" MaxValue="5.00:00:00:00"/>
<Attribute Name="myTimeSpan2" Type="TimeSpan" DefaultValue="05:12:00" />
```

TimeSpan attributes are serialized as an attribute.

## The environment variables

Environment variables are saved in the database and hold settings for a CsAlCore database instance.

Available variables:

| Name               | Description                              |
| ------------------ | ---------------------------------------- |
| BinaryDataRoot     | The path where [binary data](#binarydata) are stored |
| BinaryDataMaxFiles | The maximum number of files for each folder in the [binary data](#binarydata) path |

## Soft delete and hard delete

An object can be deleted only if there are no other objects referencing it (i.e. you cannot delete an article if an orderDetailObj is referencing it). Children objects are automatically deleted with their parent objects.

When deleting objects it is possible to unreference or extend objects.

- *Unreference*: removes reference to the object that is being deleted (i.e. unreference the orderObj we are deleting from a shipmentObj)
- *Extend*: also delete the objects referencing the object that is being deleted (i.e. delete orderDetailObjs referencing the article we are deleting)

CsAlCore supports soft delete (logical delete) and hard delete.

When soft deleting objects the objects are marked as "deleted" but data remains in the database and it is possible to restore them.

When hard deleting objects the data are deleted from the database and it is not possible to restore them.

To delete objects it is possible to use the [CsAlManager](#deleting-objects) utility.

##The query language

CsAlCore uses a SQL-like language to select objects.

###The select syntax

The select command is used to read objects. Its general syntax is:

`SELECT objectTypeName [WHERE condition] [ORDER BY clause] [OFFSET offset] [LIMIT limit]`

The *objectTypeName* is mandatory and defines what type of object the query returns.  
The optional *where condition* is used to restrict object selection.  
The optional *order by* clause is used to sort results.   
The optional *offset* and *limit* values are used to limit results number.

###The object type name

The object type name is used to select what type of object the query returns.
The object type name is case insensitive.

```sql
select orderObj
select ORDEROBJ
select articleObj
```

### Data types

#### Boolean

Boolean values are represented by the words *true* and *false* (case insensitive).

```
True
False
```

#### Date

Date values are represented by the date in the format *yyyy-MM-dd* enclosed in quotes.

```
'2018-01-01'
'2018-02-28'
```

#### Date time

Date time values are represented by the UTC date time in the format *yyyy-MM-ddTHH:mm:ss.fffZ* enclosed in quotes.

```
'2018-01-01T12:34:56.000Z'
'2018-02-28T16:21:44.000Z'
```

#### Integer

Integer values are represented by a sequence of numbers without thousand separator.

```
101
1455
```

#### Null

Null is an "unset value". There isn't a representation of the null value.

You can check for a null value using the [IsNull](#isnull) function or manipulate it using the [NullValue](#nullvalue) function.

#### Real

Real values are represented by a sequence of numbers without thousand separator and using . (dot) as decimal separator.

```
1.65
4350.27
```

#### String

String values are represented by a sequence of characters enclosed in quotes. A quote inside the string must be escaped with the character \ (backslash)

```
'this is a string'
'it\'s a string!'
```

#### Time span

Time span values are represented by the time span in the format *days.HH:mm:ss.fff* enclosed in quotes.

```
'3.03:50:34.000' 3 days, 3 hours, 50 minutes and 34 seconds
'2:05:12.000' 2 hours, 5 minutes and 12 seconds
'30:34.000' 30 minutes and 34 seconds
```

### Operators

#### Comparison operators

Comparison operators are used to compare values.

| Op     | Description           | Example                              |
| ------ | --------------------- | ------------------------------------ |
| ==, =  | Equal                 | quantity == 5<br />quantity = 5      |
| !=, <> | Not equal             | quantity != 7.2<br />quantity <> 7.2 |
| >      | Greater than          | quantity >3                          |
| >=     | Greater than or equal | quantity >= 4.8                      |
| <      | Less than             | quantity <7                          |
| <=     | Less than or equal    | quantity <= 7                        |

#### Math operators

The math operators can be used only with numbers ([integers](#integer) and [reals](#real)).

| Op   | Description    | Example        |
| :--- | -------------- | -------------- |
| +    | Addition       | quantity + 5   |
| -    | Subtraction    | quantity - 7.2 |
| *    | Multiplication | quantity * 3   |
| /    | Division       | quantity / 4.8 |
| %    | Modulo         | quantity % 7   |

#### Bitwise operators

The bitwise operators can be used only with [integers](#integers) numbers.

| Op   | Description | Example    |
| ---- | ----------- | ---------- |
| &    | And         | value & 5  |
| \|   | Or          | value \| 3 |

Bitwise operators can be used to test [flags](#flags) attributes

```sql
select objectName where flagsAtt & 'Value' == 'Value'
select objectName where not(flagsAtt & 'Value' == 'Value')
```

#### Logical operators

Logical operators can be used only with boolean expressions and returns true or false.

| Op       | Description | Example                                  |
| -------- | ----------- | ---------------------------------------- |
| AND, &&  | And         | value == 1 and 5 == 3<br />value == 1 && 5 == 3 |
| OR, \|\| | Or          | value == 1 or 3 == 5<br />value == 1 \|\| 3 == 5 |
| NOT, !   | Not         | NOT(value == 1)<br />!(value == 1)       |

### Functions

Functions can be used to manipulate or test data.

#### Null functions

Null functions work with null values.

##### IsNull

The IsNull function returns true if *target* is null

```
IsNull(target)
Not(IsNull(target))
```

##### NullValue

The NullValue function returns *target* if *target* is not null, *value* if *target* is null

```
NullValue(target, value)
```

#### String functions

##### Contains

The contains function returns true if the *target* string contains the *test* string.

```
Contains(target, test)
```

##### EndsWith

Tha EndsWith function returns true if the *target* string ends with the *test* string.

```
EndsWith(target, test)
```

##### Length

The Length function returns the length in bytes of the *string*.

```
Length(string)
```

##### StartsWith

Tha StartsWith function returns true if a *target* string starts with the *test* string.

```
StartsWith(target, test)
```

##### Substr

The Substr function returns a substring of a *target* string, starting from the *indexFrom* for *length* characters.

```
Substr(target, indexFrom, length)
```

Examples:

```
Substr(name, 0, 3)
Substr(name, 3, 5)
```

#### Mathematical functions

##### Cos

The function Cos returns the cosine of a *number*.

```
Cos(number)
```

##### Power

The function Power returns the value of a *number* raised to the power of *power*.

```
Power(number, power)
```

##### Round

The function Round returns the value of a *number* rounded up to a *decimals* of digits to the right of the decimal point.

```
Round(number, decimals)
```

##### Sin

The function Sin returns the sine of a *number*.

```
Sin(number)
```

##### Sqrt

The function Sqrt returns the square root of a *number*.

```
Sqrt(number)
```

##### Tan

The function Tan returns the tangent of a *number*.

```
Tan(number)
```

#### Date time functions

##### DateAdd

The DateAdd function adds *number* *datePart*s to a *date* and returns the result.

Date parts are case sensitive.

| Date part        | Description      |
| ---------------- | ---------------- |
| years, y         | The year         |
| months, m        | The month        |
| days, d          | The days         |
| hours, h         | The hours        |
| minutes, n       | The minutes      |
| seconds, s       | The seconds      |
| milliseconds, ms | The milliseconds |

```
DateAdd(datePart, number, date)
```

Examples:

```
DateAdd(days, 5, date)
DateAdd(ms, 150, date)
```

##### DateDiff

The DateDiff function subtracts *number* *datePart*s from a *date* and returns the result.

Date parts are the same used in the [DateAdd](#dateadd) function.

```
DateDiff(datePart, number, date)
```

Examples:

```
DateDiff(days, 5, date)
DateDiff(ms, 150, date)
```

##### Day

The Day function returns the day date part of a *date* or date time value.

```
Day(date)
```

##### Hour

The Hour function returns the hours date part of a *date* time value.

```
Hour(dateTime)
```

##### Minute

The Minute function returns the minutes date part of a *dateTime* value.

```
Minute(dateTime)
```

##### Month

The Month function returns the month date part of a *date* or date time value.

```
Month(date)
```

##### Second

The Second function returns the seconds date part of a *dateTime* value.

```
Second(dateTime)
```

##### Year

The Year function returns the year date part of a date or date time value.

```
Year(date)
```

### Attribute path

Objects attributes are accessible via an attribute path. Reference attributes can be navigated using a . (dot) to access the referenced object.

```sql
select orderObj where orderObj.subjectRef.subjectObj.name == 'Subject number 495'
```

The attribute path reflects the xml representation of the object.

```xml
<orderObj
  id="10020"
  created="2016-09-07T18:17:07Z"
  modified="2016-09-07T18:17:07Z"
  priority="Normal"
  status="Pending">
  <name>Order number 0</name>
  <subjectRef
    idRef="5502">
    <subjectObj
      id="5502"
      created="2016-09-07T18:17:05Z"
      modified="2016-09-07T18:17:05Z">
      <name>Subject number 495</name>
    </subjectObj>
  </subjectRef>
  <detail>
    <orderDetailObj>
    ...
    </orderDetailObj>
  </detail>
</orderObj>
```

### The where condition

The where condition is used to restrict object selection.

#### Parameters

It is possible to use parameters in queries. Parameters are prefixed with a @.

A parameter's name cannot contain spaces or special characters.

```
@paramName
```

Examples:

```sql
select orderObj where name == @name
select orderObj where detail.ref.orderDetailObj.quantity >= @minQuantity
```

### The order by clause

The order by clause is used to sort objects returned by a query.

```
select orderObj order by name
select orderObj order by created desc, name
```

## Executing raw SQL commands

It is possible to execute raw SQL commands by using the database connection provided by CsAlCore

```c#
using (System.Data.IDbConnection conn = csal.CreateDbConnection()) {
    using (System.Data.IDbCommand cmd = conn.CreateCommand()) {
        cmd.CommandText = "update ...";
        int affected = cmd.ExecuteNonQuery();
    }

    using (System.Data.IDbCommand cmd = conn.CreateCommand()) {
        cmd.CommandText = "select ...";
        using (System.Data.IDataReader reader = cmd.ExecuteReader()) {
            while (reader.Read()) {
                ...
            }
        }
    }
}
```

## The CsAlManager utility

The CsAlManger utility can be used to create and manage a CsAlCore database instance.

The utility has an internal help system to provide help with commands. To see the general help and a list of available commands use the command

```bash
CsAlManager.exe help
dotnet CsAlManager.dll help
```

Almost any command needs the arguments *--dbtype* (the database type) and *--dbconn* (the database connection string). The help contains informations on mandatory and optional arguments.

### Creating, upgrading and exporting the schema

The command SchemaUpgrade creates a new schema or upgrades an existing schema.

```bash
CsAlManager.exe schemaupgrade --in schema.xml --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

If upgrading an existing schema involves dropping or modifying a database column CsAlManager will ask for confirmation. To avoid this (i.e. in a command run in background) add the option *--force*.

To export a schema from a database instance use the command SchemaExport.

```bash
CsAlManager.exe schemaexport --singlefile --out schema.xml --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

The above command will output a single file containing the full schema (the schema computed from all the included schemas and the system schema). To export the original files remove the *--singlefile* argument.

### Generating C# classes from the schema

The command SchemaToClass generates C# classes reflecting the [object types](#the-object-type) defined in the schema. Using the schema classes is not mandatory (it is always possible to use the GenericObject class to read and write objects).

```bash
CsAlManager.exe schematoclass --out project\SchemaClasses --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

The above command will generate a class for each object type found in the schema. It is possible to filter object type using the argument *--objectTypes*

```bash
CsAlManager.exe schematoclass --out project\SchemaClasses --objecttypes "object1;object2" --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

The default namespace for the generated classes is SchemaClasses. It is possible to change the namespace using the argument *--namespace*

```bash
CsAlManager.exe schematoclass --out project\SchemaClasses --namespace "MyNamespace" --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

The command can generate C# classes from a CsAlCore database instance or from the schema file

```bash
CsAlManager.exe schematoclass --out project\SchemaClasses --in schema.xml
```

### Generating the schema documentation

The command SchemaToDoc generates an HTML document with the schema documentation (object types, attributes and relations).

```bash
CsAlManager.exe schematodoc --out schema.html --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

### Exporting objects

The command DataExport exports objects to a XML file.

Objects can be selected by a query:

```bash
CsAlManager.exe dataexport --out export.xml --query "select orderObj" --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

or by Ids:

```bash
CsAlManager.exe dataexport --out export.xml --ids 10;15;22;67 --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

It is possible to limit the maximum depth ([reference](#reference) and [multiReference](#multireference) navigation) using the argument *--maxdepth*

```bash
CsAlManager.exe dataexport --out export.xml --ids 10;15;22;67 --maxdepth 5 --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

### Importing objects

The command DataImport imports objects from a XML file. Existing objects are updated, new objects are created.

```bash
CsAlManager.exe dataimport --in export.xml --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

### Deleting objects

The command ObjsSoftDelete soft deletes objects (logical delete).

For more informations about *unref* and *extend* read the section [soft delete and hard delete](#Soft-delete-and-hard-delete).

```bash
CsAlManager.exe objssoftdelete --query "select orderObj where name = 'order name'" --unref "shipmentObj.orders" --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

The command ObjsHardDelete hard deletes objects.

```bash
CsAlManager.exe objsharddelete --query "select orderObj where name = 'order name'" --unref "shipmentObj.orders" --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

### Restoring objects

The command ObjsRestore restores soft deleted objects. It is not possible to restore references removed using the *--unref* argument when the objects have been soft deleted.

```bash
CsAlManager.exe objsrestore --query "select orderObj where name = 'order name'" --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

### Reading environment variables

The command EnvVariableList lists all environment variables with their values.

```bash
CsAlManager.exe envvariablelist --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

The command EnvVariableGet displays the value of an environment variable.

```bash
CsAlManager.exe envvariableget --EnvVarName "BinaryDataRoot" --dbtype sqlite --dbconn "Data Source=test.sqlite;"
```

### Setting environment variables

The command EnvVariableSet sets the value of an environment variable. The environment variable name must be a valid [environment variable](#The-environment-variables)'s name

```bash
CsAlManager.exe envvariableset --EnvVarName "BinaryDataRoot" --EnvVarValue "c:\Data\"
```

## The example schema

This is the schema used in this documentation.

```xml
<!-- Subject -->
<ObjectType Name="subjectObj" Inherits="object">
  <Attributes>
      <Attribute Name="name" Type="t:name" Required="true" />
      <Attribute Name="surname" Type="t:name" />
      <Attribute Name="email" Type="t:name" />
      <Attribute Name="addresses" Type="MultiReference" RefObjectType="addressObj" 
                 ChildrenReference="true" />
  </Attributes>
  <Indexes>
    <Index Name="idx_subject_name" >
      <Attributes>
          <Attribute Name="name" />
          <Attribute Name="surname" />
      </Attributes>
    </Index>
  </Indexes>
</ObjectType> 

<!-- Address -->
<ObjectType Name="addressObj" Inherits="object">
  <Attributes>
    <Attribute Name="address" Type="t:name" />
    <Attribute Name="city" Type="t:name" />
    <Attribute Name="zipCode" Type="String" MaxLength="10"/>
  </Attributes>
  <Indexes>
    <Index Name="idx_address_zipCode" >
      <Attributes>
        <Attribute Name="zipCode" />
      </Attributes>
    </Index>
  </Indexes>
</ObjectType> 

<!-- Article -->
<ObjectType Name="articleObj" Inherits="object">
  <Attributes>
      <Attribute Name="name" Type="t:name" Required="true" />
      <Attribute Name="description" Type="t:description" />
      <Attribute Name="weight" Type="Integer" />
      <Attribute Name="volume" Type="Integer" />
	  <Attribute Name="image" Type="BinaryData" 
                 AllowedMimeTypes="image/jpeg;image/png" />
	  <Attribute Name="documentation" Type="BinaryData" 
                 AllowedMimeTypes="application/pdf" />    
  </Attributes>
  <Indexes>
    <Index Name="idx_article_name" Unique="true">
      <Attributes>
          <Attribute Name="name" />
      </Attributes>
    </Index>
  </Indexes>
</ObjectType> 

<!-- Order -->
<ObjectType Name="orderObj" Inherits="object">
  <Attributes>
      <Attribute Name="name" Type="t:name" Required="true" />
      <Attribute Name="subjectRef" Type="Reference" RefObjectType="subjectObj" 
                 Required="true" />
      <Attribute Name="detail" Type="MultiReference" RefObjectType="orderDetailObj" 
                 Sortable="true" ChildrenReference="true" />
      <Attribute Name="priority" Type="Enum" Required="true" DefaultValue="Normal">
          <Values>
              <Value Value="0" Name="VeryLow" />
              <Value Value="1" Name="Low" />
              <Value Value="2" Name="Normal" />
              <Value Value="3" Name="High" />
              <Value Value="4" Name="VeryHigh" />
              <Value Value="5" Name="Critical" />
          </Values>
      </Attribute>
      <Attribute Name="status" Type="Enum" Required="true" DefaultValue="Pending">
          <Values>
              <Value Value="0" Name="Pending" />
              <Value Value="1" Name="AwaitingFulfillment" />
              <Value Value="2" Name="AwaitingShipment" />
              <Value Value="3" Name="PartiallyShipped" />
              <Value Value="4" Name="Shipped" />
              <Value Value="5" Name="Cancelled" />
          </Values>
      </Attribute>
  </Attributes>
  <Indexes>
    <Index Name="idx_order_name" Unique="true">
      <Attributes>
          <Attribute Name="name" />
      </Attributes>
    </Index>
    <Index Name="idx_order_subject" >
      <Attributes>
          <Attribute Name="subjectRef" />
      </Attributes>
    </Index>
  </Indexes>
</ObjectType> 

<!-- Order detail-->
<ObjectType Name="orderDetailObj" Inherits="object">
  <Attributes>
      <Attribute Name="articleRef" Type="Reference" RefObjectType="articleObj" 
                 Required="true" />
      <Attribute Name="quantity" Type="Integer" Required="true" MinValue="1"/>
  </Attributes>
</ObjectType>
```