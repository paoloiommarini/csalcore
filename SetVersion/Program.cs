﻿using System;
using System.IO;
using System.Text;

namespace SetVersion
{
    class Program
    {
        static int Main(string[] args)
        {
            try {
                return SetVersion(args);
            } catch (Exception ex) {
                Console.WriteLine($"Exception: { ex.Message }");
                return -1;
            }                 
        }

        static int SetVersion(string[] args)
        {
            string fileName = args[0];
            if (File.Exists(fileName)) {
                if (File.Exists($"{ fileName }.tmp"))
                    File.Delete($"{ fileName }.tmp");
                File.Move(fileName, $"{ fileName }.tmp");

                using (StreamReader sr = new StreamReader($"{ fileName }.tmp", Encoding.UTF8)) {
                    using (StreamWriter sw = new StreamWriter($"{ fileName }", false, Encoding.UTF8)) {
                        string line = sr.ReadLine();
                        while (line != null) {
                            if (line.Contains("<Version>")) {
                                line = line.Trim();
                                line = line.Replace("<Version>", string.Empty).Replace("</Version>", string.Empty);
                                string[] v = line.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                                Version version = new Version(line);

                                int newBuild = (DateTime.Now - new DateTime(2017, 1, 1)).Days;
                                int newRevision = newBuild == version.Build ? version.Revision + 1 : 0;
                                Version newVersion = new Version(version.Major, version.Minor, newBuild, newRevision);
                                Console.WriteLine($"Version: { newVersion }");
                                line = $"    <Version>{ newVersion }</Version>";
                            } else if (line.Contains("<Copyright>")) {
                                if (DateTime.Now.Year > 2018)
                                    line = $"    <Copyright>Copyright © 2018, { DateTime.Now.Year } Paolo Iommarini &lt;paolo.iommarini@live.com&gt;</Copyright>";
                                else
                                    line = "    <Copyright>Copyright © 2018 Paolo Iommarini &lt;paolo.iommarini@live.com&gt;</Copyright>";
                            }
                            sw.WriteLine(line);
                            line = sr.ReadLine();
                        }
                    }
                }
                File.Delete($"{ fileName }.tmp");
                return 0;
            } else {
                Console.WriteLine($"File { fileName } not found");
                return -1;
            }

        }
    }
}
