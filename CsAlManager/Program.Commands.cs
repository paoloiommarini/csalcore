﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace CsAlCoreManager
{
    partial class Program
    {
        #region Utility
        static string GetOption(Options opt, Dictionary<string, string> options)
        {
            return GetOption(opt, options, string.Empty);
        } // GetOption

        static string GetOption(Options opt, Dictionary<string, string> options, string defaultValue)
        {
            string res = string.Empty;
            if (options.TryGetValue(opt.ToString(), out res))
                return res;
            return defaultValue;
        } // GetOption

        static CsAlCore.CsAlCore.DbType GetDbType(string strValue)
        {
            CsAlCore.CsAlCore.DbType res = CsAlCore.CsAlCore.DbType.None;
            Enum.TryParse<CsAlCore.CsAlCore.DbType>(strValue, true, out res);
            return res;
        } // GetDbType

        static CsAlCore.CsAlCore OpenDatabase(Dictionary<string, string> options)
        {
            return OpenDatabase(options, false);
        }

        static CsAlCore.CsAlCore OpenDatabase(Dictionary<string, string> options, bool ignoreSchemaError)
        {
            string dbConn = options[Options.DbConn.ToString()];
            CsAlCore.CsAlCore.DbType dbType = GetDbType(options[Options.DbType.ToString()]);
            if (dbType == CsAlCore.CsAlCore.DbType.None) {
                Console.WriteLine($"Invalid database type { options[Options.DbType.ToString()] }");
                return null;
            }

            CsAlCore.Configuration config = new CsAlCore.Configuration()
            {
                DbType = dbType,
                DbConnectionString = dbConn
            };

            CsAlCore.CsAlCore csal = new CsAlCore.CsAlCore(config);
            if (!csal.Open()) {
                Console.WriteLine("Failed to open database");
                return null;
            }

            Schema schema = csal.DbSchema;
            if (!ignoreSchemaError && schema == null) {
                Console.WriteLine("No schema found");
                return null;
            }

            if (schema != null)
                Console.WriteLine($"Database { dbType } v.{ csal.DatabaseVersion }, schema { schema.Name } revision { schema.Revision }");
            else
                Console.WriteLine($"Database { dbType } v.{ csal.DatabaseVersion }");
            return csal;
        } // OpenDatabase
        #endregion

        #region Schema
        static bool SchemaUpgrade(Dictionary<string, string> options)
        {
            bool force = string.Compare(GetOption(Options.Force, options), "true", StringComparison.OrdinalIgnoreCase) == 0;
            string inputFile = Path.GetFullPath(options[Options.In.ToString()]);
            if (!File.Exists(inputFile)) {
                Console.WriteLine("Schema file not found: {0}", inputFile);
                return false;
            }

            using (CsAlCore.CsAlCore csal = OpenDatabase(options, true)) {
                if (csal == null)
                    return false;

                if (csal.DbSchema != null)
                    Console.WriteLine("Current schema {0} revision {1}", csal.DbSchema.Name, csal.DbSchema.Revision);

                Schema newSchema = Schema.Load(inputFile);
                if (newSchema != null) {
                    Console.WriteLine("Upgrading schema {0} to revision {1}", newSchema.Name, newSchema.Revision);

                    if (csal.DbSchema != null) {
                        // Compare schemas
                        CsAlCore.CsAlCore.SchemaDifferences diff = csal.CompareSchema(newSchema);
                        if (diff != null) {
                            if (diff.NewObjectTypes.Count > 0) {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("New object types:");
                                Console.ResetColor();
                                foreach (ObjectType ot in diff.NewObjectTypes)
                                    Console.WriteLine($"  { ot.Name }");
                            }

                            if (diff.NewObjectAttributes.Count > 0) {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("New attributes:");
                                Console.ResetColor();
                                foreach (ObjectAttribute at in diff.NewObjectAttributes)
                                    Console.WriteLine($"  { at.ObjectTypeName }.{ at.Name }");
                            }

                            if (diff.NewObjectIndexes.Count > 0) {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("New indexes:");
                                Console.ResetColor();
                                foreach (KeyValuePair<ObjectType, List<ObjectIndex>> kvp in diff.NewObjectIndexes) {
                                    foreach (ObjectIndex idx in kvp.Value)
                                        Console.WriteLine($"  { kvp.Key.Name }.{ idx.Name }");
                                }
                            }

                            if (diff.RemovedObjectTypes.Count > 0) {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("Removed object types:");
                                Console.ResetColor();
                                foreach (ObjectType ot in diff.RemovedObjectTypes)
                                    Console.WriteLine($"  { ot.Name }");
                            }

                            if (diff.RemovedObjectAttributes.Count > 0) {                                
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("Removed attributes:");
                                Console.ResetColor();
                                foreach (ObjectAttribute at in diff.RemovedObjectAttributes)
                                    Console.WriteLine($"  { at.ObjectTypeName }.{ at.Name }");
                            }

                            if (diff.RemovedObjectIndexes.Count > 0) {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("Removed indexes:");
                                Console.ResetColor();
                                foreach (KeyValuePair<ObjectType, List<ObjectIndex>> kvp in diff.RemovedObjectIndexes) {
                                    foreach (ObjectIndex idx in kvp.Value)
                                        Console.WriteLine($"  { kvp.Key.Name }.{ idx.Name }");
                                }
                            }

                            if (diff.ModifiedObjectAttributes.Count > 0) {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine("Modified attributes:");
                                Console.ResetColor();
                                foreach (ObjectAttribute at in diff.ModifiedObjectAttributes)
                                    Console.WriteLine($"  { at.ObjectTypeName }.{ at.Name }");
                            }
                            
                            if (!force && diff.RemovedObjectTypes.Count > 0 || diff.RemovedObjectAttributes.Count > 0) {
                                Console.WriteLine();
                                while (true) {
                                    Console.Write("Confirm schema upgrade (y/N)? ");
                                    string res = Console.ReadLine().Trim();
                                    if (string.Compare(res, "y", StringComparison.OrdinalIgnoreCase) == 0) {
                                        break;
                                    } else if (string.IsNullOrEmpty(res) || string.Compare(res, "n", StringComparison.OrdinalIgnoreCase) == 0) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    csal.UpgradeSchema(newSchema, OnSchemaUpgradeStep);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Database schema upgraded");
                    Console.ResetColor();
                }
            }
            return true;
        } // SchemaUpgrade

        static void OnSchemaUpgradeStep(CsAlCore.CsAlCore.SchemaUpgradeStepType type, string objectTypeName)
        {
            if (type == CsAlCore.CsAlCore.SchemaUpgradeStepType.Create)
                Console.WriteLine($"Creating { objectTypeName }");
            else if (type == CsAlCore.CsAlCore.SchemaUpgradeStepType.Delete)
                Console.WriteLine($"Deleting { objectTypeName }");
            else
                Console.WriteLine($"Modifying { objectTypeName }");
        } // OnSchemaUpgradeStep

        static bool SchemaExport(Dictionary<string, string> options)
        {
            bool singleFile = string.Compare(GetOption(Options.SingleFile, options), "true", StringComparison.OrdinalIgnoreCase) == 0;

            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;

                if (singleFile) {
                    // Single file
                    string outputFile = Path.GetFullPath(options[Options.Out.ToString()]);
                    string xml = CsAlCore.Common.Utility.Serialization.Serialize(csal.DbSchema);
                    using (StreamWriter sw = new StreamWriter(outputFile, false, Encoding.UTF8)) {
                        sw.Write(xml);
                    }
                    Console.WriteLine("Schema saved to file {0}", outputFile);
                } else {
                    // Multiple files:
                    string path = Path.GetFullPath(options[Options.Out.ToString()]);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    foreach (Schema.SchemaFile sf in csal.DbSchema.Files) {
                        string outputFile = Path.Combine(path, sf.FileName);                        
                        using (StreamWriter sw = new StreamWriter(outputFile, false, Encoding.UTF8)) {
                            sw.Write(sf.Content);
                        }
                        Console.WriteLine("Schema saved to file {0}", outputFile);
                    }
                }
            }
            return true;
        } // SchemaExport

        static bool SchemaRevision(Dictionary<string, string> options)
        {
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;

                Console.WriteLine("Schema {0} revision {1}", csal.DbSchema.Name, csal.DbSchema.Revision);
            }
            return true;
        } // SchemaRevision

        static bool SchemaToClass(Dictionary<string, string> options)
        {
            string dbConn = GetOption(Options.DbConn, options);
            string dbType = GetOption(Options.DbType, options);
            string schemaFile = GetOption(Options.In, options);

            if (!string.IsNullOrEmpty(schemaFile) && !string.IsNullOrEmpty(dbConn)) {
                Console.WriteLine("Cannot set both In and DbConn");
                return false;
            }

            if (string.IsNullOrEmpty(schemaFile) && string.IsNullOrEmpty(dbConn)) {
                Console.WriteLine($"Missing option { Options.DbConn } or { Options.In }");
                return true;
            }

            Schema schema = null;
            if (!string.IsNullOrEmpty(dbConn)) {
                using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                    if (csal == null)
                        return false;

                    schema = csal.DbSchema;
                }
            } else {
                schema = Schema.Load(schemaFile);
            }

            string outputFolder = options[Options.Out.ToString()];
            string classNamespace = GetOption(Options.Namespace, options);
            string[] objectTypes = null;
            string strOt = GetOption(Options.ObjectTypes, options);
            if (!string.IsNullOrEmpty(strOt))
                objectTypes = GetOption(Options.ObjectTypes, options).Split(';');
            else
                objectTypes = schema.ObjectTypeNames.ToArray();

            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

            CsAlCore.ClassGenerator.Generator gen = new CsAlCore.ClassGenerator.Generator(schema);
            Console.WriteLine("Exporting object types");
            foreach (string ot in objectTypes) {
                ObjectType sot = schema.GetObjectType(ot);
                if (sot == null) {
                    Console.WriteLine("Unknown object type {0}", ot);
                    return false;
                }
                if (!sot.SystemObject || string.Compare(sot.Name, "Object", StringComparison.OrdinalIgnoreCase) == 0) {
                    Console.WriteLine("  {0}", sot.Name);
                    using (StreamWriter sw = new StreamWriter(Path.Combine(outputFolder, string.Format("{0}.cs", string.Format("{0}{1}", char.ToUpper(sot.Name[0]), sot.Name.Substring(1)))), false, Encoding.UTF8)) {
                        sw.Write(gen.GenerateClass(classNamespace, ot, string.Empty));
                    }
                }
            }

            return true;
        } // SchemaToClass

        static bool SchemaToDoc(Dictionary<string, string> options)
        {
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;

                XslCompiledTransform xslt = new XslCompiledTransform();
                using (Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("CsAlManager.Resources.SchemaToDoc.xslt")) {
                    using (XmlReader xtr = XmlReader.Create(stream)) {
                        xslt.Load(xtr);
                    }
                }

                string outputFile = Path.GetFullPath(options[Options.Out.ToString()]);
                string xml = CsAlCore.Common.Utility.Serialization.Serialize(csal.DbSchema);
                using (TextReader tr = new StringReader(xml)) {
                    using (XmlReader xr = XmlReader.Create(tr)) {
                        using (StreamWriter sw = new StreamWriter(outputFile, false, Encoding.UTF8)) {
                            XsltArgumentList args = new XsltArgumentList();
                            args.AddParam("version", string.Empty, Assembly.GetExecutingAssembly().GetName().Version.ToString());
                            xslt.Transform(xr, args, sw);
                        }
                    }
                }
                Console.WriteLine("Schema documentation saved to file {0}", outputFile);
            }
            return true;
        } // SchemaToDoc
        #endregion

        #region Data
        /// <summary>
        /// Exports data to XML
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns>True on success</returns>
        static bool DataExport(Dictionary<string, string> options)
        {
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;

                string strIdList = GetOption(Options.Ids, options);
                string query = GetOption(Options.Query, options);
                string outputFile = options[Options.Out.ToString()];
                string xsltFile = GetOption(Options.Xslt, options);
                int maxDepth = int.Parse(GetOption(Options.MaxDepth, options, "-1"));

                if (!string.IsNullOrEmpty(strIdList) && !string.IsNullOrEmpty(query)) {
                    Console.WriteLine("Cannot set both Ids and Query");
                    return false;
                }

                List<long> idList = !string.IsNullOrEmpty(strIdList) ? CsAlCore.Common.Utility.String.StringIdListToList(strIdList) : null;
                if (!string.IsNullOrEmpty(query)) {
                    if (idList == null)
                        idList = new List<long>();

                    ObjectType objectType = null;
                    List<CsAlCore.Common.IdName> ids = csal.SearchObjs(query, out objectType);
                    foreach (CsAlCore.Common.IdName idName in ids) {
                        idList.Add(idName.Id);
                    }
                }

                if (string.IsNullOrEmpty(query) && (idList == null || idList.Count == 0)) {
                    Console.WriteLine("No object to export");
                    return true;
                }

                int count = 0;
                if (idList != null && idList.Count > 0) {
                    Console.Write("Exporting objects [");
                    int col = Console.CursorLeft;
                    Console.CursorLeft += 21;
                    Console.Write("]");
                    Console.CursorLeft = col;

                    XslCompiledTransform xslt = null;
                    if (!string.IsNullOrEmpty(xsltFile)) {
                        xslt = new XslCompiledTransform();
                        xslt.Load(xsltFile);
                    }

                    idList.Sort();
                    string xmlOutputFile = xslt != null ? Path.GetTempFileName() : outputFile;
                    using (StreamWriter sw = new StreamWriter(xmlOutputFile, false, Encoding.UTF8) { AutoFlush = true }) {
#if DEBUG
                        XmlWriterSettings xSettings = new XmlWriterSettings()
                        {
                            Encoding = Encoding.UTF8,
                            Indent = true,
                            NewLineOnAttributes = true
                        };
#else
                    XmlWriterSettings xSettings = new XmlWriterSettings()
                    {
                        Encoding = Encoding.UTF8,
                        Indent = false,
                        NewLineOnAttributes = false
                    };
#endif
                        Console.CursorVisible = false;
                        using (XmlWriter xw = XmlWriter.Create(sw, xSettings)) {
                            xw.WriteStartElement(Schema.ObjectsListElementName);
                            int stepSize = idList.Count < 20 ? idList.Count : idList.Count / 20;
                            string stepChars = new string('.', idList.Count < 20 ? 21 / idList.Count : 1);

                            foreach (GenericObject obj in csal.LoadObjects(idList, maxDepth)) {
                                obj.Serialize(xw);
                                xw.Flush();
                                if (count + 1 == idList.Count || count % stepSize == 0)
                                    Console.Write(stepChars);
                                count++;
                            }

                            Console.WriteLine();
                            xw.WriteEndElement();
                        }
                        Console.CursorVisible = true;
                    }

                    if (xslt != null) {
                        Console.WriteLine("Applying stylesheet");
                        xslt.Transform(xmlOutputFile, outputFile);
                    }
                }
                Console.WriteLine("Objects exported: {0}", count);
            }
            return true;
        } // DataExport

        /// <summary>
        /// Imports data from XML
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns>True on success</returns>
        static bool DataImport(Dictionary<string, string> options)
        {
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;
                string inputFile = options[Options.In.ToString()];

                int newCount = 0;
                int updateCount = 0;
                using (StreamReader sr = new StreamReader(inputFile)) {
                    using (XmlReader xr = XmlReader.Create(sr)) {
                        xr.MoveToContent();
                        if (xr.NodeType == XmlNodeType.Element && xr.Name == Schema.ObjectsListElementName) {
                            int stepSize = 100;
                            Console.Write("Importing objects: ");
                            int row = Console.CursorTop;
                            int col = Console.CursorLeft;
                            Console.Write(newCount + updateCount);

                            while (xr.Read()) {
                                if (xr.NodeType == XmlNodeType.Element) {
                                    string nodeName = xr.Name;
                                    GenericObject obj = GenericObject.Deserialize(csal.DbSchema, xr);
                                    if (obj != null) {
                                        if (obj.Id != 0) {
                                            if (csal.UpdateObject(obj))
                                                updateCount++;
                                        } else {
                                            if (csal.CreateObject(obj) != 0)
                                                newCount++;
                                        }

                                        if (newCount + updateCount % stepSize == 0) {
                                            Console.CursorLeft = col;
                                            Console.Write(newCount + updateCount);
                                        }
                                    } else {
                                        Console.WriteLine("Failed to deserialize object from node: {0}", nodeName);
                                        return false;
                                    }
                                }
                            }

                            Console.CursorLeft = col;
                            Console.WriteLine(newCount + updateCount);
                            Console.CursorTop = row;
                            Console.CursorLeft = 0;
                        } else {
                            Console.WriteLine("No objects to import");
                            return false;
                        }
                    }
                }                

                Console.WriteLine("Total objects imported: {0}", newCount + updateCount);
                Console.WriteLine("Objects created: {0}", newCount);
                Console.WriteLine("Objects updated: {0}", updateCount);
            }
            return true;
        } // DataImport
        #endregion

        #region Delete and restore
        /// <summary>
        /// Soft delete objects
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns>True on success</returns>
        static bool SoftDeleteObjects(Dictionary<string, string> options)
        {
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;

                string strIdList = GetOption(Options.Ids, options);
                string query = GetOption(Options.Query, options);
                string unref = GetOption(Options.Unref, options);
                string extend = GetOption(Options.Extend, options);

                if (!string.IsNullOrEmpty(strIdList) && !string.IsNullOrEmpty(query)) {
                    Console.WriteLine("Cannot set both Ids and Query");
                    return false;
                }

                List<long> idList = !string.IsNullOrEmpty(strIdList) ? CsAlCore.Common.Utility.String.StringIdListToList(strIdList) : null;
                if (!string.IsNullOrEmpty(query)) {
                    ObjectType objectType = null;
                    List<CsAlCore.Common.IdName> ids = csal.SearchObjs(query, out objectType);
                    foreach (CsAlCore.Common.IdName idName in ids) {
                        if (idList == null)
                            idList = new List<long>();
                        idList.Add(idName.Id);
                    }
                }

                if (string.IsNullOrEmpty(query) && (idList == null || idList.Count == 0)) {
                    Console.WriteLine("No object to soft delete");
                    return true;
                }

                List<long> deleted = null;
                if (idList != null && idList.Count > 0)
                    deleted = csal.SoftDeleteObjects(idList, new CsAlCore.CsAlCore.DeleteOptions(unref, extend));

                if (deleted == null || deleted.Count == 0) {
                    Console.WriteLine("No object soft deleted");
                } else {
                    Console.Write("Soft deleted objects: ");
                    bool first = true;
                    foreach (long id in deleted) {
                        if (!first)
                            Console.Write(", ");
                        Console.Write(id);
                        first = false;
                    }
                }
            }
            return true;
        } // SoftDeleteObjects

        /// <summary>
        /// Restore soft deleted objects
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns>True on success</returns>
        static bool RestoreObjects(Dictionary<string, string> options)
        {
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;

                string strIdList = GetOption(Options.Ids, options);
                string query = GetOption(Options.Query, options);
                string extend = GetOption(Options.Extend, options);

                if (!string.IsNullOrEmpty(strIdList) && !string.IsNullOrEmpty(query)) {
                    Console.WriteLine("Cannot set both Ids and Query");
                    return false;
                }

                List<long> idList = !string.IsNullOrEmpty(strIdList) ? CsAlCore.Common.Utility.String.StringIdListToList(strIdList) : null;
                if (!string.IsNullOrEmpty(query)) {
                    ObjectType objectType = null;
                    List<CsAlCore.Common.IdName> ids = csal.SearchObjs(query, out objectType);
                    foreach (CsAlCore.Common.IdName idName in ids) {
                        if (idList == null)
                            idList = new List<long>();
                        idList.Add(idName.Id);
                    }
                }

                if (string.IsNullOrEmpty(query) && (idList == null || idList.Count == 0)) {
                    Console.WriteLine("No object to restore");
                    return true;
                }

                List<long> restored = null;
                if (idList != null && idList.Count > 0)
                    restored = csal.RestoreObjects(idList, new CsAlCore.CsAlCore.RestoreOptions(extend));

                if (restored == null || restored.Count == 0) {
                    Console.WriteLine("No object restored");
                } else {
                    Console.Write("Restored objects: ");
                    bool first = true;
                    foreach (long id in restored) {
                        if (!first)
                            Console.Write(", ");
                        Console.Write(id);
                        first = false;
                    }
                }
            }
            return true;
        } // RestoreObjects

        /// <summary>
        /// Hard delete objects
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns>True on success</returns>
        static bool HardDeleteObjects(Dictionary<string, string> options)
        {
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                if (csal == null)
                    return false;

                string strIdList = GetOption(Options.Ids, options);
                string query = GetOption(Options.Query, options);
                string unref = GetOption(Options.Unref, options);
                string extend = GetOption(Options.Extend, options);

                if (!string.IsNullOrEmpty(strIdList) && !string.IsNullOrEmpty(query)) {
                    Console.WriteLine("Cannot set both Ids and Query");
                    return false;
                }

                List<long> idList = !string.IsNullOrEmpty(strIdList) ? CsAlCore.Common.Utility.String.StringIdListToList(strIdList) : null;
                if (!string.IsNullOrEmpty(query)) {
                    ObjectType objectType = null;
                    List<CsAlCore.Common.IdName> ids = csal.SearchObjs(query, out objectType);
                    foreach (CsAlCore.Common.IdName idName in ids) {
                        if (idList == null)
                            idList = new List<long>();
                        idList.Add(idName.Id);
                    }
                }

                if (string.IsNullOrEmpty(query) && (idList == null || idList.Count == 0)) {
                    Console.WriteLine("No object to restore");
                    return true;
                }

                List<long> deleted = null;
                if (idList != null && idList.Count > 0)
                    deleted = csal.DeleteObjects(idList, new CsAlCore.CsAlCore.DeleteOptions(unref, extend));

                if (deleted == null || deleted.Count == 0) {
                    Console.WriteLine("No object hard deleted");
                } else {
                    Console.Write("Hard deleted objects: ");
                    bool first = true;
                    foreach (long id in deleted) {
                        if (!first)
                            Console.Write(", ");
                        Console.Write(id);
                        first = false;
                    }
                }
            }
            return true;
        } // HardDeleteObjects
        #endregion

        #region Env variables
        /// <summary>
        /// List all env variables
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns></returns>
        static bool EnvVariableList(Dictionary<string, string> options)
        {            
            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                Console.WriteLine("Env variables:");
                foreach (EnvVariable ev in csal.GetEnvVariables()) {
                    if (ev.DataType == EnvVariable.DataTypes.String)
                        Console.WriteLine($"  { ev.Name } = \"{ ev.StringValue }\"");
                    else
                        Console.WriteLine($"  { ev.Name } = { ev.StringValue }");
                }
            }
            return true;
        } // EnvVariableList

        /// <summary>
        /// Get env variable value
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns></returns>
        static bool EnvVariableGet(Dictionary<string, string> options)
        {
            string name = options[Options.EnvVarName.ToString()];


            EnvVariable.Names ev;
            if (!Enum.TryParse(name, out ev)) {
                Console.WriteLine("Invalid env variable name: {0}", name);
                return false;
            }

            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                EnvVariable evo = csal.GetEnvVariable(ev);
                if (evo != null) {
                    Console.WriteLine($"  { evo.Name } = { evo.StringValue }");
                    return true;
                }
            }
            return false;
        } // EnvVariableGet

        /// <summary>
        /// Set env variable value
        /// </summary>
        /// <param name="options">The command options</param>
        /// <returns></returns>
        static bool EnvVariableSet(Dictionary<string, string> options)
        {
            string name = options[Options.EnvVarName.ToString()];
            string value = options[Options.EnvVarValue.ToString()];

            EnvVariable.Names ev;
            if (!Enum.TryParse(name, out ev)) {
                Console.WriteLine("Invalid env variable name: {0}", name);
                return false;
            }

            using (CsAlCore.CsAlCore csal = OpenDatabase(options)) {
                EnvVariable evo = new EnvVariable(ev, value);
                bool res = csal.SetEnvVariable(evo.Name, evo.StringValue);
                if (res)
                    Console.WriteLine($"Env variable '{ ev }' set to '{ value }'");
                return res;
            }
        } // EnvVariableSet
        #endregion
    }
}
