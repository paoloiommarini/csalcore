<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="version"/>
  <xsl:template match="/">
    <html lang="en">
      <head>
        <title>
          <xsl:value-of select="Schema/@Name"/> rev. <xsl:value-of select="Schema/@Revision"/>
        </title>
        <style>
          body {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          font-size: 16px;
          color: #5a5858;
          }

          a {
          color: #5a5858;
          }

          a:hover {
          color: black;
          }

          table {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          font-size: 16px;
          border-collapse: collapse;
          width: 100%;
          }

          td, th {
          border: 1px solid #ddd;
          padding: 8px;
          }

          tr:nth-child(even){background-color: #f2f2f2;}

          tr:hover {
          background-color: #c6c6c6;
          }

          th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #5a5858;
          color: white;
          }
        </style>
      </head>
      <body>
        <div style="background-color: #5a5858; color: white; height: 50px; display: flex; justify-content: center; align-items: center;">
          <h2>
            <xsl:value-of select="Schema/@Name"/> rev. <xsl:value-of select="Schema/@Revision"/>
          </h2>
        </div>
        <xsl:if test="Schema/Description">
          <p><xsl:value-of select="Schema/Description"/></p>        
        </xsl:if>
        <!-- Links -->
        <h3>Object types:</h3>
        <ul>
          <xsl:for-each select="Schema/ObjectTypes/ObjectType">
            <xsl:sort select="@Name"/>
            <li>
              <a>
                <xsl:attribute name="href">#<xsl:value-of select="@Name"/></xsl:attribute>
                <xsl:value-of select="@Name"/>
              </a>
              <xsl:if test="@SystemObject = 'true'">
                (system object)
              </xsl:if>                
            </li>
          </xsl:for-each>
        </ul>

        <!-- Object types -->
        <xsl:for-each select="Schema/ObjectTypes/ObjectType">
          <xsl:sort select="@Name"/>
          <xsl:variable name="ObjectTypeName" select="current()/@Name" />
          <br/>
          <a>
            <xsl:attribute name="id">
              <xsl:value-of select="@Name"/>
            </xsl:attribute>
          </a>
          <h3>
            <xsl:value-of select="@Name"/>
          </h3>
          <div>
            <xsl:if test="@Inherits">
              <p>
                Inherits:
                <a>
                  <xsl:attribute name="href">#<xsl:value-of select="@Inherits"/></xsl:attribute>
                  <xsl:value-of select="@Inherits"/>
                </a>
              </p>
            </xsl:if>
            <xsl:if test="@SystemObject = 'true'">
              <p>
                System Object: True
              </p>
            </xsl:if>
            <xsl:if test="@Abstract = 'true'">
              <p>
                Abstract: True
              </p>
            </xsl:if>            
            <p>
              <xsl:value-of select="Description"/>
            </p>

            <!-- Attributes -->
            <table>
              <tr>
                <th>Attribute</th>
                <th style="width: 30px;">Required</th>
                <th>Description</th>
                <th>Data type</th>
                <th>Inherited from</th>
                <th>Referenced object type</th>
              </tr>
              <xsl:for-each select="Attributes/Attribute">
                <tr>
                  <td>
                    <xsl:value-of select="@Name"/>
                  </td>
                  <td style="text-align: center;">
                    <xsl:if test="@Required = 'true'">
                      True
                    </xsl:if>
                  </td>
                  <td>
                    <xsl:value-of select="Description"/>
                  </td>
                  <td>
                    <xsl:value-of select="@Type"/>
                    <!-- Values -->
                    <xsl:if test="count(Values/Value) > 0">
                      <p style="font-size: 12px;">Values</p>
                      <ul style="font-size: 12px;">
                        <xsl:for-each select="Values/Value">
                          <li>
                            <xsl:value-of select="@Name"/> (<xsl:value-of select="@Value"/>)
                          </li>
                        </xsl:for-each>
                      </ul>
                    </xsl:if>                    
                    <p style="font-size: 12px;">
                      <xsl:if test="@ChildrenReference = 'true'">
                        Children reference: <xsl:value-of select="@ChildrenReference"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test="@Sortable = 'true'">
                        Sortable: <xsl:value-of select="@Sortable"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test="@DefaultValue">
                        Default value: <xsl:value-of select="@DefaultValue"/>
                        <br/>
                      </xsl:if>                      
                      <xsl:if test="@MinValue != 0">
                        Min value: <xsl:value-of select="@MinValue"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test="@MaxValue != 0">
                        Max value: <xsl:value-of select="@MaxValue"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test="@MinLength != 0">
                        Min length: <xsl:value-of select="@MinLength"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test="@MaxLength != 0">
                        Max length: <xsl:value-of select="@MaxLength"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test="@Precision != 0">
                        Precision: <xsl:value-of select="@Precision"/>
                        <br/>
                      </xsl:if>
                      <xsl:if test="@ValidationRegEx">
                        Validation reg exp: <xsl:value-of select="@ValidationRegEx"/>
                        <br/>
                      </xsl:if>

                      <!-- Sub attributes -->
                      <xsl:if test="count(SubAttributes/Attribute) > 0">
                        SubAttributes:
                        <ul style="font-size: 12px;">
                          <xsl:for-each select="SubAttributes/Attribute">
                            <li>
                              <xsl:value-of select="@Name"/> (<xsl:value-of select="@Type"/>)
                            </li>
                          </xsl:for-each>
                        </ul>
                      </xsl:if>
                    </p>
                  </td>
                  <td>
                    <xsl:if test="not(@ObjectTypeName = $ObjectTypeName)">
                      <a>
                        <xsl:attribute name="href">#<xsl:value-of select="@ObjectTypeName"/></xsl:attribute>
                        <xsl:value-of select="@ObjectTypeName"/>
                      </a>
                    </xsl:if>
                  </td>
                  <td>
                    <xsl:if test="@RefObjectType">
                      <a>
                        <xsl:attribute name="href">#<xsl:value-of select="@RefObjectType"/>
                        </xsl:attribute>
                        <xsl:value-of select="@RefObjectType"/>
                      </a>
                    </xsl:if>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          
            <!-- Indexes -->
            <xsl:if test="count(Indexes/Index) > 0">
              <br/>
              <h4>Indexes:</h4>
              <xsl:for-each select="Indexes/Index">
                <xsl:value-of select="@Name"/>
                <xsl:if test="@Unique = 'true'">
                  (unique)
                </xsl:if>
                <table>
                  <tr>
                    <th>Attribute</th>
                    <th>Asc/desc</th>
                  </tr>
                  <xsl:for-each select="Attributes/Attribute">
                    <tr>
                      <td>
                        <xsl:value-of select="@Name"/>
                      </td>
                      <td>
                        <xsl:if test="@IsDescending = 'true'">
                          Desc
                        </xsl:if>
                        <xsl:if test="@IsDescending = 'false'">
                          Asc
                        </xsl:if>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
                <br/>              
              </xsl:for-each>
            </xsl:if>
          </div>
        </xsl:for-each>

        <p style="font-size: 12px;">
          Generated by CsAlManager v.<xsl:value-of select="$version"/>
        </p>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>