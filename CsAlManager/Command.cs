﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCoreManager
{
    class CommandOption
    {
        public enum DataTypes
        {
            None,
            Bool,
            String,
            Integer,
            IdList,
            StringList
        }

        public CommandOption(string name, DataTypes dataType, bool required)
        {
            Name = name;
            DataType = dataType;
            Required = required;
        }

        public CommandOption(CommandOption opt)
        {
            Name = opt.Name;
            DataType = opt.DataType;
            Required = opt.Required;
        }

        public string Name { get; set; }
        public DataTypes DataType { get; set; }
        public bool Required { get; set; }
        public string Help { get; set; }

        public bool CheckValue(string value)
        {
            if (DataType == DataTypes.None)
                return true;

            switch (DataType) {
                case DataTypes.String:
                    return true;
                case DataTypes.Integer:
                    int test = 0;
                    return int.TryParse(value, out test);
            }
            return true;
        } // CheckValue

        public new string ToString()
        {
            if (DataType != DataTypes.None)
                return string.Format("{0} [{1}]{2}", Name, DataType, Required ? "*" : string.Empty);
            return string.Format("{0}{1}", Name, Required ? "*" : string.Empty);
        }
    } // CommandOption

    class Command
    {
        public Command(string name)
        {
            Name = name;
            Options = new List<CommandOption>();
        }

        public string Name { get; set; }
        public List<CommandOption> Options { get; set; }
        public string Help { get; set; }

        public CommandOption GetOption(string name)
        {
            foreach (CommandOption opt in Options) {
                if (string.Compare(opt.Name, name, StringComparison.OrdinalIgnoreCase) == 0)
                    return opt;
            }
            return null;
        } // GetOption

        public List<string> CheckOptions(Dictionary<string, string> options)
        {
            List<string> errors = new List<string>();

            // Missing options:
            List<string> validOptions = new List<string>();
            foreach (CommandOption co in Options) {
                validOptions.Add(co.Name);

                string value = null;
                bool exists = options.TryGetValue(co.Name, out value);
                if (co.Required && !exists) {
                    errors.Add(string.Format("Missing required option: --{0}", co.ToString()));
                } else if (exists && co.DataType != CommandOption.DataTypes.None && string.IsNullOrEmpty(value)) {
                    errors.Add(string.Format("Missing value for option: {0}", co.ToString()));
                } else if (exists && !co.CheckValue(value)) {
                    errors.Add(string.Format("Invalid value for option: --{0}", co.ToString()));
                }
            }

            // Unknown options
            foreach (string option in options.Keys) {
                if (!validOptions.Contains(option)) {
                    errors.Add(string.Format("Invalid option: {0}", option));
                }
            }
            return errors;
        } // CheckOptions
    } // Command
}
