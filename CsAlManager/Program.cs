﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCoreManager
{
    partial class Program
    {
        enum Commands
        {
            DataExport,
            DataImport,
            Help,
            SchemaToClass,
            SchemaExport,
            SchemaRevision,
            SchemaUpgrade,
            SchemaToDoc,
            ObjsSoftDelete,
            ObjsHardDelete,
            ObjsRestore,
            EnvVariableList,
            EnvVariableSet,
            EnvVariableGet,
        }

        enum Options
        {
            DbConn,
            DbType,
            Ids,
            In,
            Out,
            Query,
            ObjectTypes,
            Namespace,
            Xslt,
            Extend,
            Unref,
            EnvVarName,
            EnvVarValue,
            SingleFile,
            Force,
            MaxDepth
        }

        class ProgramArgs
        {
            public ProgramArgs()
            {
                Options = new Dictionary<string, string>();
            }

            public string Command { get; set; }
            public Dictionary<string, string> Options { get; set; }
        } // ProgramArgs

        static void PrintInfo()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0} v.{1}", assembly.GetName().Name, assembly.GetName().Version);            
            Console.ResetColor();
            CsAlCore.CsAlCore.VersionInfo ver = CsAlCore.CsAlCore.Version;
            Console.WriteLine("Using CsAl v.{0} [{1}]", ver.Version, ver.BuildDate.ToShortDateString());
            Console.WriteLine(GetCopyright());
            Console.WriteLine();
        } // PrintInfo

        static string GetCopyright()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            object[] obj = asm.GetCustomAttributes(false);
            foreach (object o in obj) {
                if (o.GetType() == typeof(System.Reflection.AssemblyCopyrightAttribute)) {
                    AssemblyCopyrightAttribute aca = (AssemblyCopyrightAttribute)o;
                    return aca.Copyright;
                }
            }
            return string.Empty;
        }

        static void PrintHelp()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Console.WriteLine("Usage:");
            Console.WriteLine("{0} command [options]", AppDomain.CurrentDomain.FriendlyName);
            Console.WriteLine();
            Console.WriteLine("Options marked with an asterisk are required.");
            Console.WriteLine("Available commands:");
            foreach (Command c in GetCommands()) {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("{0}", c.Name.PadRight(36, ' '));
                Console.ResetColor();

                int pos = Console.CursorLeft;
                int width = Console.WindowWidth - 36;
                foreach (string line in SplitStringInLines(c.Help, width)) {
                    Console.CursorLeft = 36;
                    Console.WriteLine(line);
                }

                foreach (CommandOption opt in c.Options) {
                    int lineIdx = 0;
                    foreach (string line in SplitStringInLines(opt.Help, width)) {
                        if (lineIdx++ == 0)
                            Console.WriteLine("    --{0}{1}", opt.ToString().PadRight(30, ' '), line);
                        else {
                            Console.CursorLeft = 36;
                            Console.WriteLine(line);
                        }
                    }
                }
            }
        } // PrintHelp    

        static int Main(string[] args)
        {
            PrintInfo();

            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            ProgramArgs pArgs = ParseArguments(args);
            if (pArgs == null)
                return -1;

            try {
                if (string.Compare(pArgs.Command, Commands.Help.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    PrintHelp();
                    return 0;
                } else if (string.Compare(pArgs.Command, Commands.DataExport.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!DataExport(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.DataImport.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!DataImport(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.SchemaUpgrade.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!SchemaUpgrade(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.SchemaRevision.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!SchemaRevision(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.SchemaExport.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!SchemaExport(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.SchemaToClass.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!SchemaToClass(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.SchemaToDoc.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!SchemaToDoc(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.ObjsSoftDelete.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!SoftDeleteObjects(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.ObjsRestore.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!RestoreObjects(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.ObjsHardDelete.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!HardDeleteObjects(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.EnvVariableList.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!EnvVariableList(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.EnvVariableSet.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!EnvVariableSet(pArgs.Options))
                        return -1;
                } else if (string.Compare(pArgs.Command, Commands.EnvVariableGet.ToString(), StringComparison.OrdinalIgnoreCase) == 0) {
                    if (!EnvVariableGet(pArgs.Options))
                        return -1;
                }
            } catch (Exception ex) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }
            Console.ResetColor();
            Console.CursorVisible = true;
            return 0;
        } // Main

        private static ProgramArgs ParseArguments(string[] args)
        {
            ProgramArgs res = new ProgramArgs();

            List<Command> cmds = GetCommands();
            Command cmd = null;

            for (int i = 0; i < args.Length; i++) {
                if (i == 0) {
                    // Command
                    res.Command = args[i];
                    foreach (Command c in cmds) {
                        if (string.Compare(c.Name, res.Command, StringComparison.OrdinalIgnoreCase) == 0) {
                            cmd = c;
                            break;
                        }
                    }

                    if (cmd == null) {
                        Console.WriteLine("Unknown command: {0}", res.Command);
                        return null;
                    }
                } else {
                    // Option
                    if (args[i].StartsWith("--"))
                        args[i] = args[i].Remove(0, 2);
                    string optName = args[i].ToLower();
                    CommandOption opt = cmd.GetOption(optName);
                    res.Options[opt != null ? opt.Name : optName] = null;
                    if (opt != null && opt.DataType != CommandOption.DataTypes.None) {
                        if (opt.DataType == CommandOption.DataTypes.Bool)
                            res.Options[opt.Name] = "true";
                        else if (i < args.Length - 1) {
                            // Value
                            res.Options[opt.Name] = args[++i];
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(res.Command)) {
                Console.WriteLine("No command specified");
                Console.WriteLine("Use the command 'help' to display the command list and informations");
                return null;
            }

            // Check command ond options
            List<string> optErrors = cmd.CheckOptions(res.Options);
            if (optErrors.Count > 0) {
                foreach (string err in optErrors) {
                    Console.WriteLine(err);
                }
                return null;
            }
            return res;
        } // ParseArguments

        private static List<Command> GetCommands()
        {
            List<Command> res = new List<Command>();

            Dictionary<string, CommandOption> commonOptions = new Dictionary<string, CommandOption>()
            {
                { Options.DbType.ToString(), new CommandOption(Options.DbType.ToString(), CommandOption.DataTypes.String, true)
                                              {
                                                Help = "The database type. Possible values: MariaDB, MySQL, SQLite, SQLServer, PostgreSQL"
                                              }
                },

                { Options.DbConn.ToString(), new CommandOption(Options.DbConn.ToString(), CommandOption.DataTypes.String, true)
                                              {
                                                Help = "The database connection string"
                                              }
                }
            };

            res.Add(new Command(Commands.Help.ToString())
            {
                Help = "Display this help",
            });

            res.Add(new Command(Commands.SchemaRevision.ToString())
            {
                Help = "Display the schema revision",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                }
            });

            res.Add(new Command(Commands.SchemaExport.ToString())
            {
                Help = "Exports the schema to a file",
                Options = new List<CommandOption>()
                {
                    commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                    new CommandOption(Options.SingleFile.ToString(), CommandOption.DataTypes.Bool, false)
                    {
                        Help = "If set the schema is exported to a single file"
                    },
                    new CommandOption(Options.Out.ToString(), CommandOption.DataTypes.String, true)
                    {
                        Help = "The schema output file path"
                    }
                }
            });

            res.Add(new Command(Commands.SchemaToClass.ToString())
            {
                Help = "Create C# classes from the schema object types",
                Options = new List<CommandOption>()
                {
                  new CommandOption(commonOptions[Options.DbType.ToString()])
                  {
                      Required = false
                  },
                  new CommandOption(commonOptions[Options.DbConn.ToString()])
                  {
                      Required = false
                  },
                  new CommandOption(Options.In.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "The schema file path"
                  },
                  new CommandOption(Options.Out.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The classes output folder"
                  },
                  new CommandOption(Options.ObjectTypes.ToString(), CommandOption.DataTypes.StringList, false)
                  {
                    Help = "Semicolon separated list of object types"
                  },
                  new CommandOption(Options.Namespace.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "Namespace (default: 'SchemaClasses')"
                  },
                }
            });

            res.Add(new Command(Commands.SchemaToDoc.ToString())
            {
                Help = "Save the schema documentation to a HTML file",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.Out.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The documentation output file"
                  },
                }
            });

            res.Add(new Command(Commands.SchemaUpgrade.ToString())
            {
                Help = "Upgrades the database schema",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.In.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The schema file path"
                  },
                  new CommandOption(Options.Force.ToString(), CommandOption.DataTypes.Bool, false)
                  {
                    Help = "Do not ask for confirmation and assume YES as response"
                  }
                }
            });

            res.Add(new Command(Commands.DataImport.ToString())
            {
                Help = "Imports data from xml to the database",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.In.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The data file path"
                  }
                }
            });

            res.Add(new Command(Commands.DataExport.ToString())
            {
                Help = "Exports data to a xml file",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.Out.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The output file path"
                  },
                  new CommandOption(Options.Ids.ToString(), CommandOption.DataTypes.IdList, false)
                  {
                    Help = "Semicolon separated list of Ids of the objects to export"
                  },
                  new CommandOption(Options.Query.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "The query to select the objects to export"
                  },
                  new CommandOption(Options.Xslt.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "The optional XSLT file to be applied to the exported objects"
                  },
                  new CommandOption(Options.MaxDepth.ToString(), CommandOption.DataTypes.Integer, false)
                  {
                    Help = "The optional max depth of the exported objects"
                  }
                }
            });

            res.Add(new Command(Commands.ObjsSoftDelete.ToString())
            {
                Help = "Soft deletes objects",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.Ids.ToString(), CommandOption.DataTypes.IdList, false)
                  {
                    Help = "Semicolon separated list of Ids of the objects to soft delete"
                  },
                  new CommandOption(Options.Query.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "The query to select the objects to soft delete"
                  },
                  new CommandOption(Options.Unref.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "Semicolon separated list of attributes to navigate to unreference objects"
                  },
                  new CommandOption(Options.Extend.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "Semicolon separated list of attributes to navigate to extend objects list"
                  },
                }
            });

            res.Add(new Command(Commands.ObjsRestore.ToString())
            {
                Help = "Restore soft deleted objects",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.Ids.ToString(), CommandOption.DataTypes.IdList, false)
                  {
                    Help = "Semicolon separated list of Ids of the objects to soft delete"
                  },
                  new CommandOption(Options.Query.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "The query to select the objects to soft delete"
                  },
                  new CommandOption(Options.Extend.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "Semicolon separated list of attributes to navigate to extend objects list"
                  },
                }
            });

            res.Add(new Command(Commands.ObjsHardDelete.ToString())
            {
                Help = "Hard deletes objects",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.Ids.ToString(), CommandOption.DataTypes.IdList, false)
                  {
                    Help = "Semicolon separated list of Ids of the objects to hard delete"
                  },
                  new CommandOption(Options.Query.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "The query to select the objects to hard delete"
                  },
                  new CommandOption(Options.Unref.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "Semicolon separated list of attributes to navigate to unreference objects"
                  },
                  new CommandOption(Options.Extend.ToString(), CommandOption.DataTypes.String, false)
                  {
                    Help = "Semicolon separated list of attributes to navigate to extend objects list"
                  },
                }
            });

            res.Add(new Command(Commands.EnvVariableList.ToString())
            {
                Help = "List all defined env variables",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                }
            });

            res.Add(new Command(Commands.EnvVariableGet.ToString())
            {
                Help = "Get a env variable's value",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.EnvVarName.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The name of the env variable to set"
                  },
                }
            });

            res.Add(new Command(Commands.EnvVariableSet.ToString())
            {
                Help = "Set env variable's value",
                Options = new List<CommandOption>()
                {
                  commonOptions[Options.DbType.ToString()], commonOptions[Options.DbConn.ToString()],
                  new CommandOption(Options.EnvVarName.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The name of the env variable to set"
                  },
                  new CommandOption(Options.EnvVarValue.ToString(), CommandOption.DataTypes.String, true)
                  {
                    Help = "The new value of the env variable"
                  },
                }
            });
            return res.OrderBy(o => o.Name).ToList();
        }

        static List<string> SplitStringInLines(string str, int width)
        {
            List<char> wordSeparators = new List<char>() { ' ', '.', ':', ',', ';', '!', '?' };
            List<string> lines = new List<string>();
            // Split in lines if needed:
            if (str.Length > width || str.Contains("\n")) {
                StringBuilder line = new StringBuilder();
                StringBuilder word = new StringBuilder();
                for (int i = 0; i < str.Length; i++) {
                    char c = str[i];
                    if (c == 10) {
                        if (word.Length > 0) {
                            line.Append(word.ToString());
                            word.Clear();
                        }
                        lines.Add(line.ToString());
                        line.Clear();
                        line.Append(word.ToString());
                    } else if (c == 13) {
                        // Ignore \r
                    } else if (wordSeparators.Contains(c)) {
                        if (line.Length + word.Length + 1 < width) {
                            line.Append(word.ToString());
                            line.Append(c);
                        } else {
                            lines.Add(line.ToString());
                            line.Clear();
                            line.Append(word.ToString());
                            if (line.Length > 0 || c != ' ')
                                line.Append(c);
                        }
                        word.Clear();
                    } else {
                        word.Append(c);
                        if (line.Length + word.Length + 1 >= width) {
                            lines.Add(line.ToString());
                            line.Clear();
                        }
                    }
                }

                if (word.Length > 0) {
                    line.Append(word.ToString());
                    word.Clear();
                }
                if (line.Length > 0)
                    lines.Add(line.ToString());
            } else
                lines.Add(str);

            return lines;
        } // SplitStringInLines
    }
}
