﻿using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCore.Query
{
    class LexerErrorListener : IAntlrErrorListener<int>
    {
        public void SyntaxError(IRecognizer recognizer, int offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            throw new Common.Exceptions.QuerySyntaxError(string.Format("Error at line {0} pos {1}: {2}", line, charPositionInLine, msg));
        }
    } // LexerErrorListener

    class ParserErrorListener : IAntlrErrorListener<IToken>
    {
        public void SyntaxError(IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            throw new Common.Exceptions.QuerySyntaxError(string.Format("Error at line {0} pos {1}: {2}", line, charPositionInLine, msg));
        }
    } // ParserErrorListener
}
