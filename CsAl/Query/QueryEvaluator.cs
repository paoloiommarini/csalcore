﻿using Antlr4.Runtime;
using CsAlCore.Common.Schema;
using CsAlQueryParser;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCore.Query
{
    /// <summary>
    /// Defines a query parameter
    /// </summary>
    public class QueryParameter
    {
        /// <summary>
        /// Creates a new <see cref="QueryParameter"/>
        /// </summary>
        /// <param name="name">Tha parameter name</param>
        /// <param name="value">Tha parameter value</param>
        public QueryParameter(string name, string value)
        {
            Name = name;
            Value = value;
        } // QueryParameter

        /// <summary>
        /// Creates a new <see cref="QueryParameter"/>
        /// </summary>
        /// <param name="name">Tha parameter name</param>
        /// <param name="value">Tha parameter value</param>
        public QueryParameter(string name, int value)
        {
            Name = name;
            Value = value;
        } // QueryParameter

        /// <summary>
        /// Creates a new <see cref="QueryParameter"/>
        /// </summary>
        /// <param name="name">Tha parameter name</param>
        /// <param name="value">Tha parameter value</param>
        public QueryParameter(string name, double value)
        {
            Name = name;
            Value = value;
        } // QueryParameter

        /// <summary>
        /// Creates a new <see cref="QueryParameter"/>
        /// </summary>
        /// <param name="name">Tha parameter name</param>
        /// <param name="value">Tha parameter value</param>
        public QueryParameter(string name, bool value)
        {
            Name = name;
            Value = value;
        } // QueryParameter

        /// <summary>
        /// Creates a new <see cref="QueryParameter"/>
        /// </summary>
        /// <param name="name">Tha parameter name</param>
        /// <param name="value">Tha parameter value</param>
        public QueryParameter(string name, DateTime value)
        {
            Name = name;
            Value = value;
        } // QueryParameter


        /// <summary>
        /// Creates a new <see cref="QueryParameter"/>
        /// </summary>
        /// <param name="name">Tha parameter name</param>
        /// <param name="value">Tha parameter value</param>
        public QueryParameter(string name, object value)
        {
            Name = name;
            Value = value;
        } // QueryParameter

        /// <summary>
        /// The parameter name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The parameter value
        /// </summary>
        public object Value { get; private set; }
    } // QueryParameter

    class QueryEvaluator
    {
        Schema m_Schema = null;
        DB.DbConnectionBase m_DbConn = null;

        public QueryEvaluator(Schema schema, DB.DbConnectionBase dbConn)
        {
            m_Schema = schema;
            m_DbConn = dbConn;
        }

        public EvalResult GetSql(Schema schema, DB.DbConnectionBase dbConn,
          List<Common.IdName> objectTypeIds,
          string query,
          List<QueryParameter> pars,
          CsAlCore.DeletedObjectsStrategy deleted)
        {
            // Check parameters:
            if (pars != null) {
                HashSet<string> names = new HashSet<string>();
                foreach (QueryParameter qp in pars) {
                    if (string.IsNullOrEmpty(qp.Name))
                        throw new Common.Exceptions.UnnamedParameterError();
                    if (names.Contains(qp.Name))
                        throw new Common.Exceptions.DuplicatedParameterNameError(qp.Name);
                    names.Add(qp.Name);
                }
            }

            var eval = new EvalVisitor(schema, dbConn);
            eval.ObjectTypeIds = objectTypeIds;
            eval.Pars = pars;
            eval.DeletedObjects = deleted;

            var input = new AntlrInputStream(query);
            var lexer = new CsAlQueryExprLexer(input);
            lexer.RemoveErrorListeners();
            lexer.AddErrorListener(new LexerErrorListener());
            var tokens = new CommonTokenStream(lexer);
            var parser = new CsAlQueryExprParser(tokens);
            parser.RemoveErrorListeners();
            parser.AddErrorListener(new ParserErrorListener());
            var tree = parser.eval();

            eval.InitAndVisit(tree);

            return eval.Result();
        }
    } // QueryEvaluator
}
