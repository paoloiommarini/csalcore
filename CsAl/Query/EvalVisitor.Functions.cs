﻿using Antlr4.Runtime.Misc;
using CsAlCore.Common.Schema;
using CsAlQueryParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsAlCore.Query
{
    partial class EvalVisitor
    {
        #region Mathematical functions
        public override EvalStep VisitRound([NotNull] CsAlQueryExprParser.RoundContext context)
        {
            var target = Visit(context.expr(0));
            if (!target.IsNumeric)
                throw new Common.Exceptions.ExpectingNumericValueError(context.GetText());

            var dec = Visit(context.expr(1));
            if (!dec.IsNumeric)
                throw new Common.Exceptions.ExpectingNumericValueError(context.GetText());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddRound(sb, target.Text, dec.Text);

            string stepText = sb.ToString();
            return new EvalStep(stepText, EvalStep.StepType.Constant, EvalStep.DataTypes.Real);
        } // VisitRound

        public override EvalStep VisitSqrt([NotNull] CsAlQueryExprParser.SqrtContext context)
        {
            var expr = Visit(context.expr());
            if (!expr.IsNumeric)
                throw new Common.Exceptions.ExpectingNumericValueError(context.GetText());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddSqrt(sb, expr.Text);

            string stepText = sb.ToString();
            return new EvalStep(stepText, EvalStep.StepType.Constant, EvalStep.DataTypes.Real);
        } // VisitSqrt

        public override EvalStep VisitSin([NotNull] CsAlQueryExprParser.SinContext context)
        {
            var expr = Visit(context.expr());
            if (!expr.IsNumeric)
                throw new Common.Exceptions.ExpectingNumericValueError(context.GetText());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddSin(sb, expr.Text);

            string stepText = sb.ToString();
            return new EvalStep(stepText, EvalStep.StepType.Constant, EvalStep.DataTypes.Real);
        } // VisitSin

        public override EvalStep VisitCos([NotNull] CsAlQueryExprParser.CosContext context)
        {
            var expr = Visit(context.expr());
            if (!expr.IsNumeric)
                throw new Common.Exceptions.ExpectingNumericValueError(context.GetText());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddCos(sb, expr.Text);

            string stepText = sb.ToString();
            return new EvalStep(stepText, EvalStep.StepType.Constant, EvalStep.DataTypes.Real);
        } // VisitCos

        public override EvalStep VisitTan([NotNull] CsAlQueryExprParser.TanContext context)
        {
            var expr = Visit(context.expr());
            if (!expr.IsNumeric)
                throw new Common.Exceptions.ExpectingNumericValueError(context.GetText());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddTan(sb, expr.Text);

            string stepText = sb.ToString();
            return new EvalStep(stepText, EvalStep.StepType.Constant, EvalStep.DataTypes.Real);
        } // VisitTan

        public override EvalStep VisitPower([NotNull] CsAlQueryExprParser.PowerContext context)
        {
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));

            // Check data types:
            if (left.DataType != right.DataType) {
                if (!left.IsNumeric || !right.IsNumeric)
                    throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), right.DataType.ToString());
            }

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddPower(sb, left.Text, right.Text);

            string stepText = sb.ToString();
            return new EvalStep(stepText, EvalStep.StepType.Constant, EvalStep.DataTypes.Real);
        } // VisitPower
        #endregion

        public override EvalStep VisitExists([NotNull] CsAlQueryExprParser.ExistsContext context)
        {
            string stepText = GetExistsCondition(context.GetText());
            if (WriteWhere(context))
                m_WhereBuilder.Append(stepText);
            return new EvalStep(stepText, EvalStep.StepType.None, EvalStep.DataTypes.Boolean);
        } // VisitExists

        private string GetExistsCondition(string path)
        {
            string res = string.Empty;
            path = CompletePath(path);
            PathInfo pInfo = m_Schema.ResolvePath(path);
            if (pInfo != null) {
                ObjectAttribute at = pInfo.AttributeTypes.LastOrDefault();
                // Exists condition is valid only against object
                if (pInfo.Result != PathInfo.ResolveResult.Object)
                    throw new Common.Exceptions.ExistConditionOnNonObjectError(path);
                if (at.Type == ObjectAttribute.Types.MultiReference) {
                    if (at.ChildrenReference) {
                        // Children
                        ObjectType mrefOt = m_Schema.GetObjectType(at.RefObjectType);
                        res = string.Format("exists(select 1 from {0} where a_{1} = {2}.a_{3})",
                                            mrefOt.TableName,
                                            Schema.ParentObjElementName.ToLower(),
                                            GetTable(at.ObjectTypeName).Alias, Schema.ObjectIdName);
                    } else {
                        // Multireference
                        res = string.Format("exists(select 1 from {0} where a_objid = {1}.a_{2})",
                                            m_DbConn.GetMultiReferenceTableName(at),
                                            GetTable(at.ObjectTypeName).Alias, Schema.ObjectIdName);
                    }
                    AddUsedTableAlias(GetTable(at.ObjectTypeName).Alias);
                } else if (at.Type == ObjectAttribute.Types.Reference) {
                    // Reference
                    res = string.Format("{0}.{1} != 0", GetTable(at.ObjectTypeName).Alias, at.ColumnName);
                    AddUsedTableAlias(GetTable(at.ObjectTypeName).Alias);
                } else if (at.Type == ObjectAttribute.Types.BinaryData) {
                    res = string.Format("{0}.{1} != 0", GetTable(at.ObjectTypeName).Alias, at.ColumnName);
                    AddUsedTableAlias(GetTable(at.ObjectTypeName).Alias);
                } else {
                    throw new Common.Exceptions.ExistConditionOnNonObjectError(at.Name);
                }
            } else
                throw new Common.Exceptions.UnknownAttributeType(path);

            return res;
        } // GetExistsCondition

        public override EvalStep VisitStringFunctions([NotNull] CsAlQueryExprParser.StringFunctionsContext context)
        {
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));

            if (left.DataType != EvalStep.DataTypes.String || right.DataType != EvalStep.DataTypes.String)
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), right.DataType.ToString());

            if (left.Type == EvalStep.StepType.Constant) {
                left.Text = GetLiteralValue(left.Text);
            }
            if (right.Type == EvalStep.StepType.Constant) {
                right.Text = GetLiteralValue(right.Text);
            }

            StringBuilder sb = new StringBuilder();

            if (string.Compare(context.op.Text, "contains", StringComparison.OrdinalIgnoreCase) == 0)
                m_DbConn.AddContains(sb, left.Text, right.Text);
            else if (string.Compare(context.op.Text, "startswith", StringComparison.OrdinalIgnoreCase) == 0)
                m_DbConn.AddStartsWith(sb, left.Text, right.Text);
            else if (string.Compare(context.op.Text, "endswith", StringComparison.OrdinalIgnoreCase) == 0)
                m_DbConn.AddEndsWith(sb, left.Text, right.Text);

            string stepText = sb.ToString();
            m_WhereBuilder.Append(stepText);
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.Boolean };
        } // VisitStringFunctions

        public override EvalStep VisitSubstring([NotNull] CsAlQueryExprParser.SubstringContext context)
        {
            var target = Visit(context.expr(0));
            var start = Visit(context.expr(1));
            var end = Visit(context.expr(2));

            if (target.DataType != EvalStep.DataTypes.String)
                throw new Common.Exceptions.WrongArgumentDataTypeError(target.DataType.ToString(), EvalStep.DataTypes.String.ToString(), context.GetText());
            if (start.DataType != EvalStep.DataTypes.Integer)
                throw new Common.Exceptions.WrongArgumentDataTypeError(start.DataType.ToString(), EvalStep.DataTypes.Integer.ToString(), context.GetText());
            if (end.DataType != EvalStep.DataTypes.Integer)
                throw new Common.Exceptions.WrongArgumentDataTypeError(end.DataType.ToString(), EvalStep.DataTypes.Integer.ToString(), context.GetText());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddSubString(sb, target.Text, start.Text, end.Text);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.String };
        } // VisitSubstring

        public override EvalStep VisitLength([NotNull] CsAlQueryExprParser.LengthContext context)
        {
            var target = Visit(context.expr());

            if (target.DataType != EvalStep.DataTypes.String)
                throw new Common.Exceptions.WrongArgumentDataTypeError(target.DataType.ToString(), EvalStep.DataTypes.String.ToString(), context.GetText());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddLength(sb, target.Text);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.String };
        } // VisitLength

        public override EvalStep VisitCount([NotNull] CsAlQueryExprParser.CountContext context)
        {
            string stepText = GetCountCondition(context.expr().GetText());
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitCount

        private string GetCountCondition(string path)
        {
            string res = string.Empty;
            path = CompletePath(path);
            PathInfo pInfo = m_Schema.ResolvePath(path);
            if (pInfo != null) {
                ObjectAttribute at = pInfo.AttributeTypes.LastOrDefault();
                // Count is valid only against object
                if (pInfo.Result != PathInfo.ResolveResult.Object)
                    throw new Common.Exceptions.CountOnNonObjectError(path);
                if (at.Type == ObjectAttribute.Types.MultiReference) {
                    if (at.ChildrenReference) {
                        // Children
                        ObjectType mrefOt = m_Schema.GetObjectType(at.RefObjectType);
                        res = string.Format("(select count(1) from {0} where a_{1} = {2}.a_{3})",
                                            mrefOt.TableName,
                                            Schema.ParentObjElementName.ToLower(),
                                            GetTable(at.ObjectTypeName).Alias, Schema.ObjectIdName);
                    } else {
                        // Multireference
                        res = string.Format("(select count(1) from {0} where a_objid = {1}.a_{2})",
                                            m_DbConn.GetMultiReferenceTableName(at),
                                            GetTable(at.ObjectTypeName).Alias, Schema.ObjectIdName);
                    }
                    AddUsedTableAlias(GetTable(at.ObjectTypeName).Alias);
                } else {
                    throw new Common.Exceptions.CountOnNonObjectError(at.Name);
                }
            } else
                throw new Common.Exceptions.UnknownAttributeType(path);

            return res;
        } // GetCountCondition

        #region date time functions
        private string GetDateFromExpr(string expression, CsAlQueryExprParser.ExprContext expr)
        {
            StringBuilder sb = new StringBuilder();
            var date = Visit(expr);
            string strDate = date.Text;
            if (date.DataType != EvalStep.DataTypes.DateTime && date.DataType != EvalStep.DataTypes.Date)
                throw new Common.Exceptions.WrongArgumentDataTypeError(date.DataType.ToString(), EvalStep.DataTypes.DateTime.ToString(), expression);
            return strDate;
        } // GetDateFromExpr

        public override EvalStep VisitDateAdd([NotNull] CsAlQueryExprParser.DateAddContext context)
        {
            StringBuilder sb = new StringBuilder();
            var number = Visit(context.expr(0));
            if (number.DataType != EvalStep.DataTypes.Integer)
                throw new Common.Exceptions.WrongArgumentDataTypeError(number.DataType.ToString(), EvalStep.DataTypes.Integer.ToString(), context.GetText());

            string strDate = GetDateFromExpr(context.GetText(), context.expr(1));

            int iNumber = 0;
            iNumber = int.Parse(number.Text);

            sb.Clear();
            switch (context.op.Type) {
                case CsAlQueryExprParser.DAY:
                    m_DbConn.AddDateAdd(sb, DB.DbConnectionBase.DateParts.Day, strDate, iNumber);
                    break;
                case CsAlQueryExprParser.MONTH:
                    m_DbConn.AddDateAdd(sb, DB.DbConnectionBase.DateParts.Month, strDate, iNumber);
                    break;
                case CsAlQueryExprParser.YEAR:
                    m_DbConn.AddDateAdd(sb, DB.DbConnectionBase.DateParts.Month, strDate, iNumber);
                    break;
                case CsAlQueryExprParser.HOUR:
                    m_DbConn.AddDateAdd(sb, DB.DbConnectionBase.DateParts.Hour, strDate, iNumber);
                    break;
                case CsAlQueryExprParser.MINUTE:
                    m_DbConn.AddDateAdd(sb, DB.DbConnectionBase.DateParts.Minute, strDate, iNumber);
                    break;
                case CsAlQueryExprParser.SECOND:
                    m_DbConn.AddDateAdd(sb, DB.DbConnectionBase.DateParts.Second, strDate, iNumber);
                    break;
            }

            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.DateTime };
        } // VisitDateAdd

        public override EvalStep VisitDateDiff([NotNull] CsAlQueryExprParser.DateDiffContext context)
        {
            string strDate1 = GetDateFromExpr(context.GetText(), context.expr(0));
            string strDate2 = GetDateFromExpr(context.GetText(), context.expr(1));

            StringBuilder sb = new StringBuilder();
            switch (context.op.Type) {
                case CsAlQueryExprParser.DAY:
                    m_DbConn.AddDateDiff(sb, DB.DbConnectionBase.DateParts.Day, strDate1, strDate2);
                    break;
                case CsAlQueryExprParser.MONTH:
                    m_DbConn.AddDateDiff(sb, DB.DbConnectionBase.DateParts.Month, strDate1, strDate2);
                    break;
                case CsAlQueryExprParser.YEAR:
                    m_DbConn.AddDateDiff(sb, DB.DbConnectionBase.DateParts.Month, strDate1, strDate2);
                    break;
                case CsAlQueryExprParser.HOUR:
                    m_DbConn.AddDateDiff(sb, DB.DbConnectionBase.DateParts.Hour, strDate1, strDate2);
                    break;
                case CsAlQueryExprParser.MINUTE:
                    m_DbConn.AddDateDiff(sb, DB.DbConnectionBase.DateParts.Minute, strDate1, strDate2);
                    break;
                case CsAlQueryExprParser.SECOND:
                    m_DbConn.AddDateDiff(sb, DB.DbConnectionBase.DateParts.Second, strDate1, strDate2);
                    break;
                case CsAlQueryExprParser.MILLISECOND:
                    m_DbConn.AddDateDiff(sb, DB.DbConnectionBase.DateParts.MilliSecond, strDate1, strDate2);
                    break;
            }

            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitDateDiff

        public override EvalStep VisitDateYear([NotNull] CsAlQueryExprParser.DateYearContext context)
        {
            string strDate = GetDateFromExpr(context.GetText(), context.expr());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddDateYear(sb, strDate);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitDateYear

        public override EvalStep VisitDateMonth([NotNull] CsAlQueryExprParser.DateMonthContext context)
        {
            string strDate = GetDateFromExpr(context.GetText(), context.expr());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddDateMonth(sb, strDate);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitDateMonth

        public override EvalStep VisitDateDay([NotNull] CsAlQueryExprParser.DateDayContext context)
        {
            string strDate = GetDateFromExpr(context.GetText(), context.expr());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddDateDay(sb, strDate);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitDateDay

        public override EvalStep VisitDateHour([NotNull] CsAlQueryExprParser.DateHourContext context)
        {
            string strDate = GetDateFromExpr(context.GetText(), context.expr());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddDateHour(sb, strDate);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitDateHour

        public override EvalStep VisitDateMinute([NotNull] CsAlQueryExprParser.DateMinuteContext context)
        {
            string strDate = GetDateFromExpr(context.GetText(), context.expr());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddDateMinute(sb, strDate);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitDateMinute

        public override EvalStep VisitDateSecond([NotNull] CsAlQueryExprParser.DateSecondContext context)
        {
            string strDate = GetDateFromExpr(context.GetText(), context.expr());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddDateSecond(sb, strDate);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Integer };
        } // VisitDateSecond
        #endregion

        public override EvalStep VisitIsNull([NotNull] CsAlQueryExprParser.IsNullContext context)
        {
            var target = Visit(context.expr());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddIsNull(sb, target.Text);
            string stepText = sb.ToString();
            if (WriteWhere(context))
                m_WhereBuilder.Append(stepText);
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = EvalStep.DataTypes.Boolean };
        } // VisitIsNull

        public override EvalStep VisitNullValue([NotNull] CsAlQueryExprParser.NullValueContext context)
        {
            var target = Visit(context.expr(0));
            var value = Visit(context.expr(1));

            if (target.DataType != value.DataType) {
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), target.DataType.ToString(), value.DataType.ToString());
            }

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddNullValue(sb, target.Text, value.Text);
            string stepText = sb.ToString();
            return new EvalStep() { Text = stepText, Type = EvalStep.StepType.Constant, DataType = value.DataType };

        } // VisitNullValue
    }
}
