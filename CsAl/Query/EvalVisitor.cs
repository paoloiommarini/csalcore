﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr4.Runtime.Misc;
using CsAlQueryParser;
using CsAlCore.DB;
using Antlr4.Runtime.Tree;
using CsAlCore.Common.Schema;
using System.Globalization;

namespace CsAlCore.Query
{
    class EvalResult
    {
        public EvalResult()
        {
            Pars = new List<SqlParameterWithValue>();
        } // EvalResult

        public ObjectType ObjectType { get; set; }
        public string Sql { get; set; }
        public List<SqlParameterWithValue> Pars { get; set; }

        public SqlParameterWithValue GetPar(string name)
        {
            if (Pars != null) {
                foreach (SqlParameterWithValue p in Pars) {
                    if (string.Compare(p.Name, name, StringComparison.OrdinalIgnoreCase) == 0)
                        return p;
                }
            }
            return null;
        }
    } // EvalResult

    class EvalStep
    {
        public enum DataTypes
        {
            None,
            Boolean,
            String,
            Integer,
            Real,
            Date,
            DateTime,
            TimeSpan,
        }

        public enum StepType
        {
            None,
            ObjectName,
            AttributeName,
            Constant,
            Parameter,
            Function
        }

        public EvalStep()
        {

        }

        public EvalStep(string text, StepType stepType, DataTypes dataType)
        {
            Text = text;
            Type = stepType;
            DataType = dataType;
        }

        public string Text { get; set; }
        public StepType Type { get; set; }
        public DataTypes DataType { get; set; }
        public bool IsNumeric
        {
            get {
                return DataType == DataTypes.Integer || DataType == DataTypes.Real;
            }
        }

        public bool IsDateTime
        {
            get {
                return DataType == DataTypes.DateTime || DataType == DataTypes.Date;
            }
        }

        public ObjectAttribute Attribute { get; set; }
        public ObjectAttribute.Types AttributeType
        {
            get {
                if (Attribute != null)
                    return Attribute.Type;
                return ObjectAttribute.Types.None;
            }
        }

        public static DataTypes GetParameterDataType(object parameterValue)
        {
            if (parameterValue is bool)
                return EvalStep.DataTypes.Boolean;
            if (parameterValue is Int64 || parameterValue is Int32)
                return EvalStep.DataTypes.Integer;
            if (parameterValue is float || parameterValue is double)
                return DataTypes.Real;
            if (parameterValue is DateTime)
                return DataTypes.DateTime;
            if (parameterValue is TimeSpan)
                return DataTypes.TimeSpan;
            if (parameterValue is string)
                return EvalStep.DataTypes.String;
            return EvalStep.DataTypes.None;
        } // GetParameterDataType

        public static DataTypes GetAttributeDataType(ObjectAttribute at)
        {
            switch (at.Type) {
                case ObjectAttribute.Types.Boolean:
                    return DataTypes.Boolean;
                case ObjectAttribute.Types.CreatedDateTime:
                case ObjectAttribute.Types.LastModifiedDateTime:
                case ObjectAttribute.Types.DateTime:
                    return DataTypes.DateTime;
                case ObjectAttribute.Types.Date:
                    return DataTypes.Date;
                case ObjectAttribute.Types.TimeSpan:
                    return DataTypes.TimeSpan;
                case ObjectAttribute.Types.Enum:
                case ObjectAttribute.Types.Flags:
                    return DataTypes.Integer;
                case ObjectAttribute.Types.Identity:
                case ObjectAttribute.Types.Integer:
                    return DataTypes.Integer;
                case ObjectAttribute.Types.Real:
                    return DataTypes.Real;
                case ObjectAttribute.Types.Password:
                case ObjectAttribute.Types.String:
                    return DataTypes.String;
                case ObjectAttribute.Types.MultiReference:
                case ObjectAttribute.Types.Reference:
                case ObjectAttribute.Types.BinaryData:
                    return DataTypes.Integer;
            }
            return DataTypes.None;
        }
    } // EvalStep

    partial class EvalVisitor : CsAlQueryExprBaseVisitor<EvalStep>
    {
        class DbJoin
        {
            public enum JoinType
            {
                Reference,
                Children,
                MultiReference,
            }

            public DbTable Table { get; set; }
            public ObjectAttribute JoinAttribute { get; set; }
            public JoinType Type { get; set; }
        } // DbJoin

        class DbTable
        {
            public DbTable()
            {

            }

            public string Alias { get; set; }
            public ObjectType ObjectType { get; set; }
            public DbJoin JoinInfo { get; set; }
            public int Depth { get; set; }

            public new string ToString()
            {
                if (ObjectType != null)
                    return string.Format("{0} {1}", ObjectType.TableName, Alias);
                return string.Empty;
            }
        } // DbTable

        int m_Depth = 0;
        Schema m_Schema = null;
        DB.DbConnectionBase m_DbConn = null;
        List<DbTable> m_DbTables = null;
        HashSet<string> m_UsedDbTablesAlias = null;
        string m_ObjectTypeWhere = string.Empty;
        StringBuilder m_WhereBuilder = null;
        StringBuilder m_OrderByBuilder = null;
        int m_Limit = 0;
        int m_Offset = 0;
        EvalResult m_Result = null;

        #region constructor
        /// <summary>
        /// Create a new <see cref="EvalVisitor"/>
        /// </summary>
        /// <param name="schema">The database <see cref="Schema.Schema"/></param>
        /// <param name="dbConn">The database <see cref="DbConnectionBase"/> </param>
        public EvalVisitor(Schema schema, DB.DbConnectionBase dbConn)
        {
            if (schema == null)
                throw new ArgumentNullException("schema");
            if (dbConn == null)
                throw new ArgumentNullException("dbConn");

            m_Schema = schema;
            m_DbConn = dbConn;
        }
        #endregion

        #region public members
        public List<QueryParameter> Pars { get; set; }

        public CsAlCore.DeletedObjectsStrategy DeletedObjects { get; set; }

        /// <summary>
        /// The object type ids. Set this before calling Eval() to avoid reading ids from the
        /// object type table.
        /// </summary>
        public List<Common.IdName> ObjectTypeIds { get; set; }
        #endregion

        #region overrides
        public EvalStep InitAndVisit(IParseTree tree)
        {
            m_Result = new EvalResult();
            m_UsedDbTablesAlias = new HashSet<string>();
            m_DbTables = new List<DbTable>();
            m_ObjectTypeWhere = string.Empty;
            m_WhereBuilder = new StringBuilder();
            m_OrderByBuilder = new StringBuilder();

            return base.Visit(tree);
        } // InitAndVisit

        private long GetObjectTypeId(string name)
        {
            if (ObjectTypeIds != null) {
                foreach (Common.IdName idName in ObjectTypeIds) {
                    if (string.Compare(idName.Name, name, StringComparison.OrdinalIgnoreCase) == 0)
                        return idName.Id;
                }
            }
            return 0;
        } // GetObjetTypeId

        /// <summary>
        /// Check data types compatibility for operators
        /// </summary>
        /// <returns></returns>
        private bool CheckDataTypes(EvalStep left, EvalStep right)
        {
            if (left.IsNumeric && right.IsNumeric)
                return true;

            if (left.IsDateTime && right.IsDateTime)
                return true;

            if (left.DataType == right.DataType)
                return true;
            return false;
        } // CheckDataTypes

        public override EvalStep VisitErrorNode(IErrorNode node)
        {
            string text = string.Format("Syntax error at '{0}'", node.GetText());
            throw new Common.Exceptions.QuerySyntaxError(text);
        }

        public override EvalStep VisitExpr([NotNull] CsAlQueryExprParser.ExprContext context)
        {
            return base.VisitExpr(context);
        }

        public override EvalStep VisitSelect([NotNull] CsAlQueryExprParser.SelectContext context)
        {
            return base.VisitSelect(context);
        } // VisitSelect

        public override EvalStep VisitObjectName([NotNull] CsAlQueryExprParser.ObjectNameContext context)
        {
            string otName = context.GetText();
            ObjectType ot = m_Schema.GetObjectType(otName);
            if (ot == null)
                throw new Common.Exceptions.UnknownObjectType(otName);

            if (ot.SystemObject)
                throw new Common.Exceptions.QueryOnSystemObjectError(ot.Name);

            AddObjectTypeTables(ot.Name, null);
            // Object table is always used
            AddUsedTableAlias(GetTableAlias(m_DbTables.First().ObjectType));

            bool inheritsObject = string.Compare(m_DbTables.First().ObjectType.Name, Schema.ObjectObjectType, StringComparison.OrdinalIgnoreCase) == 0;
            if (inheritsObject) {
                // Add condition on objecTypeRef (including descendants)
                StringBuilder otwBuilder = new StringBuilder();
                string objectTable = GetTableAlias(m_DbTables.First().ObjectType);
                if (ObjectTypeIds == null) {
                    // No ids: use "in (select ...)"
                    otwBuilder.AppendFormat("{0}.a_{1} in (select id from sys_objecttype where name in(", objectTable, Schema.ObjectTypeRefName.ToLower());
                    m_DbConn.AddStringValue(otwBuilder, ot.Name);

                    foreach (ObjectType objectType in m_Schema.GetObjectTypeDescendants(ot.Name)) {
                        otwBuilder.Append(", ");
                        m_DbConn.AddStringValue(otwBuilder, objectType.Name);
                    }
                    otwBuilder.Append("))");
                } else {
                    // Ids: use "in (..., ...)"
                    otwBuilder.AppendFormat("{0}.a_{1} in (", objectTable, Schema.ObjectTypeRefName.ToLower());
                    long otId = GetObjectTypeId(ot.Name);
                    m_DbConn.AddIntegerValue(otwBuilder, otId);
                    foreach (ObjectType objectType in m_Schema.GetObjectTypeDescendants(ot.Name)) {
                        otId = GetObjectTypeId(objectType.Name);
                        otwBuilder.Append(", ");
                        m_DbConn.AddIntegerValue(otwBuilder, otId);
                    }
                    otwBuilder.Append(")");
                }
                m_ObjectTypeWhere = otwBuilder.ToString();
            }

            m_Result.ObjectType = ot;
            return new EvalStep() { Type = EvalStep.StepType.ObjectName };
        } // VisitObjectName

        public override EvalStep VisitPath([NotNull] CsAlQueryExprParser.PathContext context)
        {
            string stepText = string.Empty;
            string path = context.GetText();
            path = CompletePath(path);
            PathInfo pInfo = m_Schema.ResolvePath(path);
            if (pInfo != null) {
                int idx = 0;
                int oldDepth = m_Depth;
                ObjectAttribute refAt = null;
                for (int i = 0; i < pInfo.AttributeTypes.Count; i++) {
                    ObjectAttribute iAt = pInfo.AttributeTypes[i];
                    ObjectType ot = pInfo.ObjectTypes[idx];

                    if (iAt.Type == ObjectAttribute.Types.Reference) {
                        if (i != pInfo.AttributeTypes.Count - 1 || pInfo.Result == PathInfo.ResolveResult.Object) {
                            refAt = iAt;
                            m_Depth++;
                            idx++;
                            // Add data tables for referenced object type
                            if (GetTable(iAt.RefObjectType) == null)
                                AddObjectTypeTables(iAt.RefObjectType, refAt);
                            AddUsedTableAlias(GetTable(refAt.ObjectTypeName, m_Depth - 1).Alias);
                        }
                    } else if (iAt.Type == ObjectAttribute.Types.MultiReference) {
                        refAt = iAt;
                        m_Depth++;
                        idx++;

                        if (GetTable(iAt.RefObjectType) == null)
                            AddObjectTypeTables(iAt.RefObjectType, refAt);

                        if (iAt.ChildrenReference)
                            AddUsedTableAlias(GetTable(refAt.ObjectTypeName, m_Depth - 1).Alias);
                    } else if (iAt.Type == ObjectAttribute.Types.BinaryData) {
                        // Add join with binary data table
                        idx++;
                        if (GetTable("binaryData") == null) {
                            string alias = $"{ GetTableAlias($"bd_{ iAt.Name }", m_Depth) }r";
                            DbTable joinTable = GetTable(iAt.ObjectTypeName, m_Depth);
                            DbTable bdTable = new DbTable()
                            {
                                Alias = alias,
                                Depth = m_Depth,
                                ObjectType = new ObjectType()
                                {
                                    Name = "binaryData",
                                    SystemObject = true,
                                },
                                JoinInfo = new DbJoin()
                                {
                                    Type = DbJoin.JoinType.Reference,
                                    Table = joinTable,
                                    JoinAttribute = iAt
                                }
                            };
                            m_DbTables.Add(bdTable);
                            if (pInfo.ObjectTypes.Count > idx && pInfo.ObjectTypes[idx].SystemObject && pInfo.Result == PathInfo.ResolveResult.Attribute)
                                AddUsedTableAlias(alias);
                        }                        
                        AddUsedTableAlias(GetTable(iAt.ObjectTypeName, m_Depth).Alias);
                    }
                }

                ObjectAttribute at = pInfo.AttributeTypes.LastOrDefault();
                if (m_DbTables.Count > 0) {
                    int aIdx = pInfo.AttributeTypes.IndexOf(at);
                    if (at.Type == ObjectAttribute.Types.MultiReference) {
                        string dbt = string.Format("{0}r", GetTableAlias(at.RefObjectType, m_Depth));
                        AddUsedTableAlias(dbt);
                        stepText = string.Format("{0}.{1}", dbt, pInfo.AttributeTypes.Last().ColumnName);
                    } else {
                        int depth = aIdx > m_Depth ? m_Depth : aIdx;
                        string dbt = at.IsInherited ? GetTable(at.InheritedFrom).Alias : GetTable(pInfo.ObjectTypes[aIdx].Name, depth)?.Alias;
                        if (at.ObjectTypeName == null) {
                            if (at.AttributeType is Common.Schema.AttributeTypes.ChildOfAttrType) {
                                // Nothing to do for children
                            } else if (string.Compare(at.Name, Schema.ChildProgressiveAttributeName, StringComparison.OrdinalIgnoreCase) == 0) { 

                            } else {
                                // MultiReference sub attribute
                                ObjectAttribute mrefAt = pInfo.AttributeTypes[pInfo.AttributeTypes.IndexOf(at) - 1];
                                dbt = string.Format("{0}r", GetTableAlias(mrefAt.RefObjectType, m_Depth));
                            }
                        }
                        AddUsedTableAlias(dbt);
                        stepText = string.Format("{0}.{1}", dbt, pInfo.AttributeTypes.Last().ColumnName);
                    }
                }
                m_Depth = oldDepth;

                return new EvalStep(stepText, EvalStep.StepType.AttributeName, EvalStep.GetAttributeDataType(at))
                {
                    Attribute = at
                };
            } else
                throw new Common.Exceptions.UnknownAttributeType(path);
        } // VisitPath

        public override EvalStep VisitMathOperators([NotNull] CsAlQueryExprParser.MathOperatorsContext context)
        {
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));
            EvalStep.DataTypes resType = EvalStep.DataTypes.Integer;
            ObjectAttribute resAttribute = null;

            // Flags:
            ObjectAttribute at = null;
            EvalStep value = null;
            if (left.AttributeType == ObjectAttribute.Types.Flags && right.AttributeType != ObjectAttribute.Types.Flags) {
                at = left.Attribute;
                value = right;
            } else if (right.AttributeType == ObjectAttribute.Types.Flags && left.AttributeType != ObjectAttribute.Types.Flags) {
                at = right.Attribute;
                value = left;
            }
            if (at != null) {
                if (value.DataType != EvalStep.DataTypes.String)
                    throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), "Flags", value.DataType.ToString());

                value.Text = GetLiteralValue(value.Text);
                ObjectAttribute.EnumValue ev = at.GetValue(value.Text);
                if (ev == null)
                    throw new Common.Exceptions.InvalidEnumValueError(value.Text, Common.Utility.String.EnumFlagsValuesToString(at.Values));

                // Operations on a flags attribute return the flags attribute
                resAttribute = at;
                value.DataType = EvalStep.DataTypes.Integer;
                value.Text = ev.Value.ToString(CultureInfo.InvariantCulture);
            }

            // Check data types:
            if (!left.IsNumeric || !right.IsNumeric)
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), right.DataType.ToString());

            string op = context.op.Text;
            string stepText = string.Empty;
            if (context.op.Type == CsAlQueryExprParser.BITAND) {
                StringBuilder sb = new StringBuilder();
                m_DbConn.AddBitAnd(sb, left.Text, right.Text);
                stepText = sb.ToString();
            } else if (context.op.Type == CsAlQueryExprParser.BITOR) {
                StringBuilder sb = new StringBuilder();
                m_DbConn.AddBitOr(sb, left.Text, right.Text);
                stepText = sb.ToString();
            } else {
                stepText = string.Format("{0} {1} {2}", left.Text, context.op.Text, right.Text);
            }
            return new EvalStep(stepText, EvalStep.StepType.Function, resType) { Attribute = resAttribute };
        } // VisitMathOperators

        #endregion

        public override EvalStep VisitCompare([NotNull] CsAlQueryExprParser.CompareContext context)
        {
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));
            string op = context.op.Text;
            if (op == "==")
                op = "=";
            else if (op == "!=")
                op = "<>";

            // Enums:
            ObjectAttribute at = null;
            EvalStep value = null;
            if (left.AttributeType == ObjectAttribute.Types.Enum && right.AttributeType != ObjectAttribute.Types.Enum) {
                at = left.Attribute;
                value = right;
            } else if (right.AttributeType == ObjectAttribute.Types.Enum && left.AttributeType != ObjectAttribute.Types.Enum) {
                at = right.Attribute;
                value = left;
            }
            if (at != null) {
                if (value.DataType != EvalStep.DataTypes.String)
                    throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), "Enum", value.DataType.ToString());

                value.Text = GetLiteralValue(value.Text);
                ObjectAttribute.EnumValue ev = at.GetValue(value.Text);
                if (ev == null)
                    throw new Common.Exceptions.InvalidEnumValueError(value.Text, Common.Utility.String.EnumFlagsValuesToString(at.Values));

                value.DataType = EvalStep.DataTypes.Integer;
                value.Text = ev.Value.ToString(CultureInfo.InvariantCulture);
            }

            // Flags
            at = null;
            value = null;
            if (left.AttributeType == ObjectAttribute.Types.Flags && right.AttributeType != ObjectAttribute.Types.Flags) {
                at = left.Attribute;
                value = right;
            } else if (right.AttributeType == ObjectAttribute.Types.Flags && left.AttributeType != ObjectAttribute.Types.Flags) {
                at = right.Attribute;
                value = left;
            }
            if (at != null) {
                if (value.DataType != EvalStep.DataTypes.String)
                    throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), "Flags", value.DataType.ToString());

                value.Text = GetLiteralValue(value.Text);
                ObjectAttribute.EnumValue ev = at.GetValue(value.Text);
                if (ev == null)
                    throw new Common.Exceptions.InvalidEnumValueError(value.Text, Common.Utility.String.EnumFlagsValuesToString(at.Values));

                value.DataType = EvalStep.DataTypes.Integer;
                value.Text = ev.Value.ToString(CultureInfo.InvariantCulture);
            }

            // Check data types:
            if (!CheckDataTypes(left, right))
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), right.DataType.ToString());

            // Check NullValue:
            // TODO try to convert a = null in isnull(a) and a != null in not(isnull(a))

            string stepText = string.Empty;
            stepText = string.Format("{0} {1} {2}", left.Text, op, right.Text);

            if (WriteWhere(context))
                m_WhereBuilder.Append(stepText);
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.Boolean };
        } // VisitCompare

        public override EvalStep VisitConcat([NotNull] CsAlQueryExprParser.ConcatContext context)
        {
            var left = Visit(context.expr(0));
            var right = Visit(context.expr(1));

            // Check data types:
            if (left.DataType != EvalStep.DataTypes.String || right.DataType != EvalStep.DataTypes.String)
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), right.DataType.ToString());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddStringConcat(sb, left.Text, right.Text);
            return new EvalStep(sb.ToString(), EvalStep.StepType.Function, EvalStep.DataTypes.String);
        } // VisitConcat

        public override EvalStep VisitIn([NotNull] CsAlQueryExprParser.InContext context)
        {
            var left = Visit(context.expr(0));

            StringBuilder sb = new StringBuilder();
            for (int i = 1; i < context.expr().Length; i++) {
                EvalStep value = Visit(context.expr(i));
                if (!CheckDataTypes(left, value))
                    throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), value.DataType.ToString());
                if (i > 1)
                    sb.Append(", ");
                sb.Append(value.Text);
            }

            string stepText = string.Format("{0} in ({1})", left.Text, sb.ToString());
            m_WhereBuilder.Append(stepText);
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.Boolean };
        } // VisitIn

        public override EvalStep VisitAnd([NotNull] CsAlQueryExprParser.AndContext context)
        {
            var left = Visit(context.whereExpr(0));
            m_WhereBuilder.AppendLine();
            m_WhereBuilder.Append(" and ");
            var right = Visit(context.whereExpr(1));

            if (left.DataType != EvalStep.DataTypes.Boolean || right.DataType != EvalStep.DataTypes.Boolean)
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), right.DataType.ToString());

            string stepText = string.Format("{0} and {1}", left.Text, right.Text);
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.Boolean };
        } // VisitAnd

        public override EvalStep VisitOr([NotNull] CsAlQueryExprParser.OrContext context)
        {
            var left = Visit(context.whereExpr(0));
            m_WhereBuilder.AppendLine();
            m_WhereBuilder.Append("  or ");
            var right = Visit(context.whereExpr(1));

            if (left.DataType != EvalStep.DataTypes.Boolean || right.DataType != EvalStep.DataTypes.Boolean)
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), left.DataType.ToString(), right.DataType.ToString());

            string stepText = string.Format("{0} or {1}", left.Text, right.Text);
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.Boolean };
        } // VisitOr

        public override EvalStep VisitNot([NotNull] CsAlQueryExprParser.NotContext context)
        {
            var expr = Visit(context.whereExpr());
            if (expr.DataType != EvalStep.DataTypes.Boolean)
                throw new Common.Exceptions.QueryDataTypesMismatch(context.GetText(), expr.DataType.ToString(), EvalStep.DataTypes.Boolean.ToString());

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddNot(sb, expr.Text);

            string stepText = sb.ToString();
            m_WhereBuilder.Append(stepText);
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.Boolean };
        } // VisitNot

        public override EvalStep VisitLiteral([NotNull] CsAlQueryExprParser.LiteralContext context)
        {
            string value = context.GetText();
            value = GetLiteralValue(value);

            StringBuilder sb = new StringBuilder();
            m_DbConn.AddStringValue(sb, value);

            return new EvalStep(sb.ToString(), EvalStep.StepType.Constant, EvalStep.DataTypes.String);
        } // VisitLiteral

        public override EvalStep VisitInteger([NotNull] CsAlQueryExprParser.IntegerContext context)
        {
            return new EvalStep(context.GetText(), EvalStep.StepType.Constant, EvalStep.DataTypes.Integer);
        } // VisitInteger

        public override EvalStep VisitDecimal([NotNull] CsAlQueryExprParser.DecimalContext context)
        {
            return new EvalStep(context.GetText(), EvalStep.StepType.Constant, EvalStep.DataTypes.Real);
        } // VisitDecimal

        public override EvalStep VisitBoolean([NotNull] CsAlQueryExprParser.BooleanContext context)
        {
            StringBuilder sb = new StringBuilder();
            m_DbConn.AddBooleanValue(sb, string.Compare(context.GetText(), "true", StringComparison.OrdinalIgnoreCase) == 0);

            return new EvalStep(sb.ToString(), EvalStep.StepType.Constant, EvalStep.DataTypes.Boolean);
        } // VisitBoolean

        public override EvalStep VisitDateTime([NotNull] CsAlQueryExprParser.DateTimeContext context)
        {
            StringBuilder sb = new StringBuilder();

            string strDate = GetLiteralValue(context.GetText());
            DateTime? dt = Common.Utility.Serialization.DateTimeFromXml(strDate);
            m_DbConn.AddDateTimeValue(sb, dt.Value);
            if (context.DATETIME() != null)
                return new EvalStep(sb.ToString(), EvalStep.StepType.Constant, EvalStep.DataTypes.DateTime);
            return new EvalStep(sb.ToString(), EvalStep.StepType.Constant, EvalStep.DataTypes.Date);
        } // VisitDateTime

        public override EvalStep VisitTimeSpan([NotNull] CsAlQueryExprParser.TimeSpanContext context)
        {
            StringBuilder sb = new StringBuilder();

            string strDate = GetLiteralValue(context.GetText());
            TimeSpan? ts = Common.Utility.Serialization.TimeSpanFromXml(strDate);
            m_DbConn.AddTimeSpanValue(sb, ts.Value);

            return new EvalStep(sb.ToString(), EvalStep.StepType.Constant, EvalStep.DataTypes.TimeSpan);
        } // VisitTimeSpan

        public override EvalStep VisitParameter([NotNull] CsAlQueryExprParser.ParameterContext context)
        {
            QueryParameter par = null;
            string pName = context.GetText();
            pName = pName.Remove(0, 1);
            if (Pars != null) {
                foreach (QueryParameter p in Pars) {
                    if (string.Compare(p.Name, pName, StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(p.Name, $"@{ pName }", StringComparison.OrdinalIgnoreCase) == 0) {
                        par = p;
                        break;
                    }
                }
            }
            if (par == null)
                throw new Common.Exceptions.UnknownQueryParameter(pName);

            if (m_Result.GetPar(pName) == null)
                m_Result.Pars.Add(new SqlParameterWithValue(pName, par.Value));

            return new EvalStep(m_DbConn.GetParameterName(pName), EvalStep.StepType.Parameter, EvalStep.GetParameterDataType(par.Value));
        } // VisitParameter

        public override EvalStep VisitOrder([NotNull] CsAlQueryExprParser.OrderContext context)
        {
            VisitChildren(context);
            return new EvalStep();
        } // VisitOrder

        public override EvalStep VisitOrderField([NotNull] CsAlQueryExprParser.OrderFieldContext context)
        {
            var op = context.op;
            var path = context.field.Text;
            path = CompletePath(path);
            PathInfo pInfo = m_Schema.ResolvePath(path);
            string ob = string.Empty;
            if (pInfo != null) {
                // Order by is valid only against attributes
                if (pInfo.Result != PathInfo.ResolveResult.Attribute)
                    throw new Common.Exceptions.ExpectingAttributeInPathError(path);

                int idx = 0;
                int oldDepth = m_Depth;
                ObjectAttribute refAt = null;
                for (int i = 0; i < pInfo.AttributeTypes.Count; i++) {
                    ObjectAttribute iAt = pInfo.AttributeTypes[i];
                    ObjectType ot = pInfo.ObjectTypes[idx];

                    if (iAt.Type == ObjectAttribute.Types.Reference) {
                        if (i != pInfo.AttributeTypes.Count - 1 || pInfo.Result == PathInfo.ResolveResult.Object) {
                            refAt = iAt;
                            m_Depth++;
                            idx++;
                            // Add data tables for referenced object type
                            if (GetTable(iAt.RefObjectType) == null)
                                AddObjectTypeTables(iAt.RefObjectType, refAt);
                        }
                    }
                }

                ob = string.Format("{0}.{1}", GetTable(pInfo).Alias, pInfo.AttributeTypes.Last().ColumnName);
                AddUsedTableAlias(GetTable(pInfo).Alias);
                if (op != null && string.Compare(op.Text, "desc", StringComparison.OrdinalIgnoreCase) == 0)
                    ob = string.Format("{0} desc", ob);

                m_Depth = oldDepth;

                if (m_OrderByBuilder.Length > 0)
                    m_OrderByBuilder.Append(", ");
                m_OrderByBuilder.Append(ob);
            }
            return new EvalStep() { Text = ob };
        } // VisitOrderField

        public override EvalStep VisitParens([NotNull] CsAlQueryExprParser.ParensContext context)
        {
            m_WhereBuilder.Append("(");
            var expr = Visit(context.whereExpr());
            m_WhereBuilder.Append(")");

            string stepText = string.Format("({0})", expr.Text);
            return new EvalStep() { Text = stepText, DataType = EvalStep.DataTypes.Boolean };
        } // VisitParens

        public override EvalStep VisitLimit([NotNull] CsAlQueryExprParser.LimitContext context)
        {
            int limit = 0;
            if (int.TryParse(context.children[0].GetText(), out limit)) {
                if (limit < 0)
                    throw new Common.Exceptions.InvalidLimitValueError(context.children[0].GetText());
                m_Limit = limit;
            }
            return new EvalStep();
        } // VisitLimit

        public override EvalStep VisitOffset([NotNull] CsAlQueryExprParser.OffsetContext context)
        {
            int offset = 0;
            if (int.TryParse(context.children[0].GetText(), out offset)) {
                if (offset < 0)
                    throw new Common.Exceptions.InvalidOffsetValueError(context.children[0].GetText());
                m_Offset = offset;
            }
            return new EvalStep();
        } // VisitOffset

        #region public operations
        public EvalResult Result()
        {
            StringBuilder sb = new StringBuilder();

            bool needsDistinct = false;
            foreach (DbTable dbt in m_DbTables) {
                if (dbt.JoinInfo != null && dbt.JoinInfo.JoinAttribute.Type == ObjectAttribute.Types.MultiReference) {
                    needsDistinct = true;
                    break;
                }
            }

            bool inheritsObject = string.Compare(m_DbTables.First().ObjectType.Name, Schema.ObjectObjectType, StringComparison.OrdinalIgnoreCase) == 0;
            string mainTable = m_DbTables[0].Alias;
            string identityField = m_DbTables[0].ObjectType.GetIdentity(true).ColumnName;
            if (needsDistinct)
                sb.AppendLine(string.Format("select distinct {0}.{1}", mainTable, identityField));
            else
                sb.AppendLine(string.Format("select {0}.{1}", mainTable, identityField));
            sb.Append("from ");

            // Remove unused tables to avoid useless joins
            for (int i = 0; i < m_DbTables.Count; i++) {
                DbTable dbt = m_DbTables[i];
                if (!m_UsedDbTablesAlias.Contains(dbt.Alias))
                    m_DbTables.RemoveAt(i--);
            }

            // Joins
            int idx = 0;
            foreach (DbTable dbt in m_DbTables) {
                if (idx == 0) {
                    sb.AppendLine(string.Format("{0} {1} ", dbt.ObjectType.TableName, dbt.Alias));
                } else {
                    // Join
                    if (dbt.JoinInfo.JoinAttribute.Type == ObjectAttribute.Types.MultiReference) {
                        if (dbt.JoinInfo.JoinAttribute.ChildrenReference) {
                            sb.AppendLine(string.Format("join {0} {1}", dbt.ObjectType.TableName, dbt.Alias));
                            sb.AppendLine(string.Format("  on {0}.a_{1} = {2}.a_{3}", dbt.Alias, Schema.ParentObjElementName.ToLower(),
                                                                                      dbt.JoinInfo.Table.Alias, Schema.ObjectIdName));
                        } else {
                            if (dbt.JoinInfo.Type == DbJoin.JoinType.MultiReference) {
                                string mrefTable = m_DbConn.GetMultiReferenceTableName(dbt.JoinInfo.JoinAttribute);
                                sb.AppendLine(string.Format("join {0} {1}", mrefTable, dbt.Alias));
                                sb.AppendLine(string.Format("  on {0}.a_objid = {1}.a_{2}", dbt.Alias,
                                                                                            dbt.JoinInfo.Table.Alias, Schema.ObjectIdName));
                            } else {
                                sb.AppendLine(string.Format("join {0} {1}", dbt.ObjectType.TableName, dbt.Alias));
                                sb.AppendLine(string.Format("  on {0}.a_{1} = {2}r.a_refid", dbt.Alias, Schema.ObjectIdName,
                                                                                            dbt.Alias));
                            }
                        }
                    } else {
                        sb.AppendLine(string.Format("join {0} {1}", dbt.ObjectType.TableName, dbt.Alias));
                        sb.AppendLine(string.Format("  on {0}.a_{1} = {2}.{3}", dbt.Alias, Schema.ObjectIdName,
                                                                                dbt.JoinInfo.Table.Alias, dbt.JoinInfo.JoinAttribute.ColumnName));
                    }
                }
                idx++;
            }

            // Where
            bool first = true;
            StringBuilder whereSb = new StringBuilder();

            if (!string.IsNullOrEmpty(m_ObjectTypeWhere)) {
                whereSb.Append(m_ObjectTypeWhere);
                first = false;
            }

            if (inheritsObject) {
                switch (DeletedObjects) {
                    case CsAlCore.DeletedObjectsStrategy.OnlyLive:
                        if (!first) {
                            whereSb.AppendLine();
                            whereSb.Append("and ");
                        }
                        first = false;
                        whereSb.Append(string.Format("{0}.a_{1} = ", GetTableAlias("object", 0), Schema.ObjectIsDeletedAttributeName.ToLower()));
                        m_DbConn.AddBooleanValue(whereSb, false);
                        break;
                    case CsAlCore.DeletedObjectsStrategy.OnlyDeleted:
                        if (!first) {
                            whereSb.AppendLine();
                            whereSb.Append("and ");
                        }
                        first = false;
                        whereSb.Append(string.Format("{0}.a_{1} = ", GetTableAlias("object", 0), Schema.ObjectIsDeletedAttributeName.ToLower()));
                        m_DbConn.AddBooleanValue(whereSb, true);
                        break;
                }
            }

            if (m_WhereBuilder.Length > 0) {
                if (!first) {
                    whereSb.AppendLine();
                    whereSb.Append("and ");
                }
                first = false;
                whereSb.Append("(");
                whereSb.Append(m_WhereBuilder.ToString());
                whereSb.AppendLine(") ");
            }

            if (whereSb.Length > 0) {
                sb.Append("where ");
                sb.Append(whereSb.ToString());
            }

            // Order by
            if (m_OrderByBuilder.Length > 0) {
                sb.AppendLine();
                sb.Append("order by ");
                sb.Append(m_OrderByBuilder.ToString());
            }

            // Offset and limit
            if (m_Offset > 0 || m_Limit > 0) {
                if (m_OrderByBuilder.Length == 0)
                    throw new Common.Exceptions.OffsetLimitWithoutOrderByError();
                m_DbConn.AddOffsetLimit(sb, m_Offset, m_Limit);
            }

            m_WhereBuilder.Clear();
            m_OrderByBuilder.Clear();
            m_Result.Sql = sb.ToString();
            return m_Result;
        } // Result
        #endregion

        #region private operations
        private string GetTableAlias(ObjectType objectType)
        {
            return GetTableAlias(objectType.Name, m_Depth);
        } // GetTableAlias

        private string GetTableAlias(string objectTypeName, int depth)
        {
            return string.Format("{0}_{1}", objectTypeName, depth);
        } // GetTableAlias

        private DbTable GetTable(string objectTypeName)
        {
            return GetTable(objectTypeName, m_Depth);
        } // GetTable

        private DbTable GetTable(string objectTypeName, int depth)
        {
            if (string.IsNullOrEmpty(objectTypeName))
                return GetMainTable();
            foreach (DbTable dt in m_DbTables) {
                if (dt.Depth == depth && dt.ObjectType != null && string.Compare(dt.ObjectType.Name, objectTypeName, StringComparison.OrdinalIgnoreCase) == 0)
                    return dt;
            }
            return null;
        } // GetTable

        private DbTable GetTable(PathInfo pInfo)
        {
            ObjectAttribute at = pInfo.AttributeTypes.LastOrDefault();
            if (at != null) {
                if (at.IsInherited) {
                    return GetTable(at.InheritedFrom);
                } else {
                    return GetTable(pInfo.ObjectTypes.Last().Name);
                }
            }
            return null;
        } // GetTable

        private DbTable GetMainTable()
        {
            if (m_DbTables.Count > 0)
                return m_DbTables.Where(dbt => dbt.Depth == 0).LastOrDefault();
            return null;
        } // GetMainTable

        private string CompletePath(string path)
        {
            if (m_DbTables.Count > 0) {
                if (!path.Contains("."))
                    path = string.Format("{0}.{1}", GetMainTable().ObjectType.Name, path);
                else {
                    string[] parts = path.Split('.');
                    if (string.Compare(parts[0], GetMainTable().ObjectType.Name, StringComparison.OrdinalIgnoreCase) != 0)
                        path = string.Format("{0}.{1}", GetMainTable().ObjectType.Name, path);
                }
            }
            return path;
        } // CompletePath

        private void AddObjectTypeTables(string objectTypeName, ObjectAttribute refAttribute)
        {
            List<ObjectType> oTypes = m_Schema.GetObjectTypeInheritance(objectTypeName);
            if (oTypes[0].Name != Schema.ObjectObjectType) {
                if (oTypes[0].GetIdentity(true) == null)
                    throw new Common.Exceptions.CannotQueryObjectWithoutIdentityError(objectTypeName);
            }

            // Add tables for all the object types involved
            DbTable first = null;
            foreach (ObjectType objectType in oTypes) {
                if (first == null) {
                    if (refAttribute == null)
                        first = new DbTable() { Alias = GetTableAlias(objectType), ObjectType = objectType, Depth = m_Depth };
                    else {
                        if (refAttribute.Type == ObjectAttribute.Types.MultiReference && !refAttribute.ChildrenReference) {
                            // Join with multi reference table
                            // Join with object table if possible
                            string alias = string.Format("{0}r", GetTableAlias(refAttribute.RefObjectType, m_Depth));
                            DbTable joinTable = GetTable(Schema.ObjectObjectType, m_Depth - 1);
                            if (joinTable == null)
                                joinTable = GetTable(refAttribute.ObjectTypeName, m_Depth - 1);
                            else
                                AddUsedTableAlias(joinTable.Alias);
                            first = new DbTable()
                            {
                                Alias = alias,
                                ObjectType = objectType,
                                Depth = m_Depth,
                                JoinInfo = new DbJoin()
                                {
                                    Type = DbJoin.JoinType.MultiReference,
                                    Table = joinTable,
                                    JoinAttribute = refAttribute
                                }
                            };
                            AddUsedTableAlias(alias);
                        } else if (refAttribute.Type == ObjectAttribute.Types.Reference || refAttribute.Type == ObjectAttribute.Types.MultiReference && refAttribute.ChildrenReference) {
                            first = new DbTable()
                            {
                                Alias = GetTableAlias(objectType),
                                ObjectType = objectType,
                                Depth = m_Depth,
                                JoinInfo = new DbJoin()
                                {
                                    Type = DbJoin.JoinType.Reference,
                                    Table = GetTable(refAttribute.ObjectTypeName, m_Depth - 1),
                                    JoinAttribute = refAttribute
                                }
                            };
                            AddUsedTableAlias(GetTableAlias(refAttribute.IsInherited ? refAttribute.InheritedFrom : refAttribute.ObjectTypeName, m_Depth - 1));
                        }
                    }
                    if (first != null)
                        m_DbTables.Add(first);
                } else {
                    if (refAttribute == null) {
                        m_DbTables.Add(new DbTable()
                        {
                            Alias = GetTableAlias(objectType),
                            ObjectType = objectType,
                            Depth = m_Depth,
                            JoinInfo = new DbJoin()
                            {
                                Table = first,
                                JoinAttribute = objectType.GetIdentity(true)
                            }
                        });
                    } else {
                        // Always join with the reference attribute if present, so we can later
                        // remove any non used tables
                        m_DbTables.Add(new DbTable()
                        {
                            Alias = GetTableAlias(objectType),
                            ObjectType = objectType,
                            Depth = m_Depth,
                            JoinInfo = new DbJoin()
                            {
                                Table = GetTable(refAttribute.ObjectTypeName, m_Depth - 1),
                                JoinAttribute = refAttribute
                            }
                        });
                    }

                }
            }
        } // AddObjectTypeTables

        private void AddUsedTableAlias(string alias)
        {
            if (m_UsedDbTablesAlias != null && !m_UsedDbTablesAlias.Contains(alias))
                m_UsedDbTablesAlias.Add(alias);
        } // AddUsedTableAlias

        /// <summary>
        /// Get the default value for the given data type
        /// </summary>
        /// <returns></returns>
        private object GetDataTypesDefaultValue(EvalStep.DataTypes type)
        {
            switch (type) {
                case EvalStep.DataTypes.Integer:
                    return 0;
                case EvalStep.DataTypes.Real:
                    return 0.0;
                case EvalStep.DataTypes.String:
                    return string.Empty;
            }
            return null;
        } // GetDataTypesDefaultValue

        private string GetLiteralValue(string value)
        {
            if (value.StartsWith("'") && value.EndsWith("'")) {
                value = value.Remove(0, 1);
                value = value.Remove(value.Length - 1, 1);
                // Unescape:
                value = value.Replace("\\'", "'");
            }
            return value;
        } // GetLiteralValue

        /// <summary>
        /// Returns true if the given context should write directly to the where condition
        /// </summary>
        /// <param name="context">The context</param>
        /// <returns></returns>
        private bool WriteWhere(Antlr4.Runtime.ParserRuleContext context)
        {
            if (context.parent is CsAlQueryExprParser.NotContext)
                return false;
            return true;
        } // WriteWhere
        #endregion
    } // EvalVisitor
}
