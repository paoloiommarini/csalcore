/* https://github.com/antlr/antlr4/tree/master/doc */
grammar CsAlQueryExpr;
 
eval: command;
 
command:   K_SELECT objectsExpr (K_WHERE whereExpr)? (K_ORDER K_BY orderExpr)? (K_OFFSET offsetExpr)? (K_LIMIT limitExpr)? ';'?   # Select
       ;

objectsExpr: NAME               # ObjectName
           | NAME (',' NAME)*   # ObjectNames
           ;

/* Conditions, returning a boolean value */
whereExpr:   whereExpr K_AND whereExpr                                                                    # And
         |   whereExpr K_OR whereExpr                                                                     # Or
         |   expr op=( DIFFERENT | EQUAL | GREATER | GREATEROREQUAL | LESS | LESSOREQUAL ) expr           # Compare
		 |   op=(K_CONTAINS | K_STARTSWITH | K_ENDSWITH) '(' expr ',' expr ')'							  # StringFunctions		 
         |   expr K_IN '(' expr ( ',' expr )* ')'                                                         # In
         |   K_ISNULL '(' expr ')'																		  # IsNull
         |   '(' whereExpr ')'                                                                            # Parens
         |   K_NOT '(' whereExpr ')'                                                                      # Not
         |   '!' '(' whereExpr ')'                                                                        # Not
         |   (PATH | NAME )                                                                               # Exists
         ;


/* Order expression */
orderExpr: orderItem (',' orderItem)*     # order
         ;

orderItem: field=(PATH | NAME) op=(K_DESC | K_ASC)?       # orderField
         ;

offsetExpr: INT                           # offset
          ;

limitExpr: INT                            # limit
         ;

/* Expressions, returning a value */
expr:   expr op=(MUL | DIV | ADD | SUB | BITAND | BITOR | MOD) expr     # MathOperators
	|   K_ROUND '(' expr ',' expr ')'								    # Round
    |   K_SQRT '(' expr ')'												# Sqrt
    |   K_SIN '(' expr ')'												# Sin
    |   K_COS '(' expr ')'												# Cos
    |   K_TAN '(' expr ')'												# Tan
    |   K_POWER '(' expr ',' expr ')'									# Power
	|   K_COUNT '(' expr ')'											# Count
	|   K_DATEADD '(' op=(YEAR | MONTH | DAY | HOUR | MINUTE | SECOND | MILLISECOND) ',' expr ',' expr ')'			# DateAdd
	|   K_DATEDIFF '(' op=(YEAR | MONTH | DAY | HOUR | MINUTE | SECOND | MILLISECOND) ',' expr ',' expr ')'			# DateDiff
	|   K_YEAR '(' expr ')'																							# DateYear
	|   K_MONTH '(' expr ')'																						# DateMonth
	|   K_DAY '(' expr ')'																							# DateDay
	|   K_HOUR '(' expr ')'																							# DateHour
	|   K_MINUTE '(' expr ')'																						# DateMinute
	|   K_SECOND '(' expr ')'																						# DateSecond    	
	|   K_NULLVALUE '(' expr ',' expr ')'						  # NullValue
	|	K_SUBSTRING '(' expr ',' expr ',' expr ')'				  # Substring
	|   K_LENGTH '(' expr ')'									  # Length
    |   expr CONCAT expr                                          # Concat
    |   (PATH | NAME)                                             # Path
    |   (K_TRUE | K_FALSE)                                        # Boolean
	|   (DATETIME | DATE)                                         # DateTime
	|   TIMESPAN												  # TimeSpan
    |   LITERAL                                                   # Literal
    |   INT                                                       # Integer
    |   DECIMAL                                                   # Decimal
    |   PARAM                                                     # Parameter
    ;

/** Keywords */  
K_SELECT : S E L E C T;
K_WHERE  : W H E R E;
K_ORDER  : O R D E R;
K_BY     : B Y;
K_DESC   : D E S C;
K_ASC    : A S C;
K_TRUE   : T R U E;
K_FALSE  : F A L S E;
K_IN     : I N;
K_CONTAINS: C O N T A I N S;
K_STARTSWITH: S T A R T S W I T H;
K_ENDSWITH: E N D S W I T H;
K_SUBSTRING: S U B S T R;
K_LENGTH: L E N G T H;
K_COUNT: C O U N T;
K_DATEADD: D A T E A D D;
K_DATEDIFF: D A T E D I F F;
K_YEAR: Y E A R;
K_MONTH: M O N T H;
K_DAY: D A Y;
K_HOUR: H O U R;
K_MINUTE: M I N U T E;
K_SECOND: S E C O N D;
K_ROUND: R O U N D;
K_SQRT: S Q R T;
K_SIN: S I N;
K_COS: C O S;
K_TAN: T A N;
K_POWER: P O W E R;
K_OFFSET: O F F S E T;
K_LIMIT: L I M I T;
K_ISNULL: I S N U L L;
K_NULLVALUE: N U L L V A L U E;

/** Date parts */
YEAR:			'y'|'years';
MONTH:			'm'|'months';
DAY:			'd'|'days';
HOUR:			'h'|'hours';
MINUTE:			'n'|'minutes';
SECOND:			's'|'seconds';
MILLISECOND:	'ms'|'milliseconds';

/** Operators */     
DIFFERENT:		'!='|'<>';
EQUAL:			'=='|'=';
GREATER:		'>';
GREATEROREQUAL: '>=';
LESS:			'<';
LESSOREQUAL:	'<=';
BITAND:			'&';
BITOR:			'|';

/** Math operators */     
MUL:    '*';
DIV:    '/';
ADD:    '+';
SUB:    '-';
MOD:	'%';

/** String operators */     
CONCAT: '||';

/** Logical operators */
K_AND:    A N D;
K_OR:     O R;
K_NOT:    N O T;


INT:          '-'? [0-9]+;
DECIMAL:      '-'? INT ( '.' (INT)? )?;
/* yyyy-MM-ddTHH:mm:ss.fffZ */
DATETIME:     '\'' [0-9][0-9][0-9][0-9] '-' [0-9][0-9] '-' [0-9][0-9] 'T' [0-9][0-9] ':' [0-9][0-9] ':' [0-9][0-9] ('.' [0-9][0-9]?[0-9]?)? 'Z\'';
/* yyyy-MM-dd */
DATE:         '\'' [0-9][0-9][0-9][0-9] '-' [0-9][0-9] '-' [0-9][0-9] '\'';
/* d.hh:mm:ss.fff */
TIMESPAN:	  '\'' (([0-9])+ '.')? ([0-9][0-9] ':')? [0-9][0-9] ':' [0-9][0-9] ('.'[0-9][0-9]?[0-9]?)? '\'';
LITERAL:      '\'' ('\\\''|~'\'')* '\'';
NAME:         [a-zA-Z0-9]+;
OBJECTNAMES:  [a-zA-Z0-9|]+;
PATH:         [a-zA-Z0-9.]+;
PARAM:        '@'[a-zA-Z0-9]+;
WS:           (' '|'\t') -> skip;
NEWLINE:      ('\r'?'\n'|'\r') -> skip;

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
