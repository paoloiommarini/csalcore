﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace CsAlCore
{
    public partial class CsAlCore : IDisposable
    {
        #region Classes
        /// <summary>
        /// Delete options
        /// </summary>
        public class DeleteOptions
        {
            /// <summary>
            /// Create new <see cref="DeleteOptions"/>
            /// </summary>
            /// <param name="unreference">Unreference attributes</param>
            /// <param name="extend">Extend attributes</param>
            public DeleteOptions(string unreference, string extend)
            {
                Unreference = unreference;
                Extend = extend;
            }

            /// <summary>
            /// Create new <see cref="DeleteOptions"/>
            /// </summary>
            /// <param name="unreference">Unreference attributes</param>
            /// <param name="extend">Extend attributes</param>
            public DeleteOptions(IEnumerable<ObjectAttribute> unreference, IEnumerable<ObjectAttribute> extend)
            {
                if (unreference != null) {
                    StringBuilder sb = new StringBuilder();
                    foreach (ObjectAttribute oa in unreference) {
                        sb.Append($"{ oa.ObjectTypeName }.{ oa.Name },");
                    }
                    Unreference = sb.ToString();
                }
                
                if (extend != null) {
                    StringBuilder sb = new StringBuilder();
                    foreach (ObjectAttribute oa in extend) {
                        sb.Append($"{ oa.ObjectTypeName }.{ oa.Name },");
                    }
                    Extend = sb.ToString();
                }                
            }   
            
            /// <summary>
            /// Paths to unreference (a comma separated list of paths)
            /// </summary>
            public string Unreference { get; set; }

            /// <summary>
            /// Paths to extend (a comma separated list of paths)
            /// </summary>
            public string Extend { get; set; }
        } // DeleteOptions

        /// <summary>
        /// Restore options
        /// </summary>
        public class RestoreOptions
        {
            /// <summary>
            /// Create new <see cref="RestoreOptions"/>
            /// </summary>
            /// <param name="extend">Extend attributes</param>
            public RestoreOptions(string extend)
            {
                Extend = extend;
            }

            /// <summary>
            /// Paths to extend (a comma separated list of paths)
            /// </summary>
            public string Extend { get; set; }
        } // RestoreOptions

        class ReferenceInfo
        {
            public long ObjectId { get; set; }
            public string RefAttributePath { get; set; }
            public List<long> RefObjectIds { get; set; }
        } // ReferenceInfo
        #endregion

        /// <summary>
        /// Get the last modified date time of an object
        /// </summary>
        /// <param name="id">The object id</param>
        /// <returns>The object last modified date time or null</returns>
        public DateTime? GetObjectLastModifiedDate(long id)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                return conn.GetObjectLastModifiedDate(id);
            }
        } // GetOGetObjectLastModifiedDate

        #region Bulk load
        /// <summary>
        /// Bulk load objects
        /// </summary>
        /// <param name="ids">The object ids</param>
        /// <param name="maxDepth">The maximum depth</param>
        /// <returns>A list of <see cref="GenericObject"/></returns>
        public List<GenericObject> BulkLoadObjects(List<Common.IdName> ids, int maxDepth)
        {
            List<long> lIds = new List<long>(ids.Count);
            foreach (Common.IdName idName in ids)
                lIds.Add(idName.Id);
            return BulkLoadObjects(lIds, maxDepth: maxDepth);
        } // BulkLoadObjects

        /// <summary>
        /// Bulk load objects
        /// </summary>
        /// <param name="objectType">The object type</param>
        /// <param name="ids">The object ids</param>
        /// <param name="maxDepth">The maximum depth</param>
        /// <returns>A list of <see cref="GenericObject"/></returns>
        public List<GenericObject> BulkLoadObjects(List<long> ids, ObjectType objectType = null, int maxDepth = 0)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                return LoadObjectsPrimitive(conn, objectType, ids, maxDepth);
            }
        } // BulkLoadObjects
        #endregion

        #region Load objects
        /// <summary>
        /// Load an object
        /// </summary>
        /// <param name="id">The object id</param>
        /// <param name="objectType">The object type</param>
        /// <param name="maxDepth">The maximum depth</param>
        /// <param name="attributes">The list of atributes to load</param>
        /// <returns>A <see cref="GenericObject"/></returns>
        public GenericObject LoadObject(long id, ObjectType objectType = null, int maxDepth = 0, List<string> attributes = null)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                return conn.LoadObject(DbSchema, id, objectType, maxDepth, attributes);
            }
        } // LoadObject

        /// <summary>
        /// Load objects
        /// </summary>
        /// <param name="query">The query</param>
        /// <param name="pars">The query parameters</param>
        /// <param name="deleted">The <see cref="DeletedObjectsStrategy"/> to use</param>
        /// <param name="maxDepth">The maximum depth</param>
        /// <returns>A IEnumerable of <see cref="GenericObject"/></returns>
        public IEnumerable<GenericObject> LoadObjects(string query, List<Query.QueryParameter> pars = null, DeletedObjectsStrategy deleted = DeletedObjectsStrategy.OnlyLive, int maxDepth = 0)
        {
            ObjectType objectType = null;
            List<Common.IdName> ids = SearchObjs(query, out objectType, pars, deleted);
            return LoadObjects(ids, objectType, maxDepth);
        } // LoadObjects

        /// <summary>
        /// Load objects
        /// </summary>
        /// <param name="query">The query</param>
        /// <param name="pars">The query parameters</param>
        /// <param name="deleted">The <see cref="DeletedObjectsStrategy"/> to use</param>
        /// <param name="maxDepth">The maximum depth</param>
        /// <returns>A IEnumerable of <see cref="Common.Schema"/></returns>
        public IEnumerable<T> LoadObjects<T>(string query, List<Query.QueryParameter> pars, DeletedObjectsStrategy deleted, int maxDepth) where T : Common.SchemaObject
        {
            ObjectType objectType = null;
            List<Common.IdName> ids = SearchObjs(query, out objectType, pars, deleted);
            foreach (GenericObject obj in LoadObjects(ids, objectType, maxDepth)){
                yield return (T)Activator.CreateInstance(typeof(T), obj);
            }
        } // LoadObjects

        /// <summary>
        /// Load objects
        /// </summary>
        /// <param name="ids">The object ids</param>
        /// <param name="objectType">The object type</param>
        /// <param name="maxDepth">The maximum depth</param>
        /// <returns>A IEnumerable of <see cref="GenericObject"/></returns>
        public IEnumerable<GenericObject> LoadObjects(List<Common.IdName> ids, ObjectType objectType = null, int maxDepth = 0)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                foreach (Common.IdName idName in ids) {
                    List<GenericObject> objs = LoadObjectsPrimitive(conn, objectType, new List<long>() { idName.Id }, maxDepth);
                    if (objs != null && objs.Count > 0)
                        yield return objs[0];
                }
            }
        } // LoopObjects

        /// <summary>
        /// Load objects
        /// </summary>
        /// <param name="ids">The object ids</param>
        /// <param name="maxDepth">The maximum depth</param>
        /// <returns>A IEnumerable of <see cref="GenericObject"/></returns>
        public IEnumerable<GenericObject> LoadObjects(List<long> ids, int maxDepth)
        {
            List<Common.IdName> idNames = new List<Common.IdName>();
            foreach (long id in ids)
                idNames.Add(new Common.IdName(id, string.Empty));
            return LoadObjects(idNames, null, maxDepth);
        } // LoadObjects
        #endregion

        #region Create objects
        /// <summary>
        /// Save a new object
        /// </summary>
        /// <param name="obj">The <see cref="GenericObject"/> to create</param>
        /// <returns>The new object id</returns>
        public long CreateObject(GenericObject obj)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                return CreateObjectComplete(conn, obj);
            }
        } // CreateObject

        /// <summary>
        /// Save a new object
        /// </summary>
        /// <param name="schemaObj">The <see cref="Common.SchemaObject"/> to create</param>
        /// <returns>The new object id</returns>
        public long CreateObject(Common.SchemaObject schemaObj)
        {
            long res = 0;
            GenericObject obj = schemaObj.ToGenericObject();
            using (DB.DbConnectionBase conn = NewConnection()) {
                res = CreateObjectComplete(conn, obj);
            }

            if (res != 0)
                schemaObj.SetAttributes(obj);

            return res;
        } // CreateObject

        /// <summary>
        /// Create the given objects
        /// </summary>
        /// <param name="objs">The objects</param>
        /// <returns>A List of the new objects ids</returns>
        public List<long> CreateObjects(IEnumerable<GenericObject> objs)
        {
            if (objs == null)
                throw new ArgumentNullException("objs");

            List<long> res = new List<long>();

            using (DB.DbConnectionBase conn = NewConnection()) {
                conn.BeginTransaction();
                try {
                    foreach (GenericObject obj in objs) {
                        long id = CreateObjectComplete(conn, obj);
                        res.Add(id);
                    }
                } catch (Exception ex) {
                    Log?.Write(Common.Utility.LogFile.LogLevels.Error, "CsAlCore", $"CreateObjects error: { ex.Message }");
                    conn.RollbackTransaction();
                    throw ex;
                }
                conn.CommitTransaction();
                return res;
            }
        } // CreateObjects

        /// <summary>
        /// Create the given objects
        /// </summary>
        /// <param name="schemaObjs">The objects</param>
        /// <returns>A List of the new objects ids</returns>
        public List<long> CreateObjects(IEnumerable<Common.SchemaObject> schemaObjs)
        {
            IEnumerable<GenericObject> objs = from value in schemaObjs
                                              select value.ToGenericObject();
            return CreateObjects(objs);
        } // CreateObjects
        #endregion

        #region Update objects
        /// <summary>
        /// Update the given objects
        /// </summary>
        /// <param name="objs">The objects</param>
        /// <returns>True on success</returns>
        public bool UpdateObjects(IEnumerable<GenericObject> objs)
        {
            if (objs == null)
                throw new ArgumentNullException("objs");

            using (DB.DbConnectionBase conn = NewConnection()) {
                conn.BeginTransaction();
                foreach (GenericObject obj in objs) {
                    try {
                        if (!UpdateObjectPrimitive(conn, obj, null)) {
                            conn.RollbackTransaction();
                            return false;
                        }
                    } catch (Exception ex) {
                        System.Diagnostics.Debug.WriteLine(string.Format("UpdateObjects error: {0}", ex.Message));
                        conn.RollbackTransaction();
                        throw ex;
                    }
                }
                conn.CommitTransaction();
            }
            return true;
        } // UpdateObjects

        /// <summary>
        /// Update the given objects
        /// </summary>
        /// <param name="schemaObjs">The objects</param>
        /// <returns>True on success</returns>
        public bool UpdateObjects(IEnumerable<Common.SchemaObject> schemaObjs)
        {
            IEnumerable<GenericObject> objs = from value in schemaObjs
                                              select value.ToGenericObject();
            return UpdateObjects(objs);
        } // UpdateObjects

        /// <summary>
        /// Update the given object
        /// </summary>
        /// <param name="obj">The object</param>
        /// <returns>True on success</returns>
        public bool UpdateObject(GenericObject obj)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                return UpdateObjectPrimitive(conn, obj, null);
            }
        } // UpdateObject

        /// <summary>
        /// Update the given object
        /// </summary>
        /// <param name="schemaObj">The object</param>
        /// <returns>True on success</returns>
        public bool UpdateObject(Common.SchemaObject schemaObj)
        {
            bool res = false;
            GenericObject obj = schemaObj.ToGenericObject();
            using (DB.DbConnectionBase conn = NewConnection()) {
                res = UpdateObjectPrimitive(conn, obj, null);
            }

            if (res)
                schemaObj.SetAttributes(obj);

            return res;
        } // UpdateObject
        #endregion

        #region Delete and restore objects
        /// <summary>
        /// Logically delete the object with the given id
        /// </summary>
        /// <param name="id">The object id</param>
        /// <param name="options">the <see cref="DeleteOptions"/> to use</param>
        /// <returns>A list of soft deleted ids</returns>
        public List<long> SoftDeleteObject(long id, DeleteOptions options)
        {
            return SoftDeleteObjects(new List<long>() { id }, options);
        } // SoftDeleteObject

        /// <summary>
        /// Logically delete the objects with the given ids
        /// </summary>
        /// <param name="ids">The object ids</param>
        /// <param name="options">The <see cref="DeleteOptions"/> to use</param>
        /// <returns>A list of soft deleted ids</returns>
        public List<long> SoftDeleteObjects(List<long> ids, DeleteOptions options)
        {
            List<long> res = null;
            using (DB.DbConnectionBase conn = NewConnection()) {
                conn.BeginTransaction();
                Dictionary<string, List<long>> typeIds = GroupObjectsByType(conn, ids);
                foreach (KeyValuePair<string, List<long>> kvp in typeIds) {
                    ObjectType ot = DbSchema.GetObjectType(kvp.Key);
                    if (!DeleteObjectsPrimitive(conn, ot, true, kvp.Value, options, out res)) {
                        conn.RollbackTransaction();
                        return null;
                    }
                }
                conn.CommitTransaction();
                return res;
            }
        } // SoftDeleteObject

        /// <summary>
        /// Restore a logically deleted object
        /// </summary>
        /// <param name="id">The object id</param>
        /// <param name="options">The <see cref="RestoreOptions"/> to use</param>
        /// <returns>A list of restored ids</returns>
        public List<long> RestoreObject(long id, RestoreOptions options)
        {
            return RestoreObjects(new List<long>() { id }, options);
        } // RestoreObject

        /// <summary>
        /// Restore logically deleted objects with the given ids
        /// </summary>
        /// <param name="ids">The object ids</param>
        /// <param name="options">The <see cref="RestoreOptions"/> to use</param>
        /// <returns>True on success</returns>
        public List<long> RestoreObjects(List<long> ids, RestoreOptions options)
        {
            List<long> res = null;
            using (DB.DbConnectionBase conn = NewConnection()) {
                conn.BeginTransaction();
                Dictionary<string, List<long>> typeIds = GroupObjectsByType(conn, ids);
                foreach (KeyValuePair<string, List<long>> kvp in typeIds) {
                    ObjectType ot = DbSchema.GetObjectType(kvp.Key);
                    if (!RestoreObjectsPrimitive(conn, ot, kvp.Value, options, out res)) {
                        conn.RollbackTransaction();
                        return null;
                    }
                }
                conn.CommitTransaction();
                return res;
            }
        } // RestoreObjects

        /// <summary>
        /// Delete the object with the given id
        /// </summary>
        /// <param name="ids">The object ids</param>
        /// <param name="options">the <see cref="DeleteOptions"/> to use</param>
        /// <returns>A list of deleted ids</returns>
        public List<long> DeleteObjects(List<long> ids, DeleteOptions options)
        {
            List<long> res = null;
            using (DB.DbConnectionBase conn = NewConnection()) {
                conn.BeginTransaction();
                Dictionary<string, List<long>> typeIds = GroupObjectsByType(conn, ids);
                foreach (KeyValuePair<string, List<long>> kvp in typeIds) {
                    ObjectType ot = DbSchema.GetObjectType(kvp.Key);
                    if (!DeleteObjectsPrimitive(conn, ot, false, kvp.Value, options, out res)) {
                        conn.RollbackTransaction();
                        return null;
                    }
                }
                conn.CommitTransaction();
                return res;
            }
        } // DeleteObjects

        /// <summary>
        /// Delete the object with the given id
        /// </summary>
        /// <param name="id">The object id</param>
        /// <param name="options">the <see cref="DeleteOptions"/> to use</param>
        /// <returns>A list of deleted ids</returns>
        public List<long> DeleteObject(long id, DeleteOptions options)
        {
            return DeleteObjects(new List<long>() { id }, options);
        } // DeleteObject
        #endregion

        #region private operations
        private List<GenericObject> LoadObjectsPrimitive(DB.DbConnectionBase conn, ObjectType objectType, List<long> ids, int maxDepth)
        {
            List<GenericObject> res = new List<GenericObject>();

            // Group objects by type
            if (objectType == null) {
                Dictionary<string, List<long>> typeIds = GroupObjectsByType(conn, ids);
                foreach (KeyValuePair<string, List<long>> kvp in typeIds) {
                    string type = kvp.Key;
                    List<long> tIds = kvp.Value;

                    ObjectType ot = DbSchema.GetObjectType(type);
                    res.AddRange(conn.LoadObjects(DbSchema, ot, tIds, maxDepth));
                }
            } else {
                res.AddRange(conn.LoadObjects(DbSchema, objectType, ids, maxDepth));
            }

            return res;
        } // LoadObjectsPrimitive

        private bool ValidateObject(DB.DbConnectionBase conn, GenericObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            if (obj.ObjectType == null)
                return false;

            // Required attributes:
            foreach (ObjectAttribute att in obj.ObjectType.Attributes) {
                if (att.Required && att.Type != ObjectAttribute.Types.Identity && !att.SystemManaged) {
                    GenericAttribute oAtt = obj.GetAttribute(att.Name);
                    if (oAtt == null || oAtt.Value == null)
                        throw new Common.Exceptions.MissingRequiredAttributeError(att.Name);
                }

                // Binary data:
                if (att.Type == ObjectAttribute.Types.BinaryData) {
                    Common.Schema.AttributeTypes.BinaryDataAttrType bAtt = (Common.Schema.AttributeTypes.BinaryDataAttrType)att.AttributeType;
                    Common.Schema.BinaryDataValue bv = obj.GetAttribute(att.Name).Value as Common.Schema.BinaryDataValue;                    
                    if (bv != null && bAtt.AllowedMimeTypes != null && bAtt.AllowedMimeTypes.Length > 0) {
                        if (!bAtt.AllowedMimeTypes.Contains(bv.MimeType)) 
                            throw new Common.Exceptions.InvalidMimeTypeError(att.Name);
                    }
                }
            }

            // References:
            foreach (ObjectAttribute refAt in obj.ObjectType.GetReferences()) {
                if (!refAt.SystemManaged) {
                    GenericAttribute oAtt = obj.GetAttribute(refAt.Name);
                    if (oAtt != null && oAtt.Value != null) {
                        if (oAtt.Value is GenericObject) {
                            GenericObject refObj = oAtt.Value as GenericObject;
                            if (String.Compare(refObj.ObjectTypeName, oAtt.Attribute.RefObjectType, StringComparison.OrdinalIgnoreCase) != 0)
                                throw new Common.Exceptions.InvalidReferenceError(oAtt.Name, refObj.ObjectTypeName);
                        } else if (oAtt.Value is long || oAtt.Value is int) {
                            long refId = System.Convert.ToInt64(oAtt.Value);
                            if (refId != 0) {
                                string refObjectType = conn.GetObjectObjectType(refId);
                                if (string.Compare(refObjectType, oAtt.Attribute.RefObjectType, StringComparison.OrdinalIgnoreCase) != 0)
                                    throw new Common.Exceptions.InvalidReferenceError(oAtt.Name, refObjectType);
                            }
                        }
                    }
                }
            }

            // Multi references
            foreach (ObjectAttribute mrefAt in obj.ObjectType.GetMultiReferences()) {
                if (!mrefAt.SystemManaged) 
                    ValidateMultiReference(conn, obj, mrefAt);
            }

            // Children
            foreach (ObjectAttribute chAt in obj.ObjectType.GetChildrenReferences()) {
                if (!chAt.SystemManaged)
                    ValidateMultiReference(conn, obj, chAt);
            }

            // Unique indexes:
            if (!obj.ObjectType.SystemObject) {
                ValidateUniqueIndexes(obj);                    
                
                foreach (ObjectIndex idx in obj.ObjectType.Indexes) {
                    if (idx.Unique) {
                        bool skip = false;
                        StringBuilder sb = new StringBuilder($@"select { obj.ObjectType.Name } where ");
                        List<Query.QueryParameter> pars = new List<Query.QueryParameter>();
                        int iaIdx = 0;
                        foreach (ObjectIndex.IndexAttribute iAt in idx.Attributes) {
                            if(string.Compare(iAt.Name, Schema.ChildProgressiveAttributeName, StringComparison.OrdinalIgnoreCase) == 0) {
                                // Skip check for children progressive indexes
                                skip = true;
                                break;
                            }

                            if (iaIdx > 0)
                                sb.Append("and ");

                            object value = obj.GetAttributeValue(iAt.Name);
                            if (value != null) {
                                sb.Append($"{ iAt.Name } = @{ iAt.Name } ");
                                pars.Add(new Query.QueryParameter(iAt.Name, value));
                            } else {
                                sb.Append($"isNull({ iAt.Name })");
                            }
                            iaIdx++;
                        }

                        if (!skip) {
                            List<Common.IdName> idNames = SearchObjs(sb.ToString(), out _, pars, DeletedObjectsStrategy.OnlyLive);
                            if (idNames.Count > 1 || idNames.Count == 1 && idNames[0].Id != obj.Id)
                                throw new Common.Exceptions.UniqueIndexDuplicateValueError(idx.Name);
                        }
                    }
                }
            }

            // Validation from schema:
            if (obj.ObjectType.ValidationXsltCompiled != null) {
                DataNavigator.ObjectNavigator nav = new DataNavigator.ObjectNavigator(this, obj);
                StringBuilder sb = new StringBuilder();
                using (XmlWriter xw = XmlWriter.Create(sb)) {
                    obj.ObjectType.ValidationXsltCompiled.Transform(nav, xw);
                }

                ValidationErrors ve = Common.Utility.Serialization.Deserialize<ValidationErrors>(sb.ToString());
                if (ve != null && ve.Errors != null && ve.Errors.Count > 0)
                    throw new Common.Exceptions.ObjectValidationError(obj.ObjectType.Name, ve.Errors);
            }

            return true;
        } // ValidateObject

        private bool ValidateUniqueIndexes(GenericObject obj)
        {
            foreach (ObjectIndex idx in obj.ObjectType.Indexes) {
                if (idx.Unique) {
                    bool skip = false;
                    StringBuilder sb = new StringBuilder($@"select { obj.ObjectType.Name } where ");
                    List<Query.QueryParameter> pars = new List<Query.QueryParameter>();
                    int iaIdx = 0;
                    foreach (ObjectIndex.IndexAttribute iAt in idx.Attributes) {
                        if(string.Compare(iAt.Name, Schema.ChildProgressiveAttributeName, StringComparison.OrdinalIgnoreCase) == 0) {
                            // Skip check for children progressive indexes
                            skip = true;
                            break;
                        }

                        if (iaIdx > 0)
                            sb.Append("and ");

                        object value = obj.GetAttributeValue(iAt.Name);
                        if (value != null) {
                            sb.Append($"{ iAt.Name } = @{ iAt.Name } ");
                            pars.Add(new Query.QueryParameter(iAt.Name, value));
                        } else {
                            sb.Append($"isNull({ iAt.Name })");
                        }
                        iaIdx++;
                    }

                    if (!skip) {
                        List<Common.IdName> idNames = SearchObjs(sb.ToString(), out _, pars, DeletedObjectsStrategy.OnlyLive);
                        if (idNames.Count > 1 || idNames.Count == 1 && idNames[0].Id != obj.Id)
                            throw new Common.Exceptions.UniqueIndexDuplicateValueError(idx.Name);
                    }
                }
            }

            return true;
        } // ValidateUniqueIndexes
        
        /// <summary>
        /// Validate a multi reference attribute value.
        /// Check data type and object type of referenced objects
        /// </summary>
        /// <param name="conn">The database connection</param>
        /// <param name="obj">The object</param>
        /// <param name="mrefAt">The multireference attribute</param>
        /// <returns>True on success</returns>
        private bool ValidateMultiReference(DB.DbConnectionBase conn, GenericObject obj, ObjectAttribute mrefAt)
        {
            GenericAttribute oAtt = obj.GetAttribute(mrefAt.Name);
            if (oAtt != null && oAtt.Value != null) {
                if (oAtt.Value is List<MultiReferenceValue>) {
                    List<MultiReferenceValue> mrefObjs = oAtt.Value as List<MultiReferenceValue>;
                    List<long> mrefIds = new List<long>();
                    foreach (MultiReferenceValue crv in mrefObjs) {
                        GenericObject mrefObj = crv.RefObject;
                        if (mrefObj != null) {
                            if (string.Compare(mrefObj.ObjectTypeName, oAtt.Attribute.RefObjectType, StringComparison.OrdinalIgnoreCase) != 0)
                                throw new Common.Exceptions.InvalidReferenceError(oAtt.Name, mrefObj.ObjectTypeName);
                        } else {
                            mrefIds.Add(crv.RefId);
                        }
                    }

                    if (mrefIds.Count > 0) {
                        string[] refObjectsType = conn.GetObjectsObjectType(mrefIds);
                        foreach (string refOt in refObjectsType) {
                            if (string.Compare(refOt, oAtt.Attribute.RefObjectType, StringComparison.OrdinalIgnoreCase) != 0)
                                throw new Common.Exceptions.InvalidReferenceError(oAtt.Name, refOt);
                        }
                    }
                } else {
                    throw new Common.Exceptions.InvalidAttributeValueError(oAtt.Name, oAtt.Value);
                }
            }
            return true;
        } // ValidateMultiReference

        private bool UpdateObjectPrimitive(DB.DbConnectionBase conn, GenericObject obj, List<string> attributes)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            if (obj.Id == 0)
                throw new Common.Exceptions.UpdateObjectWithoutIdError();

            // Check object type:
            string originalOt = conn.GetObjectObjectType(obj.Id);
            if (string.Compare(originalOt, obj.ObjectTypeName, StringComparison.OrdinalIgnoreCase) != 0)
                throw new Common.Exceptions.ObjectTypeMismatchError(originalOt, obj.ObjectTypeName);

            // Check that no system managed attributes have been updated:
            foreach (ObjectAttribute oa in obj.ObjectType.Attributes) {
                if (oa.SystemManaged && obj.GetAttribute(oa.Name)?.Changed == true) {
                    throw new Common.Exceptions.CannotUpdateSystemManagedAttributesError(oa.Name);
                }                    
            }
            
            CompleteObject(obj);
            ValidateObject(conn, obj);
            CreateReferencedObjects(conn, obj);
            CreateChildrenObjects(conn, obj, obj.Id);
            bool res = conn.UpdateObject(DbSchema, GetBinaryDataRoot(), obj, attributes);

            if (res)
                obj.ResetChanged();
            return res;
        } // UpdateObjectPrimitive

        /// <summary>
        /// Create a new object, creating also new referenced objects
        /// </summary>
        /// <param name="conn">The <see cref="DB.DbConnectionBase"/></param>
        /// <param name="obj">The object</param>
        /// <returns>The new object id</returns>
        private long CreateObjectComplete(DB.DbConnectionBase conn, GenericObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            if (obj.ObjectType.Abstract)
                throw new Common.Exceptions.CannotCreateAbstractObjectError(obj.ObjectTypeName);

            // Create new referenced objects:
            List<long> refCreatedIds = null;

            long res = 0;
            try {
                refCreatedIds = CreateReferencedObjects(conn, obj);
                res = CreateObjectPrimitive(conn, obj);
                // Create new children objects:
                refCreatedIds.AddRange(CreateChildrenObjects(conn, obj, res));
            } catch (Exception ex) {
                if (refCreatedIds != null) {
                    foreach (long id in refCreatedIds)
                        conn.DeleteObject(DbSchema, GetBinaryDataRoot(), id);
                }

                if (res != 0)
                    conn.DeleteObject(DbSchema, GetBinaryDataRoot(), res);
                throw ex;
            }

            obj.ResetChanged();
            return res;
        } // CreateObjectComplete

        /// <summary>
        /// Create the new referenced (and multireferenced) objects 
        /// </summary>
        /// <param name="conn">The <see cref="DB.DbConnectionBase"/></param>
        /// <param name="obj">The <see cref="GenericObject"/> </param>
        /// <returns>A list of the created objects ids</returns>
        private List<long> CreateReferencedObjects(DB.DbConnectionBase conn, GenericObject obj)
        {
            List<long> refCreatedIds = new List<long>();
            foreach (ObjectAttribute refAt in obj.ObjectType.GetReferences()) {
                object refValue = obj.GetAttributeValue(refAt.Name);
                if (refValue is GenericObject) {
                    GenericObject refObj = refValue as GenericObject;
                    if (refObj.Id == 0) {
                        long refObjId = CreateObjectComplete(conn, refObj);
                        if (refObjId != 0)
                            refCreatedIds.Add(refObjId);
                    }
                }
            }

            // Create new multi referenced objects:
            foreach (ObjectAttribute refAt in obj.ObjectType.GetMultiReferences()) {
                object mRefValue = obj.GetAttributeValue(refAt.Name);
                if (mRefValue is List<MultiReferenceValue>) {
                    List<MultiReferenceValue> mRefObjs = mRefValue as List<MultiReferenceValue>;
                    foreach (MultiReferenceValue mrv in mRefObjs) {
                        GenericObject mrefObj = mrv.RefObject;
                        if (mrefObj != null) {
                            if (mrefObj.Id == 0) {
                                long refObjId = CreateObjectComplete(conn, mrefObj);
                                if (refObjId != 0)
                                    refCreatedIds.Add(refObjId);
                            }
                        }
                    }
                }
            }

            return refCreatedIds;
        } // CreateReferencedObjects

        /// <summary>
        /// Create the new children objects
        /// </summary>
        /// <param name="conn">The <see cref="DB.DbConnectionBase"/></param>
        /// <param name="obj">The <see cref="GenericObject"/></param>
        /// <param name="id">The object id</param>
        /// <returns>A list of the created objects ids</returns>
        private List<long> CreateChildrenObjects(DB.DbConnectionBase conn, GenericObject obj, long id)
        {
            List<long> refCreatedIds = new List<long>();
            foreach (ObjectAttribute cRefAt in obj.ObjectType.GetChildrenReferences()) {
                object mRefValue = obj.GetAttributeValue(cRefAt.Name);
                if (mRefValue is List<MultiReferenceValue>) {
                    List<MultiReferenceValue> mRefObjs = mRefValue as List<MultiReferenceValue>;
                    int idx = 0;
                    foreach (MultiReferenceValue mrv in mRefObjs) {                        
                        if (mrv.RefId == 0) {
                            GenericObject mrefObj = mrv.RefObject;
                            mrefObj.SetAttributeValue(Schema.ParentObjElementName, id);
                            if (cRefAt.Sortable)
                                mrefObj.SetAttributeValue(Schema.ChildProgressiveAttributeName, idx++);
                            long refObjId = CreateObjectComplete(conn, mrefObj);
                            if (refObjId != 0)
                                refCreatedIds.Add(refObjId);
                        }
                    }
                }
            }

            return refCreatedIds;
        } // CreateChildrenObjects

        /// <summary>
        /// Creates a single object
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private long CreateObjectPrimitive(DB.DbConnectionBase conn, GenericObject obj)
        {
            if (obj.Id != 0)
                throw new Common.Exceptions.CreateObjectWithIdError();
           
            CompleteObject(obj);
            ValidateObject(conn, obj);

            string dataRootPath = GetBinaryDataRoot();
            long? maxFilesInFolder = m_EnvVariablesCache.GetValue<EnvVariable>(EnvVariable.Names.BinaryDataMaxFiles.ToString())?.GetValue<long>();

            // Get object types
            List<ObjectType> objectTypes = DbSchema.GetObjectTypeInheritance(obj.ObjectTypeName);
            long objectId = 0;
            foreach (ObjectType ot in objectTypes) {
                bool main = ot.GetIdentity(false) != null;

                List<GenericAttribute> attributes = obj.GetAttributes(ot.Name);
                if (!main && objectId != 0)
                    attributes.Add(new GenericAttribute(ot.GetIdentity(true), objectId));

                if (attributes != null && attributes.Count > 0) {
                    long tempId = conn.InsertObject(dataRootPath, maxFilesInFolder.HasValue ? maxFilesInFolder.Value : 0, ot, attributes);
                    if (main)
                        objectId = tempId;
                }
            }

            obj.SetAttributeValue(obj.ObjectType.GetIdentity(true).Name, objectId);
            m_ObjectsObjectType.AddOrUpdateValue(objectId.ToString(), obj.ObjectTypeName);
            return objectId;
        } // CreateObjectPrimitive

        /// <summary>
        /// Complete the given object with autocomputed attributes
        /// </summary>
        /// <param name="obj">The <see cref="GenericObject"/> to complete</param>
        private void CompleteObject(GenericObject obj)
        {
            // objectTypeRef
            if (obj.Id == 0) {
                ObjectAttribute ota = obj.ObjectType.GetAttribute(Schema.ObjectTypeRefName);
                if (ota != null)
                    obj.SetAttributeValue(ota.Name, GetObjectTypeId(obj.ObjectType.Name));
            }

            // Version number
            ObjectAttribute vta = obj.ObjectType.GetAttribute(Schema.ObjectVersionNumberAttributeName);
            if (vta != null) {
                if (obj.Id == 0)
                    obj.SetAttributeValue(vta.Name, 1L);
                else {
                    obj.SetAttributeValue(vta.Name, (long)obj.GetAttributeValue(vta.Name) + 1);
                }
            }

            // CreatedDateTime/LastModifiedDateTime            
            foreach (ObjectAttribute att in obj.ObjectType.Attributes) {
                if (att.Type == ObjectAttribute.Types.LastModifiedDateTime) {
                    obj.SetAttributeValue(att.Name, DateTime.UtcNow);
                } else if (obj.Id == 0 && att.Type == ObjectAttribute.Types.CreatedDateTime) {
                    obj.SetAttributeValue(att.Name, DateTime.UtcNow);
                }
            }
        } // CompleteObject

        /// <summary>
        /// Group objects bu object type
        /// </summary>
        /// <param name="conn">The <see cref="DB.DbConnectionBase"/></param>
        /// <param name="ids">The object ids</param>
        /// <returns>The ids grouped by object type</returns>
        private Dictionary<string, List<long>> GroupObjectsByType(DB.DbConnectionBase conn, List<long> ids)
        {
            Dictionary<string, List<long>> typeIds = new Dictionary<string, List<long>>();
            string[] types = conn.GetObjectsObjectType(ids);
            foreach (string type in types.Distinct()) {
                typeIds[type] = new List<long>();
                int idx = 0;
                while ((idx = Array.IndexOf(types, type, idx)) != -1) {
                    typeIds[type].Add(ids[idx++]);
                }
            }
            return typeIds;
        } // GroupObjectsByType

        /// <summary>
        /// Complete the given id list with the unref and extend from the options
        /// </summary>
        /// <param name="conn">The database connection</param>
        /// <param name="ids">The object ids</param>
        /// <param name="options">The options</param>
        /// <param name="extendIds">List of ids from the Extend option</param>
        /// <param name="unrefIds">List of ids from the Unreference option</param>
        private void CompleteIds(DB.DbConnectionBase conn, List<long> ids, DeleteOptions options, out List<ReferenceInfo> unrefIds, out List<long> extendIds)
        {
            extendIds = new List<long>();
            unrefIds = new List<ReferenceInfo>();
            Dictionary<string, List<long>> typeIds = GroupObjectsByType(conn, ids);

            // Automatically extend to children:
            foreach (KeyValuePair<string, List<long>> kvp in typeIds) {
                string objectTypeName = kvp.Key;
                List<long> objIds = kvp.Value;
                ObjectType ot = DbSchema.GetObjectType(objectTypeName);                
                foreach (ObjectAttribute chAt in ot.GetChildrenReferences()) {
                    Dictionary<long, List<long>> children = conn.GetoObjectsChildren(DbSchema, objIds, chAt);
                    foreach (KeyValuePair<long, List<long>> ckvp in children) {
                        extendIds.AddRange(ckvp.Value);
                    }
                }
            }

            if (options != null) {
                Dictionary<string, List<PathInfo>> unrefPaths = new Dictionary<string, List<PathInfo>>();
                Dictionary<string, List<PathInfo>> extendPaths = new Dictionary<string, List<PathInfo>>();

                // Unref paths:
                if (!string.IsNullOrEmpty(options.Unreference)) {
                    string[] paths = options.Unreference.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string path in paths) {
                        PathInfo pi = DbSchema.ResolvePath(path.Trim());
                        if (pi.AttributeTypes.Last().Type != ObjectAttribute.Types.Reference && pi.AttributeTypes.Last().Type != ObjectAttribute.Types.MultiReference ||
                            pi.AttributeTypes.Last().Type == ObjectAttribute.Types.Reference && pi.AttributeTypes.Last().Required) 
                            throw new Common.Exceptions.InvalidUnrefAttributeError(path);

                        List<PathInfo> pis = null;
                        string oType = string.Empty;
                        ObjectAttribute at = pi.AttributeTypes.Last();
                        if (at.AttributeType.Type == ObjectAttribute.Types.MultiReference)
                            oType = at.RefObjectType;
                        else if (at.AttributeType.Type == ObjectAttribute.Types.Reference)
                            oType = at.ObjectTypeName;

                        if (unrefPaths.TryGetValue(oType, out pis))
                            pis.Add(pi);
                        else
                            unrefPaths[oType] = new List<PathInfo>() { pi };
                    }
                }

                // Extend paths:
                if (!string.IsNullOrEmpty(options.Extend)) {
                    string[] paths = options.Extend.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string path in paths) {
                        PathInfo pi = DbSchema.ResolvePath(path.Trim());
                        if (pi.AttributeTypes.Last().Type != ObjectAttribute.Types.Reference && pi.AttributeTypes.Last().Type != ObjectAttribute.Types.MultiReference)
                            throw new Common.Exceptions.InvalidExtendAttributeError(path);

                        List<PathInfo> pis = null;
                        string oType = string.Empty;
                        ObjectAttribute at = pi.AttributeTypes.Last();
                        if (at.AttributeType.Type == ObjectAttribute.Types.MultiReference)
                            oType = at.RefObjectType;
                        else if (at.AttributeType.Type == ObjectAttribute.Types.Reference)
                            oType = at.ObjectTypeName;
                        if (extendPaths.TryGetValue(oType, out pis))
                            pis.Add(pi);
                        else
                            extendPaths[oType] = new List<PathInfo>() { pi };
                    }
                }

                // Loop object types
                foreach (KeyValuePair<string, List<long>> kvp in typeIds) {
                    string objectTypeName = kvp.Key;
                    List<long> objIds = kvp.Value;
                    ObjectType ot = DbSchema.GetObjectType(objectTypeName);
                    List<PathInfo> unref = null;
                    if (unrefPaths.TryGetValue(objectTypeName, out unref)) {
                        foreach (PathInfo pi in unref) {
                            // Check referenced objects:
                            Dictionary<long, List<long>> refIds = conn.GetObjectsReferencing(DbSchema, ids, pi.AttributeTypes.Last(), false, false);
                            if (refIds != null) {
                                foreach (KeyValuePair<long, List<long>> rkvp in refIds) {
                                    unrefIds.Add(new ReferenceInfo()
                                    {
                                        ObjectId = rkvp.Key,
                                        RefAttributePath = pi.Path,
                                        RefObjectIds = rkvp.Value
                                    });
                                }
                            }
                        }
                    }

                    if (extendPaths.TryGetValue(objectTypeName, out unref)) {
                        foreach (PathInfo pi in unref) {
                            // Check extended objects:
                            Dictionary<long, List<long>> extIds = conn.GetObjectsReferencing(DbSchema, ids, pi.AttributeTypes.Last(), false, false);
                            if (extIds != null) {
                                foreach (KeyValuePair<long, List<long>> rkvp in extIds) {
                                    extendIds.AddRange(rkvp.Value);
                                }
                            }
                        }
                    }
                }
            }
        } // CompleteIds

        /// <summary>
        /// Delete the objects with the given ids
        /// </summary>
        /// <param name="conn">The <see cref="DB.DbConnectionBase"/></param>
        /// <param name="objectType">The object type</param>
        /// <param name="softDelete">If true the objects are soft deleted</param>
        /// <param name="ids">The object ids</param>
        /// <param name="options">the <see cref="DeleteOptions"/> to use</param>
        /// <param name="deletedIds">The list of deleted ids</param>
        /// <returns>True on success</returns>
        private bool DeleteObjectsPrimitive(DB.DbConnectionBase conn, ObjectType objectType, bool softDelete, List<long> ids, DeleteOptions options, out List<long> deletedIds)
        {
            deletedIds = new List<long>();
            if (ids == null)
                throw new ArgumentNullException("ids");

            if (softDelete) {
                // Check that the objectType has the isDeleted attribute:
                if (objectType.GetAttribute(Schema.ObjectIsDeletedAttributeName) == null)
                    throw new Common.Exceptions.CannotSoftDeleteObjectNonInheritingObjectError(objectType.Name);

                // Check if the objects are soft deleted
                List<bool> deletedStatus = conn.GetObjectsDeletedStatus(DbSchema, objectType, ids);
                for (int i = 0; i < deletedStatus.Count; i++) {
                    if (deletedStatus[i])
                        throw new Common.Exceptions.ObjectIsSoftDeletedError(ids[i]);
                }
            }

            List<ReferenceInfo> unrefIds = null;
            List<long> extendIds = null;
            CompleteIds(conn, ids, options, out unrefIds, out extendIds);

            // Get all objects referencing the objects we want to delete
            Dictionary<string, List<long>> oTypes = GroupObjectsByType(conn, ids);
            foreach (KeyValuePair<string, List<long>> oType in oTypes) {
                List<ObjectAttribute> refAttrs = DbSchema.GetReferencing(oType.Key);
                if (refAttrs != null && refAttrs.Count > 0) {
                    foreach (ObjectAttribute refAttr in refAttrs) {
                        Dictionary<long, List<long>> refIds = conn.GetObjectsReferencing(DbSchema, ids, refAttr, false, false);
                        if (refIds != null && refIds.Count > 0) {
                            // Check if one object is referenced by an object we are not going to delete or unreference
                            foreach (KeyValuePair<long, List<long>> kvp in refIds) {
                                foreach (long id in kvp.Value) {
                                    bool ok = false;

                                    foreach (ReferenceInfo rInfo in unrefIds) {
                                        if (rInfo.RefObjectIds.Contains(id)) {
                                            ok = true;
                                            break;
                                        }
                                    }

                                    if (!ok && !extendIds.Contains(id) && !ids.Contains(id))
                                        throw new Common.Exceptions.CannotDeleteReferencedObjectError(kvp.Key, id);
                                }
                            }
                        }
                    }
                }
            }

            // Unreference objects:
            if (unrefIds.Count > 0) {
                foreach (ReferenceInfo rInfo in unrefIds) {
                    PathInfo pi = DbSchema.ResolvePath(rInfo.RefAttributePath);
                    if (!conn.UnreferenceObjects(DbSchema, rInfo.ObjectId, rInfo.RefObjectIds, pi.AttributeTypes.LastOrDefault()))
                        return false;
                }
            }

            // Delete extended objects (we must check for references)
            List<long> extdelIds = null;
            if (extendIds.Count > 0 && !DeleteObjectsPrimitive(conn, objectType, softDelete, extendIds, options, out extdelIds))
                return false;

            // Delete objects
            foreach (long id in ids) {
                if (softDelete) {
                    if (!conn.SoftDeleteObject(DbSchema, GetBinaryDataRoot(), id))
                        return false;
                } else {
                    if (!conn.DeleteObject(DbSchema, GetBinaryDataRoot(), id))
                        return false;
                }
                deletedIds.Add(id);
            }

            if (extdelIds != null)
                deletedIds.AddRange(extdelIds);

            return true;
        } // DeleteObjects

        /// <summary>
        /// Restore logically deleted objects with the given ids
        /// </summary>
        /// <param name="conn">The <see cref="DB.DbConnectionBase"/></param>
        /// <param name="objectType">The object type</param>
        /// <param name="ids">The object ids</param>
        /// <param name="options">the <see cref="RestoreOptions"/> to use</param>
        /// <param name="restoredIds">The object ids</param>
        /// <returns>True on success</returns>
        private bool RestoreObjectsPrimitive(DB.DbConnectionBase conn, ObjectType objectType, List<long> ids, RestoreOptions options, out List<long> restoredIds)
        {
            restoredIds = new List<long>();
            if (ids == null)
                throw new ArgumentNullException("ids");

            // Check if the objects are soft deleted
            List<bool> deletedStatus = conn.GetObjectsDeletedStatus(DbSchema, objectType, ids);
            for (int i = 0; i < deletedStatus.Count; i++) {
                bool deleted = deletedStatus[i];
                if (!deleted)
                    throw new Common.Exceptions.ObjectIsNotSoftDeletedError(ids[i]);
            }

            List<long> extendIds = new List<long>();
            Dictionary<string, List<long>> typeIds = GroupObjectsByType(conn, ids);
            foreach (KeyValuePair<string, List<long>> kvp in typeIds) {
                string objectTypeName = kvp.Key;
                List<long> objIds = kvp.Value;
                ObjectType ot = DbSchema.GetObjectType(objectTypeName);
                // Automatically extend to children:
                foreach (ObjectAttribute chAt in ot.GetChildrenReferences()) {
                    Dictionary<long, List<long>> children = conn.GetoObjectsChildren(DbSchema, objIds, chAt);
                    foreach (KeyValuePair<long, List<long>> ckvp in children) {
                        extendIds.AddRange(ckvp.Value);
                    }
                }
            }

            // Extend restore:
            if (options != null) {
                // Extend paths:
                Dictionary<string, List<PathInfo>> extendPaths = new Dictionary<string, List<PathInfo>>();

                if (!string.IsNullOrEmpty(options.Extend)) {
                    string[] paths = options.Extend.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string path in paths) {
                        PathInfo pi = DbSchema.ResolvePath(path.Trim());
                        if (pi.AttributeTypes.Last().Type != ObjectAttribute.Types.Reference && pi.AttributeTypes.Last().Type != ObjectAttribute.Types.MultiReference)
                            throw new Common.Exceptions.InvalidExtendAttributeError(path);

                        List<PathInfo> pis = null;
                        string oType = string.Empty;
                        ObjectAttribute at = pi.AttributeTypes.Last();
                        if (at.AttributeType.Type == ObjectAttribute.Types.MultiReference)
                            oType = at.RefObjectType;
                        else if (at.AttributeType.Type == ObjectAttribute.Types.Reference)
                            oType = at.ObjectTypeName;
                        if (extendPaths.TryGetValue(oType, out pis))
                            pis.Add(pi);
                        else
                            extendPaths[oType] = new List<PathInfo>() { pi };
                    }
                }

                foreach (KeyValuePair<string, List<long>> oType in typeIds) {
                    List<PathInfo> extend = null;
                    if (extendPaths.TryGetValue(oType.Key, out extend)) {
                        foreach (PathInfo pi in extend) {
                            // Check extended objects:
                            Dictionary<long, List<long>> extIds = conn.GetObjectsReferencing(DbSchema, ids, pi.AttributeTypes.Last(), false, true);
                            if (extIds != null) {
                                foreach (KeyValuePair<long, List<long>> rkvp in extIds) {
                                    extendIds.AddRange(rkvp.Value);
                                }
                            }
                        }
                    }
                }
            }

            // Check if the objects we are restoring reference soft deleted objects (including parents)
            foreach (KeyValuePair<string, List<long>> oType in typeIds) {
                List<ObjectAttribute> refAttrs = DbSchema.GetReferencing(oType.Key);
                if (refAttrs != null && refAttrs.Count > 0) {
                    foreach (ObjectAttribute refAttr in refAttrs) {
                        Dictionary<long, List<long>> refIds = conn.GetObjectsReferencing(DbSchema, ids, refAttr, true, true);
                        if (refIds != null && refIds.Count > 0) {
                            foreach (KeyValuePair<long, List<long>> kvp in refIds) {
                                foreach (long id in kvp.Value) {
                                    if (!ids.Contains(id) && !extendIds.Contains(id))
                                        throw new Common.Exceptions.CannotRestoreObjectReferencedBydeletedObjectError(kvp.Key, id);
                                }
                            }
                        }
                    }
                }
            }

            // Restore objects
            ids.AddRange(extendIds);
            foreach (long id in ids) {
                ObjectType ot = DbSchema.GetObjectType(conn.GetObjectObjectType(id));
                // Check if there is at least one unique index:
                foreach (ObjectIndex idx in ot.Indexes) {
                    if (idx.Unique) {
                        GenericObject obj = LoadObject(id);
                        ValidateUniqueIndexes(obj);
                        break;
                    }
                }
                
                if (!conn.RestoreObject(DbSchema, id))
                    return false;
                restoredIds.Add(id);
            }

            return true;
        } // RestoreObjectsPrimitive
        #endregion
    }
}
