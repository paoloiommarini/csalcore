﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CsAlCore
{
    /// <summary>
    /// The CsAl main class
    /// </summary>
    public partial class CsAlCore : IDisposable
    {
        Mutex m_CacheMutex = new Mutex();
        /// <summary>
        /// Cache containing loaded objects
        /// </summary>
        Common.Utility.ObjectCache m_ObjectCache = null;
        /// <summary>
        /// Cache cotaining the SQL select for object types
        /// </summary>
        Common.Utility.ObjectCache m_ObjectSelectCache = new Common.Utility.ObjectCache(0, TimeSpan.Zero);
        /// <summary>
        /// Cache containing env variables
        /// </summary>
        Common.Utility.ObjectCache m_EnvVariablesCache = null;
        /// <summary>
        /// Cache containing objects object type (id/object type)
        /// </summary>
        Common.Utility.ObjectCache m_ObjectsObjectType = new Common.Utility.ObjectCache(100000, TimeSpan.Zero);

        Dictionary<long, string> m_ObjectTypesCache = null;

        /// <summary>
        /// The instance <see cref="Configuration"/>
        /// </summary>
        Configuration m_Config = null;

        #region Classes
        /// <summary>
        /// The CsAl version info
        /// </summary>
        public class VersionInfo
        {
            /// <summary>
            /// Create a new instance of <see cref="VersionInfo"/>
            /// </summary>
            /// <param name="version">The version</param>
            /// <param name="buildDate">The build date</param>
            public VersionInfo(string version, DateTime buildDate)
            {
                Version = version;
                BuildDate = buildDate;
            }

            /// <summary>
            /// The version
            /// </summary>
            public string Version { get; private set; }

            /// <summary>
            /// The build date
            /// </summary>
            public DateTime BuildDate { get; private set; }
        } // VersionInfo

        /// <summary>
        /// Differences between two schemas
        /// </summary>
        public class AttributesDifferences
        {
            /// <summary>
            /// Create a new instance of <see cref="AttributesDifferences"/>
            /// </summary>
            public AttributesDifferences()
            {
                RemovedObjectAttributes = new List<ObjectAttribute>();
                NewObjectAttributes = new List<ObjectAttribute>();
                ModifiedObjectAttributes = new List<ObjectAttribute>();
            }

            /// <summary>
            /// A list of removed <see cref="ObjectAttribute"/>
            /// </summary>
            public List<ObjectAttribute> RemovedObjectAttributes { get; set; }

            /// <summary>
            /// A list of new <see cref="ObjectAttribute"/>
            /// </summary>
            public List<ObjectAttribute> NewObjectAttributes { get; set; }

            /// <summary>
            /// A list of modified <see cref="ObjectAttribute"/>
            /// </summary>
            public List<ObjectAttribute> ModifiedObjectAttributes { get; set; }
        } // AttributesDifferences

        /// <summary>
        /// Differences between two schemas
        /// </summary>
        public class SchemaDifferences : AttributesDifferences
        {
            /// <summary>
            /// Create a new instance of <see cref="SchemaDifferences"/>
            /// </summary>
            public SchemaDifferences() :
                base()
            {
                RemovedObjectTypes = new List<ObjectType>();
                NewObjectTypes = new List<ObjectType>();
                RemovedObjectIndexes = new Dictionary<ObjectType, List<ObjectIndex>>();
                NewObjectIndexes = new Dictionary<ObjectType, List<ObjectIndex>>();
                ModifiedObjectIndexes = new Dictionary<ObjectType, List<ObjectIndex>>();
            }

            /// <summary>
            /// A list of removed <see cref="ObjectType"/>
            /// </summary>
            public List<ObjectType> RemovedObjectTypes { get; set; }

            /// <summary>
            /// A list of new <see cref="ObjectType"/>
            /// </summary>
            public List<ObjectType> NewObjectTypes { get; set; }

            /// <summary>
            /// A list of removed <see cref="ObjectIndex"/>
            /// </summary>
            public Dictionary<ObjectType, List<ObjectIndex>> RemovedObjectIndexes { get; set; }

            /// <summary>
            /// A list of new <see cref="ObjectIndex"/>
            /// </summary>
            public Dictionary<ObjectType, List<ObjectIndex>> NewObjectIndexes { get; set; }

            /// <summary>
            /// A list of modified <see cref="ObjectIndex"/>
            /// </summary>
            public Dictionary<ObjectType, List<ObjectIndex>> ModifiedObjectIndexes { get; set; }
        } // SchemaDifferences
        #endregion

        /// <summary>
        /// Supported databases
        /// </summary>
        public enum DbType
        {
            /// <summary>
            /// No database
            /// </summary>
            None,
            /// <summary>
            /// SQLite
            /// </summary>
            SQLite,
            /// <summary>
            /// SQL Server
            /// </summary>
            SQLServer,
            /// <summary>
            /// Oracle
            /// </summary>
            Oracle,
            /// <summary>
            /// MySQL
            /// </summary>
            MySQL,
            /// <summary>
            /// MariaDB
            /// </summary>
            MariaDB,
            /// <summary>
            /// PostgreSQL
            /// </summary>
            PostgreSQL,
        } // DbType

        /// <summary>
        /// Create a new instance of <see cref="CsAlCore"/> using the given <see cref="Configuration"/>
        /// </summary>
        /// <param name="config">The CsAl configuration</param>
        public CsAlCore(Configuration config)
        {
            if (config == null)
                throw new ArgumentNullException("config");
            if (string.IsNullOrEmpty(config.DbConnectionString))
                throw new ArgumentNullException("DbConnectionString");

            m_Config = config;
            if (!string.IsNullOrEmpty(config.LogPath)) {
                if (!Directory.Exists(config.LogPath))
                    Directory.CreateDirectory(config.LogPath);
                Log = new Common.Utility.LogFile(config.LogLevel, config.LogPath);
            }

            m_ObjectCache = new Common.Utility.ObjectCache(m_Config.ObjectCacheSize, TimeSpan.Zero);
            m_QueryCache = new Common.Utility.ObjectCache(m_Config.QueryCacheSize, TimeSpan.Zero);
            m_EnvVariablesCache = new Common.Utility.ObjectCache(1000, TimeSpan.Zero);
        }

        #region Public members
        /// <summary>
        /// The <see cref="DbType"/> 
        /// </summary>
        public DbType DatabaseType
        {
            get { return m_Config != null ? m_Config.DbType : DbType.None; }
        }

        /// <summary>
        /// The database connection string
        /// </summary>
        public string ConnectionString
        {
            get { return m_Config != null ? m_Config.DbConnectionString : string.Empty; }
        }

        /// <summary>
        /// The database engine version
        /// </summary>
        public Version DatabaseVersion
        {
            get {
                using (DB.DbConnectionBase conn = NewConnection()) {
                    return conn.GetVersion();
                }
            }
        }

        /// <summary>
        /// The <see cref="Common.Utility.LogFile"/>
        /// </summary>
        public Common.Utility.LogFile Log
        {
            get;
            private set;
        }

        /// <summary>
        /// The <see cref="Schema"/>
        /// </summary>
        public Schema DbSchema
        {
            get;
            private set;
        }

        /// <summary>
        /// A list of <see cref="Common.IdName"/> with object types
        /// </summary>
        public List<Common.IdName> ObjectTypes
        {
            get;
            private set;
        }

        /// <summary>
        /// The library <see cref="VersionInfo"/>
        /// </summary>
        public static VersionInfo Version
        {
            get {
                Version v = Assembly.GetExecutingAssembly().GetName().Version;
                DateTime dt = new DateTime(2017, 01, 01).AddDays(v.Build);

                return new VersionInfo(string.Format("{0}.{1}.{2}.{3}", v.Major, v.Minor, v.Build, v.Revision), dt);
            }
        }
        #endregion

        #region public operations
        /// <summary>
        /// Dispose the CsAl instance
        /// </summary>
        public void Dispose()
        {
            if (Log != null) {
                Log.Dispose();
                Log = null;
            }
        } // Dispose

        /// <summary>
        /// Initialize CsAl by loading the <see cref="Schema.Schema"/> from the database
        /// </summary>
        /// <returns>True on success</returns>
        public bool Open()
        {
            bool res = false;

            using (DB.DbConnectionBase conn = NewConnection()) {
                if (conn != null) {
                    WriteLog(Common.Utility.LogFile.LogLevels.Info, string.Format("Database opened, type: {0}, connection string: \"{1}\"", DatabaseType, ConnectionString));

                    // Load schema and object types from database
                    DbSchema = conn.GetSchema();
                    if (DbSchema != null) {
                        DbSchema.Validate();
                        LoadObjectTypes(conn);

                        // Load env variables
                        foreach (KeyValuePair<string, string> kvp in conn.GetEnvVariables()) {
                            EnvVariable.Names name;
                            if (Enum.TryParse(kvp.Key, out name)) {
                                m_EnvVariablesCache.AddOrUpdateValue(name.ToString(), new EnvVariable(name, kvp.Value));
                            } else {
                                // TODO invalid name
                                WriteLog(Common.Utility.LogFile.LogLevels.Warning, $"Invalid env variable name: { kvp.Key }");
                            }
                        }
                    }

                    res = true;
                } else {
                    WriteLog(Common.Utility.LogFile.LogLevels.Error, string.Format("Failed to open database, type: {0}, connection string: \"{1}\"", DatabaseType, ConnectionString));
                }
            }
            return res;
        } // Open

        /// <summary>
        /// Get an object type id
        /// </summary>
        /// <param name="name">The object type name</param>
        /// <returns>The object type Id or 0</returns>
        public long GetObjectTypeId(string name)
        {
            if (ObjectTypes != null) {
                foreach (Common.IdName idn in ObjectTypes) {
                    if (string.Compare(idn.Name, name, StringComparison.OrdinalIgnoreCase) == 0)
                        return idn.Id;
                }
            }
            return 0;
        } // GetObjectTypeId

        /// <summary>
        /// Write a log message
        /// </summary>
        /// <param name="level">The log level</param>
        /// <param name="message">The message</param>
        public void WriteLog(Common.Utility.LogFile.LogLevels level, string message)
        {
            WriteLog(level, "CsAl", message);
        } // WriteLog

        /// <summary>
        /// Write a log message
        /// </summary>
        /// <param name="level">The log level</param>
        /// <param name="component">The component name</param>
        /// <param name="message">The message</param>
        public void WriteLog(Common.Utility.LogFile.LogLevels level, string component, string message)
        {
            if (Log != null)
                Log.Write(level, component, message);
        } // WriteLog

        /// <summary>
        /// Get the next value for the given counter
        /// </summary>
        /// <param name="counterName">The counter name</param>
        /// <returns></returns>
        public long GetNextCounterValue(string counterName)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                return conn.GetNextCounterValue(counterName);
            }
        } // GetNextCounterValue

        /// <summary>
        /// Create a new <see cref="System.Data.Common.DbConnection"/> to execute raw SQL commands
        /// </summary>
        /// <remarks>
        /// Pay attention to differences in SQL language between databases.
        /// </remarks>
        /// <example>
        /// using (var conn = csal.CreateDbConnection()) {
        ///     using (var cmd = conn.CreateCommand()) {
        ///         cmd.CommandText = "update obj_table set a_field = 'value' where id = 101";
        ///         cmd.ExecuteNonQuery();
        ///     }
        /// }
        /// </example>
        /// <returns>A a new <see cref="System.Data.Common.DbConnection"/></returns>
        public System.Data.Common.DbConnection CreateDbConnection()
        {
            System.Data.Common.DbConnection res = null;
            DB.DbConnectionBase db = null;
            switch (DatabaseType) {
                case DbType.SQLite:
                    db = new DB.SQLiteConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.SQLServer:
                    db = new DB.SQLServerConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.Oracle:
                  res = new DB.OracleConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess).CreateConection();
                  break;
                case DbType.MySQL:
                    db = new DB.MySQLConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.MariaDB:
                    db = new DB.MariaDBConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.PostgreSQL:
                    db = new DB.PostgreSqlConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
            }

            if (db != null) {
                res = db.CreateConection();
                db.Dispose();
            }
            return res;
        } // CreateDbConnection
        #endregion

        #region Private operations
        private DB.DbConnectionBase NewConnection()
        {
            DB.DbConnectionBase res = null;
            switch (DatabaseType) {
                case DbType.SQLite:
                    res = new DB.SQLiteConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.SQLServer:
                    res = new DB.SQLServerConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.Oracle:
                  res = new DB.OracleConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                  break;
                case DbType.MySQL:
                    res = new DB.MySQLConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.MariaDB:
                    res = new DB.MariaDBConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
                case DbType.PostgreSQL:
                    res = new DB.PostgreSqlConnection(ConnectionString, Log, m_Config.MultiDatabaseAccess);
                    break;
            }

            if (res != null) {
                if (!res.Open()) {
                    res.Dispose();
                    res = null;
                } else 
                    res.SetCache(m_CacheMutex, m_ObjectCache, m_ObjectSelectCache, m_ObjectTypesCache, m_ObjectsObjectType);
            }

            return res;
        } // NewConnection

        private void LoadObjectTypes(DB.DbConnectionBase conn)
        {
            ObjectTypes = conn.GetObjectTypes(DbSchema);
            m_ObjectTypesCache = new Dictionary<long, string>();
            if (ObjectTypes != null) {
                foreach (Common.IdName idn in ObjectTypes)
                    m_ObjectTypesCache[idn.Id] = idn.Name;
            }
        } // LoadObjectTypes
        #endregion

        #region Env variables
        /// <summary>
        /// Get the binary data root path (from the <see cref="EnvVariable"/> <see cref="EnvVariable.Names.BinaryDataRoot"/>)
        /// </summary>
        /// <returns>The binary data root path</returns>
        public string GetBinaryDataRoot()
        {
            EnvVariable bsp = GetEnvVariable(EnvVariable.Names.BinaryDataRoot);
            return bsp?.GetValue<string>();
        } // GetBinaryDataRoot

        /// <summary>
        /// Get a list of all the env variables
        /// </summary>
        /// <returns></returns>
        public List<EnvVariable> GetEnvVariables()
        {
            List<EnvVariable> res = new List<EnvVariable>();
            foreach (EnvVariable.Names ev in Enum.GetValues(typeof(EnvVariable.Names)).Cast<EnvVariable.Names>().OrderBy(t => t.ToString())) {
                res.Add(new EnvVariable(ev, m_EnvVariablesCache.GetValue<EnvVariable>(ev.ToString()).StringValue));
            }
            return res;
        } // GetEnvVariables

        /// <summary>
        /// Get the given env variable value
        /// </summary>
        /// <param name="envVar">The env variable</param>
        /// <returns>The variable value</returns>
        public EnvVariable GetEnvVariable(EnvVariable.Names envVar)
        {
            return m_EnvVariablesCache.GetValue<EnvVariable>(envVar.ToString());
        } // GetEnvVariable

        /// <summary>
        /// Create/update the given env variable
        /// </summary>
        /// <param name="envVar">The variable</param>
        /// <param name="value">The variable value</param>
        /// <returns>True on success</returns>
        public bool SetEnvVariable(EnvVariable.Names envVar, object value)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                EnvVariable ev = new EnvVariable(envVar, value);
                bool res = conn.SetEnvVariable(ev.Name.ToString(), ev.DataType, ev.StringValue);
                if (res)
                    m_EnvVariablesCache.AddOrUpdateValue(envVar.ToString(), ev);
                return res;
            }
        } // SetEnvVariable
        #endregion
    }
}
