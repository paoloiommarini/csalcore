﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CsAlCore
{
    /// <summary>
    /// The CsAl configuration
    /// </summary>
    [XmlRoot("CsAlConfiguration")]
    public class Configuration
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Configuration"/> class
        /// </summary>
        public Configuration()
        {
            LogLevel = Common.Utility.LogFile.LogLevels.Info;
            ObjectCacheSize = 20000;
            QueryCacheSize = 20000;
        }

        /// <summary>
        /// The database type
        /// </summary>
        [XmlAttribute]
        public CsAlCore.DbType DbType { get; set; }

        /// <summary>
        /// The database connection string
        /// </summary>
        [XmlAttribute]
        public string DbConnectionString { get; set; }

        /// <summary>
        /// Set to true if there are multiple database access using CsAl
        /// </summary>
        [XmlAttribute]
        public bool MultiDatabaseAccess { get; set; }

        /// <summary>
        /// The path where log files are stored
        /// </summary>
        [XmlAttribute]
        public string LogPath { get; set; }

        /// <summary>
        /// The <see cref="Common.Utility.LogFile.LogLevel"/>. (default: Info)
        /// </summary>
        [XmlAttribute]
        public Common.Utility.LogFile.LogLevels LogLevel { get; set; }

        /// <summary>
        /// The number of objects to keep in the memory cache (default: 20000)
        /// </summary>
        [XmlAttribute]
        public uint ObjectCacheSize { get; set; }

        /// <summary>
        /// The number of query evaluated to SQL to keep in the memory cache (default: 20000)
        /// </summary>
        [XmlAttribute]
        public uint QueryCacheSize { get; set; }
    } // Configuration
}
