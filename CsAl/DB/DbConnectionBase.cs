﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace CsAlCore.DB
{
    public abstract class DbConnectionBase : IDisposable
    {
        const int MaxInValues = 500;

        public enum DateParts
        {
            Day,
            Month,
            Year,
            Hour,
            Minute,
            Second,
            MilliSecond
        }

        /// <summary>
        /// The epoch date time
        /// </summary>
        protected DateTime EpochTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        class CacheElement
        {
            public CacheElement(DateTime? lastModified, GenericObject obj)
            {
                LastModified = lastModified;
                Object = obj;
            }

            public DateTime? LastModified { get; set; }
            public GenericObject Object { get; set; }
        } // CacheElement

        Mutex m_ObjectCacheMutex = null;
        Common.Utility.ObjectCache m_ObjectCache = null;
        Common.Utility.ObjectCache m_ObjectSelectCache = null;
        Common.Utility.ObjectCache m_ObjectsObjectType = null;
        Dictionary<long, string> m_ObjectTypesCache = null;

        Version m_Version = null;

        public DbConnectionBase(string connectionString, Common.Utility.LogFile log, bool multipleAccess)
        {
            ConnectionString = connectionString;
            Log = log;
            MultipleAccess = multipleAccess;
            SupportsForeignKeys = true;
        }

        /// <summary>
        /// Defines if the database supports foreign keys
        /// </summary>
        public bool SupportsForeignKeys
        {
            get;
            protected set;
        }

        /// <summary>
        /// The database connection string
        /// </summary>
        public string ConnectionString
        {
            get;
            protected set;
        }

        /// <summary>
        /// The <see cref="Common.Utility.LogFile"/>
        /// </summary>
        public Common.Utility.LogFile Log
        {
            get;
            private set;
        }

        /// <summary>
        /// True if there are multiple access to the db
        /// </summary>
        public bool MultipleAccess { get; private set; }

        /// <summary>
        /// The internal object cache size (objects number)
        /// </summary>
        public uint ObjectCacheSize
        {
            get {
                return m_ObjectCache.Capacity;
            }
        }

        /// <summary>
        /// Minimum DateTime value managed in the database
        /// </summary>
        protected virtual DateTime MinDateTimeManaged
        {
            get { return DateTime.MinValue; }
        }

        /// <summary>
        /// The minimum date time value managed by the database
        /// </summary>
        public virtual DateTime MinimumDateTime
        {
            get { return DateTime.MinValue; }
        }

        public virtual Version MinimumVersion
        {
            get { return new Version(); }
        }

        public bool Open()
        {
            bool res = OpenPrimitive();
            if (res) {
                // Check db minimum versione
                if (GetVersion() < MinimumVersion)
                    throw new Common.Exceptions.MinimumDbVersionNotSatisfiedError(GetVersion().ToString(), MinimumVersion.ToString());
            }
            return res;
        } // Open

        public abstract System.Data.Common.DbConnection CreateConection();

        protected abstract bool OpenPrimitive();

        public abstract void Dispose();

        /// <summary>
        /// Set the caches (shared between different instances) of the database connection instance
        /// </summary>
        /// <param name="objsCacheMutex">The mutex for the object cache</param>
        /// <param name="objsCache">The object cache</param>
        /// <param name="objsSelectCache"></param>
        /// <param name="objectTypes"></param>
        /// <param name="objsObjectTypeCache"></param>
        public void SetCache(Mutex objsCacheMutex, Common.Utility.ObjectCache objsCache, Common.Utility.ObjectCache objsSelectCache, Dictionary<long, string> objectTypes, Common.Utility.ObjectCache objsObjectTypeCache)
        {
            m_ObjectCacheMutex = objsCacheMutex;
            m_ObjectCache = objsCache;
            m_ObjectSelectCache = objsSelectCache;
            m_ObjectTypesCache = objectTypes;
            m_ObjectsObjectType = objsObjectTypeCache;
        } // SetCache

        #region Transaction management
        public abstract void BeginTransaction();
        public abstract void CommitTransaction();
        public abstract void RollbackTransaction();
        #endregion

        #region Schema management
        public void CreateObjectType(Schema schema, ObjectType objectType)
        {
            List<string> scripts = new List<string>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("CREATE TABLE {0}(", objectType.TableName));
            int idx = 0;

            List<ObjectAttribute> mrefAttrs = new List<ObjectAttribute>();
            foreach (ObjectAttribute att in objectType.Attributes) {
                if (att.IsInherited && att.Type != ObjectAttribute.Types.Identity)
                    continue;

                if (att.Type == ObjectAttribute.Types.MultiReference && !att.ChildrenReference)
                    mrefAttrs.Add(att);

                List<string> ts = new List<string>();
                string aScript = GetAttributeScript(schema, att, out ts);
                if (!string.IsNullOrEmpty(aScript)) {
                    if (idx++ > 0)
                        sb.AppendLine(",");
                    sb.Append(string.Format("  {0}", aScript));
                }

                if (ts != null && ts.Count > 0)
                    scripts.AddRange(ts);
            }

            sb.AppendLine(string.Empty);
            if (schema.EnableForeignKeys && SupportsForeignKeys && !string.IsNullOrEmpty(objectType.Inherits)) {
                // Foreign key for inherited objects:
                ObjectType pOt = schema.GetObjectType(objectType.Inherits);
                if (pOt != null) {
                    sb.AppendLine(", ");
                    AddForeignKeyCreateScript(sb, pOt, objectType.GetAttribute(Schema.ObjectIdName));
                }
            }

            // Reference/BinaryData foreign keys:
            if (schema.EnableForeignKeys && SupportsForeignKeys) {
                foreach (ObjectAttribute att in objectType.Attributes) {
                    if (!att.IsInherited && (att.AttributeType.Type == ObjectAttribute.Types.Reference || att.AttributeType.Type == ObjectAttribute.Types.BinaryData)) {
                        sb.AppendLine(", ");
                        AddForeignKeyCreateScript(sb, schema.GetObjectType(att.RefObjectType), att);
                    }
                }
            }

            sb.AppendLine(")");
            ExecuteNonQuery(sb.ToString());

            // Indexes
            if (objectType.Indexes != null) {
                foreach (ObjectIndex oIdx in objectType.Indexes) {
                    CreateIndex(objectType, oIdx);
                }
            }

            foreach (string script in scripts)
                ExecuteNonQuery(script);

            // Multi reference tables
            foreach (ObjectAttribute mrefAt in mrefAttrs)
                CreateMultiReference(schema, mrefAt);
        } // CreateObjectType

        public virtual void AddObjectAttribute(Schema schema, ObjectAttribute at)
        {
            ObjectType ot = schema.GetObjectType(at.ObjectTypeName);
            if (at.IsTableField) {
                StringBuilder sb = new StringBuilder();

                List<string> ts = new List<string>();
                sb.Append($"alter table { ot.TableName } add column { GetAttributeScript(schema, at, out ts) }");
                ExecuteNonQuery(sb.ToString());

                foreach (string script in ts)
                    ExecuteNonQuery(script);
            } else if (at.AttributeType.Type == ObjectAttribute.Types.MultiReference) {
                if (!at.ChildrenReference) {
                    CreateMultiReference(schema, at);
                } else {
                    // Add parentRef attribute to referenced object type
                    ObjectAttribute pAt = new ObjectAttribute()
                    {
                        ObjectTypeName = at.RefObjectType,
                        Name = Schema.ParentObjElementName,
                        TypeName = ObjectAttribute.Types.Reference.ToString(),
                        RefObjectType = at.ObjectTypeName,
                        Required = true,
                        AttributeType = new Common.Schema.AttributeTypes.ChildOfAttrType(at.RefObjectType)
                    };
                    AddObjectAttribute(schema, pAt);
                }
            }
        } // AddObjectAttribute

        public virtual void DeleteObjectAttribute(Schema schema, string dataRootPath, ObjectAttribute at)
        {
            if (at.IsTableField) {
                ObjectType ot = schema.GetObjectType(at.ObjectTypeName);
                // For binary data delete all files
                if (at.Type == ObjectAttribute.Types.BinaryData) {
                    string path = $"{ dataRootPath }\\{ at.ObjectTypeName }.{ at.Name }";
                    if (Directory.Exists(path))
                        Directory.Delete(path, true);

                    ExecuteNonQuery($@"delete from sys_binarydata 
                                    where a_id in (select { at.ColumnName } from { ot.TableName })");
                }

                List<string> ts = new List<string>();
                ExecuteNonQuery($"alter table { ot.TableName } drop column { at.ColumnName }");
            } else if (at.AttributeType.Type == ObjectAttribute.Types.MultiReference) {
                if (!at.ChildrenReference) {
                    ExecuteNonQuery($"drop table { GetMultiReferenceTableName(at) }");
                } else {
                    ObjectType rOt = schema.GetObjectType(at.RefObjectType);
                    DeleteObjectAttribute(schema, dataRootPath, rOt.GetAttribute(Schema.ParentObjElementName));
                }
            }
        } // DeleteObjectAttribute

        public virtual void ModifyObjectAttribute(Schema schema, ObjectAttribute oldAt, ObjectAttribute newAt)
        {
            ObjectType ot = schema.GetObjectType(newAt.ObjectTypeName);
            if (newAt.IsTableField) {                
                StringBuilder sb = new StringBuilder();

                List<string> ts = new List<string>();
                sb.Append($"alter table { ot.TableName } modify column { GetAttributeScript(schema, newAt, out ts) }");
                ExecuteNonQuery(sb.ToString());

                foreach (string script in ts)
                    ExecuteNonQuery(script);
            } else if (newAt.AttributeType.Type == ObjectAttribute.Types.MultiReference && !newAt.ChildrenReference) {
                // Modify reference table (add/remove subattributes)
                CsAlCore.AttributesDifferences diff = CsAlCore.CompareAttributes(oldAt.SubAttributes, newAt.SubAttributes);
                foreach (ObjectAttribute oa in diff.RemovedObjectAttributes) {
                    ExecuteNonQuery($"alter table { GetMultiReferenceTableName(newAt) } drop column { oa.ColumnName }");
                }

                foreach (ObjectAttribute oa in diff.NewObjectAttributes) {
                    List<string> ts = new List<string>();
                    ExecuteNonQuery($"alter table { GetMultiReferenceTableName(newAt) } add column { GetAttributeScript(schema, oa, out ts) }");
                }

                foreach (ObjectAttribute oa in diff.ModifiedObjectAttributes) {
                    List<string> ts = new List<string>();
                    ExecuteNonQuery($"alter table { GetMultiReferenceTableName(newAt) } modify column { GetAttributeScript(schema, oa, out ts) }");
                }

            }
        } // ModifyObjectAttribute

        public virtual void DeleteObjectType(Schema schema, ObjectType objectType)
        {
            // Drop multireference tables:
            List<ObjectAttribute> refs = objectType.GetMultiReferences();
            foreach (ObjectAttribute at in refs) {
                ExecuteNonQuery($"drop table { GetMultiReferenceTableName(at) }");
            }

            ExecuteNonQuery($"drop table { objectType.TableName }");
        } // DeleteObjectType

        public string GetMultiReferenceTableName(ObjectAttribute attr)
        {
            //return string.Format("ref_{0}_{1}", objectType1, objectType2);
            return ($"ref_{ attr.ObjectTypeName }_{ attr.Name }");
        } // GetMultiReferenceTableName

        public abstract void AddForeignKeyCreateScript(StringBuilder sb, ObjectType parentObjectType, ObjectAttribute childAttribute);

        public virtual void CreateMultiReference(Schema schema, ObjectAttribute attribute)
        {
            string tName = GetMultiReferenceTableName(attribute);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("CREATE TABLE {0} (", tName);
            List<string> ts = new List<string>();

            ObjectAttribute objId = new ObjectAttribute()
            {
                Name = "ObjId",
                TypeName = "Integer",
                Required = true,
                AttributeType = new Common.Schema.AttributeTypes.IntegerAttrType()
            };
            string aScript = GetAttributeScript(schema, objId, out ts);
            sb.AppendFormat("{0}, ", aScript);

            ObjectAttribute refAt = new ObjectAttribute()
            {
                Name = "RefId",
                TypeName = "Integer",
                Required = true,
                AttributeType = new Common.Schema.AttributeTypes.IntegerAttrType()
            };
            aScript = GetAttributeScript(schema, refAt, out ts);
            sb.AppendFormat("{0}", aScript);

            // Sort field
            if (attribute.Sortable) {
                aScript = GetAttributeScript(schema, new ObjectAttribute()
                {
                    Name = "Order",
                    TypeName = "Integer",
                    Required = true,
                    AttributeType = new Common.Schema.AttributeTypes.IntegerAttrType()
                }, out ts);
                sb.AppendFormat(", {0}", aScript);
            }

            // Sub attributes:
            if (attribute.SubAttributes != null) {
                foreach (ObjectAttribute sAt in attribute.SubAttributes)
                    sb.AppendFormat(", {0}", GetAttributeScript(schema, sAt, out ts));
            }

            if (schema.EnableForeignKeys && SupportsForeignKeys) {
                sb.Append(", ");
                AddForeignKeyCreateScript(sb, schema.GetObjectType(attribute.ObjectTypeName), objId);
                sb.Append(", ");
                AddForeignKeyCreateScript(sb, schema.GetObjectType(attribute.RefObjectType), refAt);
            }

            sb.Append(")");

            ExecuteNonQuery(sb.ToString());

            // Indexes
            if (attribute.Sortable)
                ExecuteNonQuery(string.Format("CREATE unique index idx_{0} on {1}(a_objid, a_order)", GetUniqueName(), tName));
            else
                ExecuteNonQuery(string.Format("CREATE index idx_{0} on {1}(a_objid)", GetUniqueName(), tName));
            ExecuteNonQuery(string.Format("CREATE index idx_{0} on {1}(a_refid)", GetUniqueName(), tName));
        } // CreateMultiReference

        /// <summary>
        /// Create the given index
        /// </summary>
        /// <remarks>Nevere create a unique index. Unique indexes are managed by CsAlCore.</remarks>
        /// <param name="objectType">The <see cref="ObjectType"/></param>
        /// <param name="index">The <see cref="ObjectIndex"/></param>
        public virtual void CreateIndex(ObjectType objectType, ObjectIndex index)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("CREATE {0} index {1} on {2}(",
              /*index.Unique ? "unique" : */ string.Empty,
              index.GetDbName(objectType),
              objectType.TableName);

            int idx = 0;
            foreach (ObjectIndex.IndexAttribute at in index.Attributes) {
                // Skip inherited attributes.
                // An index mixing inherited and non inherited attributes will create two indexes.
                ObjectAttribute oAt = objectType.GetAttribute(at.Name);
                if (oAt.IsInherited)
                    continue;
                
                if (idx++ > 0)
                    sb.Append(", ");
                sb.Append(GetIndexAttributeScript(objectType, at));
            }
            sb.Append(")");

            if (idx > 0)
                ExecuteNonQuery(sb.ToString());
        } // CreateIndex

        public virtual void DeleteIndex(ObjectType objectType, ObjectIndex index)
        {
            ExecuteNonQuery($"drop index { index.GetDbName(objectType) }");
        } // DeleteIndex

        protected abstract string GetAttributeScript(Schema schema, ObjectAttribute attribute, out List<string> scripts);

        protected virtual string GetIndexAttributeScript(ObjectType objectType, ObjectIndex.IndexAttribute attribute)
        {
            ObjectAttribute at = objectType.GetAttribute(attribute.Name);
            if (at != null)
                return string.Format("{0} {1}", at.ColumnName, attribute.IsDescending ? "desc" : string.Empty).Trim();
            return string.Empty;
        } // GetIndexAttributeScript
        #endregion

        #region Env variables
        public Dictionary<string, string> GetEnvVariables()
        {
            Dictionary<string, string> res = new Dictionary<string, string>();
            using (System.Data.Common.DbDataReader dr = Select(@"select a_name, a_value
                                                                from sys_envvariables
                                                                order by a_name")) {
                while (dr.Read()) {
                    res[dr.GetString(0)] = dr.GetString(1);
                }
            }
            return res;
        } // GetEnvVariables

        /// <summary>
        /// Get an env variable value
        /// </summary>
        /// <param name="name">The variable name</param>
        /// <returns>The env variable value</returns>
        public string GetEnvVariable(string name)
        {            
            using (System.Data.Common.DbDataReader dr = Select($@"select a_value
                                                                 from sys_envvariables
                                                                 where a_name = { GetParameterName("name") }",
                                                                 new List<SqlParameterWithValue>()
                                                                 {
                                                                     new SqlParameterWithValue("name", name)
                                                                 })) {
                if (dr.Read()) {
                    return dr.GetString(0);
                }
            }
            return null;
        } // GetEnvVariable

        /// <summary>
        /// Set an env variable value
        /// </summary>
        /// <param name="name">The variable name</param>
        /// <param name="value">The value</param>
        /// <returns>True on success</returns>
        public bool SetEnvVariable(string name, EnvVariable.DataTypes dataType, string value)
        {
            int res = ExecuteNonQuery($@"update sys_envvariables
                                          set a_value = { GetParameterName("value") }, 
                                              a_modified = { GetParameterName("modified") }
                                          where a_name = { GetParameterName("name") }",
                                    new List<SqlParameterWithValue>()
                                    {
                                        new SqlParameterWithValue("name", name),
                                        new SqlParameterWithValue("value", value),
                                        new SqlParameterWithValue("modified", DateTime.UtcNow)
                                    });
            if (res == 0) {
                long newId = GetNextCounterValue("envVariables_id");
                res = ExecuteNonQuery($@"insert into sys_envvariables(a_id, a_name, a_datatype, a_value, a_created)
                                         values({ GetParameterName("id") }, { GetParameterName("name") }, { GetParameterName("dataType") }, { GetParameterName("value") }, { GetParameterName("created") })",
                                    new List<SqlParameterWithValue>()
                                    {
                                        new SqlParameterWithValue("id", newId),
                                        new SqlParameterWithValue("name", name),
                                        new SqlParameterWithValue("dataType", (int)dataType),
                                        new SqlParameterWithValue("value", value),
                                        new SqlParameterWithValue("created", DateTime.UtcNow)
                                    });
            }
            return res == 1;
        } // SetEnvVariable
        #endregion

        #region Binary data
        /// <summary>
        /// Get the folder to save a binary data. 
        /// Check number of files in subfolders, create a new folder if needed
        /// </summary>
        /// <param name="path">The target path</param>
        /// <param name="maxFilesInFolder">The maximum number of files to keep in a folder</param>
        /// <returns>The path to save the binary data</returns>
        private string GetBinaryDataFolder(string path, long maxFilesInFolder)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            // Check lock file in folder (for multiple access, also for multiple database access)
            int waited = 0;
            string lockFile = Path.Combine(path, "csal.lock");
            while (File.Exists(lockFile)) {
                Thread.Sleep(10);
                waited += 10;
                // Check max wait time
                if (waited >= 10000)
                    throw new Common.Exceptions.WaitOnLockFileTimeoutError(lockFile);
            }
            using (FileStream fs = File.Create(lockFile)) { }

            string res = string.Empty;
            try {
                int idx = 1;
                List<string> sf = Directory.GetDirectories(path).ToList();
                if (sf.Count > 0) {
                    sf.Sort();
                    string folder = sf.Last();
                    if (!folder.EndsWith("\\"))
                        folder = $"{ folder }\\";
                    string fName = Path.GetFileName(Path.GetDirectoryName(folder));
                    if (!int.TryParse(fName, out idx))
                        throw new Exception($"Wrong name for binary folder { fName }");
                    string[] files = Directory.GetFiles(folder);
                    if (files.Length >= maxFilesInFolder)
                        idx++;
                }
                res = Path.Combine(path, idx.ToString("0000000000"));
                if (!Directory.Exists(res))
                    Directory.CreateDirectory(res);
            } finally {
                File.Delete(lockFile);
            }

            return res;
        } // GetBinaryDataFolder

        /// <summary>
        /// Get the binary data with the given Id
        /// </summary>
        /// <param name="dataRootPath">The root path where binary data files are saved</param>
        /// <param name="id">The binary data Id</param>
        /// <param name="includeStream">If true the Stream attribute is set</param>
        /// <returns>A <see cref="BinaryDataValue"/></returns>
        public BinaryDataValue GetBinaryData(string dataRootPath, long id, bool includeStream)
        {
            if (string.IsNullOrEmpty(dataRootPath))
                throw new Common.Exceptions.MissingBinarySavePathError();

            BinaryDataValue res = null;
            using (System.Data.Common.DbDataReader dr = Select($@"select a_filename, a_filesize, a_mimetype, a_relativepath
                                                                  from sys_binarydata 
                                                                  where id = { GetParameterName("id") }",
                                                                  new List<SqlParameterWithValue>()
                                                                  {
                                                                      new SqlParameterWithValue("id", id)
                                                                  })) {
                if (dr.Read()) {
                    res = new BinaryDataValue()
                    {
                        Id = id,
                        FileName = dr.GetString(0),
                        Size = dr.GetInt64(1),
                        MimeType = dr.GetString(2)
                    };
                    if (includeStream)
                        res.Stream = new FileStream(Path.Combine(dataRootPath, dr.GetString(3), $"{ id }.bdv"), FileMode.Open);
                } else {
                    throw new Common.Exceptions.BinaryDataDoesNotExistsError(id);
                }
            }

            return res;
        } // GetBinaryData

        /// <summary>
        /// Save a binary data value
        /// </summary>
        /// <param name="dataRootPath">The root path where binary data files are saved</param>
        /// <param name="maxFilesInFolder">The maximum number of files to keep in a folder</param>
        /// <param name="value">The <see cref="BinaryDataValue"/></param>
        /// <param name="attr">The attribute for which we are saving the binary data</param>
        /// <returns>The id of the new <see cref="BinaryDataValue"/></returns>
        public long SaveBinaryData(string dataRootPath, long maxFilesInFolder, ObjectAttribute attr, BinaryDataValue value)
        {
            if (string.IsNullOrEmpty(dataRootPath))
                throw new Common.Exceptions.MissingBinarySavePathError();

            long res = GetNextCounterValue("binaryData_id");

            string relativePath = GetBinaryDataFolder($"{dataRootPath}\\{ attr.ObjectTypeName }.{ attr.Name }", maxFilesInFolder);
            string fullPath = Path.Combine(dataRootPath, relativePath, $"{ res }.bdv");
            relativePath = relativePath.Remove(0, dataRootPath.Length + 1);

            try {
                // Save the file:
                byte[] buffer = new byte[4096];
                using (FileStream fs = new FileStream(fullPath, FileMode.CreateNew)) {
                    using (BinaryWriter bw = new BinaryWriter(fs)) {
                        int totalRead = 0;
                        int read = 0;
                        do {
                            read = value.Stream.Read(buffer, 0, buffer.Length);
                            totalRead += read;
                            if (read > 0)
                                bw.Write(buffer, 0, read);
                        } while (read > 0);
                    }
                }

                string sql = $@"insert into sys_binarydata(a_id, a_filename, a_filesize, a_mimetype, a_relativepath, a_created)
                                values({ GetParameterName("id") }, { GetParameterName("fileName") }, { GetParameterName("fileSize") }, { GetParameterName("mimeType") }, { GetParameterName("relativePath") }, { GetParameterName("created") })";
                List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>()
            {
                new SqlParameterWithValue("id", res),
                new SqlParameterWithValue("fileName", value.FileName),
                new SqlParameterWithValue("fileSize", value.Size),
                new SqlParameterWithValue("mimeType", value.MimeType),
                new SqlParameterWithValue("relativePath", relativePath),
                new SqlParameterWithValue("created", DateTime.UtcNow),
            };

                if (ExecuteNonQuery(sql, pars) != 1) {
                    File.Delete(fullPath);
                    return 0;
                }
            } catch (Exception ex) {
                if (File.Exists(fullPath))
                    File.Delete(fullPath);
                throw ex;
            }

            return res;
        } // SaveBinaryData

        private bool DeleteBinaryData(string dataRootPath, long id)
        {
            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();

            sb.Append(@"select a_relativepath
                        from sys_binarydata 
                        where a_id = ");
            AddParameterWithValue(sb, pars, "id", id);
            using (System.Data.Common.DbDataReader dr = Select(sb.ToString(), pars)) {
                if (dr.Read()) {
                    string fullPath = Path.Combine(dataRootPath, dr.GetString(0), $"{ id }.bdv");
                    if (File.Exists(fullPath))
                        File.Delete(fullPath);

                    sb.Clear();
                    pars.Clear();
                    sb.Append(@"delete from sys_binarydata
                                where a_id = ");
                    AddParameterWithValue(sb, pars, "id", id);
                    if (ExecuteNonQuery(sb.ToString(), pars) != 1)
                        return false;
                } else
                    return false;
            }
            return true;
        } // DeleteBinaryData
        #endregion

        #region Object management
        public List<Common.IdName> GetIdNames(string sql, List<SqlParameterWithValue> pars)
        {
            List<Common.IdName> res = new List<Common.IdName>();

            using (System.Data.Common.DbDataReader dr = Select(sql, pars)) {
                while (dr.HasRows & dr.Read()) {
                    res.Add(new Common.IdName(dr.GetInt64(0), dr.FieldCount > 1 ? dr.GetString(1) : string.Empty));
                }
            }
            return res;
        } // GetIdName

        /// <summary>
        /// Load the <see cref="Schema"/> from the database
        /// </summary>
        /// <returns>The loaded <see cref="Schema"/></returns>
        public Schema GetSchema()
        {
            try {
                Schema res = null;
                long? schemaId = 0;
                using (System.Data.Common.DbDataReader dr = Select("select a_id, a_schema from sys_schema order by a_created desc", null)) {
                    if (dr.Read()) {                        
                        schemaId = DbToLong(dr[0]);
                        string xml = Encoding.UTF8.GetString(DbToBlob(dr[1]));
                        res = Common.Utility.Serialization.Deserialize<Schema>(xml);
                        if (res != null)
                            res.CompleteAttributeTypes();
                    }
                }

                // Files:
                res.Files = new List<Schema.SchemaFile>();
                using (System.Data.Common.DbDataReader dr = Select($"select a_name, a_file from sys_schemafile where a_{ Schema.ParentObjElementName } = { GetParameterName("schemaId") } order by a_{ Schema.ChildProgressiveAttributeName }", 
                                                                   new List<SqlParameterWithValue>()
                                                                   {
                                                                       new SqlParameterWithValue("schemaId", schemaId)
                                                                   })) {
                    while (dr.Read()) {
                        res.Files.Add(new Schema.SchemaFile()
                        {
                            FileName = dr.GetString(0),
                            Content = Encoding.UTF8.GetString(DbToBlob(dr[1]))
                        });
                    }
                }
                return res;
            } catch (Exception ex) {
                Debug.WriteLine(string.Format("GetSchema error: {0}", ex.Message));
            }
            return null;
        } // GetSchema

        /// <summary>
        /// Get the last modified date of a object
        /// </summary>
        /// <param name="objectId">The object id</param>
        /// <returns>The object last modified date or null</returns>
        public DateTime? GetObjectLastModifiedDate(long objectId)
        {
            List<DateTime?> res = GetObjectsLastModifiedDate(new List<long>() { objectId });
            if (res.Count > 0)
                return res[0];
            return null;
        } // GetObjectLastModifiedDate

        /// <summary>
        /// Get the last modified date of a list of objects
        /// </summary>
        /// <param name="objectIds">The object ids list</param>
        /// <returns>A list of last modified dates</returns>
        public List<DateTime?> GetObjectsLastModifiedDate(List<long> objectIds)
        {
            List<DateTime?> res = new List<DateTime?>();
            int pages = objectIds.Count / MaxInValues;
            if (objectIds.Count % MaxInValues != 0)
                pages++;
            for (int page = 0; page < pages; page++) {
                List<long> ids = objectIds.GetRange(page * MaxInValues,
                                                   page * MaxInValues + MaxInValues < objectIds.Count ? MaxInValues : objectIds.Count - page * MaxInValues);
                List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
                StringBuilder sb = new StringBuilder(@"select o.a_id, o.a_modified
                                                       from sys_object o 
                                                       where o.a_id ");
                AddIdsInCondition(sb, pars, ids);

                Dictionary<long, DateTime?> lmd = new Dictionary<long, DateTime?>();
                try {
                    using (System.Data.Common.DbDataReader dr = Select(sb.ToString(), pars)) {
                        while (dr.Read()) {
                            lmd[dr.GetInt64(0)] = DbToDateTime(dr[1]);
                        }
                    }

                    foreach (long id in objectIds) {
                        DateTime? dt = null;
                        if (lmd.TryGetValue(id, out dt))
                            res.Add(dt);
                        else
                            res.Add(DateTime.MinValue);
                    }
                } catch (Exception ex) {
                    Debug.WriteLine(string.Format("GetObjectsLastModifiedDate error: {0}", ex.Message));
                }
            }
            return res;
        } // GetObjectsLastModifiedDate

        /// <summary>
        /// Get the object type name of a object
        /// </summary>
        /// <param name="objectId">The object id</param>
        /// <returns>The object type name</returns>
        public string GetObjectObjectType(long objectId)
        {
            string[] res = GetObjectsObjectType(new List<long>() { objectId });
            if (res.Length > 0)
                return res[0];
            return string.Empty;
        } // GetObjectObjectType

        /// <summary>
        /// Get th object type of te given objects
        /// </summary>
        /// <param name="objectIds">The object ids</param>
        /// <returns>A list of the corresponding object types</returns>
        public string[] GetObjectsObjectType(List<long> objectIds)
        {
            if (objectIds == null)
                throw new ArgumentNullException("objectIds");

            List<long> tempIds = new List<long>();
            string[] res = new string[objectIds.Count];

            // Check the cache:
            for (int idx = 0; idx < objectIds.Count; idx++) {
                long id = objectIds[idx];
                string fromCache = m_ObjectsObjectType.GetValue<string>(id.ToString());
                if (fromCache != null)
                    res[idx] = fromCache;
                else
                    tempIds.Add(id);
            }

            int pages = tempIds.Count / MaxInValues;
            if (tempIds.Count % MaxInValues != 0)
                pages++;
            for (int page = 0; page < pages; page++) {
                List<long> ids = tempIds.GetRange(page * MaxInValues,
                                                  page * MaxInValues + MaxInValues < tempIds.Count ? MaxInValues : tempIds.Count - page * MaxInValues);
                List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
                StringBuilder sb = new StringBuilder(@"select o.a_id, o.a_objecttyperef
                                                       from sys_object o 
                                                       where o.a_id ");
                AddIdsInCondition(sb, pars, ids);

                Dictionary<long, string> ots = new Dictionary<long, string>();
                using (System.Data.Common.DbDataReader dr = Select(sb.ToString(), pars)) {
                    while (dr.Read()) {
                        long otId = dr.GetInt64(1);
                        string otName = string.Empty;
                        if (m_ObjectTypesCache.TryGetValue(otId, out otName))
                            ots[dr.GetInt64(0)] = otName;
                        else
                            throw new Common.Exceptions.UnknownObjectType(otId.ToString());
                    }
                }

                foreach (long id in ids) {
                    string otName = string.Empty;
                    if (ots.TryGetValue(id, out otName)) {
                        res[objectIds.IndexOf(id)] = otName;
                        m_ObjectsObjectType.AddOrUpdateValue(id.ToString(), otName);
                    } else
                        throw new Common.Exceptions.ObjectDoesNotExistsError(id);
                }
            }
            return res;
        } // GetObjectsObjectType

        /// <summary>
        /// Get the object deleted status of the given objects
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="objectType">The object type</param>
        /// <param name="objectIds">The object ids</param>
        /// <returns>A list of bools. True indicates that the object is softdeleted</returns>
        public List<bool> GetObjectsDeletedStatus(Schema schema, ObjectType objectType, List<long> objectIds)
        {
            if (schema == null)
                throw new ArgumentNullException("schema");

            if (objectType == null)
                objectType = schema.GetObjectType(Schema.ObjectObjectType);
            ObjectAttribute iAt = objectType.GetIdentity(true);
            ObjectType iOt = schema.GetObjectType(iAt.ObjectTypeName);
            ObjectAttribute deletedAt = objectType.GetAttribute(Schema.ObjectIsDeletedAttributeName);

            List<bool> res = new List<bool>();

            int pages = objectIds.Count / MaxInValues;
            if (objectIds.Count % MaxInValues != 0)
                pages++;
            for (int page = 0; page < pages; page++) {
                List<long> ids = objectIds.GetRange(page * MaxInValues,
                                                    page * MaxInValues + MaxInValues < objectIds.Count ? MaxInValues : objectIds.Count - page * MaxInValues);

                List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
                StringBuilder sb = new StringBuilder();
                if (deletedAt != null)
                    sb.Append($@"select o.{ iAt.ColumnName }, o.{ deletedAt.ColumnName }
                                 from { iOt.TableName } o 
                                 where o.{ iAt.ColumnName } ");
                else
                    sb.Append($@"select o.{ iAt.ColumnName }, { BooleanToDb(false) }
                                 from { iOt.TableName } o 
                                 where o.{ iAt.ColumnName } ");
                AddIdsInCondition(sb, pars, ids);

                Dictionary<long, bool> ods = new Dictionary<long, bool>();
                using (System.Data.Common.DbDataReader dr = Select(sb.ToString(), pars)) {
                    while (dr.Read()) {
                        ods[dr.GetInt64(0)] = DbToBoolean(dr[1]) != null ? DbToBoolean(dr[1]).Value : false;
                    }
                }

                foreach (long id in ids) {
                    bool dStatus = false;
                    if (ods.TryGetValue(id, out dStatus))
                        res.Add(dStatus);
                    else
                        throw new Common.Exceptions.ObjectDoesNotExistsError(id);
                }
            }
            return res;
        } // GetObjectsDeletedStatus

        /// <summary>
        /// Remove reference through the given <see cref="ObjectAttribute"/> to the given object ids 
        /// </summary>
        /// <param name="objectId">The object id to unreference</param>
        /// <param name="objectIds">The object ids of the object referencing objectId</param>
        /// <param name="refAttr">The reference attribute</param>
        /// <returns></returns>
        public bool UnreferenceObjects(Schema schema, long objectId, List<long> objectIds, ObjectAttribute refAttr)
        {
            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();

            if (refAttr.AttributeType.Type == ObjectAttribute.Types.MultiReference) {
                string tName = GetMultiReferenceTableName(refAttr);
                sb.Append($@"delete from { tName }
                             where a_objId ");
                AddIdsInCondition(sb, pars, objectIds);
                sb.Append(" and a_refId = ");
                AddParameterWithValue(sb, pars, "objId", objectId);
            } else if (refAttr.AttributeType.Type == ObjectAttribute.Types.Reference) {
                ObjectType ot = schema.GetObjectType(refAttr.ObjectTypeName);
                sb.Append($@"update { ot.TableName } 
                            set { refAttr.ColumnName }  = null
                            where a_id ");
                AddIdsInCondition(sb, pars, objectIds);
                sb.Append($" and { refAttr.ColumnName } = ");
                AddParameterWithValue(sb, pars, "objId", objectId);
            }
            return ExecuteNonQuery(sb.ToString(), pars) > 0;
        } // UnreferenceObjects

        private List<long> GetObjectChildren(Schema schema, long parentId, ObjectAttribute childrenReferenceAttr)
        {
            Dictionary<long, List<long>> children = GetoObjectsChildren(schema, new List<long>() { parentId }, childrenReferenceAttr);
            if (children != null && children.Count > 0)
                return children[parentId];
            return new List<long>();
        } // GetObjectChildren

        /// <summary>
        /// Get a list of Ids of children objects of the given objects through the given <see cref="ObjectAttribute"/>
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="ids">The object Ids</param>
        /// <param name="refAttr">The reference <see cref="ObjectAttribute"/> (must be a <see cref="ObjectAttribute.Types.Reference"/>)</param>
        /// <returns>A list of ids</returns>
        public Dictionary<long, List<long>> GetoObjectsChildren(Schema schema, List<long> ids, ObjectAttribute mcrefAt)
        {
            Dictionary<long, List<long>> res = new Dictionary<long, List<long>>();

            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();

            ObjectType mrefOt = schema.GetObjectType(mcrefAt.RefObjectType);
            sb.AppendFormat("select a_{2}, a_{0} from {1} where a_{2} ", Schema.ObjectIdName, mrefOt.TableName, Schema.ParentObjElementName.ToLower());
            AddIdsInCondition(sb, pars, ids);
            if (mcrefAt.Sortable)
                sb.AppendFormat(" order by a_{0}, a_{1}", Schema.ParentObjElementName, Schema.ChildProgressiveAttributeName.ToLower());

            using (System.Data.Common.DbDataReader drmRef = Select(sb.ToString(), pars)) {
                while (drmRef.Read()) {
                    long objId = drmRef.GetInt64(0);
                    long refId = drmRef.GetInt64(1);
                    List<long> tIds = null;
                    if (!res.TryGetValue(objId, out tIds)) {
                        tIds = new List<long>();
                        res[objId] = tIds;
                    }
                    tIds.Add(refId);
                }
            }
            return res;
        } // GetoObjectsChildren

        /// <summary>
        /// Get a list of Ids of objects referencing the given objects through the given <see cref="ObjectAttribute"/>
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="ids">The object Ids</param>
        /// <param name="refAttr">The reference <see cref="ObjectAttribute"/> (must be a <see cref="ObjectAttribute.Types.Reference"/> or <see cref="ObjectAttribute.Types.MultiReference"/>)</param>
        /// <param name="includeParents">If set to true also parent objects are returned</param>
        /// <param name="deleted">Filter returned objects by the isDeleted attribute (null returns both deleted and live objects)</param>
        /// <returns>A list of ids</returns>
        public Dictionary<long, List<long>> GetObjectsReferencing(Schema schema, List<long> ids, ObjectAttribute refAttr, bool includeParents, bool? deleted)
        {
            Dictionary<long, List<long>> res = new Dictionary<long, List<long>>();

            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();

            if (refAttr.AttributeType.Type == ObjectAttribute.Types.Reference) {
                if (refAttr.AttributeType is Common.Schema.AttributeTypes.ChildOfAttrType) {
                    // Children
                    ObjectType refOt = schema.GetObjectType(((Common.Schema.AttributeTypes.ChildOfAttrType)refAttr.AttributeType).RefObjectType);
                    sb.AppendFormat(@"select o.a_{0}, {2} 
                                      from {1} 
                                      join sys_object o
                                        on o.a_id = {1}.a_{0}
                                      where {2} ", Schema.ObjectIdName, refOt.TableName, refAttr.ColumnName);
                    AddIdsInCondition(sb, pars, ids);
                } else {
                    // Reference
                    ObjectType refOt = schema.GetObjectType(refAttr.ObjectTypeName);
                    sb.AppendFormat(@"select {1}.{2}, mo.a_{0} 
                                      from {1}
                                      join sys_object mo
                                        on mo.a_id = {1}.a_{0}
                                      join sys_object o
                                        on o.a_id = {1}.{2}
                                      where {1}.a_{0} ", Schema.ObjectIdName, refOt.TableName, refAttr.ColumnName);
                    AddIdsInCondition(sb, pars, ids);
                }
            } else if (refAttr.AttributeType.Type == ObjectAttribute.Types.MultiReference) {
                if (refAttr.ChildrenReference) {
                    // Parents
                    if (includeParents) {
                        ObjectType refOt = schema.GetObjectType(refAttr.RefObjectType);
                        sb.AppendFormat(@"select a_parentobjref, {0}.a_id
                                          from {0} 
                                          join sys_object mo
                                            on mo.a_id = {0}.a_id
                                          join sys_object o
                                            on o.a_id = a_parentobjref
                                          where o.a_isdeleted = {1} 
                                            and {0}.a_id ", refOt.TableName, BooleanToDb(false));
                        AddIdsInCondition(sb, pars, ids);
                    }
                } else {
                    // Multireference
                    string tName = GetMultiReferenceTableName(refAttr);
                    sb.AppendFormat(@"select a_objid, a_refid 
                                      from {0} 
                                      join sys_object o
                                        on o.a_id = {0}.a_objid
                                      where a_refid ", tName);
                    AddIdsInCondition(sb, pars, ids);
                }
            }

            if (sb.Length > 0) {
                // Add deleted condition
                if (deleted.HasValue) {
                    sb.Append($" and o.a_isdeleted = ");
                    AddBooleanValue(sb, deleted.Value);
                }

                using (System.Data.Common.DbDataReader dr = Select(sb.ToString(), pars)) {
                    while (dr.Read()) {
                        long id = dr.GetInt64(0);
                        long refId = dr.GetInt64(1);
                        List<long> tIds = null;
                        if (!res.TryGetValue(refId, out tIds)) {
                            tIds = new List<long>();
                            res[refId] = tIds;
                        }
                        tIds.Add(id);
                    }
                }
            }
            return res;
        } // GetObjectsReferencing

        /// <summary>
        /// Get the list of available object types
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <returns>A list of <see cref="Common.IdName"/> </returns>
        public List<Common.IdName> GetObjectTypes(Schema schema)
        {
            ObjectType ot = schema.GetObjectType("objectType");
            if (ot != null)
                return GetIdNames(string.Format("select a_id, a_name from {0} order by a_id", ot.TableName), null);
            return null;
        } // GetObjectTypes

        /// <summary>
        /// Insert an object in the database.
        /// This method inserts data in a sungle table (the <see cref="ObjectType.TableName"/>).
        /// </summary>
        /// <param name="dataRootPath">The data root path</param>
        /// <param name="maxFilesInFolder">The maximum files in a binary data folder</param>
        /// <param name="objectType">The object type</param>
        /// <param name="attributes">The attributes</param>
        /// <returns>The id of the new object</returns>
        public long InsertObject(string dataRootPath, long maxFilesInFolder, ObjectType objectType, List<GenericAttribute> attributes)
        {
            long objectId = 0;
            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
            sb.AppendFormat("insert into {0}(", objectType.TableName);

            int idx = 0;
            foreach (GenericAttribute at in attributes) {
                if (!at.Attribute.IsTableField)
                    continue;

                // Save binary data
                if (at.Attribute.Type == ObjectAttribute.Types.BinaryData) {
                    BinaryDataValue bdv = at.Value as BinaryDataValue;
                    if (bdv != null)
                        bdv.Id = SaveBinaryData(dataRootPath, maxFilesInFolder, at.Attribute, at.Value as BinaryDataValue);
                }

                if (at.Attribute.IsIdentity && (!at.Attribute.IsInherited || string.Compare(at.Attribute.InheritedFrom, objectType.Name, StringComparison.OrdinalIgnoreCase) == 0)) {
                    // ID:
                    objectId = GetNextCounterValue(string.Format("{0}_id", objectType.Name));
                    at.Value = objectId;
                }

                if (idx++ > 0)
                    sb.Append(", ");
                sb.Append(at.Attribute.ColumnName);
            }
            sb.Append(") Values(");

            idx = 0;
            foreach (GenericAttribute at in attributes) {
                if (!at.Attribute.IsTableField)
                    continue;

                if (idx > 0)
                    sb.Append(", ");

                if (at.Attribute.Type == ObjectAttribute.Types.BinaryData && at.Value != null)
                    AddParameterWithValue(sb, pars, string.Format("p{0}", idx++), (at.Value as BinaryDataValue).Id);
                else
                    AddParameterWithValue(sb, pars, string.Format("p{0}", idx++), at.Value);
            }
            sb.Append(")");

            ExecuteNonQuery(sb.ToString(), pars);

            if (objectId == 0) {
                foreach (GenericAttribute at in attributes) {
                    if (at.Attribute.IsIdentity) {
                        objectId = (long)at.Value;
                        break;
                    }
                }
            }

            foreach (GenericAttribute at in attributes) {
                if (at.Attribute.Type == ObjectAttribute.Types.MultiReference && !at.Attribute.ChildrenReference)
                    SaveMultiReference(objectType, objectId, at);
            }
            return objectId;
        } // InsertObject

        private bool SaveMultiReference(ObjectType objectType, long objectId, GenericAttribute attribute)
        {
            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();

            string tName = GetMultiReferenceTableName(attribute.Attribute);
            List<MultiReferenceValue> mrefValue = new List<MultiReferenceValue>();
            if (attribute.Value is List<MultiReferenceValue>)
                mrefValue = attribute.Value as List<MultiReferenceValue>;

            int idx = 0;
            foreach (MultiReferenceValue mRef in mrefValue) {
                sb.Clear();
                pars.Clear();

                long refId = mRef.RefId;
                sb.AppendFormat("insert into {0} (a_objid, a_refid", tName);
                if (attribute.Attribute.Sortable)
                    sb.Append(", a_order ");

                if (attribute.Attribute.SubAttributes != null)
                    foreach (ObjectAttribute sAt in attribute.Attribute.SubAttributes) {
                        sb.AppendFormat(", {0}", sAt.ColumnName);
                    }

                sb.Append(") ");

                sb.Append("values(");
                AddParameterWithValue(sb, pars, "id", objectId);
                sb.Append(", ");
                AddParameterWithValue(sb, pars, "refId", refId);

                if (attribute.Attribute.Sortable) {
                    sb.Append(", ");
                    AddParameterWithValue(sb, pars, "sOrder", idx++);
                }

                if (attribute.Attribute.SubAttributes != null) {
                    int sIdx = 0;
                    foreach (ObjectAttribute sAt in attribute.Attribute.SubAttributes) {
                        sb.Append(", ");
                        AddParameterWithValue(sb, pars, string.Format("sap{0}", sIdx), mRef.SubAttributes?.GetAttributeValue(sAt.Name));
                        sIdx++;
                    }
                }
                sb.Append(")");
                if (ExecuteNonQuery(sb.ToString(), pars) != 1) {
                    // TODO
                    return false;
                }
            }
            return true;
        } // SaveMultiReference

        private object GetAttributeValueFromDb(ObjectAttribute attribute, object dbValue)
        {
            if (dbValue is System.DBNull)
                dbValue = null;

            switch (attribute.Type) {
                case ObjectAttribute.Types.Date:
                case ObjectAttribute.Types.DateTime:
                case ObjectAttribute.Types.LastModifiedDateTime:
                case ObjectAttribute.Types.CreatedDateTime:
                    return DbToDateTime(dbValue);
                case ObjectAttribute.Types.TimeSpan:
                    return DbToTimeSpan(dbValue);
                case ObjectAttribute.Types.Boolean:
                    return DbToBoolean(dbValue);
                case ObjectAttribute.Types.Blob:
                    return DbToBlob(dbValue);
                case ObjectAttribute.Types.Identity:
                case ObjectAttribute.Types.Integer:
                case ObjectAttribute.Types.Enum:
                case ObjectAttribute.Types.Flags:
                case ObjectAttribute.Types.Reference:
                case ObjectAttribute.Types.BinaryData:
                    return DbToLong(dbValue);
            }
            return dbValue;
        } // GetAttributeValueFromDb

        #region Object update
        public bool UpdateObject(Schema schema, string dataRootPath, GenericObject obj, List<string> attributes)
        {
            List<ObjectType> objectTypes = schema.GetObjectTypeInheritance(obj.ObjectTypeName);
            foreach (ObjectType ot in objectTypes) {
                List<GenericAttribute> objAttributes = obj.GetAttributes(ot.Name);
                for (int i = 0; i < objAttributes.Count; i++) {
                    // Include only changed attributes
                    if (attributes != null && !attributes.Contains(objAttributes[i].Name) || !objAttributes[i].Changed)
                        objAttributes.RemoveAt(i--);
                }

                if (objAttributes != null && objAttributes.Count > 0) {
                    if (!UpdateObjectPrimitive(schema, dataRootPath, ot, obj.Id, objAttributes))
                        return false;
                }
            }
            return true;
        } // UpdateObject

        protected bool UpdateObjectPrimitive(Schema schema, string dataRootPath, ObjectType objectType, long id, List<GenericAttribute> attributes)
        {
            // TODO update binary data attributes (delete old bd, create the new one)
            foreach (GenericAttribute at in attributes) {
                if (at.Attribute.AttributeType.Type == ObjectAttribute.Types.BinaryData) {

                }
            }

            // Update main attributes
            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
            sb.AppendFormat("update {0} set ", objectType.TableName);

            int idx = 0;
            foreach (GenericAttribute at in attributes) {
                if (!at.Attribute.IsTableField || at.Attribute.IsIdentity)
                    continue;

                if (idx++ > 0)
                    sb.AppendLine(", ");
                sb.AppendFormat("{0} = ", at.Attribute.ColumnName);
                AddParameterWithValue(sb, pars, at.Attribute.Name, at.Value);
            }

            if (idx > 0) {
                sb.AppendFormat(" where {0} = ", objectType.GetIdentity(true).ColumnName);
                AddParameterWithValue(sb, pars, "id", id);

                if (ExecuteNonQuery(sb.ToString(), pars) != 1) {
                    // TODO Update failed
                    return false;
                }
            }

            foreach (GenericAttribute at in attributes) {
                // Update multireference:
                if (at.Attribute.Type == ObjectAttribute.Types.MultiReference && !at.Attribute.ChildrenReference && at.Changed) {
                    sb.Clear();
                    pars.Clear();

                    string tName = GetMultiReferenceTableName(at.Attribute);
                    sb.AppendFormat("delete from {0} where a_ObjId = ", tName);
                    AddParameterWithValue(sb, pars, "id", id);
                    ExecuteNonQuery(sb.ToString(), pars);

                    SaveMultiReference(objectType, id, at);
                }

                // Update childrenReference
                if (at.Attribute.Type == ObjectAttribute.Types.MultiReference && at.Attribute.ChildrenReference && at.Changed) {
                    sb.Clear();
                    pars.Clear();

                    List<long> mRefValue = new List<long>();
                    if (at.Value is List<long>)
                        mRefValue.AddRange(at.Value as List<long>);
                    else if (at.Value is List<MultiReferenceValue>) {
                        foreach (MultiReferenceValue mrv in at.Value as List<MultiReferenceValue>) {
                            // Update child object
                            if (!UpdateObject(schema, dataRootPath, mrv.RefObject, null)) {
                                // TODO Update failed
                                return false;
                            }
                            mRefValue.Add(mrv.RefId);
                        }
                    }

                    // Read current children:
                    ObjectType mrefOt = schema.GetObjectType(at.Attribute.RefObjectType);
                    List<long> mRefPrevValue = GetObjectChildren(schema, id, at.Attribute);

                    // Delete removed children
                    foreach (long refId in mRefPrevValue) {
                        if (!mRefValue.Contains(refId))
                            DeleteObjectPrimitive(schema, dataRootPath, mrefOt, refId, false);
                    }

                    // Update children sort order
                    if (at.Attribute.Sortable) {
                        int cIdx = 0;
                        foreach (int childId in mRefValue) {
                            sb.Clear();
                            pars.Clear();
                            sb.AppendFormat("update {0} set a_{1} = ", mrefOt.TableName, Schema.ParentObjElementName);
                            AddParameterWithValue(sb, pars, "parentId", id);
                            sb.Append(", ");
                            sb.AppendFormat("a_{0} = ", Schema.ChildProgressiveAttributeName);
                            AddParameterWithValue(sb, pars, "prog", cIdx++);
                            sb.AppendFormat(" where a_{0} = ", Schema.ObjectIdName);
                            AddParameterWithValue(sb, pars, "id", childId);

                            if (ExecuteNonQuery(sb.ToString(), pars) != 1) {
                                // TODO Update failed
                                return false;
                            }
                        }
                    }
                }
            }

            // Remove updated object from the cache
            m_ObjectCache.RemoveValue(GetObjectCacheKey(objectType, id));

            return true;
        } // UpdateObjectPrimitive
        #endregion

        #region Load object
        public GenericObject LoadObject(Schema schema, long id, ObjectType objectType = null, int maxDepth = 0, List<string> attributes = null)
        {
            HashSet<long> loadedIds = new HashSet<long>();
            ObjectType ot = objectType != null ? objectType : schema.GetObjectType(GetObjectObjectType(id));
            List<GenericObject> objs = LoadObjectsPrimitive(schema, ot, new List<long>() { id }, maxDepth, attributes, loadedIds);
            return objs.FirstOrDefault();
        } // LoadObject

        public List<GenericObject> LoadObjects(Schema schema, ObjectType objectType, List<long> ids)
        {
            HashSet<long> loadedIds = new HashSet<long>();
            return LoadObjectsPrimitive(schema, objectType, ids, -1, null, loadedIds);
        } // LoadObjects

        public List<GenericObject> LoadObjects(Schema schema, ObjectType objectType, List<long> ids, int maxDepth)
        {
            HashSet<long> loadedIds = new HashSet<long>();
            return LoadObjectsPrimitive(schema, objectType, ids, maxDepth, null, loadedIds);
        } // LoadObjects

        /// <summary>
        /// Load a list of objects of the same <see cref="ObjectType"/>
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="objectType">The objects <see cref="ObjectType"/></param>
        /// <param name="ids">The object ids</param>
        /// <param name="maxDepth">The max depth</param>
        /// <param name="attributes">The list of attributes to load</param>
        /// <param name="loadedIds">A List of loaded ids to avoid circular loading</param>
        /// <returns></returns>
        protected List<GenericObject> LoadObjectsPrimitive(Schema schema, ObjectType objectType, List<long> ids, int maxDepth,
                                                           List<string> attributes, HashSet<long> loadedIds)
        {
            GenericObject[] res = new GenericObject[ids.Count];

            // Check if the objects are soft deleted (this throws en exception for non existing objects)
            List<bool> deletedStatus = GetObjectsDeletedStatus(schema, objectType, ids);

            // Check the cache:            
            List<long> idsToLoad = new List<long>(ids.Count);
            foreach (long id in ids.Distinct()) {
                m_ObjectCacheMutex.WaitOne();
                string cacheKey = GetObjectCacheKey(objectType, id);
                CacheElement fromCache = m_ObjectCache.GetValue<CacheElement>(cacheKey);
                if (fromCache != null) {
                    // For multiple database access check last modified date
                    DateTime? lm = MultipleAccess ? GetObjectLastModifiedDate(id) : DateTime.MinValue;
                    if (!MultipleAccess || lm == fromCache.LastModified) {
                        AddObjectToResult(ids, res, new GenericObject(fromCache.Object));
                        if (!loadedIds.Contains(fromCache.Object.Id))
                            loadedIds.Add(fromCache.Object.Id);
                    } else {
                        m_ObjectCache.RemoveValue(cacheKey);
                        fromCache = null;
                    }
                }
                m_ObjectCacheMutex.ReleaseMutex();

                if (fromCache == null)
                    idsToLoad.Add(id);
            }

            // Load objects from database:
            List<ObjectAttribute> refAttrs = objectType.GetReferences();
            List<ObjectAttribute> mRefAttrs = objectType.GetMultiReferences();
            List<ObjectAttribute> mChildRefAttrs = objectType.GetChildrenReferences();

            if (attributes == null || attributes.Count == 0)
                attributes = objectType.AttributeNames;
            StringBuilder tempSb = new StringBuilder();
            AddObjectSelect(tempSb, schema, objectType, attributes);
            string baseSql = tempSb.ToString();

            int pages = idsToLoad.Count / MaxInValues;
            if (idsToLoad.Count % MaxInValues != 0)
                pages++;

            for (int page = 0; page < pages; page++) {
                List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
                List<long> pIds = idsToLoad.GetRange(page * MaxInValues,
                                                     page * MaxInValues + MaxInValues < idsToLoad.Count ? MaxInValues : idsToLoad.Count - page * MaxInValues);

                StringBuilder sb = new StringBuilder(baseSql);
                AddIdsInCondition(sb, pars, pIds);

                // Read main objects attributes
                using (System.Data.Common.DbDataReader dr = Select(sb.ToString(), pars)) {
                    while (dr.Read()) {
                        GenericObject obj = GenericObject.Create(objectType);
                        int idx = 0;
                        foreach (string atName in attributes) {
                            ObjectAttribute at = objectType.GetAttribute(atName);
                            if (at.IsTableField) {
                                object aValue = dr[idx++];
                                aValue = GetAttributeValueFromDb(at, aValue);
                                obj.SetAttributeValue(atName, aValue);
                            }
                        }

                        AddObjectToResult(ids, res, obj);
                        if (!loadedIds.Contains(obj.Id))
                            loadedIds.Add(obj.Id);
                    }
                }

                // Read multi references id values        
                if (mRefAttrs != null && mRefAttrs.Count > 0) {
                    foreach (ObjectAttribute mrefAt in mRefAttrs) {
                        if (!mrefAt.SystemManaged &&  !attributes.Contains(mrefAt.Name))
                            continue;

                        string tName = GetMultiReferenceTableName(mrefAt);
                        sb.Clear();
                        pars.Clear();

                        sb.Append("select a_ObjId, a_RefId");
                        if (mrefAt.SubAttributes != null) {
                            foreach (ObjectAttribute sAt in mrefAt.SubAttributes)
                                sb.AppendFormat(", {0}", sAt.ColumnName);
                        }
                        sb.AppendFormat(" from {0} where a_ObjId ", tName);
                        AddIdsInCondition(sb, pars, pIds);
                        if (mrefAt.Sortable)
                            sb.Append(" order by a_ObjId, a_Order");

                        using (System.Data.Common.DbDataReader drmRef = Select(sb.ToString(), pars)) {
                            while (drmRef.Read()) {
                                long objId = drmRef.GetInt64(0);
                                long refId = drmRef.GetInt64(1);
                                MultiReferenceValue mRefValue = new MultiReferenceValue(refId);
                                if (mrefAt.SubAttributes != null && mrefAt.SubAttributes.Count > 0) {
                                    int idx = 2;
                                    foreach (ObjectAttribute sAt in mrefAt.SubAttributes) {
                                        if (mRefValue.SubAttributes == null)
                                            mRefValue.SubAttributes = GenericObject.Create(string.Empty, mrefAt.SubAttributes);
                                        mRefValue.SubAttributes.SetAttributeValue(sAt.Name, GetAttributeValueFromDb(sAt, drmRef[idx++]));
                                    }
                                    mRefValue.SubAttributes.ResetChanged();
                                }

                                GenericObject obj = res[ids.IndexOf(objId)];
                                List<MultiReferenceValue> mrefValue = obj.GetAttribute(mrefAt.Name).Value as List<MultiReferenceValue>;
                                if (mrefValue != null)
                                    mrefValue.Add(mRefValue);
                                else
                                    obj.SetAttributeValue(mrefAt.Name, new List<MultiReferenceValue>() { mRefValue });
                            }
                        }
                    }
                }

                // Read children objects id values
                if (mChildRefAttrs != null && mChildRefAttrs.Count > 0) {
                    foreach (ObjectAttribute mcrefAt in mChildRefAttrs) {
                        if (!attributes.Contains(mcrefAt.Name))
                            continue;

                        Dictionary<long, List<long>> children = GetoObjectsChildren(schema, pIds, mcrefAt);
                        foreach (KeyValuePair<long, List<long>> kvp in children){
                            List<MultiReferenceValue> values = new List<MultiReferenceValue>();
                            foreach (long cId in kvp.Value)
                                values.Add(new MultiReferenceValue(cId));

                            GenericObject obj = res[ids.IndexOf(kvp.Key)];
                            List<MultiReferenceValue> mrefValue = obj.GetAttribute(mcrefAt.Name).Value as List<MultiReferenceValue>;
                            if (mrefValue != null)
                                mrefValue.AddRange(values);
                            else
                                obj.SetAttributeValue(mcrefAt.Name, values);
                        }
                    }
                }

                // Read binary data values
                foreach (ObjectAttribute bdAttr in objectType.Attributes) {
                    if (bdAttr.Type == ObjectAttribute.Types.BinaryData) {
                        if (!bdAttr.SystemManaged && !attributes.Contains(bdAttr.Name))
                            continue;

                        sb.Clear();
                        pars.Clear();

                        ObjectType bdAttrOt = schema.GetObjectType(bdAttr.ObjectTypeName);
                        sb.Append($@"select o.a_id, bd.a_id, bd.a_fileName, bd.a_fileSize, bd.a_mimeType
                                    from sys_binarydata bd
                                    join { bdAttrOt.TableName } o
                                      on o.{ bdAttr.ColumnName } = bd.a_id
                                    where o.a_id ");
                        AddIdsInCondition(sb, pars, pIds);
                        using (System.Data.Common.DbDataReader bdDr = Select(sb.ToString(), pars)) {
                            if (bdDr.Read()) {
                                BinaryDataValue bdv = new BinaryDataValue()
                                {
                                    Id = bdDr.GetInt64(1),
                                    FileName = bdDr.GetString(2),
                                    Size = bdDr.GetInt64(3),
                                    MimeType = bdDr.GetString(4)
                                };
                                GenericObject obj = res[ids.IndexOf(bdDr.GetInt64(0))];
                                obj.SetAttributeValue(bdAttr.Name, bdv);
                            }
                        }
                    }
                }
            }

            // Add the objects to the cache (only if complete)
            if (attributes.Count == objectType.AttributeNames.Count) {
                m_ObjectCacheMutex.WaitOne();
                foreach (GenericObject obj in res) {
                    if (obj != null) {
                        m_ObjectCache.AddOrUpdateValue<CacheElement>(GetObjectCacheKey(obj.ObjectType, obj.Id), new CacheElement(obj.LastModifiedDateTime, new GenericObject(obj)));
                        m_ObjectsObjectType.AddOrUpdateValue(obj.Id.ToString(), obj.ObjectTypeName);
                    }
                }
                m_ObjectCacheMutex.ReleaseMutex();
            }
            // Here the object is complete with all its main attributes (references and multireferences set to Ids)


            // Load referenced objects
            if (maxDepth < 0 || maxDepth > 0) {
                // Read referenced objects (including the parent object)
                if (refAttrs != null && refAttrs.Count > 0) {
                    foreach (ObjectAttribute refAt in refAttrs) {
                        if (!refAt.SystemManaged && !attributes.Contains(refAt.Name))
                            continue;

                        List<GenericObject> fatherObjs = new List<GenericObject>();
                        List<long> refIds = new List<long>();
                        foreach (GenericObject obj in res) {
                            GenericAttribute oAt = obj.GetAttribute(refAt.Name);
                            if (oAt != null && oAt.Value is long) {
                                ObjectType oAtOt = schema.GetObjectType(oAt.Attribute.RefObjectType);
                                if (!oAtOt.SystemObject) {
                                    long refId = (long)oAt.Value;
                                    if (refId != 0) {
                                        if (!loadedIds.Contains(refId)) {
                                            fatherObjs.Add(obj);
                                            refIds.Add(refId);
                                        }
                                    }
                                }
                            }
                        }

                        if (refIds.Count > 0) {
                            List<GenericObject> refObjs = LoadObjectsPrimitive(schema, schema.GetObjectType(refAt.RefObjectType), refIds, maxDepth - 1, null, loadedIds);
                            for (int i = 0; i < fatherObjs.Count; i++) {
                                if (refObjs[i] != null)
                                    fatherObjs[i].SetAttributeValue(refAt.Name, refObjs[i]);
                            }
                        }
                    }
                }

                // Read multi referenced (including children) objects
                if (mChildRefAttrs != null && mChildRefAttrs.Count > 0) {
                    if (mRefAttrs == null)
                        mRefAttrs = new List<ObjectAttribute>();
                    mRefAttrs.AddRange(mChildRefAttrs);
                }

                if (mRefAttrs != null && mRefAttrs.Count > 0) {
                    foreach (ObjectAttribute mrefAt in mRefAttrs) {
                        if (!mrefAt.SystemManaged && !attributes.Contains(mrefAt.Name))
                            continue;

                        // Collect all ids
                        List<long> mrefIds = new List<long>();
                        foreach (GenericObject obj in res) {
                            GenericAttribute oAt = obj.GetAttribute(mrefAt.Name);
                            if (oAt != null && oAt.Value is List<MultiReferenceValue>) {
                                foreach (MultiReferenceValue mrv in oAt.Value as List<MultiReferenceValue>)
                                    mrefIds.Add(mrv.RefId);
                            }
                            if (oAt != null && oAt.Value is List<long>)
                                mrefIds.AddRange(oAt.Value as List<long>);
                        }

                        // Read all multi referenced objects
                        if (mrefIds.Count > 0) {
                            mrefIds = mrefIds.Distinct().ToList();
                            List<GenericObject> mrefObjs = LoadObjectsPrimitive(schema, schema.GetObjectType(mrefAt.RefObjectType), mrefIds, maxDepth - 1, null, loadedIds);
                            Dictionary<long, GenericObject> mrefDict = new Dictionary<long, GenericObject>();
                            foreach (GenericObject mrefObj in mrefObjs)
                                mrefDict[mrefObj.Id] = mrefObj;

                            // Set object attributes
                            foreach (GenericObject obj in res) {
                                GenericAttribute oAt = obj.GetAttribute(mrefAt.Name);
                                if (oAt != null && oAt.Value is List<MultiReferenceValue>) {
                                    List<MultiReferenceValue> objmRefValue = oAt.Value as List<MultiReferenceValue>;
                                    foreach (MultiReferenceValue mrv in objmRefValue) {
                                        mrv.RefObject = mrefDict[mrv.RefId];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Reset changed flags
            foreach (GenericObject obj in res)
                obj.ResetChanged();

            return res.ToList();
        } // LoadObjectsPrimitive
        #endregion

        #region Delete object
        public bool SoftDeleteObject(Schema schema, string dataRootPath, long id)
        {
            ObjectType ot = schema.GetObjectType(GetObjectObjectType(id));
            return DeleteObjectPrimitive(schema, dataRootPath, ot, id, true);
        } // SoftDeleteObject

        public bool DeleteObject(Schema schema, string dataRootPath, long id)
        {
            ObjectType ot = schema.GetObjectType(GetObjectObjectType(id));
            return DeleteObjectPrimitive(schema, dataRootPath, ot, id, false);
        } // DeleteObject

        protected bool DeleteObjectPrimitive(Schema schema, string dataRootPath, ObjectType objectType, long id, bool softDelete)
        {
            List<ObjectAttribute> referecing = schema.GetReferencing(objectType.Name);

            // Delete children objects:
            foreach (ObjectAttribute at in objectType.GetChildrenReferences()) {
                List<long> cIds = GetObjectChildren(schema, id, at);
                ObjectType rOt = schema.GetObjectType(at.RefObjectType);
                foreach (long cId in cIds) {
                    if (!DeleteObjectPrimitive(schema, dataRootPath, rOt, cId, softDelete)) {
                        // TODO
                        return false;
                    }
                }
            }

            // Delete binary data:
            if (!softDelete) {
                foreach (ObjectAttribute at in objectType.Attributes) {
                    if (at.Type == ObjectAttribute.Types.BinaryData) {
                        StringBuilder sb = new StringBuilder();
                        List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();

                        ObjectType bdOt = null;
                        if (at.IsInherited)
                            bdOt = schema.GetObjectType(at.InheritedFrom);
                        else
                            bdOt = schema.GetObjectType(at.ObjectTypeName);
                        sb.Append($@"select { at.ColumnName } 
                                     from { bdOt.TableName } 
                                     where a_id = ");
                        AddParameterWithValue(sb, pars, "id", id);
                        using (System.Data.Common.DbDataReader dr = Select(sb.ToString(), pars)) {
                            if (dr.Read()) {
                                if (!DeleteBinaryData(dataRootPath, dr.GetInt64(0)))
                                    return false;
                            }
                        }

                    }
                }
            }

            if (softDelete) {
                // Soft delete
                StringBuilder sb = new StringBuilder();
                List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
                sb.AppendFormat("update sys_object set a_{0} = ", Schema.ObjectIsDeletedAttributeName);
                AddBooleanValue(sb, true);
                sb.Append(", a_deleted = ");
                AddDateTimeValue(sb, DateTime.UtcNow);
                sb.AppendFormat(" where a_{0} = ", Schema.ObjectIdName);
                AddParameterWithValue(sb, pars, "id", id);
                if (ExecuteNonQuery(sb.ToString(), pars) != 1)
                    return false;
            } else {
                // Hard delete
                List<ObjectType> objectTypes = schema.GetObjectTypeInheritance(objectType.Name);
                if (objectTypes != null)
                    objectTypes.Reverse();

                foreach (ObjectType ot in objectTypes) {
                    StringBuilder sb = new StringBuilder();
                    List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
                    sb.AppendFormat("delete from {0} where a_{1} = ", ot.TableName, Schema.ObjectIdName);
                    AddParameterWithValue(sb, pars, "id", id);
                    if (ExecuteNonQuery(sb.ToString(), pars) != 1)
                        return false;
                }
            }

            // Remove deleted object from the cache
            m_ObjectCache.RemoveValue(GetObjectCacheKey(objectType, id));
            m_ObjectsObjectType.RemoveValue(id.ToString());
            return true;
        } // DeleteObjectPrimitive
        #endregion

        #region Restore objects
        /// <summary>
        /// Restore the given soft deleted object 
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="id">The object id</param>
        /// <param name="objectType">The <see cref="ObjectType"/></param>
        /// <returns></returns>
        public bool RestoreObject(Schema schema, long id, ObjectType objectType = null)
        {
            ObjectType ot = objectType ?? schema.GetObjectType(GetObjectObjectType(id));
            return RestoreObjectPrimitive(schema, ot, id);
        } // RestoreObject

        protected bool RestoreObjectPrimitive(Schema schema, ObjectType objectType, long id)
        {
            StringBuilder sb = new StringBuilder();
            List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
            sb.AppendFormat("update sys_object set a_{0} = ", Schema.ObjectIsDeletedAttributeName);
            AddBooleanValue(sb, false);
            sb.Append(", a_deleted = null");
            sb.AppendFormat(" where a_{0} = ", Schema.ObjectIdName);
            AddParameterWithValue(sb, pars, "id", id);
            if (ExecuteNonQuery(sb.ToString(), pars) != 1)
                return false;

            return true;
        } // RestoreObjectPrimitive
        #endregion

        private void AddObjectToResult(List<long> ids, GenericObject[] objs, GenericObject obj)
        {
            int idx = 0;
            while ((idx = ids.IndexOf(obj.Id, idx)) != -1) {
                objs[idx++] = obj;
            }
        } // AddObjectToResult

        private void AddObjectSelect(StringBuilder sb, Schema schema, ObjectType ot, List<string> attributes)
        {
            // Get only used object types
            List<ObjectType> oTypes = new List<ObjectType>();
            bool complete = attributes != null && attributes.Count == ot.AttributeNames.Count;
            if (!complete) {
                foreach (string atName in attributes) {
                    ObjectAttribute at = ot.GetAttribute(atName);
                    if (at != null && at.IsTableField) {
                        if (at.IsInherited) {
                            ObjectType iOt = schema.GetObjectType(at.InheritedFrom);
                            if (iOt != null && !oTypes.Contains(iOt))
                                oTypes.Add(iOt);
                        } else {
                            if (!oTypes.Contains(ot))
                                oTypes.Add(ot);
                        }
                    }
                }
            } else {
                // Check the cache:
                string fromCache = m_ObjectSelectCache.GetValue<string>(ot.Name);
                if (!string.IsNullOrEmpty(fromCache)) {
                    sb.Append(fromCache);
                    return;
                }

                oTypes = schema.GetObjectTypeInheritance(ot.Name);
                attributes = ot.AttributeNames;
            }

            // Read attributes
            StringBuilder selectSb = new StringBuilder();
            selectSb.Append("select ");
            int idx = 0;
            foreach (string atName in attributes) {
                ObjectAttribute at = ot.GetAttribute(atName);
                if (at.IsTableField) {
                    ObjectType aOt = schema.GetObjectType(at.IsInherited ? at.InheritedFrom : ot.Name);
                    if (idx > 0)
                        selectSb.Append(", ");
                    selectSb.AppendFormat("{0}.{1}", aOt.TableName, at.ColumnName);

                    idx++;
                }
            }

            selectSb.AppendLine(string.Empty);
            selectSb.Append("from ");
            ObjectType previousOt = null;
            foreach (ObjectType objectType in oTypes) {
                if (previousOt == null) {
                    selectSb.AppendLine(objectType.TableName);
                } else {
                    selectSb.AppendLine(string.Format("join {0} on {0}.{1}={2}.{3}",
                                                objectType.TableName, objectType.GetIdentity(true).ColumnName,
                                                previousOt.TableName, previousOt.GetIdentity(true).ColumnName));
                }
                previousOt = objectType;
            }
            selectSb.AppendFormat("where {0}.{1} ", ot.TableName, ot.GetIdentity(true).ColumnName);

            if (complete)
                m_ObjectSelectCache.AddOrUpdateValue(ot.Name, selectSb.ToString());
            sb.Append(selectSb.ToString());
        } // AddObjectSelect
        #endregion

        #region Counters
        /// <summary>
        /// Get the next value for the given counter
        /// </summary>
        /// <param name="counterName">The counter name</param>
        /// <returns>The counter value</returns>
        public virtual long GetNextCounterValue(string counterName)
        {
            return GetNextCounterValuePrimitive(counterName);
        } // GetNextCounterValue

        /// <summary>
        /// Get the next value for the given counter.
        /// This is the default (slow) implementation that works on most of the supported databases.
        /// Override <see cref="GetNextCounterValue(string)"/> in the specific database implementation to use 
        /// a faster implementation.
        /// </summary>
        /// <param name="counterName">The counter name</param>
        /// <returns>The counter value</returns>
        protected long GetNextCounterValuePrimitive(string counterName)
        {
            long res = 0;
            // Always use a new connection for counters (for concurrent db access)
            using (DbConnectionBase conn = DbConnectionBase.Create(this)) {
                // Read the current value
                while (true) {
                    StringBuilder sb = new StringBuilder(@"select a_value from sys_counter where a_name = ");
                    List<SqlParameterWithValue> pars = new List<SqlParameterWithValue>();
                    AddParameterWithValue(sb, pars, "name", counterName);
                    using (System.Data.Common.DbDataReader dr = conn.Select(sb.ToString(), pars)) {
                        if (dr.Read())
                            res = dr.GetInt64(0) + 1;
                        else
                            res = -1;
                    }

                    if (res < 0) {
                        // Counter not found, try to create it
                        res = 1;
                        sb.Clear();
                        pars.Clear();

                        sb.Append(@"insert into sys_counter(a_name, a_value, a_modified) values(");
                        AddParameterWithValue(sb, pars, "name", counterName);
                        sb.Append(", ");
                        AddParameterWithValue(sb, pars, "value", res);
                        sb.Append(", ");
                        AddParameterWithValue(sb, pars, "modified", DateTime.UtcNow);
                        sb.Append(")");
                        try {
                            if (conn.ExecuteNonQuery(sb.ToString(), pars) == 1)
                                return res;
                        } catch (Exception ex) {
                            Log?.Write(Common.Utility.LogFile.LogLevels.Error, "DbConnection", $"GetNextCounterObjectValue Exception: { ex.Message }");
                        }
                    } else {
                        // Counter found, try to update it
                        sb.Clear();
                        pars.Clear();

                        sb.Append(@"update sys_counter set a_value = ");
                        AddParameterWithValue(sb, pars, "newValue", res);
                        sb.Append(@", a_modified = ");
                        AddParameterWithValue(sb, pars, "modified", DateTime.UtcNow);
                        sb.Append(@" where a_name = ");
                        AddParameterWithValue(sb, pars, "name", counterName);
                        sb.Append(@" and a_value = ");
                        AddParameterWithValue(sb, pars, "oldValue", res - 1);

                        if (conn.ExecuteNonQuery(sb.ToString(), pars) == 1)
                            return res;
                    }
                }
            }
        } // GetNextCounterValuePrimitive
        #endregion

        #region Data types 
        public virtual long? DbToLong(object value)
        {
            if (value == null)
                return null;
            return (long)value;
        } // DbToLong

        public abstract byte[] DbToBlob(object value);
        public abstract DateTime? DbToDateTime(object value);
        public abstract object DateTimeToDb(DateTime? value);

        public abstract TimeSpan? DbToTimeSpan(object value);
        public abstract object TimeSpanToDb(TimeSpan? value);

        public abstract bool? DbToBoolean(object value);
        public abstract object BooleanToDb(bool? value);

        public abstract void AddIntegerValue(StringBuilder sb, long value);
        public abstract void AddBooleanValue(StringBuilder sb, bool value);
        public abstract void AddStringValue(StringBuilder sb, string value);
        public abstract void AddDateTimeValue(StringBuilder sb, DateTime value);
        public abstract void AddTimeSpanValue(StringBuilder sb, TimeSpan value);
        #endregion

        #region Functions
        public virtual void AddBitAnd(StringBuilder sb, string leftExpr, string rightExpr)
        {
            sb.AppendFormat("{0} & {1}", leftExpr, rightExpr);
        } // AddBitAnd

        public virtual void AddBitOr(StringBuilder sb, string leftExpr, string rightExpr)
        {
            sb.AppendFormat("{0} | {1}", leftExpr, rightExpr);
        } // AddBitOr

        public virtual void AddSqrt(StringBuilder sb, string expr)
        {
            sb.AppendFormat("SQRT({0})", expr);
        } // AddSqrt

        public virtual void AddRound(StringBuilder sb, string expr, string dec)
        {
            sb.AppendFormat("ROUND({0}, {1})", expr, dec);
        } // AddRound

        public virtual void AddSin(StringBuilder sb, string expr)
        {
            sb.AppendFormat("SIN({0})", expr);
        } // AddSin

        public virtual void AddCos(StringBuilder sb, string expr)
        {
            sb.AppendFormat("COS({0})", expr);
        } // AddCos

        public virtual void AddTan(StringBuilder sb, string expr)
        {
            sb.AppendFormat("TAN({0})", expr);
        } // AddTan

        public virtual void AddPower(StringBuilder sb, string expr, string exp)
        {
            sb.AppendFormat("POWER({0}, {1})", expr, exp);
        } // AddPower

        public virtual void AddStringConcat(StringBuilder sb, string leftExpr, string rightExpr)
        {
            sb.AppendFormat("{0} || {1}", leftExpr, rightExpr);
        } // AddStringConcat

        public virtual void AddOffsetLimit(StringBuilder sb, int offset, int limit)
        {
            if (limit > 0)
                sb.AppendFormat(" LIMIT {0}", limit);
            if (offset > 0)
                sb.AppendFormat(" OFFSET {0}", offset);
        } // AddOffsetLimit

        public abstract void AddNot(StringBuilder sb, string expr);
        public abstract void AddContains(StringBuilder sb, string target, string value);
        public abstract void AddStartsWith(StringBuilder sb, string target, string value);
        public abstract void AddEndsWith(StringBuilder sb, string target, string value);
        public abstract void AddDateAdd(StringBuilder sb, DateParts part, string target, int value);
        public abstract void AddDateDiff(StringBuilder sb, DateParts part, string date1, string date2);
        public abstract void AddDateYear(StringBuilder sb, string date);
        public abstract void AddDateMonth(StringBuilder sb, string date);
        public abstract void AddDateDay(StringBuilder sb, string date);
        public abstract void AddDateHour(StringBuilder sb, string date);
        public abstract void AddDateMinute(StringBuilder sb, string date);
        public abstract void AddDateSecond(StringBuilder sb, string date);
        public abstract void AddSubString(StringBuilder sb, string target, string start, string length);
        public abstract void AddLength(StringBuilder sb, string target);

        public virtual void AddNullValue(StringBuilder sb, string target, string value)
        {
            sb.Append($"coalesce({ target }, { value })");
        } // AddNullValue

        public virtual void AddIsNull(StringBuilder sb, string target)
        {
            sb.Append($"{ target } IS NULL");
        } // AddIsNull


        public virtual Version GetVersion()
        {
            if (m_Version == null)
                m_Version = GetVersionPrimitive();
            return m_Version;
        }

        protected abstract Version GetVersionPrimitive();
        #endregion

        #region Sql execution
        public abstract string GetParameterName(string baseName);

        public virtual void AddParameterWithValue(StringBuilder sb, List<SqlParameterWithValue> pars, string parName, object value)
        {
            sb.AppendFormat(GetParameterName(parName), parName);

            if (value is GenericObject) {
                GenericObject gObj = value as GenericObject;
                pars.Add(new SqlParameterWithValue(parName, gObj.Id));
            } else if (value is DateTime) {
                pars.Add(new SqlParameterWithValue(parName, DateTimeToDb((DateTime)value)));
            } else if (value is TimeSpan) {
                pars.Add(new SqlParameterWithValue(parName, TimeSpanToDb((TimeSpan)value)));
            } else if (value is bool) {
                pars.Add(new SqlParameterWithValue(parName, BooleanToDb((bool)value)));
            } else if (value != null && value.GetType().IsEnum) {
                pars.Add(new SqlParameterWithValue(parName, (long)value));
            } else
                pars.Add(new SqlParameterWithValue(parName, value));
        } // AddParameterWithValue

        public int ExecuteNonQuery(string sql)
        {
            return ExecuteNonQuery(sql, new List<SqlParameterWithValue>());
        }

        public int ExecuteNonQuery(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int res = ExecuteNonQueryPrimitive(sql, pars);
            sw.Stop();
            LogSQL(sql, pars, sw.Elapsed);
            return res;
        } // ExecuteNonQuery

        protected abstract int ExecuteNonQueryPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars);

        /// <summary>
        /// Execute the given select statement
        /// </summary>
        /// <param name="sql">The SQL statement</param>
        /// <param name="pars">The statement parameters</param>
        /// <returns>The resulting <see cref="System.Data.Common.DbDataReader"/></returns>
        public System.Data.Common.DbDataReader Select(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            System.Data.Common.DbDataReader res = SelectPrimitive(sql, pars);
            sw.Stop();
            LogSQL(sql, pars, sw.Elapsed);
            return res;
        } // Select

        protected abstract System.Data.Common.DbDataReader SelectPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars);

        /// <summary>
        /// Execute the given select statement
        /// </summary>
        /// <param name="sql">The SQL statement</param>
        /// <returns>The resulting <see cref="System.Data.Common.DbDataReader"/></returns>
        public System.Data.Common.DbDataReader Select(string sql)
        {
            return Select(sql, null);
        } // Select

        protected void AddIdsInCondition(StringBuilder sb, List<SqlParameterWithValue> pars, List<long> values)
        {
            sb.Append("in (");
            int idx = 0;
            foreach (long id in values) {
                if (idx > 0)
                    sb.Append(", ");
                AddParameterWithValue(sb, pars, string.Format("id{0}", idx), id);
                idx++;
            }
            sb.Append(")");
        } // AddIdsInCondition
        #endregion

        /// <summary>
        /// Writes a SQL statement and all its parameters to the log file
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="pars"></param>
        /// <param name="executionTime">The SQL execution time</param>
        protected void LogSQL(string sql, IEnumerable<SqlParameterWithValue> pars, TimeSpan executionTime)
        {
            if (Log != null && Log.LogLevel <= Common.Utility.LogFile.LogLevels.Debug) {
                Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", sql);
                if (pars != null) {
                    int idx = 0;
                    foreach (SqlParameterWithValue p in pars) {
                        if (idx++ >= 20) {
                            Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", "Par ... = ...");
                            break;
                        }

                        string pValue = string.Empty;
                        if (p.Value == null)
                            pValue = "NULL";
                        else if (p.Value is string) {
                            pValue = p.Value as string;
                            if (pValue.Length > 256)
                                pValue = string.Format("'{0}...'", pValue.Substring(0, 256));
                            else
                                pValue = string.Format("'{0}'", pValue);
                        } else if (p.Value is byte[]) {
                            byte[] bytes = (byte[])p.Value;
                            pValue = string.Format("byte[{0}]", bytes.Length);
                        } else {
                            pValue = p.Value.ToString();
                        }
                        Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", string.Format("Par {0} = {1}", p.Name, pValue));
                    }
                }

                if (executionTime.TotalSeconds >= 1)
                    Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", string.Format("Execution time: {0}", executionTime.ToString()));
                else
                    Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", string.Format("Execution time: {0}ms", executionTime.TotalMilliseconds));
            }
        } // LogSQL

        protected string GetObjectCacheKey(ObjectType ot, long id)
        {
            return $"{ot.Name}_{id.ToString()}";
        } // GetObjectCacheKey

        /// <summary>
        /// Get a unique string name
        /// </summary>
        /// <returns></returns>
        protected virtual string GetUniqueName()
        {
            Common.Utility.ShortGuid sg = Guid.NewGuid();
            return sg.Value.Replace("-", "_");
        } // GetUniqueName

        /// <summary>
        /// Create a new <see cref="DbConnectionBase"/> from an existing one
        /// </summary>
        /// <param name="conn">The db connection</param>
        /// <returns></returns>
        protected static DbConnectionBase Create(DbConnectionBase conn)
        {
            if (conn == null)
                return null;

            System.Reflection.ConstructorInfo cs = conn.GetType().GetConstructor(new Type[] { typeof(string), typeof(Common.Utility.LogFile), typeof(bool) });
            if (cs != null) {
                DbConnectionBase res = cs.Invoke(new object[] { conn.ConnectionString, conn.Log, conn.MultipleAccess }) as DbConnectionBase;
                if (!res.OpenPrimitive()) {
                    res.Dispose();
                    res = null;
                } else 
                    res.SetCache(conn.m_ObjectCacheMutex, conn.m_ObjectCache, conn.m_ObjectSelectCache, conn.m_ObjectTypesCache, conn.m_ObjectsObjectType);
                return res;
            }

            return null;
        } // Create
    }
}
