﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCore.DB
{
    class SQLServerConnection : DbConnectionBase
    {
        string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fff";
        SqlConnection m_Conn = null;
        SqlTransaction m_Trans = null;

        public SQLServerConnection(string connectionString, Common.Utility.LogFile log, bool multipleAccess) :
          base(connectionString, log, multipleAccess)
        {

        }

        public override DateTime MinimumDateTime
        {
            get {
                return new DateTime(1753, 1, 1);
            }
        }

        public override Version MinimumVersion
        {
            get { return new Version(11, 0); }
        }

        public override DbConnection CreateConection()
        {
            SqlConnection res = new SqlConnection(ConnectionString);
            res.Open();
            return res;
        }

        protected override bool OpenPrimitive()
        {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                m_Conn = (SqlConnection)CreateConection();
                sw.Stop();
                if (Log != null)
                    Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", string.Format("Connection open (time {0}): {1}", sw.Elapsed.ToString(), ConnectionString));
            } catch (Exception ex) {
                Log?.Write(Common.Utility.LogFile.LogLevels.Error, "DbConnection", $"Error opening database: { ex.Message }");
                return false;
            }
            return true;
        } // OpenPrimitive

        public override void Dispose()
        {
            if (m_Conn != null) {
                RollbackTransaction();
                m_Conn.Dispose();
                m_Conn = null;
            }
        } // Dispose

        #region Transaction management
        public override void BeginTransaction()
        {
            m_Trans = m_Conn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        } // StartTransaction

        public override void CommitTransaction()
        {
            if (m_Trans != null) {
                m_Trans.Commit();
                m_Trans.Dispose();
                m_Trans = null;
            }
        } // CommitTransaction

        public override void RollbackTransaction()
        {
            if (m_Trans != null) {
                m_Trans.Rollback();
                m_Trans.Dispose();
                m_Trans = null;
            }
        } // RollbackTransaction
        #endregion

        #region Schema upgrade
        public override void AddForeignKeyCreateScript(StringBuilder sb, ObjectType parentObjectType, ObjectAttribute childAttribute)
        {
            sb.AppendFormat("CONSTRAINT FK_{1} FOREIGN KEY({2}) REFERENCES {0}(a_Id)", parentObjectType.TableName, GetUniqueName(), childAttribute.ColumnName);
        } // CreateForeignKey

        protected override string GetAttributeScript(Schema schema, ObjectAttribute attribute, out List<string> scripts)
        {
            scripts = new List<string>();

            string req = attribute.Required ? "NOT NULL" : string.Empty;
            string defaultValue = String.Empty;

            switch (attribute.Type) {
                case ObjectAttribute.Types.Identity:
                    return string.Format("{0} BIGINT PRIMARY KEY NOT NULL", attribute.ColumnName);
                case ObjectAttribute.Types.String:
                case ObjectAttribute.Types.Password:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT '{ attribute.AttributeType.GetDefaultValue() }'";
                    if (attribute.MaxLength > 0)
                        return string.Format("{0} VARCHAR({1}) {2} {3}", attribute.ColumnName, attribute.MaxLength, req, defaultValue);
                    else
                        return string.Format("{0} TEXT {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Boolean:
                    if (attribute.HasDefaultValue) {
                        if ((bool)(attribute.AttributeType.GetDefaultValue()) == true)
                            defaultValue = $"DEFAULT 1";
                        else
                            defaultValue = $"DEFAULT 0";
                    }
                    return string.Format("{0} INT {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Date:
                case ObjectAttribute.Types.DateTime:
                case ObjectAttribute.Types.CreatedDateTime:
                case ObjectAttribute.Types.LastModifiedDateTime:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { DateTimeToDb((DateTime?)attribute.AttributeType.GetDefaultValue()) }";
                    return string.Format("{0} DATETIME {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Integer:
                case ObjectAttribute.Types.Enum:
                case ObjectAttribute.Types.TimeSpan:
                case ObjectAttribute.Types.Reference:
                case ObjectAttribute.Types.BinaryData:
                case ObjectAttribute.Types.Flags:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { attribute.AttributeType.GetDefaultValue() }";
                    return string.Format("{0} BIGINT {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Real:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { attribute.AttributeType.GetDefaultValue() }";
                    return string.Format("{0} REAL {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Blob:
                    if (attribute.MaxLength > 0 && attribute.MaxLength <= 8000)
                        return string.Format("{0} VARBINARY({1}) {2}", attribute.ColumnName, attribute.MaxLength, req);
                    else
                        return string.Format("{0} VARBINARY(max) {1}", attribute.ColumnName, req);
            }
            return string.Empty;
        } // GetAttributeScript
        #endregion

        #region Sql execution
        public override void AddIntegerValue(StringBuilder sb, long value)
        {
            sb.Append(value.ToString(CultureInfo.InvariantCulture));
        } // AddIntegerValue

        public override void AddBooleanValue(StringBuilder sb, bool value)
        {
            sb.Append(value ? "1" : "0");
        } // AddBooleanValue

        public override void AddStringValue(StringBuilder sb, string value)
        {
            sb.AppendFormat("'{0}'", value.Replace("'", "''"));
        } // AddStringValue

        public override void AddDateTimeValue(StringBuilder sb, DateTime value)
        {
            sb.AppendFormat("'{0}'", DateTimeToDb(value));
        } // AddDateTimeValue

        public override void AddTimeSpanValue(StringBuilder sb, TimeSpan value)
        {
            sb.AppendFormat("{0}", TimeSpanToDb(value));
        } // AddTimeSpanValue

        public override DateTime? DbToDateTime(object value)
        {
            if (value is DateTime)
                return (DateTime)value;
            return null;
        } // DbToDateTime

        public override object DateTimeToDb(DateTime? value)
        {
            if (value == null)
                return null;
            return value.Value.ToString(DateTimeFormat);
        } // DateTimeToDb

        public override TimeSpan? DbToTimeSpan(object value)
        {
            if (value is long) {
                long mSecs = (long)value;
                return TimeSpan.FromMilliseconds(mSecs);
            }
            return null;
        } // DbToTimeSpan

        public override object TimeSpanToDb(TimeSpan? value)
        {
            if (value == null)
                return null;
            return (long)Math.Round(value.Value.TotalMilliseconds);
        } // TimeSpanToDb

        public override bool? DbToBoolean(object value)
        {
            if (value == null || value == DBNull.Value)
                return null;
            if (value is long)
                return (long)value == 1;
            return (int)value == 1;
        } // GetBoolean

        public override object BooleanToDb(bool? value)
        {
            if (value.HasValue)
                return value.Value ? 1 : 0;
            return null;
        } // BooleanToDb

        public override byte[] DbToBlob(object value)
        {
            if (value is byte[])
                return (byte[])value;
            return null;
        } // GetBLob

        public override void AddNot(StringBuilder sb, string expr)
        {
            sb.AppendFormat("NOT({0})", expr);
        } // AddNot

        public override void AddContains(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '%{1}%'", target, value);
        } // AddContains

        public override void AddStartsWith(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '{1}%'", target, value);
        } // AddStartsWith

        public override void AddEndsWith(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '%{1}'", target, value);
        } // AddEndsWith

        public override void AddDateAdd(StringBuilder sb, DateParts part, string target, int value)
        {
            string datePart = string.Empty;
            switch (part) {
                case DateParts.Day:
                    datePart = "day";
                    break;
                case DateParts.Month:
                    datePart = "month";
                    break;
                case DateParts.Year:
                    datePart = "year";
                    break;
                case DateParts.Hour:
                    datePart = "hour";
                    break;
                case DateParts.Minute:
                    datePart = "minute";
                    break;
                case DateParts.Second:
                    datePart = "second";
                    break;
            }

            sb.Append($"dateAdd({ datePart }, { value }, { target })");
        } // AddDateAdd

        public override void AddDateDiff(StringBuilder sb, DateParts part, string date1, string date2)
        {
            string datePart = string.Empty;
            switch (part) {
                case DateParts.Day:
                    datePart = "day";
                    break;
                case DateParts.Month:
                    datePart = "month";
                    break;
                case DateParts.Year:
                    datePart = "year";
                    break;
                case DateParts.Hour:
                    datePart = "hour";
                    break;
                case DateParts.Minute:
                    datePart = "minute";
                    break;
                case DateParts.Second:
                    datePart = "second";
                    break;
                case DateParts.MilliSecond:
                    datePart = "millisecond";
                    break;
            }

            sb.Append($"dateDiff({ datePart }, { date1 }, { date2 })");
        } // AddDateDiff

        public override void AddDateYear(StringBuilder sb, string date)
        {
            sb.Append($"year({ date })");
        } // AddDateYear

        public override void AddDateMonth(StringBuilder sb, string date)
        {
            sb.Append($"month({ date })");
        } // AddDateMonth

        public override void AddDateDay(StringBuilder sb, string date)
        {
            sb.Append($"day({ date })");
        } // AddDateDay

        public override void AddDateHour(StringBuilder sb, string date)
        {
            sb.Append($"hour({ date })");
        } // AddDateHour

        public override void AddDateMinute(StringBuilder sb, string date)
        {
            sb.Append($"minute({ date })");
        } // AddDateMinute

        public override void AddDateSecond(StringBuilder sb, string date)
        {
            sb.Append($"second({ date })");
        } // AddDateSecond

        public override void AddSubString(StringBuilder sb, string target, string start, string length)
        {
            sb.Append($"SUBSTRING({ target }, { start }, { length })");
        } // AddSubString

        public override void AddLength(StringBuilder sb, string target)
        {
            sb.Append($"LEN({ target })");
        } // AddLength

        public override string GetParameterName(string baseName)
        {
            return string.Format("@{0}", baseName);
        } // GetParameterName

        protected override int ExecuteNonQueryPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            using (SqlCommand cmd = m_Conn.CreateCommand()) {
                cmd.CommandText = sql;
                if (m_Trans != null)
                    cmd.Transaction = m_Trans;
                if (pars != null) {
                    foreach (SqlParameterWithValue p in pars)
                        cmd.Parameters.Add(new SqlParameter(p.Name, p.Value == null ? DBNull.Value : p.Value));
                }
                return cmd.ExecuteNonQuery();
            }
        } // ExecuteNonQueryPrimitive

        protected override DbDataReader SelectPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            using (SqlCommand cmd = m_Conn.CreateCommand()) {
                cmd.CommandText = sql;
                if (m_Trans != null)
                    cmd.Transaction = m_Trans;
                if (pars != null) {
                    foreach (SqlParameterWithValue p in pars)
                        cmd.Parameters.Add(new SqlParameter(p.Name, p.Value == null ? DBNull.Value : p.Value));
                }
                return cmd.ExecuteReader();
            }
        } // SelectPrimitive
        #endregion

        public override void AddOffsetLimit(StringBuilder sb, int offset, int limit)
        {
            if (limit > 0 || offset > 0) {
                if (offset > 0)
                    sb.AppendFormat(" OFFSET {0} ROWS", offset);
                if (limit > 0) {
                    if (offset == 0)
                        sb.AppendFormat(" OFFSET {0} ROWS", offset);
                    sb.AppendFormat(" FETCH NEXT {0} ROWS ONLY", limit);
                }
            }
        } // AddOffsetLimit

        protected override Version GetVersionPrimitive()
        {
            Version res = new Version();
            using (DbDataReader dr = Select("select SERVERPROPERTY('productversion')")) {
                if (dr.Read()) {
                    string v = dr.GetString(0);
                    if (!string.IsNullOrEmpty(v)) {
                        int idx = v.IndexOf('-');
                        if (idx >= 0)
                            v = v.Substring(0, idx);
                        string[] parts = v.Split(new char[] { '.' });
                        if (parts.Length >= 3)
                            res = new Version(int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]));
                    }
                }
            }
            return res;
        } // GetVersion

        public override long GetNextCounterValue(string counterName)
        {
            // Sequences supported from Sql Server 2012
            if (GetVersion() < new Version(11, 0))
                return base.GetNextCounterValue(counterName);

            long res = 0;
            while (true) {
                try {
                    using (DbDataReader dr = Select($"select (NEXT VALUE FOR { counterName }) AS value")) {
                        if (dr.Read())
                            res = dr.GetInt64(0);
                        else
                            res = -1;
                    }
                    break;
                } catch (Exception) {
                    ExecuteNonQuery($@"IF NOT EXISTS (SELECT [name] FROM sys.sequences WHERE [name] = '{ counterName }')
                                       BEGIN TRY  
                                         CREATE SEQUENCE { counterName } as [BIGINT] START WITH 1 CACHE;
                                       END TRY
                                       BEGIN CATCH
                                       END CATCH");
                }
            }
            return res;
        } // GetNextCounterValue
    } // SQLServerConnection
}
