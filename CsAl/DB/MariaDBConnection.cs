﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace CsAlCore.DB
{
    class MariaDBConnection : MySQLConnection
    {
        public MariaDBConnection(string connectionString, Common.Utility.LogFile log, bool multipleAccess) :
          base(connectionString, log, multipleAccess)
        {

        }

        public override Version MinimumVersion
        {
            get { return new Version(10, 1); }
        }

        public override long GetNextCounterValue(string counterName)
        {
            // Sequences supported from MariaDB 10.3
            if (GetVersion() < new Version(10, 3))
                return base.GetNextCounterValue(counterName);
            
            long res = 0;
            while (true) {
                try {
                    using (DbDataReader dr = Select($"select nextval({ counterName })")) {
                        if (dr.Read())
                            res = dr.GetInt64(0);
                        else
                            res = -1;
                    }
                    break;
                } catch (Exception) {
                    ExecuteNonQuery($@"CREATE SEQUENCE IF NOT EXISTS { counterName} START WITH 1");
                }
            }
            return res;
        } // GetNextCounterValue
    }
}
