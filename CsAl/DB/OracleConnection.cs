﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using CsAlCore.Common.Schema;
using System.Globalization;
using System.Data.Common;
using System.Diagnostics;

namespace CsAlCore.DB
{
  class OracleConnection : DbConnectionBase
  {    
    Oracle.ManagedDataAccess.Client.OracleConnection m_Conn = null;
    OracleTransaction m_Trans = null;

    public OracleConnection(string connectionString, Common.Utility.LogFile log, bool multipleAccess) :
      base(connectionString, log, multipleAccess)
    {

    }

    public override Version MinimumVersion
    {
      get { return new Version(12, 0); }
    }

    public override DbConnection CreateConection()
    {
      Oracle.ManagedDataAccess.Client.OracleConnection res = new Oracle.ManagedDataAccess.Client.OracleConnection(ConnectionString);
      res.Open();
      return res;
    }
    
    protected override bool OpenPrimitive()
    {
      try {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        m_Conn = new Oracle.ManagedDataAccess.Client.OracleConnection(ConnectionString);
        m_Conn.Open();
        sw.Stop();
        if (Log != null)
          Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", string.Format("Connection open (time {0}): {1}", sw.Elapsed.ToString(), ConnectionString));
      } catch (Exception ex) {
        Log?.Write(Common.Utility.LogFile.LogLevels.Error, "DbConnection", $"Error opening database: { ex.Message }");
        return false;
      }
      return true;
    } // OpenPrimitive

    public override void Dispose()
    {
      if (m_Conn != null) {
        RollbackTransaction();
        m_Conn.Dispose();
        m_Conn = null;
      }
    } // Dispose

    #region Transaction management
    public override void BeginTransaction()
    {
      m_Trans = m_Conn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
    } // StartTransaction

    public override void CommitTransaction()
    {
      if (m_Trans != null) {
        m_Trans.Commit();
        m_Trans.Dispose();
        m_Trans = null;
      }
    } // CommitTransaction

    public override void RollbackTransaction()
    {
      if (m_Trans != null) {
        m_Trans.Rollback();
        m_Trans.Dispose();
        m_Trans = null;
      }
    } // RollbackTransaction
    #endregion

    #region Schema upgrade
    public override void AddForeignKeyCreateScript(StringBuilder sb, ObjectType parentObjectType, ObjectAttribute childAttribute)
    {
      sb.AppendFormat("CONSTRAINT FK_{1} FOREIGN KEY({2}) REFERENCES {0}(a_Id)", parentObjectType.TableName, GetUniqueName(), childAttribute.ColumnName);
    } // AddForeignKeyCreateScript

    protected override string GetAttributeScript(Schema schema, ObjectAttribute attribute, out List<string> scripts)
    {
      scripts = new List<string>();

      string req = attribute.Required ? "NOT NULL" : string.Empty;

      switch (attribute.Type) {
        case ObjectAttribute.Types.Identity:
          return string.Format("{0} NUMBER PRIMARY KEY NOT NULL", attribute.ColumnName);
        case ObjectAttribute.Types.String:
        case ObjectAttribute.Types.Password:
          if (attribute.MaxLength > 0 && attribute.MaxLength <= 4000)
            return string.Format("{0} VARCHAR2({1}) {2}", attribute.ColumnName, attribute.MaxLength, req);
          else
            return string.Format("{0} CLOB {1}", attribute.ColumnName, req);
        case ObjectAttribute.Types.Boolean:
          return string.Format("{0} NUMBER(1) {1}", attribute.ColumnName, req);
        case ObjectAttribute.Types.Date:
        case ObjectAttribute.Types.DateTime:
        case ObjectAttribute.Types.CreatedDateTime:
        case ObjectAttribute.Types.LastModifiedDateTime:
          return string.Format("{0} TIMESTAMP {1}", attribute.ColumnName, req);
        case ObjectAttribute.Types.Integer:
        case ObjectAttribute.Types.Enum:
        case ObjectAttribute.Types.TimeSpan:
        case ObjectAttribute.Types.Reference:
        case ObjectAttribute.Types.BinaryData:
        case ObjectAttribute.Types.Flags:
          return string.Format("{0} NUMBER {1}", attribute.ColumnName, req);
        case ObjectAttribute.Types.Real:
          return string.Format("{0} BINARY_DOUBLE {1}", attribute.ColumnName, req);
        case ObjectAttribute.Types.Blob:
          //if (attribute.MaxLength > 0)
          //  return string.Format("{0} BLOB({1}) {2}", attribute.ColumnName, attribute.MaxLength, req);
          //else
            return string.Format("{0} BLOB {1}", attribute.ColumnName, req);
      }
      return string.Empty;
    } // GetAttributeScript
    #endregion

    #region Sql execution
    public override void AddIntegerValue(StringBuilder sb, long value)
    {
      sb.Append(value.ToString(CultureInfo.InvariantCulture));
    } // AddIntegerValue

    public override void AddBooleanValue(StringBuilder sb, bool value)
    {
      sb.Append(value ? "1" : "0");
    } // AddBooleanValue

    public override void AddStringValue(StringBuilder sb, string value)
    {
      sb.AppendFormat("'{0}'", value.Replace("'", "''"));
    } // AddStringValue

    public override void AddDateTimeValue(StringBuilder sb, DateTime value)
    {
      sb.AppendFormat("TIMESTAMP '{0}'", value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
    } // AddDateTimeValue

    public override void AddTimeSpanValue(StringBuilder sb, TimeSpan value)
    {
      sb.AppendFormat("{0}", TimeSpanToDb(value));
    } // AddTimeSpanValue

    public override DateTime? DbToDateTime(object value)
    {
      if (value is DateTime)
        return (DateTime)value;
      return null;
    } // DbToDateTime

    public override object DateTimeToDb(DateTime? value)
    {
      return value;
    } // DateTimeToDb

    public override TimeSpan? DbToTimeSpan(object value)
    {
      if (value is Decimal) {
        Decimal mSecs = (Decimal)value;
        return TimeSpan.FromMilliseconds((double)mSecs);
      }
      return null;
    } // DbToTimeSpan

    public override object TimeSpanToDb(TimeSpan? value)
    {
      if (value == null)
        return null;
      return (long)Math.Round(value.Value.TotalMilliseconds);
    } // TimeSpanToDb

    public override bool? DbToBoolean(object value)
    {
      if (value == null)
        return null;
      if (value is decimal)
        return (decimal)value == 1;      
      if (value is short)
        return (short)value == 1;         
      return (int)value == 1;
    } // GetBoolean

    public override object BooleanToDb(bool? value)
    {
      if (value.HasValue)
        return value.Value ? 1 : 0;
      return null;
    } // BooleanToDb

    public override long? DbToLong(object value)
    {
      if (value is Decimal)
        return (long)(Decimal)value;
      return null;
    } // DbToLong


    public override byte[] DbToBlob(object value)
    {
      if (value is byte[])
        return (byte[])value;
      return null;
    } // GetBLob

    public override void AddBitAnd(StringBuilder sb, string leftExpr, string rightExpr)
    {
      sb.AppendFormat("BITAND({0}, {1})", leftExpr, rightExpr);
    } // AddBitAnd

    public override void AddBitOr(StringBuilder sb, string leftExpr, string rightExpr)
    {
      sb.AppendFormat("BITOR({0}, {1})", leftExpr, rightExpr);
    } // AddBitOr

    public override void AddNot(StringBuilder sb, string expr)
    {
      sb.AppendFormat("NOT({0})", expr);
    } // AddNot

    public override void AddContains(StringBuilder sb, string target, string value)
    {
      sb.AppendFormat("{0} LIKE '%{1}%'", target, value);
    } // AddContains

    public override void AddStartsWith(StringBuilder sb, string target, string value)
    {
      sb.AppendFormat("{0} LIKE '{1}%'", target, value);
    } // AddStartsWith

    public override void AddEndsWith(StringBuilder sb, string target, string value)
    {
      sb.AppendFormat("{0} LIKE '%{1}'", target, value);
    } // AddEndsWith

    public override void AddDateAdd(StringBuilder sb, DateParts part, string target, int value)
    {
        string modifier = string.Empty;
        switch (part) {
            case DateParts.Day:
                modifier = $"INTERVAL { value.ToString("+0;-#") } day";
                break;
            case DateParts.Month:
                modifier = $"INTERVAL { value.ToString("+0;-#") } month";
                break;
            case DateParts.Year:
                modifier = $"INTERVAL { value.ToString("+0;-#") } year";
                break;
            case DateParts.Hour:
                modifier = $"INTERVAL { value.ToString("+0;-#") } hour";
                break;
            case DateParts.Minute:
                modifier = $"INTERVAL { value.ToString("+0;-#") } minute";
                break;
            case DateParts.Second:
                modifier = $"INTERVAL { value.ToString("+0;-#") } second";
                break;
          case DateParts.MilliSecond:
            modifier = $"INTERVAL { (value / 1000).ToString("+0;-#") } microsecond";
            break;            
        }

        sb.Append($"date_add({ target }, { modifier })");
    } // AddDateAdd

    public override void AddDateDiff(StringBuilder sb, DateParts part, string date1, string date2)
    {
        string modifier = string.Empty;
        switch (part) {
            case DateParts.Month:
                sb.Append($"MONTHS_BETWEEN({date1}, {date2})");
                return;
            case DateParts.Year:
                modifier = "year";
                break;
            case DateParts.Hour:
                modifier = " * 24.0";
                break;
            case DateParts.Minute:
              modifier = " * 24.0 * 60.0";
                break;
            case DateParts.Second:
              modifier = " * 24.0 * 60.0 * 60.0";
              break;
            case DateParts.MilliSecond:
              modifier = " * 24.0 * 60.0 * 60.0 * 1000.0";
                break;
        }

        sb.Append($"({ date1 } - { date2 }){ modifier }");
    } // AddDateDiff

    public override void AddDateYear(StringBuilder sb, string date)
    {
        sb.Append($"year({ date })");
    } // AddDateYear

    public override void AddDateMonth(StringBuilder sb, string date)
    {
        sb.Append($"month({ date })");
    } // AddDateMonth

    public override void AddDateDay(StringBuilder sb, string date)
    {
        sb.Append($"day({ date })");
    } // AddDateDay

    public override void AddDateHour(StringBuilder sb, string date)
    {
        sb.Append($"hour({ date })");
    } // AddDateHour

    public override void AddDateMinute(StringBuilder sb, string date)
    {
        sb.Append($"minute({ date })");
    } // AddDateMinute

    public override void AddDateSecond(StringBuilder sb, string date)
    {
        sb.Append($"second({ date })");
    } // AddDateSecond

    public override void AddStringConcat(StringBuilder sb, string leftExpr, string rightExpr)
    {
        sb.AppendFormat("{0} || {1}", leftExpr, rightExpr);
    } // AddStringConcat

    public override void AddSubString(StringBuilder sb, string target, string start, string length)
    {
        sb.Append($"SUBSTR({ target }, { start }, { length })");
    } // AddSubString

    public override void AddLength(StringBuilder sb, string target)
    {
        sb.Append($"LENGTH({ target })");
    } // AddLength

    
    public override string GetParameterName(string baseName)
    {
      return string.Format(":{0}", baseName);
    } // GetParameterName

    protected override int ExecuteNonQueryPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
    {      
      using (OracleCommand cmd = m_Conn.CreateCommand()) {
        cmd.CommandText = sql;
        cmd.BindByName = true;
        if (m_Trans != null)
          cmd.Transaction = m_Trans;
        if (pars != null) {
          foreach (SqlParameterWithValue p in pars) {
            OracleParameter op = cmd.CreateParameter();
            op.ParameterName = p.Name;
            op.Value = p.Value == null ? DBNull.Value : p.Value;
            cmd.Parameters.Add(op);
          }            
        }
        return cmd.ExecuteNonQuery();
      }
    } // ExecuteNonQuery

    protected override DbDataReader SelectPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
    {
      using (OracleCommand cmd = m_Conn.CreateCommand()) {
        cmd.CommandText = sql;
        cmd.BindByName = true;
        if (m_Trans != null)
          cmd.Transaction = m_Trans;
        if (pars != null) {
          foreach (SqlParameterWithValue p in pars) {
            OracleParameter op = cmd.CreateParameter();
            op.ParameterName = p.Name;
            op.Value = p.Value == null ? DBNull.Value : p.Value;
            cmd.Parameters.Add(op);
          }
        }
        return cmd.ExecuteReader();
      }
    } // SelectPrimitive
    #endregion

    public override void AddOffsetLimit(StringBuilder sb, int offset, int limit)
    {
      if (limit > 0 || offset > 0) {
        sb.Insert(0, "select * from (");
        sb.Append(") where");
        if (offset > 0)
          sb.AppendFormat(" rownum >= {0}", offset);

        if (limit > 0) {
          if (offset > 0)
            sb.Append(" and");
          sb.AppendFormat(" rownum < {0}", offset + limit);
        }
      }
    } // AddOffsetLimit

    protected override Version GetVersionPrimitive()
    {
      Version res = new Version();
      using (DbDataReader dr = Select("SELECT version FROM V$INSTANCE")) {
        if (dr.Read()) {
          string v = dr.GetString(0);
          if (!string.IsNullOrEmpty(v)) {
            string[] parts = v.Split(new char[] { '.' });
            if (parts.Length >= 3)
              res = new Version(int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]));
          }
        }
      }
      return res;
    } // GetVersion
    
    public override long GetNextCounterValue(string counterName)
    {
      long res = 0;
      while (true) {
        try {
          using (DbDataReader dr = Select($"select { counterName }.nextval from dual")) {
            if (dr.Read())
              res = dr.GetInt64(0);
            else
              res = -1;
          }
          break;
        } catch (Exception) {
          ExecuteNonQuery($@"BEGIN 
                               EXECUTE IMMEDIATE 'CREATE SEQUENCE { counterName} START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE';
                             EXCEPTION 
                             WHEN OTHERS THEN
                               NULL;
                             END;");
        }
      }
      return res;
    } // GetNextCounterValue    
  } // OracleConnection
}
