﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using System.Data.Common;
using CsAlCore.Common.Schema;
using System.Globalization;
using System.Diagnostics;
using System.IO;

namespace CsAlCore.DB
{
    class SQLiteConnection : DbConnectionBase
    {
        string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
        SqliteConnection m_Conn = null;
        SqliteTransaction m_Trans = null;

        public SQLiteConnection(string connectionString, Common.Utility.LogFile log, bool multipleAccess) :
          base(connectionString, log, multipleAccess)
        {
            // Foreign keys disabled because SQLite doesn't support alter column and drop column, 
            // so we need to create a new table and copy data to upgrade the schema. 
            // With foreign keys enabled we should also recreate all
            // the tables referencing the table we want to modify via foreign key.
            SupportsForeignKeys = false;
        }

        public override DbConnection CreateConection()
        {
            SqliteConnection res = new SqliteConnection(ConnectionString);
            res.Open();
            return res;
        }

        protected override bool OpenPrimitive()
        {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                m_Conn = (SqliteConnection)CreateConection();
                ExecuteNonQuery("PRAGMA synchronous = OFF");
                if (SupportsForeignKeys)
                    ExecuteNonQuery("PRAGMA foreign_keys = ON");
                else
                    ExecuteNonQuery("PRAGMA foreign_keys = OFF");
                ExecuteNonQuery("PRAGMA journal_mode = WAL");
                sw.Stop();
                if (Log != null)
                    Log.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", string.Format("Connection open (time {0}): {1}", sw.Elapsed.ToString(), ConnectionString));
            } catch (Exception ex) {
                Log.Write(Common.Utility.LogFile.LogLevels.Error, "DbConnection", string.Format("Error opening database: {0}", ex.Message));
                return false;
            }
            return true;
        } // OpenPrimitive

        public override void Dispose()
        {
            if (m_Conn != null) {
                RollbackTransaction();
                m_Conn.Dispose();
                m_Conn = null;
            }
        }

        #region Transaction management
        public override void BeginTransaction()
        {
            m_Trans = m_Conn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        } // StartTransaction

        public override void CommitTransaction()
        {
            if (m_Trans != null) {
                m_Trans.Commit();
                m_Trans.Dispose();
                m_Trans = null;
            }
        } // CommitTransaction

        public override void RollbackTransaction()
        {
            if (m_Trans != null) {
                m_Trans.Rollback();
                m_Trans.Dispose();
                m_Trans = null;
            }
        } // RollbackTransaction
        #endregion

        #region Schema upgrade
        private void DropIndexes(string tableName)
        {
            List<string> indexes = new List<string>();
            using (DbDataReader dr = Select(@"select name
                                              FROM sqlite_master
                                              WHERE type = 'index'
                                                and tbl_name = @tname",
                                            new List<SqlParameterWithValue>()
                                            {
                                                    new SqlParameterWithValue("tname", $"old_{ tableName }")
                                            }
                                            )) {
                while (dr.Read())
                    indexes.Add(dr.GetString(0));
            }
            foreach (string index in indexes)
                ExecuteNonQuery($"drop index { index }");
        } // DropIndexes

        public override void DeleteObjectAttribute(Schema schema, string dataRootPath, ObjectAttribute at)
        {
            if (at.IsTableField) {
                ObjectType ot = schema.GetObjectType(at.ObjectTypeName);

                // For binary data delete all files
                if (at.Type == ObjectAttribute.Types.BinaryData) {
                    string path = $"{ dataRootPath }\\{ at.ObjectTypeName }.{ at.Name }";
                    if (Directory.Exists(path))
                        Directory.Delete(path, true);

                    ExecuteNonQuery($@"delete from sys_binarydata 
                                       where a_id in (select { at.ColumnName } from { ot.TableName })");
                }

                // SQlite cannot drop a column from a table.
                StringBuilder sb = new StringBuilder();

                // Rename table
                ExecuteNonQuery($"alter table { ot.TableName } rename to old_{ ot.TableName }");

                // Drop indexes
                DropIndexes(ot.TableName);

                // Create new table
                CreateObjectType(schema, ot);

                // Populate new table:
                bool first = true;
                foreach (ObjectAttribute oAt in ot.Attributes) {
                    if (oAt.IsIdentity || !oAt.IsInherited && oAt.Name != at.Name) {
                        if (!first)
                            sb.Append(", ");
                        sb.Append(oAt.ColumnName);
                        first = false;
                    }
                }
                ExecuteNonQuery($@"insert into { ot.TableName }( { sb.ToString() })
                                   select { sb.ToString() } from old_{ ot.TableName }");

                // Drop old table:
                ExecuteNonQuery($"drop table old_{ ot.TableName }");
            } else if (at.AttributeType.Type == ObjectAttribute.Types.MultiReference) {
                if (!at.ChildrenReference) {
                    ExecuteNonQuery($"drop table { GetMultiReferenceTableName(at) }");
                } else {
                    ObjectType rOt = schema.GetObjectType(at.RefObjectType);
                    DeleteObjectAttribute(schema, dataRootPath, rOt.GetAttribute(Schema.ParentObjElementName));
                }
            }
        } // DeleteObjectAttribute

        public override void ModifyObjectAttribute(Schema schema, ObjectAttribute oldAt, ObjectAttribute newAt)
        {
            ObjectType ot = schema.GetObjectType(newAt.ObjectTypeName);
            if (newAt.IsTableField) {
                // SQlite cannot alter a column in a table.                
                StringBuilder sb = new StringBuilder();

                // Rename table
                ExecuteNonQuery($"alter table { ot.TableName } rename to old_{ ot.TableName }");

                // Drop indexes
                DropIndexes(ot.TableName);

                // Create new table
                CreateObjectType(schema, ot);

                // Populate new table:
                bool first = true;
                foreach (ObjectAttribute oAt in ot.Attributes) {
                    if (oAt.IsIdentity || !oAt.IsInherited) {
                        if (!first)
                            sb.Append(", ");
                        sb.Append(oAt.ColumnName);
                        first = false;
                    }
                }
                ExecuteNonQuery($@"insert into { ot.TableName }( { sb.ToString() })
                                   select { sb.ToString() } from old_{ ot.TableName }");

                // Drop old table:
                ExecuteNonQuery($"drop table old_{ ot.TableName }");
            } else if (newAt.AttributeType.Type == ObjectAttribute.Types.MultiReference) {
                if (!newAt.ChildrenReference) {
                    CsAlCore.AttributesDifferences diff = CsAlCore.CompareAttributes(oldAt.SubAttributes, newAt.SubAttributes);
                    if (diff.NewObjectAttributes.Count > 0 || diff.RemovedObjectAttributes.Count > 0 || diff.ModifiedObjectAttributes.Count > 0) {
                        ExecuteNonQuery($"alter table { GetMultiReferenceTableName(newAt) } rename to old_{ GetMultiReferenceTableName(newAt) }");
                        DropIndexes($"old_{ GetMultiReferenceTableName(newAt) }");
                        CreateMultiReference(schema, newAt);

                        StringBuilder sb = new StringBuilder();
                        sb.Append("a_ObjId, a_RefId");
                        foreach (ObjectAttribute oAt in oldAt.SubAttributes) {
                            if (newAt.GetSubAttribute(oAt.Name) != null && (oAt.IsIdentity || !oAt.IsInherited)) {
                                sb.Append(", ");
                                sb.Append(oAt.ColumnName);
                            }
                        }
                        ExecuteNonQuery($@"insert into { GetMultiReferenceTableName(newAt) }( { sb.ToString() })
                                           select { sb.ToString() } from old_{ GetMultiReferenceTableName(newAt) }");

                        ExecuteNonQuery($"drop table old_{ GetMultiReferenceTableName(newAt) }");
                    }
                }
            }
        } // ModifyObjectAttribute

        public override void AddForeignKeyCreateScript(StringBuilder sb, ObjectType parentObjectType, ObjectAttribute childAttribute)
        {
            sb.AppendFormat("FOREIGN KEY({1}) REFERENCES {0}(a_Id)", parentObjectType.TableName, childAttribute.ColumnName);
        } // CreateForeignKey

        protected override string GetAttributeScript(Schema schema, ObjectAttribute attribute, out List<string> scripts)
        {
            scripts = new List<string>();

            string req = attribute.Required ? "NOT NULL" : string.Empty;
            string defaultValue = string.Empty;

            switch (attribute.Type) {
                case ObjectAttribute.Types.Identity:
                    return string.Format("{0} INTEGER PRIMARY KEY NOT NULL", attribute.ColumnName);
                case ObjectAttribute.Types.String:
                case ObjectAttribute.Types.Password:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT '{ attribute.AttributeType.GetDefaultValue() }'";
                    if (attribute.MaxLength > 0)
                        return string.Format("{0} TEXT({1}) {2} {3} COLLATE NOCASE", attribute.ColumnName, attribute.MaxLength, req, defaultValue);
                    else
                        return string.Format("{0} TEXT {1} COLLATE NOCASE", attribute.ColumnName, req);
                case ObjectAttribute.Types.Date:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { DateTimeToDb((DateTime?)attribute.AttributeType.GetDefaultValue()) }";
                    return string.Format("{0} TEXT({1}) {2} {3} COLLATE NOCASE", attribute.ColumnName, 10, req, defaultValue);
                case ObjectAttribute.Types.DateTime:
                case ObjectAttribute.Types.CreatedDateTime:
                case ObjectAttribute.Types.LastModifiedDateTime:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { DateTimeToDb((DateTime?)attribute.AttributeType.GetDefaultValue()) }";
                    return string.Format("{0} TEXT({1}) {2} {3} COLLATE NOCASE", attribute.ColumnName, 23, req, defaultValue);
                case ObjectAttribute.Types.Boolean:
                    if (attribute.HasDefaultValue) {
                        if ((bool)(attribute.AttributeType.GetDefaultValue()) == true)
                            defaultValue = $"DEFAULT 1";
                        else
                            defaultValue = $"DEFAULT 0";
                    }
                    return string.Format("{0} INTEGER(1) {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Integer:
                case ObjectAttribute.Types.Enum:
                case ObjectAttribute.Types.TimeSpan:
                case ObjectAttribute.Types.Reference:
                case ObjectAttribute.Types.BinaryData:
                case ObjectAttribute.Types.Flags:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { attribute.AttributeType.GetDefaultValue() }";
                    return string.Format("{0} INTEGER {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Real:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { attribute.AttributeType.GetDefaultValue() }";
                    return string.Format("{0} REAL {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Blob:
                    if (attribute.MaxLength > 0)
                        return string.Format("{0} BLOB({1}) {2}", attribute.ColumnName, attribute.MaxLength, req);
                    else
                        return string.Format("{0} BLOB {1}", attribute.ColumnName, req);
            }
            return string.Empty;
        } // GetAttributeScript
        #endregion

        #region Sql execution
        public override void AddIntegerValue(StringBuilder sb, long value)
        {
            sb.Append(value.ToString(CultureInfo.InvariantCulture));
        } // AddIntegerValue

        public override void AddBooleanValue(StringBuilder sb, bool value)
        {
            sb.Append(value ? "1" : "0");
        } // AddBooleanValue

        public override void AddStringValue(StringBuilder sb, string value)
        {
            sb.AppendFormat("'{0}'", value.Replace("'", "''"));
        } // AddStringValue

        public override void AddDateTimeValue(StringBuilder sb, DateTime value)
        {
            sb.AppendFormat("'{0}'", DateTimeToDb(value));
        } // AddDateTimeValue

        public override void AddTimeSpanValue(StringBuilder sb, TimeSpan value)
        {
            sb.AppendFormat("{0}", TimeSpanToDb(value));
        } // AddTimeSpanValue

        public override DateTime? DbToDateTime(object value)
        {
            string dt = value as string;
            DateTime res = DateTime.MinValue;
            if (DateTime.TryParseExact(dt, DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out res))
                return res.ToUniversalTime();
            return null;
        } // DbToDateTime

        public override object DateTimeToDb(DateTime? value)
        {
            if (value == null)
                return null;
            return value.Value.ToString(DateTimeFormat);
        } // DateTimeToDb

        public override TimeSpan? DbToTimeSpan(object value)
        {
            if (value is long) {
                long mSecs = (long)value;
                return TimeSpan.FromMilliseconds(mSecs);
            }
            return null;
        } // DbToTimeSpan

        public override object TimeSpanToDb(TimeSpan? value)
        {
            if (value == null)
                return null;
            return (long)Math.Round(value.Value.TotalMilliseconds);
        } // TimeSpanToDb

        public override bool? DbToBoolean(object value)
        {
            if (value == null || value == DBNull.Value)
                return null;
            return (long)value == 1;
        } // GetBoolean

        public override object BooleanToDb(bool? value)
        {
            if (value.HasValue)
                return value.Value ? 1 : 0;
            return null;
        } // BooleanToDb

        public override byte[] DbToBlob(object value)
        {
            if (value is byte[])
                return (byte[])value;
            return null;
        } // GetBLob

        public override void AddNot(StringBuilder sb, string expr)
        {
            sb.AppendFormat("NOT({0})", expr);
        } // AddNot

        public override void AddContains(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '%{1}%'", target, value);
        } // AddContains

        public override void AddStartsWith(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '{1}%'", target, value);
        } // AddStartsWith

        public override void AddEndsWith(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '%{1}'", target, value);
        } // AddEndsWith

        public override void AddDateAdd(StringBuilder sb, DateParts part, string target, int value)
        {
            string modifier = string.Empty;
            switch (part) {
                case DateParts.Day:
                    modifier = $"{ value.ToString("+0;-#") } day";
                    break;
                case DateParts.Month:
                    modifier = $"{ value.ToString("+0;-#") } month";
                    break;
                case DateParts.Year:
                    modifier = $"{ value.ToString("+0;-#") } year";
                    break;
                case DateParts.Hour:
                    modifier = $"{ value.ToString("+0;-#") } hour";
                    break;
                case DateParts.Minute:
                    modifier = $"{ value.ToString("+0;-#") } minute";
                    break;
                case DateParts.Second:
                    modifier = $"{ value.ToString("+0;-#") } second";
                    break;
            }

            sb.Append($"datetime({ target }, '{ modifier }')");
        } // AddDateAdd

        public override void AddDateDiff(StringBuilder sb, DateParts part, string date1, string date2)
        {
            string modifier = string.Empty;
            switch (part) {
                case DateParts.Day:
                    modifier = $"julianday({ date1 }) - julianday({ date2 })";
                    break;
                case DateParts.Month:
                    modifier = $"(julianday({ date1 }) - julianday({ date2 })) / (365 / 12)";
                    break;
                case DateParts.Year:
                    modifier = $"(julianday({ date1 }) - julianday({ date2 }) / 365";
                    break;
                case DateParts.Hour:
                    modifier = $"(julianday({ date1 }) - julianday({ date2 })) * 24";
                    break;
                case DateParts.Minute:
                    modifier = $"(julianday({ date1 }) - julianday({ date2 })) * 24 * 60";
                    break;
                case DateParts.Second:
                    modifier = $"(julianday({ date1 }) - julianday({ date2 })) * 24 * 60 * 60";
                    break;
                case DateParts.MilliSecond:
                    modifier = $"(julianday({ date1 }) - julianday({ date2 })) * 24 * 60 * 60";
                    break;
            }

            sb.Append($"cast({ modifier } as INTEGER)");
        } // AddDateDiff

        public override void AddDateYear(StringBuilder sb, string date)
        {
            sb.Append($"strftime('%Y', { date })");
        } // AddDateYear

        public override void AddDateMonth(StringBuilder sb, string date)
        {
            sb.Append($"strftime('%m', { date })");
        } // AddDateMonth

        public override void AddDateDay(StringBuilder sb, string date)
        {
            sb.Append($"strftime('%d', { date })");
        } // AddDateDay

        public override void AddDateHour(StringBuilder sb, string date)
        {
            sb.Append($"strftime('%H', { date })");
        } // AddDateHour

        public override void AddDateMinute(StringBuilder sb, string date)
        {
            sb.Append($"strftime('%M', { date })");
        } // AddDateMinute

        public override void AddDateSecond(StringBuilder sb, string date)
        {
            sb.Append($"strftime('%S', { date })");
        } // AddDateSecond

        public override void AddSubString(StringBuilder sb, string target, string start, string length)
        {
            sb.Append($"SUBSTR({ target }, { start }, { length })");
        } // AddSubString

        public override void AddLength(StringBuilder sb, string target)
        {
            sb.Append($"LENGTH({ target })");
        } // AddLength

        public override string GetParameterName(string baseName)
        {
            return string.Format("@{0}", baseName);
        } // GetParameterName

        protected override int ExecuteNonQueryPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            using (SqliteCommand cmd = m_Conn.CreateCommand()) {
                cmd.CommandText = sql;
                if (m_Trans != null)
                    cmd.Transaction = m_Trans;
                if (pars != null) {
                    foreach (SqlParameterWithValue p in pars)
                        cmd.Parameters.Add(new SqliteParameter(p.Name, p.Value == null ? DBNull.Value : p.Value));
                }
                return cmd.ExecuteNonQuery();
            }
        } // ExecuteNonQueryPrimitive

        protected override DbDataReader SelectPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            using (SqliteCommand cmd = m_Conn.CreateCommand()) {
                cmd.CommandText = sql;
                if (m_Trans != null)
                    cmd.Transaction = m_Trans;
                if (pars != null) {
                    foreach (SqlParameterWithValue p in pars)
                        cmd.Parameters.Add(new SqliteParameter(p.Name, p.Value == null ? DBNull.Value : p.Value));
                }
                return cmd.ExecuteReader();
            }
        } // SelectPrimitive
        #endregion

        public override long GetNextCounterValue(string counterName)
        {
            // SQLite has no sequences:
            // simulate them with an autoincrement column in a table.
            // Ugly but the default implementation doesn't work for multiple connection problems
            string tName = string.Format("cnt_{0}", counterName);

            while (true) {
                try {
                    if (ExecuteNonQuery(string.Format("insert into {0}(a_value) values(NULL)", tName)) == 1) {
                        using (DbDataReader dr = Select("SELECT last_insert_rowid()")) {
                            if (dr.Read()) {
                                long res = dr.GetInt64(0);
                                ExecuteNonQuery(string.Format("delete from {0} where a_value < {1}", tName, res));
                                return res;
                            }
                        }
                    }
                } catch (Exception) {
                    // Create the table:
                    ExecuteNonQuery(string.Format("create table IF NOT EXISTS {0}(a_value INTEGER PRIMARY KEY NOT NULL)", tName));
                }
            }
        } // GetNextCounterValue

        protected override Version GetVersionPrimitive()
        {
            Version res = new Version();
            using (DbDataReader dr = Select("select sqlite_version()")) {
                if (dr.Read()) {
                    string v = dr.GetString(0);
                    if (!string.IsNullOrEmpty(v)) {
                        string[] parts = v.Split(new char[] { '.' });
                        if (parts.Length >= 3)
                            res = new Version(int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]));
                    }
                }
            }
            return res;

        } // GetVersion
    }
}
