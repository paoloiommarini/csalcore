﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CsAlCore.DB
{
    public class SqlParameterWithValue
    {
        public SqlParameterWithValue()
        {

        }

        public SqlParameterWithValue(string name, object value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }

        public object Value { get; set; }

        public new string ToString()
        {
            return string.Format("{0} = {1}", Name, Value);
        }
    }
}
