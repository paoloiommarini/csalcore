﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsAlCore.DB
{
    public static class DbConnectionFactory
    {
        /// <summary>
        /// Crete a database connection using the given <see cref="connString"/>
        /// </summary>
        /// <param name="dbType">The <see cref="CsAlCore.DbType"/></param>
        /// <param name="connString">The connection string</param>
        /// <returns></returns>
        public static DbConnectionBase Create(CsAlCore.DbType dbType, string connString)
        {
            DB.DbConnectionBase res = null;
            switch (dbType) {
                case CsAlCore.DbType.SQLite:
                    res = new SQLiteConnection(connString, null, false);
                    break;
                case CsAlCore.DbType.SQLServer:
                    res = new SQLServerConnection(connString, null, false);
                    break;
                case CsAlCore.DbType.Oracle:
                  res = new DB.OracleConnection(connString, null, false);
                  break;
                case CsAlCore.DbType.MySQL:
                    res = new MySQLConnection(connString, null, false);
                    break;
                case CsAlCore.DbType.MariaDB:
                    res = new MariaDBConnection(connString, null, false);
                    break;
                case CsAlCore.DbType.PostgreSQL:
                    res = new PostgreSqlConnection(connString, null, false);
                    break;
            }

            return res;
        } // Create
    }
}
