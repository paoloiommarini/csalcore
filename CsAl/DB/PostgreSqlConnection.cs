﻿using CsAlCore.Common.Schema;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace CsAlCore.DB
{
    class PostgreSqlConnection : DbConnectionBase
    {
        string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
        NpgsqlConnection m_Conn = null;
        NpgsqlTransaction m_Trans = null;

        public PostgreSqlConnection(string connectionString, Common.Utility.LogFile log, bool multipleAccess) :
          base(connectionString, log, multipleAccess)
        {

        }

        public override Version MinimumVersion
        {
            get { return new Version(5, 7); }
        }

        public override DbConnection CreateConection()
        {
            NpgsqlConnection res = new NpgsqlConnection(ConnectionString);
            res.Open();
            return res;
        }

        protected override bool OpenPrimitive()
        {
            try {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                m_Conn = (NpgsqlConnection)CreateConection();
                sw.Stop();
                Log?.Write(Common.Utility.LogFile.LogLevels.Debug, "DbConnection", string.Format("Connection open (time {0}): {1}", sw.Elapsed.ToString(), ConnectionString));
            } catch (Exception ex) {
                Log?.Write(Common.Utility.LogFile.LogLevels.Error, "DbConnection", $"Error opening database: { ex.Message }");
                return false;
            }
            return true;
        } // OpenPrimitive

        public override void Dispose()
        {
            if (m_Conn != null) {
                RollbackTransaction();
                m_Conn.Dispose();
                m_Conn = null;
            }
        } // Dispose

        #region Transaction management
        public override void BeginTransaction()
        {
            m_Trans = m_Conn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        } // StartTransaction

        public override void CommitTransaction()
        {
            if (m_Trans != null) {
                m_Trans.Commit();
                m_Trans.Dispose();
                m_Trans = null;
            }
        } // CommitTransaction

        public override void RollbackTransaction()
        {
            if (m_Trans != null) {
                m_Trans.Rollback();
                m_Trans.Dispose();
                m_Trans = null;
            }
        } // RollbackTransaction
        #endregion

        #region Schema upgrade

        public override void DeleteIndex(ObjectType objectType, ObjectIndex index)
        {
            ExecuteNonQuery($"drop index { index.GetDbName(objectType) } on {objectType.TableName }");
        } // DeleteIndex

        public override void AddForeignKeyCreateScript(StringBuilder sb, ObjectType parentObjectType, ObjectAttribute childAttribute)
        {
            sb.AppendFormat("FOREIGN KEY({2}) REFERENCES {0}(a_Id)", parentObjectType.TableName, GetUniqueName(), childAttribute.ColumnName);
        } // CreateForeignKey

        protected override string GetAttributeScript(Schema schema, ObjectAttribute attribute, out List<string> scripts)
        {
            scripts = new List<string>();

            string req = attribute.Required ? "NOT NULL" : string.Empty;
            string defaultValue = string.Empty;

            switch (attribute.Type) {
                case ObjectAttribute.Types.Identity:
                    return string.Format("{0} BIGINT PRIMARY KEY NOT NULL", attribute.ColumnName);
                case ObjectAttribute.Types.String:
                case ObjectAttribute.Types.Password:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT '{ attribute.AttributeType.GetDefaultValue() }'";
                    if (attribute.MaxLength > 0)
                        return string.Format("{0} VARCHAR({1}) {2} {3}", attribute.ColumnName, attribute.MaxLength, req, defaultValue);
                    else
                        return string.Format("{0} TEXT {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Boolean:
                    if (attribute.HasDefaultValue) {
                        if ((bool)(attribute.AttributeType.GetDefaultValue()) == true)
                            defaultValue = $"DEFAULT true";
                        else
                            defaultValue = $"DEFAULT false";
                    }
                    return string.Format("{0} boolean {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Date:
                case ObjectAttribute.Types.DateTime:
                case ObjectAttribute.Types.CreatedDateTime:
                case ObjectAttribute.Types.LastModifiedDateTime:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { DateTimeToDb((DateTime?)attribute.AttributeType.GetDefaultValue()) }";
                    return string.Format("{0} TIMESTAMP {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Integer:
                case ObjectAttribute.Types.Enum:
                case ObjectAttribute.Types.TimeSpan:
                case ObjectAttribute.Types.Reference:
                case ObjectAttribute.Types.BinaryData:
                case ObjectAttribute.Types.Flags:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { attribute.AttributeType.GetDefaultValue() }";
                    return string.Format("{0} BIGINT {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Real:
                    if (attribute.HasDefaultValue)
                        defaultValue = $"DEFAULT { attribute.AttributeType.GetDefaultValue() }";
                    return string.Format("{0} DOUBLE {1} {2}", attribute.ColumnName, req, defaultValue);
                case ObjectAttribute.Types.Blob:
                    return string.Format("{0} BYTEA {1}", attribute.ColumnName, req);
            }
            return string.Empty;
        } // GetAttributeScript
        #endregion

        #region Sql execution
        public override void AddIntegerValue(StringBuilder sb, long value)
        {
            sb.Append(value.ToString(CultureInfo.InvariantCulture));
        } // AddIntegerValue

        public override void AddBooleanValue(StringBuilder sb, bool value)
        {
            sb.Append(value ? "true" : "false");
        } // AddBooleanValue

        public override void AddStringValue(StringBuilder sb, string value)
        {
            sb.AppendFormat("'{0}'", value.Replace("'", "''"));
        } // AddStringValue

        public override void AddDateTimeValue(StringBuilder sb, DateTime value)
        {
            sb.AppendFormat("TIMESTAMP '{0}'", DateTimeToDb(value));
        } // AddDateTimeValue

        public override void AddTimeSpanValue(StringBuilder sb, TimeSpan value)
        {
            sb.AppendFormat("{0}", TimeSpanToDb(value));
        } // AddTimeSpanValue

        public override DateTime? DbToDateTime(object value)
        {
            if (value is DateTime)
                return (DateTime)value;
            return null;
        } // DbToDateTime

        public override object DateTimeToDb(DateTime? value)
        {
            if (value == null)
                return null;
            return value.Value.ToString(DateTimeFormat);
        } // DateTimeToDb

        public override TimeSpan? DbToTimeSpan(object value)
        {
            if (value is long) {
                long mSecs = (long)value;
                return TimeSpan.FromMilliseconds(mSecs);
            }
            return null;
        } // DbToTimeSpan

        public override object TimeSpanToDb(TimeSpan? value)
        {
            if (value == null)
                return null;
            return (long)Math.Round(value.Value.TotalMilliseconds);
        } // TimeSpanToDb

        public override bool? DbToBoolean(object value)
        {
            if (value == null || value == DBNull.Value)
                return null;
            return (bool?)value;
        } // GetBoolean

        public override object BooleanToDb(bool? value)
        {
            if (value.HasValue)
                return value.Value;
            return null;
        } // BooleanToDb

        public override long? DbToLong(object value)
        {
            if (value is long)
                return (long)value;
            return null;
        } // DbToLong


        public override byte[] DbToBlob(object value)
        {
            if (value is byte[])
                return (byte[])value;
            return null;
        } // GetBLob

        public override void AddNot(StringBuilder sb, string expr)
        {
            sb.AppendFormat("NOT({0})", expr);
        } // AddNot

        public override void AddContains(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '%{1}%'", target, value);
        } // AddContains

        public override void AddStartsWith(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '{1}%'", target, value);
        } // AddStartsWith

        public override void AddEndsWith(StringBuilder sb, string target, string value)
        {
            sb.AppendFormat("{0} LIKE '%{1}'", target, value);
        } // AddEndsWith

        public override void AddDateAdd(StringBuilder sb, DateParts part, string target, int value)
        {
            string modifier = string.Empty;
            switch (part) {
                case DateParts.Day:
                    modifier = $"INTERVAL { value.ToString("+0;-#") } days";
                    break;
                case DateParts.Month:
                    modifier = $"INTERVAL { value.ToString("+0;-#") } months";
                    break;
                case DateParts.Year:
                    modifier = $"INTERVAL { value.ToString("+0;-#") } years";
                    break;
                case DateParts.Hour:
                    modifier = $"INTERVAL { value.ToString("+0;-#") } hours";
                    break;
                case DateParts.Minute:
                    modifier = $"INTERVAL { value.ToString("+0;-#") } minutes";
                    break;
                case DateParts.Second:
                    modifier = $"INTERVAL { value.ToString("+0;-#") } seconds";
                    break;
            }

            sb.Append($"{ target } + { modifier })");
        } // AddDateAdd

        public override void AddDateDiff(StringBuilder sb, DateParts part, string date1, string date2)
        {
            string modifier = string.Empty;
            switch (part) {
                case DateParts.Day:
                    modifier = "dd";
                    break;
                case DateParts.Month:
                    modifier = "mm";
                    break;
                case DateParts.Year:
                    modifier = "yy";
                    break;
                case DateParts.Hour:
                    modifier = "hh";
                    break;
                case DateParts.Minute:
                    modifier = "mi";
                    break;
                case DateParts.Second:
                    modifier = "ss";
                    break;
                case DateParts.MilliSecond:
                    modifier = "ms";
                    break;
            }

            sb.Append($"DATEDIFF({ modifier }, { date1 }, { date2 })");
        } // AddDateDiff

        public override void AddDateYear(StringBuilder sb, string date)
        {
            sb.Append($"EXTRACT(YEAR FROM { date })");
        } // AddDateYear

        public override void AddDateMonth(StringBuilder sb, string date)
        {
            sb.Append($"EXTRACT(MONTH FROM { date })");
        } // AddDateMonth

        public override void AddDateDay(StringBuilder sb, string date)
        {
            sb.Append($"EXTRACT(DAY FROM { date })");
        } // AddDateDay

        public override void AddDateHour(StringBuilder sb, string date)
        {
            sb.Append($"EXTRACT(HOUR FROM { date })");
        } // AddDateHour

        public override void AddDateMinute(StringBuilder sb, string date)
        {
            sb.Append($"EXTRACT(MINUTE FROM { date })");
        } // AddDateMinute

        public override void AddDateSecond(StringBuilder sb, string date)
        {
            sb.Append($"EXTRACT(SECOND FROM { date })");
        } // AddDateSecond

        public override void AddStringConcat(StringBuilder sb, string leftExpr, string rightExpr)
        {
            sb.AppendFormat("{0} || {1}", leftExpr, rightExpr);
        } // AddStringConcat

        public override void AddSubString(StringBuilder sb, string target, string start, string length)
        {
            sb.Append($"SUBSTRING({ target } from { start } for { length })");
        } // AddSubString

        public override void AddLength(StringBuilder sb, string target)
        {
            sb.Append($"LENGTH({ target })");
        } // AddLength
        
        public override string GetParameterName(string baseName)
        {
            return string.Format("@{0}", baseName);
        } // GetParameterName

        protected override int ExecuteNonQueryPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            using (NpgsqlCommand cmd = m_Conn.CreateCommand()) {
                cmd.CommandText = sql;
                if (m_Trans != null)
                    cmd.Transaction = m_Trans;
                if (pars != null) {
                    foreach (SqlParameterWithValue p in pars) {
                        NpgsqlParameter op = cmd.CreateParameter();
                        op.ParameterName = p.Name;
                        op.Value = p.Value == null ? DBNull.Value : p.Value;
                        cmd.Parameters.Add(op);
                    }
                }
                return cmd.ExecuteNonQuery();
            }
        } // ExecuteNonQueryPrimitive

        protected override DbDataReader SelectPrimitive(string sql, IEnumerable<SqlParameterWithValue> pars)
        {
            //using (NpgsqlCommand cmd = m_Conn.CreateCommand()) {
            NpgsqlCommand cmd = m_Conn.CreateCommand();
            cmd.CommandText = sql;
            if (m_Trans != null)
                cmd.Transaction = m_Trans;
            if (pars != null) {
                foreach (SqlParameterWithValue p in pars) {
                    NpgsqlParameter op = cmd.CreateParameter();
                    op.ParameterName = p.Name;
                    op.Value = p.Value == null ? DBNull.Value : p.Value;
                    cmd.Parameters.Add(op);
                }
            }
            return cmd.ExecuteReader();
            //}
        } // SelectPrimitive
        #endregion

        protected override Version GetVersionPrimitive()
        {
            Version res = new Version();
            using (DbDataReader dr = Select("select version()")) {
                if (dr.Read()) {
                    string v = dr.GetString(0);
                    if (!string.IsNullOrEmpty(v) && v.StartsWith("PostgreSQL ")) {
                        int idx = v.IndexOf(',');
                        if (idx >= 0)
                            v = v.Substring(11, idx - 11);
                        string[] parts = v.Split(new char[] { '.' });
                        if (parts.Length >= 2)
                            res = new Version(int.Parse(parts[0]), int.Parse(parts[1]), 0);
                    }
                }
            }
            return res;
        } // GetVersion

        public override long GetNextCounterValue(string counterName)
        {
            long res = 0;
            while (true) {
                string savepoint = $"s_{ GetUniqueName() }";
                try {
                    if (m_Trans != null)
                        ExecuteNonQuery($"SAVEPOINT { savepoint }");
                    using (DbDataReader dr = Select($"select nextval('{ counterName }')")) {
                        if (dr.Read())
                            res = dr.GetInt64(0);
                        else
                            res = -1;
                    }
                    if (m_Trans != null)
                        ExecuteNonQuery($"RELEASE { savepoint }");
                    break;
                } catch (Exception) {
                    if (m_Trans != null)
                        ExecuteNonQuery($"ROLLBACK TO { savepoint }");
                    ExecuteNonQuery($@"CREATE SEQUENCE IF NOT EXISTS { counterName} START 1");
                }
            }
            return res;
        } // GetNextCounterValue
    } // PostgreSqlConnection
}
