﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCore
{
    public partial class CsAlCore : IDisposable
    {
        /// <summary>
        /// The strategy to read soft deleted objects
        /// </summary>
        public enum DeletedObjectsStrategy
        {
            /// <summary>
            /// COnsider only soft deleted objects
            /// </summary>
            OnlyDeleted,
            /// <summary>
            /// Consider only non soft deleted objects
            /// </summary>
            OnlyLive,
            /// <summary>
            /// Consider all objects
            /// </summary>
            All,
        }

        /// <summary>
        /// Cache containing parsed queries
        /// </summary>
        Common.Utility.ObjectCache m_QueryCache = null;

        /// <summary>
        /// Search objects using the given query
        /// </summary>
        /// <param name="query">The query</param>
        /// <param name="objectType">The object type</param>
        /// <param name="pars">The query parameters</param>
        /// <param name="deleted">The <see cref="DeletedObjectsStrategy"/> to use</param>
        /// <returns>A List of <see cref="Common.IdName"/> of the matching objects</returns>
        public List<Common.IdName> SearchObjs(string query, out Common.Schema.ObjectType objectType, List<Query.QueryParameter> pars = null, DeletedObjectsStrategy deleted = DeletedObjectsStrategy.OnlyLive)
        {
            objectType = null;
            string key = Common.Utility.String.GetMD5Hash(query);

            using (DB.DbConnectionBase conn = NewConnection()) {
                Query.EvalResult res = m_QueryCache?.GetValue<Query.EvalResult>(key);
                if (res == null) {
                    Query.QueryEvaluator qe = new Query.QueryEvaluator(DbSchema, conn);
                    WriteLog(Common.Utility.LogFile.LogLevels.Debug, $"Parsing query: { query }");
                    res = qe.GetSql(DbSchema, conn, ObjectTypes, query, pars, deleted);
                    // Do not cache parameters
                    m_QueryCache.AddOrUpdateValue(key, new Query.EvalResult() { Sql = res.Sql, ObjectType = res.ObjectType });
                } else {
                    // TODO If a cached query is executed without a parameter we get a SQL exception
                    // because the QueryEvaluator isn't used
                    res.Pars = new List<DB.SqlParameterWithValue>();
                    if (pars != null) {
                        foreach (Query.QueryParameter qp in pars)
                            res.Pars.Add(new DB.SqlParameterWithValue(qp.Name, qp.Value));
                    }
                }

                objectType = res.ObjectType;
                return conn.GetIdNames(res.Sql, res.Pars);
            }
        } // SearchObjs

        /// <summary>
        /// Evaluates a query and returns the corresponding SQL.
        /// Used for unit testing
        /// </summary>
        /// <param name="query">The query</param>
        /// <param name="pars">The query parameters</param>
        /// <param name="deleted">The <see cref="DeletedObjectsStrategy"/> to use</param>
        /// <returns>The corresponding SQL</returns>
        public string EvaluateQuery(string query, List<Query.QueryParameter> pars, DeletedObjectsStrategy deleted)
        {
            using (DB.DbConnectionBase conn = NewConnection()) {
                Query.QueryEvaluator qe = new Query.QueryEvaluator(DbSchema, conn);
                Query.EvalResult res = qe.GetSql(DbSchema, conn, ObjectTypes, query, pars, deleted);
                return res.Sql;
            }
        } // EvaluateQuery

        /// <summary>
        /// Evaluates a query and returns the corresponding SQL.
        /// Used for unit testing
        /// </summary>
        /// <param name="schema">The <see cref="Common.Schema.Schema"/></param>
        /// <param name="objectTypeIds">The list of <see cref="Common.IdName"/> with object types informations</param>
        /// <param name="conn">The <see cref="DB.DbConnectionBase"/></param>
        /// <param name="query">The query</param>
        /// <param name="pars">The query parameters</param>
        /// <param name="deleted">The <see cref="DeletedObjectsStrategy"/> to use</param>
        /// <returns>The corresponding SQL</returns>
        public static string EvaluateQuery(Common.Schema.Schema schema, List<Common.IdName> objectTypeIds, DB.DbConnectionBase conn, string query, List<Query.QueryParameter> pars, DeletedObjectsStrategy deleted)
        {
            Query.QueryEvaluator qe = new Query.QueryEvaluator(schema, conn);
            Query.EvalResult res = qe.GetSql(schema, conn, objectTypeIds, query, pars, deleted);
            return res.Sql;
        } // EvaluateQuery
    }
}
