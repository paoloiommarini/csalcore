﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace CsAlCore.DataNavigator
{
    /// <summary>
    /// Data navigator used to apply XSLT to a single object (used for object validation and computed attributes)
    /// </summary>
    class ObjectNavigator : XPathNavigator
    {
        #region classes
        class MultiReferenceNav
        {
            public MultiReferenceNav()
            {
                ValueIndex = 0;
                SubAttributesIndex = 0;
            }

            public ObjectAttribute Attribute { get; set; }
            public List<MultiReferenceValue> Values { get; set; }
            public int ValueIndex { get; set; }
            public int SubAttributesIndex { get; set; }

            public MultiReferenceValue MultiReferenceValue
            {
                get {
                    if (ValueIndex >= 0 && ValueIndex < Values.Count)
                        return Values[ValueIndex];
                    return null;
                }
            }

            public GenericAttribute SubAttribute
            {
                get {
                    if (MultiReferenceValue != null && SubAttributesIndex >= 0 && SubAttributesIndex < Attribute.SubAttributes.Count)
                        return MultiReferenceValue.SubAttributes.GetAttribute(Attribute.SubAttributes[SubAttributesIndex].Name);
                    return null;
                }
            }
        }
        #endregion

        CsAlCore m_CsAl = null;
        object m_Obj = null;
        string m_CurrentName = string.Empty;
        XPathNodeType m_CurrentNodeType = XPathNodeType.Root;
        List<object> m_Parents = new List<object>();
        GenericAttribute m_CurrentAttribute = null;
        NameTable m_NameTable = new NameTable();
        List<string> m_BinaryDataAttrs = null;

        public ObjectNavigator(CsAlCore csal, GenericObject obj)
        {
            if (csal == null)
                throw new ArgumentNullException("csal");
            if (obj == null)
                throw new ArgumentNullException("obj");

            m_CsAl = csal;
            m_Obj = obj;
            m_CurrentNodeType = XPathNodeType.Root;

            m_BinaryDataAttrs = new List<string>();
            foreach (PropertyInfo pi in typeof(BinaryDataValue).GetProperties(BindingFlags.Instance | BindingFlags.Public)) {
                string name = $"{ char.ToLower(pi.Name[0]) }{ pi.Name.Substring(1) }";
                m_BinaryDataAttrs.Add(name);
            }
        }

        private ObjectNavigator(CsAlCore csal, object obj)
        {
            if (csal == null)
                throw new ArgumentNullException("csal");
            if (obj == null)
                throw new ArgumentNullException("obj");

            if (!(obj is GenericObject) && !(obj is BinaryDataValue) && !(obj is MultiReferenceNav))
                throw new ArgumentOutOfRangeException("obj", "obj must be a GenericObject, BinaryDataValue or MultiReferenceNav");

            m_CsAl = csal;
            m_Obj = obj;
            m_CurrentNodeType = XPathNodeType.Root;

            m_BinaryDataAttrs = new List<string>();
            foreach (PropertyInfo pi in typeof(BinaryDataValue).GetProperties(BindingFlags.Instance | BindingFlags.Public)) {
                string name = $"{ char.ToLower(pi.Name[0]) }{ pi.Name.Substring(1) }";                
                m_BinaryDataAttrs.Add(name);
            }
        }
        
        private GenericObject GenericObject
        {
            get {
                return m_Obj as GenericObject;
            }
        }

        private BinaryDataValue BinaryDataValue
        {
            get {
                return m_Obj as BinaryDataValue;
            }
        }

        private MultiReferenceNav MultiReferenceValues
        {
            get {
                return m_Obj as MultiReferenceNav;
            }
        }

        public override string BaseURI
        {
            get {
                return string.Empty;
            }
        }

        public override bool IsEmptyElement => throw new NotImplementedException();

        public override string LocalName
        {
            get {
                if (!string.IsNullOrEmpty(m_CurrentName))
                    return m_CurrentName;

                if (m_CurrentAttribute == null)
                    return GenericObject.ObjectType.Name;

                return m_CurrentAttribute.Name;
            }
        }

        public override string Name
        {
            get {
                if (!string.IsNullOrEmpty(m_CurrentName))
                    return m_CurrentName;

                if (m_CurrentAttribute == null)
                    return GenericObject.ObjectType.Name;
                return m_CurrentAttribute.Name;
            }
        }

        public override string NamespaceURI
        {
            get {
                return string.Empty;
            }
        }

        public override XmlNameTable NameTable
        {
            get {
                return m_NameTable;
            }
        }

        public override XPathNodeType NodeType
        {
            get {
                return m_CurrentNodeType;
            }
        }

        public override string Prefix
        {
            get {
                return string.Empty;
            }
        }

        public override string Value
        {
            get {
                if (BinaryDataValue != null) {
                    if (m_BinaryDataAttrs.Contains(LocalName)) {
                        string name = $"{ char.ToUpper(LocalName[0]) }{ LocalName.Substring(1) }";
                        return BinaryDataValue.GetType().GetProperty(name).GetValue(BinaryDataValue, null).ToString();
                    }
                }

                if (MultiReferenceValues != null) {
                    ObjectAttribute sAt = MultiReferenceValues.Attribute.GetSubAttribute(LocalName);
                    if (sAt != null) {
                        return MultiReferenceValues.MultiReferenceValue.SubAttributes.GetAttribute(sAt.Name).Value.ToString();
                    }
                }

                if (m_CurrentAttribute != null)
                    return m_CurrentAttribute.Value.ToString();
                return null;
            }
        }

        public override XPathNavigator Clone()
        {
            ObjectNavigator res = new ObjectNavigator(m_CsAl, m_Obj);
            res.m_CurrentName = m_CurrentName;
            res.m_CurrentNodeType = m_CurrentNodeType;
            res.m_CurrentAttribute = m_CurrentAttribute;
            res.m_Parents = m_Parents;
            return res;
        }

        public override bool IsSamePosition(XPathNavigator other)
        {
            throw new NotImplementedException();
        }

        public override bool MoveTo(XPathNavigator other)
        {
            return MoveToAttribute(other.Name, other.NamespaceURI);
        }

        public override bool MoveToFirstNamespace(XPathNamespaceScope namespaceScope)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToId(string id)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToNextNamespace(XPathNamespaceScope namespaceScope)
        {
            throw new NotImplementedException();
        }

        #region Attributes navigation
        /// <summary>
        /// Move to first attribute
        /// </summary>
        /// <returns>True on success</returns>        
        public override bool MoveToFirstAttribute()
        {
            if (MultiReferenceValues != null) {
                // Check subattributes
                if (MultiReferenceValues.Attribute.SubAttributes != null && MultiReferenceValues.Attribute.SubAttributes.Count > 0) {
                    foreach (ObjectAttribute sAt in MultiReferenceValues.Attribute.SubAttributes) {
                        if (sAt.IsAttribute) {
                            m_CurrentName = string.Empty;
                            m_CurrentNodeType = XPathNodeType.Element;
                            m_CurrentAttribute = MultiReferenceValues.MultiReferenceValue.SubAttributes.GetAttribute(MultiReferenceValues.Attribute.SubAttributes[MultiReferenceValues.SubAttributesIndex].Name);
                            return true;
                        }
                    }
                    return false;
                }
            }

            for (int i = 0; i < GenericObject.ObjectType.AttributeNames.Count; i++) {
                ObjectAttribute at = GenericObject.ObjectType.GetAttribute(GenericObject.ObjectType.Attributes[i].Name);
                if (!at.SystemManaged && at.IsAttribute) {
                    m_CurrentAttribute = GenericObject.GetAttribute(at.Name);
                    m_CurrentNodeType = XPathNodeType.Attribute;
                    return true;
                }
            }
            return false;
        } // MoveToFirstAttribute

        /// <summary>
        /// Move to the given attribute
        /// </summary>
        /// <returns>True on success</returns>
        public override bool MoveToAttribute(string localName, string namespaceURI)
        {
            if (MultiReferenceValues != null) {
                if (localName == Schema.RefElementName) {
                    m_CurrentName = LocalName;
                    return true;
                }

                // Sub attributes:
                ObjectAttribute sat = MultiReferenceValues.Attribute.GetSubAttribute(localName);
                if (sat != null && sat.IsAttribute) {
                    m_CurrentAttribute = null;
                    m_CurrentName = localName;
                    m_CurrentNodeType = XPathNodeType.Attribute;
                    return true;
                }

                return false;
            }

            if (BinaryDataValue != null) {
                if (m_BinaryDataAttrs.Contains(localName)) {
                    m_CurrentAttribute = null;
                    m_CurrentName = localName;
                    m_CurrentNodeType = XPathNodeType.Attribute;
                    return true;
                }
                return false;
            }

            if (localName == GenericObject.ObjectType.Name) {
                m_CurrentAttribute = null;
                return true;
            }

            GenericAttribute at = GenericObject.GetAttribute(localName);
            if (at != null) {
                m_CurrentAttribute = at;
                m_CurrentNodeType = XPathNodeType.Attribute;
                return true;
            }

            return false;
        } // MoveToAttribute

        /// <summary>
        /// Move to next attribute
        /// </summary>
        /// <returns>True on success</returns>
        public override bool MoveToNextAttribute()
        {
            if (MultiReferenceValues != null) {
                if (LocalName == Schema.RefElementName)
                    return true;

                // Sub attributes:
                for (int i = MultiReferenceValues.SubAttributesIndex; i < MultiReferenceValues.Attribute.SubAttributes.Count; i++) {
                    ObjectAttribute sat = MultiReferenceValues.Attribute.GetSubAttribute(MultiReferenceValues.Attribute.SubAttributes[i].Name);
                    if (sat != null && sat.IsAttribute) {
                        MultiReferenceValues.SubAttributesIndex = i;
                        m_CurrentAttribute = MultiReferenceValues.MultiReferenceValue.SubAttributes.GetAttribute(sat.Name);
                        m_CurrentNodeType = XPathNodeType.Attribute;
                        return true;
                    }
                }

                return false;
            }

            if (m_CurrentAttribute == null)
                return false;

            int idx = GenericObject.ObjectType.AttributeNames.IndexOf(m_CurrentAttribute.Name);
            if (idx < GenericObject.ObjectType.AttributeNames.Count) {
                for (int i = idx + 1; i < GenericObject.ObjectType.AttributeNames.Count; i++) {
                    ObjectAttribute at = GenericObject.ObjectType.GetAttribute(GenericObject.ObjectType.AttributeNames[i]);
                    if (!at.SystemManaged && at.IsAttribute) {
                        m_CurrentAttribute = GenericObject.GetAttribute(at.Name);
                        m_CurrentNodeType = XPathNodeType.Attribute;
                        return true;
                    }
                }
            }
            return false;
        } // MoveToNextAttribute
        #endregion

        #region Elements navigation
        /// <summary>
        /// Move to first child element
        /// </summary>
        /// <returns>True on success</returns>
        public override bool MoveToFirstChild()
        {
            if (MultiReferenceValues != null) {
                // Check subattributes
                if (MultiReferenceValues.Attribute.SubAttributes != null && MultiReferenceValues.Attribute.SubAttributes.Count > 0) {
                    for (int i = 0; i < MultiReferenceValues.Attribute.SubAttributes.Count; i++){
                        ObjectAttribute sAt = MultiReferenceValues.Attribute.SubAttributes[i];
                        if (sAt.IsElement) {
                            MultiReferenceValues.SubAttributesIndex = i;
                            m_CurrentName = string.Empty;
                            m_Parents.Add(m_Obj);
                            m_CurrentNodeType = XPathNodeType.Element;
                            m_CurrentAttribute = MultiReferenceValues.MultiReferenceValue.SubAttributes.GetAttribute(MultiReferenceValues.Attribute.SubAttributes[MultiReferenceValues.SubAttributesIndex].Name);
                            return true;
                        }
                    }
                    return false;
                } else {
                    m_CurrentName = MultiReferenceValues.Attribute.RefObjectType;
                    m_CurrentNodeType = XPathNodeType.Element;
                    m_Parents.Add(m_Obj);
                    m_Obj = MultiReferenceValues.MultiReferenceValue.RefObject;
                    return true;
                }
            } else if (m_CurrentAttribute?.Attribute.Type == ObjectAttribute.Types.Reference ||
                       m_CurrentAttribute?.Attribute.Type == ObjectAttribute.Types.BinaryData ||
                       m_CurrentAttribute?.Attribute.Type == ObjectAttribute.Types.MultiReference) {
                m_Parents.Add(m_Obj);
                SetCurrentObject();
                m_CurrentAttribute = null;
                m_CurrentNodeType = XPathNodeType.Element;
                return true;
            } else {
                foreach (ObjectAttribute at in GenericObject.ObjectType.Attributes) {
                    if (!at.SystemManaged && at.IsElement) {
                        m_CurrentName = string.Empty;
                        m_CurrentAttribute = GenericObject.GetAttribute(at.Name);
                        m_Parents.Add(m_Obj);
                        m_CurrentNodeType = XPathNodeType.Element;
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Move to parent element
        /// </summary>
        /// <returns>True on success</returns>
        public override bool MoveToParent()
        {
            if (m_Parents.Count > 0) {
                m_Obj = m_Parents[m_Parents.Count - 1];
                m_CurrentAttribute = null;
                m_CurrentName = string.Empty;
                m_Parents.RemoveAt(m_Parents.Count - 1);
                m_CurrentNodeType = XPathNodeType.Element;
                return true;
            } else if (m_CurrentAttribute != null) {
                //m_CurrentAttribute = null;
                //return true;
            }
            return false;
        }

        /// <summary>
        /// Move to next element
        /// </summary>
        /// <returns>True on success</returns>
        public override bool MoveToNext()
        {
            if (MultiReferenceValues != null) {
                if (LocalName == Schema.RefElementName) {
                    if (MultiReferenceValues.ValueIndex + 1 < MultiReferenceValues.Values.Count) {
                        MultiReferenceValues.ValueIndex++;
                        return true;
                    }
                    return false;
                }

                // Sub attributes:
                for (int i = MultiReferenceValues.SubAttributesIndex + 1; i < MultiReferenceValues.Attribute.SubAttributes.Count; i++) {
                    ObjectAttribute sat = MultiReferenceValues.Attribute.GetSubAttribute(MultiReferenceValues.Attribute.SubAttributes[i].Name);
                    if (sat != null && sat.IsElement) {
                        MultiReferenceValues.SubAttributesIndex = i;
                        m_Parents.Add(m_Obj);
                        m_CurrentNodeType = XPathNodeType.Element;
                        m_CurrentAttribute = MultiReferenceValues.MultiReferenceValue.SubAttributes.GetAttribute(MultiReferenceValues.Attribute.SubAttributes[MultiReferenceValues.SubAttributesIndex].Name);
                        return true;
                    }
                }

                // Referenced object:
                if (LocalName != MultiReferenceValues.Attribute.RefObjectType) {
                    m_CurrentAttribute = null;
                    m_CurrentName = MultiReferenceValues.Attribute.RefObjectType;
                    m_CurrentNodeType = XPathNodeType.Element;
                    m_Parents.Add(m_Obj);
                    m_Obj = MultiReferenceValues.MultiReferenceValue.RefObject;
                    return true;
                }

                return false;
            }

            if (m_CurrentAttribute == null)
                return false;

            if (m_CurrentAttribute.Attribute.IsElement) {
                ObjectType ot = m_CsAl.DbSchema.GetObjectType(m_CurrentAttribute.Attribute.ObjectTypeName);
                int idx = ot.AttributeNames.IndexOf(m_CurrentAttribute.Name);
                if (idx < ot.AttributeNames.Count) {
                    for (int i = idx + 1; i < ot.AttributeNames.Count; i++) {
                        ObjectAttribute at = ot.GetAttribute(ot.AttributeNames[i]);
                        if (!at.SystemManaged && at.IsElement) {
                            m_CurrentName = string.Empty;
                            m_CurrentAttribute = GenericObject.GetAttribute(at.Name);
                            m_CurrentNodeType = XPathNodeType.Element;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Move to previous element
        /// </summary>
        /// <returns>True on success</returns>
        public override bool MoveToPrevious()
        {
            if (m_CurrentAttribute == null)
                return false;

            if (m_CurrentAttribute.Attribute.IsElement) {
                int idx = GenericObject.ObjectType.AttributeNames.IndexOf(m_CurrentAttribute.Name);
                if (idx > 0) {
                    for (int i = idx - 1; i >= 0; i--) {
                        ObjectAttribute at = GenericObject.ObjectType.GetAttribute(GenericObject.ObjectType.AttributeNames[i]);
                        if (!at.SystemManaged && at.IsElement) {
                            m_CurrentName = string.Empty;
                            m_CurrentAttribute = GenericObject.GetAttribute(at.Name);
                            m_CurrentNodeType = XPathNodeType.Element;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        #endregion

        #region private operations
        private void SetCurrentObject()
        {
            GenericAttribute val = GenericObject.GetAttribute(m_CurrentAttribute.Name);
            if (m_CurrentAttribute.Attribute.AttributeType.Type == ObjectAttribute.Types.Reference) {
                if (val.Value is GenericObject) {
                    m_Obj = val.Value as GenericObject;
                    m_CurrentName = m_CurrentAttribute.Attribute.RefObjectType;
                } else if (val.Value is long) {
                    m_Obj = m_CsAl.LoadObject((long)val.Value);
                    m_CurrentName = m_CurrentAttribute.Attribute.RefObjectType;
                }                
            } else if (m_CurrentAttribute.Attribute.AttributeType.Type == ObjectAttribute.Types.BinaryData) {
                if (val.Value is BinaryDataValue) {
                    m_Obj = val.Value as BinaryDataValue;
                    m_CurrentName = Schema.BinaryDataElementName;
                } else if (val.Value is long) {
                    m_Obj = m_CsAl.GetBinaryData((long)val.Value, false);
                    m_CurrentName = Schema.BinaryDataElementName;
                }                
            } else if (m_CurrentAttribute.Attribute.AttributeType.Type == ObjectAttribute.Types.MultiReference) {
                if (val.Value is List<MultiReferenceValue>) {
                    List<MultiReferenceValue> mv = val.Value as List<MultiReferenceValue>;
                    List<long> mrToLoadIds = new List<long>();
                    List<MultiReferenceValue> mrToLoad = new List<MultiReferenceValue>();
                    foreach (MultiReferenceValue mrv in mv) {
                        if (mrv.RefObject == null)
                            mrToLoad.Add(mrv);
                            mrToLoadIds.Add(mrv.RefId);
                    }

                    if (mrToLoadIds.Count  > 0) {
                        List<GenericObject> objs = m_CsAl.BulkLoadObjects(mrToLoadIds);
                        for (int i=0; i<objs.Count; i++) {
                            mrToLoad[i].RefObject = objs[i];
                        }
                    }

                    m_Obj = new MultiReferenceNav()
                    {
                        Attribute = m_CurrentAttribute.Attribute,
                        Values = mv
                    };
                    m_CurrentName = Schema.RefElementName;
                }                
            }
        } // SetCurrentObject
        #endregion

    }
}
