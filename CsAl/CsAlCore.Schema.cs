﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsAlCore
{
    public partial class CsAlCore
    {
        /// <summary>
        /// Schema upgrade step type
        /// </summary>
        public enum SchemaUpgradeStepType
        {
            /// <summary>
            /// Delete object type/attribute
            /// </summary>
            Delete,
            /// <summary>
            /// Create object type/attribute
            /// </summary>
            Create,
            /// <summary>
            /// Modify object type/attribute
            /// </summary>
            Modify
        }

        /// <summary>
        /// Schema upgrade step delegate
        /// </summary>
        /// <param name="type">The step type </param>
        /// <param name="name">The object type/attribute name</param>
        public delegate void SchemaUpgradeStep(SchemaUpgradeStepType type, string name);

        /// <summary>
        /// Compare two attributes list
        /// </summary>
        /// <param name="oldAttrs">The old attributes</param>
        /// <param name="newAttrs">The new attrivutes</param>
        /// <returns>A <see cref="AttributesDifferences"/></returns>
        public static AttributesDifferences CompareAttributes(List<ObjectAttribute> oldAttrs, List<ObjectAttribute> newAttrs)
        {
            AttributesDifferences res = new AttributesDifferences();

            foreach (ObjectAttribute na in newAttrs) {
                ObjectAttribute osa = oldAttrs.Where(a => a.Name == na.Name).FirstOrDefault();
                if (osa == null)
                    res.NewObjectAttributes.Add(na);
                else if (!osa.AttributeType.IsEqual(na.AttributeType))
                    res.ModifiedObjectAttributes.Add(na);
            }
            foreach (ObjectAttribute oa in oldAttrs) {
                ObjectAttribute nsa = newAttrs.Where(a => a.Name == oa.Name).FirstOrDefault();
                if (nsa == null)
                    res.RemovedObjectAttributes.Add(oa);
            }

            return res;
        } // AttributesDifferences

        /// <summary>
        /// Compare a schema with the current one
        /// </summary>
        /// <param name="newSchema">The schema to compare</param>
        /// <returns>A <see cref="SchemaDifferences"/></returns>
        public SchemaDifferences CompareSchema(Schema newSchema)
        {
            if (DbSchema == null)
                throw new Common.Exceptions.NoSchemaFoundError();

            if (DbSchema.Name != newSchema.Name)
                throw new Common.Exceptions.WrongSchemaName(newSchema.Name);
            if (DbSchema.Revision >= newSchema.Revision)
                throw new Common.Exceptions.WrongSchemaRevision(newSchema.Revision);

            SchemaDifferences res = new SchemaDifferences();

            // Check removed object types:
            foreach (string otn in DbSchema.ObjectTypeNames) {
                if (!newSchema.ObjectTypeNames.Contains(otn))
                    res.RemovedObjectTypes.Add(DbSchema.GetObjectType(otn));
            }

            // Check new object types:
            foreach (string otn in newSchema.ObjectTypeNames) {
                if (!DbSchema.ObjectTypeNames.Contains(otn))
                    res.NewObjectTypes.Add(newSchema.GetObjectType(otn));
            }

            // Check object types differences:
            foreach (ObjectType newOt in newSchema.ObjectTypes) {
                if (res.NewObjectTypes.Contains(newOt) || res.RemovedObjectTypes.Contains(newOt))
                    continue;

                ObjectType oldOt = DbSchema.GetObjectType(newOt.Name);

                // Removed attributes:
                foreach (ObjectAttribute oldAt in oldOt.Attributes) {
                    ObjectAttribute newAt = newOt.GetAttribute(oldAt.Name);
                    if (newAt == null)
                        res.RemovedObjectAttributes.Add(oldAt);
                }

                // New and modified attributes:
                foreach (ObjectAttribute newAt in newOt.Attributes) {
                    ObjectAttribute oldAt = oldOt.GetAttribute(newAt.Name);
                    if (oldAt == null) {
                        res.NewObjectAttributes.Add(newAt);
                    } else {
                        if (oldAt.AttributeType.Type != newAt.AttributeType.Type)
                            throw new Common.Exceptions.CannotModifyAttributeTypeError($"{ oldAt.ObjectTypeName }.{ oldAt.Name }");
                        if (oldAt.AttributeType.Type == ObjectAttribute.Types.Reference) {
                            Common.Schema.AttributeTypes.ReferenceAttrType oldrAt = oldAt.AttributeType as Common.Schema.AttributeTypes.ReferenceAttrType;
                            Common.Schema.AttributeTypes.ReferenceAttrType newrAt = newAt.AttributeType as Common.Schema.AttributeTypes.ReferenceAttrType;
                            if (oldrAt.RefObjectType != newrAt.RefObjectType)
                                throw new Common.Exceptions.CannotChangeReferencedObjectTypeError($"{ oldAt.ObjectTypeName }.{ oldAt.Name }");
                        }

                        if (!oldAt.AttributeType.IsEqual(newAt.AttributeType)) {
                            if (oldAt.AttributeType.Type == ObjectAttribute.Types.MultiReference && !oldAt.ChildrenReference) {
                                // Check subattributes:
                                foreach (ObjectAttribute oldSat in oldAt.SubAttributes) {
                                    ObjectAttribute newSat = newAt.GetSubAttribute(oldSat.Name);
                                    if (newSat != null) {
                                        if (oldSat.AttributeType.Type != newSat.AttributeType.Type)
                                            throw new Common.Exceptions.CannotModifyAttributeTypeError($"{ oldAt.ObjectTypeName }.{ oldAt.Name }.{ oldSat.Name }");
                                    }
                                }
                            }
                            res.ModifiedObjectAttributes.Add(newAt);
                        }
                    }
                }

                // Removed indexes:
                foreach (ObjectIndex oldIdx in oldOt.Indexes) {
                    ObjectIndex newIdx = newOt.GetIndex(oldIdx.Name);
                    if (newIdx == null) {
                        List<ObjectIndex> oids = null;
                        if (!res.RemovedObjectIndexes.TryGetValue(newOt, out oids)) {
                            oids = new List<ObjectIndex>();
                            res.RemovedObjectIndexes[newOt] = oids;
                        }

                        oids.Add(oldIdx);
                    }
                }

                // New and modified indexes:
                foreach (ObjectIndex newIdx in newOt.Indexes) {
                    ObjectIndex oldIdx = oldOt.GetIndex(newIdx.Name);
                    if (oldIdx == null) {
                        List<ObjectIndex> oids = null;
                        if (!res.NewObjectIndexes.TryGetValue(newOt, out oids)) {
                            oids = new List<ObjectIndex>();
                            res.NewObjectIndexes[newOt] = oids;
                        }

                        oids.Add(newIdx);
                    } else if (!oldIdx.IsEqual(oldIdx)) {
                        List<ObjectIndex> oids = null;
                        if (!res.ModifiedObjectIndexes.TryGetValue(newOt, out oids)) {
                            oids = new List<ObjectIndex>();
                            res.ModifiedObjectIndexes[newOt] = oids;
                        }
                        oids.Add(newIdx);
                    }
                }
            }
            return res;
        } // CompareSchema

        /// <summary>
        /// Upgrade the database schema, creates/alters tables
        /// </summary>
        /// <example>
        /// CsAl.Configuration config = new CsAl.Configuration()
        /// {
        /// 	DbType = "SQLite",
        ///     DbConnectionString = "Data Source=test.sqlite;"
        /// };
        ///
        /// Schema newSchema = Schema.Load("TestSchema.xml");
        /// using (CsAl.CsAl csal = new CsAl.CsAl(config)) {
        ///   if (csal.Open()) {
        ///     csal.UpgradeSchema(newSchema);
        ///   }
        /// }
        /// </example>
        /// <param name="newSchema">The schema to import</param>
        /// <returns>True on success</returns>
        public bool UpgradeSchema(Schema newSchema)
        {
            return UpgradeSchema(newSchema, null);
        } // UpgradeSchema

        /// <summary>
        /// Upgrade the database schema, creates/alters tables
        /// </summary>
        /// <param name="newSchema">The schema to import</param>
        /// <param name="callback">The callback to call on each object type</param>
        /// <returns>True on success</returns>
        public bool UpgradeSchema(Schema newSchema, SchemaUpgradeStep callback)
        {
            if (DbSchema != null) {
                if (DbSchema.Name != newSchema.Name)
                    throw new Common.Exceptions.WrongSchemaName(newSchema.Name);
                if (DbSchema.Revision >= newSchema.Revision)
                    throw new Common.Exceptions.WrongSchemaRevision(newSchema.Revision);
            }

            Schema oldSchema = DbSchema;
            SchemaDifferences diff = oldSchema != null ? CompareSchema(newSchema) : null;
            DbSchema = newSchema;
            using (DB.DbConnectionBase conn = NewConnection()) {
                if (diff == null) {
                    // New schema
                    foreach (ObjectType ot in newSchema.ObjectTypes) {
                        callback?.Invoke(SchemaUpgradeStepType.Create, ot.Name);
                        conn.CreateObjectType(newSchema, ot);
                        if (!ot.SystemObject) {
                            GenericObject oto = GenericObject.Create(newSchema.GetObjectType("objectType"));
                            if (oto != null) {
                                oto.SetAttributeValue("name", ot.Name);
                                oto.SetAttributeValue("description", ot.Description);
                                CreateObjectComplete(conn, oto);
                            }
                        }
                    }
                } else {
                    // Upgrade schema
                    foreach (ObjectType ot in diff.NewObjectTypes) {
                        callback?.Invoke(SchemaUpgradeStepType.Create, ot.Name);
                        conn.CreateObjectType(newSchema, ot);
                        if (!ot.SystemObject) {
                            GenericObject oto = GenericObject.Create(newSchema.GetObjectType("objectType"));
                            if (oto != null) {
                                oto.SetAttributeValue("name", ot.Name);
                                oto.SetAttributeValue("description", ot.Description);
                                CreateObjectComplete(conn, oto);
                            }
                        }
                    }

                    foreach (ObjectAttribute at in diff.NewObjectAttributes) {
                        conn.AddObjectAttribute(newSchema, at);
                    }

                    foreach (KeyValuePair<ObjectType, List<ObjectIndex>> kvp in diff.RemovedObjectIndexes) {
                        foreach (ObjectIndex idx in kvp.Value) {
                            conn.DeleteIndex(kvp.Key, idx);
                        }
                    }

                    foreach (ObjectAttribute at in diff.RemovedObjectAttributes) {
                        conn.DeleteObjectAttribute(newSchema, GetBinaryDataRoot(), at);
                    }

                    foreach (ObjectAttribute at in diff.ModifiedObjectAttributes) {
                        conn.ModifyObjectAttribute(newSchema, oldSchema.GetObjectType(at.ObjectTypeName).GetAttribute(at.Name), at);
                    }

                    diff.RemovedObjectTypes.Reverse();
                    foreach (ObjectType ot in diff.RemovedObjectTypes) {
                        callback?.Invoke(SchemaUpgradeStepType.Delete, ot.Name);
                        conn.DeleteObjectType(oldSchema, ot);
                    }

                    foreach (KeyValuePair<ObjectType, List<ObjectIndex>> kvp in diff.NewObjectIndexes) {
                        foreach (ObjectIndex idx in kvp.Value) {
                            try {
                                // Workaround for SQLite.
                                // Since we need to drop and recreate modified tables the added indexes might exist here
                                conn.DeleteIndex(kvp.Key, idx);
                            } catch (Exception) { }
                            conn.CreateIndex(kvp.Key, idx);
                        }
                    }

                    foreach (KeyValuePair<ObjectType, List<ObjectIndex>> kvp in diff.ModifiedObjectIndexes) {
                        foreach (ObjectIndex idx in kvp.Value) {
                            conn.DeleteIndex(kvp.Key, idx);
                            conn.CreateIndex(kvp.Key, idx);
                        }
                    }
                }

                // Save the schema
                LoadObjectTypes(conn);
                GenericObject obj = GenericObject.Create(DbSchema.GetObjectType("schema"));
                string xml = Common.Utility.Serialization.Serialize(DbSchema);
                obj.SetAttributeValue("schema", Encoding.UTF8.GetBytes(xml));
                obj.SetAttributeValue("name", DbSchema.Name);
                obj.SetAttributeValue("revision", DbSchema.Revision);
                obj.SetAttributeValue("systemSchemaRevision", DbSchema.SystemSchemaRevision);
                obj.SetAttributeValue("csAlVersion", Version.Version.ToString());

                List<MultiReferenceValue> sFiles = new List<MultiReferenceValue>();
                foreach (Schema.SchemaFile sf in DbSchema.Files) {
                    GenericObject sfObj = GenericObject.Create(DbSchema.GetObjectType("schemaFile"));
                    sfObj.SetAttributeValue("name", sf.FileName);
                    sfObj.SetAttributeValue("file", Encoding.UTF8.GetBytes(sf.Content));                    
                    sFiles.Add(new MultiReferenceValue(sfObj));
                }
                obj.SetAttributeValue("files", sFiles);

                CreateObjectComplete(conn, obj);
                return true;
            }
        } // UpgradeSchema
    }
}
