﻿using System;
using System.Collections.Generic;
using System.Text;
using CsAlCore.Common.Schema;

namespace CsAlCore
{
    public partial class CsAlCore : IDisposable
    {
        /// <summary>
        /// Get the binary data with the given id
        /// </summary>
        /// <param name="id">The binary data Id</param>
        /// <param name="includeStream">If true the Stream attribute is set</param>
        /// <returns>A <see cref="BinaryDataValue"/></returns>
        public BinaryDataValue GetBinaryData(long id, bool includeStream)
        {
            BinaryDataValue res = null;
            using (DB.DbConnectionBase conn = NewConnection()) {
                res = conn.GetBinaryData(GetBinaryDataRoot(), id, includeStream);
            }
            return res;
        } // GetBinaryData
    }
}