﻿using CsAlCore.Common.Schema;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common
{
    /// <summary>
    /// Base class for all the generated schema object classes
    /// </summary>
    public abstract class SchemaObject
    {
        public abstract class MultiReferenceValueBase
        {
            MultiReferenceValue m_Value = null;
            protected GenericObject m_SubAttributes = null;
            string m_RefAttrObjectTypeName = string.Empty;
            string m_RefAttrName = string.Empty;

            #region Public properties
            public long RefId
            {
                get { return m_Value.RefId; }
            }

            public GenericObject RefObject
            {
                get { return m_Value.RefObject; }
            }
            #endregion

            #region Constructors
            public MultiReferenceValueBase(string refAttrObjectTypeName, string refAttrName, MultiReferenceValue value)
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                m_RefAttrObjectTypeName = refAttrObjectTypeName;
                m_RefAttrName = refAttrName;

                m_Value = value;
                m_SubAttributes = m_Value.SubAttributes;
            }

            public MultiReferenceValueBase(string refAttrObjectTypeName, string refAttrName, Schema.Schema schema, long refId)
            {
                m_RefAttrObjectTypeName = refAttrObjectTypeName;
                m_RefAttrName = refAttrName;

                m_Value = new MultiReferenceValue(refId);
                ObjectType ot = schema.GetObjectType(m_RefAttrObjectTypeName);
                ObjectAttribute mrefAt = ot.GetAttribute(m_RefAttrName);
                m_Value.SubAttributes = GenericObject.Create(string.Empty, mrefAt.SubAttributes);
                m_SubAttributes = m_Value.SubAttributes;
            }

            public MultiReferenceValueBase(string refAttrObjectTypeName, string refAttrName, Schema.Schema schema, GenericObject refObj)
            {
                if (refObj == null)
                    throw new ArgumentNullException("refObj");

                m_RefAttrObjectTypeName = refAttrObjectTypeName;
                m_RefAttrName = refAttrName;

                m_Value = new MultiReferenceValue(refObj);
                ObjectType ot = schema.GetObjectType(m_RefAttrObjectTypeName);
                ObjectAttribute mrefAt = ot.GetAttribute(m_RefAttrName);
                m_Value.SubAttributes = GenericObject.Create(string.Empty, mrefAt.SubAttributes);
                m_SubAttributes = m_Value.SubAttributes;
            }
            #endregion

            public MultiReferenceValue ToMultiReferenceValue()
            {
                return m_Value;
            }
        } // MultiReferenceValueBase

        public class MultiReferenceValueList<T> : ICollection<T>, IList<T> where T : MultiReferenceValueBase
        { 
            GenericObject m_Obj = null;
            string m_AttrName = string.Empty;
            List<MultiReferenceValue> m_List = null;

            public MultiReferenceValueList(GenericObject obj, string attributeName)
            {
                if (obj == null)
                    throw new ArgumentNullException("obj");

                m_Obj = obj;
                m_AttrName = attributeName;
                m_List = m_Obj.GetAttributeValue(m_AttrName) as List<MultiReferenceValue>;
                if (m_List == null) {
                    m_List = new List<MultiReferenceValue>();
                    m_Obj.SetAttributeValue(m_AttrName, m_List);
                }
            }

            public T this[int index]
            {
                get {
                    return (T)Activator.CreateInstance(typeof(T), m_List[index]);
                }
                set {
                    m_List[index] = value.ToMultiReferenceValue();
                }
            }

            public List<MultiReferenceValue> AttributeValue
            {
                get { return m_List; }
            }

            public int Count
            {
                get {
                    return m_List.Count;
                }
            }

            public bool IsReadOnly
            {
                get { return false; }
            }

            public void Add(T item)
            {
                m_List.Add(item.ToMultiReferenceValue());
            }

            public void Clear()
            {
                m_List.Clear();
            }

            public bool Contains(T item)
            {
                return m_List.Contains(item.ToMultiReferenceValue());
            }

            public void CopyTo(T[] array, int arrayIndex)
            {
                if (array == null)
                    throw new ArgumentNullException("array");
                T[] mrArray = array as T[];
                if (mrArray == null)
                    throw new ArgumentException();
                ((ICollection<T>)this).CopyTo(mrArray, arrayIndex);
            }

            public IEnumerator<T> GetEnumerator()
            {
                foreach (MultiReferenceValue mrv in m_List) {
                    yield return (T)Activator.CreateInstance(typeof(T), mrv);
                }
            }

            public int IndexOf(T item)
            {
                return m_List.IndexOf(item.ToMultiReferenceValue());
            }

            public void Insert(int index, T item)
            {
                m_List.Insert(index, item.ToMultiReferenceValue());
            }

            public bool Remove(T item)
            {
                return m_List.Remove(item.ToMultiReferenceValue());
            }

            public void RemoveAt(int index)
            {
                m_List.RemoveAt(index);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                foreach (MultiReferenceValue mrv in m_List) {
                    yield return (T)Activator.CreateInstance(typeof(T), mrv);
                }
            }
        } // MultiReferenceValueList

        /// <summary>
        /// The internal <see cref="GenericObject"/>
        /// </summary>
        protected GenericObject m_GenericObject = null;

        /// <summary>
        /// Get the object type name
        /// </summary>
        public abstract string ObjectTypeName
        {
            get;
        }

        /// <summary>
        /// Get if the object has been soft deleted
        /// </summary>
        public bool Deleted
        {
            get {
                if (m_GenericObject != null)
                    return (bool)m_GenericObject.GetAttributeValue(Schema.Schema.ObjectIsDeletedAttributeName);
                return false;
            }
        }

        /// <summary>
        /// Get the object created date time
        /// </summary>
        public DateTime CreatedDateTime
        {
            get {
                if (m_GenericObject != null)
                    return m_GenericObject.CreatedDateTime;
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Get the object last modified date time
        /// </summary>
        public DateTime? LastModifiedDateTime
        {
            get {
                if (m_GenericObject != null)
                    return m_GenericObject.LastModifiedDateTime;
                return null;
            }
        }

        /// <summary>
        /// Set object attributes from a <see cref="GenericObject"/>
        /// </summary>
        /// <param name="obj">The object</param>
        public void SetAttributes(GenericObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            if (obj.ObjectTypeName != ObjectTypeName)
                throw new Exception(string.Format("Cannot set attributes of {0} from {1}", ObjectTypeName, obj.ObjectTypeName));

            foreach (ObjectAttribute oAt in obj.ObjectType.Attributes) {
                m_GenericObject.SetAttributeValue(oAt.Name, obj.GetAttributeValue(oAt.Name));
            }
            m_GenericObject.ResetChanged();
        } // SetAttributes

        /// <summary>
        /// Get a new <see cref="GenericObject"/> for this object
        /// </summary>
        /// <returns>A new <see cref="GenericObject"/></returns>
        public GenericObject ToGenericObject()
        {
            return new GenericObject(m_GenericObject);
        } // ToGenericObject

        /// <summary>
        /// Serialize this object to a string
        /// </summary>
        /// <returns>The serialized object</returns>
        public string Serialize()
        {
            return m_GenericObject.Serialize();
        } // Serialize

        /// <summary>
        /// Serialize this object to a the given <see cref="XmlWriter"/>
        /// </summary>
        /// <returns>True on success</returns>
        public bool Serialize(XmlWriter xmlWriter)
        {
            return m_GenericObject.Serialize(xmlWriter);
        } // Serialize
    } // SchemaObject
}
