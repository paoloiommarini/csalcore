﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CsAlCore.Common.Utility
{
    /// <summary>
    /// Log file manager
    /// <para>This class is thread-safe</para>
    /// </summary>
    public class LogFile : IDisposable
    {
        /// <summary>
        /// The log levels
        /// </summary>
        public enum LogLevels
        {
            /// <summary>
            /// Debug
            /// </summary>
            Debug,
            /// <summary>
            /// Information
            /// </summary>
            Info,
            /// <summary>
            /// Warning
            /// </summary>
            Warning,
            /// <summary>
            /// Error
            /// </summary>
            Error,
        }

        string m_BasePath = string.Empty;
        string m_FileName = string.Empty;
        StreamWriter m_Writer = null;
        Mutex m_Mutex = null;

        /// <summary>
        /// Creare a new LogFile instance
        /// </summary>
        /// <param name="logLevel">The <see cref="LogLevels"/></param>
        /// <param name="path"></param>
        public LogFile(LogLevels logLevel, string path)
        {
            if (!string.IsNullOrEmpty(path)) {
                if (!path.EndsWith("\\"))
                    path = string.Format("{0}\\", path);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                AutoFlush = true;
                LogLevel = logLevel;
                m_Mutex = new Mutex();
                m_BasePath = path;
                m_FileName = string.Format("{0}{1}", m_BasePath, GetFileName());
                m_Writer = new StreamWriter(m_FileName, true);
            }
        }

        /// <summary>
        /// Get the current file name full path
        /// </summary>
        public string FilePath
        {
            get {
                return m_FileName;
            }
        } // FilePath

        /// <summary>
        /// The minimum <see cref="LogLevels"/>
        /// </summary>
        public LogLevels LogLevel
        {
            get;
            set;
        } // LogLevel

        /// <summary>
        /// Autoflush log file (default: True).
        /// </summary>
        public bool AutoFlush
        {
            get;
            set;
        }

        public void Dispose()
        {
            m_Mutex.WaitOne();
            if (m_Writer != null) {
                m_Writer.Dispose();
                m_Writer = null;
            }
            m_Mutex.ReleaseMutex();

            m_Mutex.Dispose();
        }

        /// <summary>
        /// Return the log filename
        /// </summary>
        /// <returns></returns>
        private string GetFileName()
        {
            return string.Format("{0}.txt", DateTime.Now.ToString("yyyyMMdd_HH00"));
        } // GetFileName

        /// <summary>
        /// Write a message to the log file
        /// The name of the file changes every hour
        /// </summary>
        /// <param name="level">The message <see cref="LogLevels"/></param>
        /// <param name="component">The component writing the log</param>
        /// <param name="message">The message to log</param>
        public void Write(LogLevels level, string component, string message)
        {
            if (level < LogLevel)
                return;

            if (message != null)
                message = message.TrimEnd();
            if (string.IsNullOrEmpty(message))
                return;

            try {
                m_Mutex.WaitOne();
                // Check filename (the file changes every hour)
                if (m_FileName != string.Format("{0}{1}", m_BasePath, GetFileName())) {
                    m_Writer.Close();
                    m_Writer.Dispose();

                    // Open the new file:
                    m_FileName = string.Format("{0}{1}", m_BasePath, GetFileName());
                    m_Writer = new StreamWriter(m_FileName, true);
                }

                if (!string.IsNullOrEmpty(component))
                    m_Writer.WriteLine("[{0} {1} {2}] {3}", level.ToString().ToUpper(), DateTime.Now.ToString("yyyyMMdd_HH:mm:ss.fff"), component, message);
                else
                    m_Writer.WriteLine("[{0} {1}] {2}", level.ToString().ToUpper(), DateTime.Now.ToString("yyyyMMdd_HH:mm:ss.fff"), message);

                if (AutoFlush)
                    m_Writer.Flush();
            } catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("LogFile Write: {0}", ex.Message);
            } finally {
                m_Mutex.ReleaseMutex();
            }
        } // Write
    }
}
