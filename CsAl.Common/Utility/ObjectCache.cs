﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CsAlCore.Common.Utility
{
    /// <summary>
    /// A cache of objects.
    /// <para>This class is thread-safe</para>
    /// </summary>
    public class ObjectCache
    {
        #region Classes
        /// <summary>
        /// A cache entry 
        /// </summary>
        class CacheEntry
        {
            public CacheEntry(string key)
            {
                Key = key;
            }

            /// <summary>
            /// Tha last entry access time (add/update/get)
            /// </summary>
            public DateTime DateTime
            {
                get;
                set;
            }

            /// <summary>
            /// The entry key
            /// </summary>
            public string Key
            {
                get;
                set;
            }

            /// <summary>
            /// The entry value
            /// </summary>
            public object Value
            {
                get;
                set;
            }

            public new string ToString()
            {
                return $"{ Key } ({ Value.GetType() })";
            }
        } // CacheEntry
        #endregion

        Mutex m_Mutex = new Mutex();
        Dictionary<string, CacheEntry> m_Cache = null;
        List<CacheEntry> m_SortedCache = null;
        DateTime m_LastExpiredCheck = DateTime.MinValue;

        /// <summary>
        /// Create a new <see cref="ObjectCache"/>
        /// </summary>
        /// <param name="capacity">The cache capacity. If set to zero the capacity is unlimited.</param>
        /// <param name="validity">The validity period of entries. If set to TimeSpan.Zero the validity is unlimited.</param>
        public ObjectCache(uint capacity, TimeSpan validity)
        {
            if (validity == null)
                throw new ArgumentNullException("validity");

            if (capacity > 0)
                m_Cache = new Dictionary<string, CacheEntry>((int)capacity);
            else
                m_Cache = new Dictionary<string, CacheEntry>();
            m_SortedCache = new List<CacheEntry>();
            Validity = validity;
            m_LastExpiredCheck = DateTime.UtcNow;
        } // ObjectCache

        /// <summary>
        /// The csache entries validity <see cref="TimeSpan"/>
        /// <para>Entries older than this value will be removed</para>
        /// </summary>
        public TimeSpan Validity
        {
            get;
            private set;
        } // Validity

        /// <summary>
        /// The cache capacity. If set to zero the capacity is unlimited.
        /// <para>Maximum number of items the cache can contain. When the capacity is reached new entries will replace the older ones.</para>
        /// </summary>
        public uint Capacity
        {
            get;
            set;
        }

        /// <summary>
        /// Return all the object keys
        /// </summary>
        public IEnumerable<string> Keys
        {
            get { return m_Cache.Keys; }
        }

        /// <summary>
        /// Clear the cache.
        /// </summary>
        public void Clear()
        {
            m_Mutex.WaitOne();
            m_Cache.Clear();
            m_SortedCache.Clear();
            m_Mutex.ReleaseMutex();
        } // Clear

        /// <summary>
        /// Geta a value from the <see cref="ObjectCache"/>
        /// </summary>
        /// <typeparam name="T">The entry type</typeparam>
        /// <param name="key">The entry key</param>
        /// <returns>The entry value</returns>
        public T GetValue<T>(string key)
        {
            CacheEntry res = GetValue(key);
            if (res != null)
                return (T)res.Value;
            return default(T);
        } // GetValue

        /// <summary>
        /// Add or update a value in the <see cref="ObjectCache"/>
        /// </summary>
        /// <typeparam name="T">The entry's value type</typeparam>
        /// <param name="key">The entry key</param>
        /// <param name="value">The entry value</param>
        public void AddOrUpdateValue<T>(string key, T value)
        {
            m_Mutex.WaitOne();

            CheckExpired();
            // Check capacity:
            if (Capacity > 0) {
                while (m_Cache.Count >= Capacity) {
                    m_Cache.Remove(m_SortedCache[0].Key);
                    m_SortedCache.RemoveAt(0);
                }
            }

            // Add/update value:
            key = key.ToLower();
            CacheEntry entry = null;
            if (!m_Cache.TryGetValue(key, out entry)) {
                entry = new CacheEntry(key);
                m_Cache[key] = entry;
            } else {
                m_SortedCache.Remove(entry);
            }
            m_SortedCache.Add(entry);
            
            entry.DateTime = DateTime.UtcNow;
            entry.Value = value;                     
            
            m_Mutex.ReleaseMutex();
        } // AddOrUpdateValue

        /// <summary>
        /// Remove a entry from the cache
        /// </summary>
        /// <param name="key">The entry key</param>
        /// <returns>True if a value has been removed from the cache</returns>
        public bool RemoveValue(string key)
        {
            m_Mutex.WaitOne();

            bool res = false;
            key = key.ToLower();
            CacheEntry entry = null;
            if (m_Cache.TryGetValue(key, out entry)) {
                m_Cache.Remove(key);
                m_SortedCache.Remove(entry);
                res = true;
            }

            m_Mutex.ReleaseMutex();
            return res;
        } // RemoveValue

        private CacheEntry GetValue(string key)
        {
            DateTime la = DateTime.MinValue;
            return GetValue(key, out la);
        } // GetValue

        /// <summary>
        /// Get the <see cref="CacheEntry"/> for the given key
        /// </summary>
        /// <param name="key">The <see cref="CacheEntry"/>'s key</param>
        /// <param name="lastAccess">The <see cref="CacheEntry"/>'s last access datetime</param>
        /// <returns>A <see cref="CacheEntry"/> or null</returns>
        private CacheEntry GetValue(string key, out DateTime lastAccess)
        {
            lastAccess = DateTime.MinValue;

            m_Mutex.WaitOne();

            CheckExpired();

            key = key.ToLower();
            CacheEntry entry = null;
            if (m_Cache.TryGetValue(key, out entry)) {
                if (Validity > TimeSpan.Zero && DateTime.UtcNow - entry.DateTime > Validity) {
                    m_Cache.Remove(key);
                    m_SortedCache.Remove(entry);
                    m_Mutex.ReleaseMutex();
                    return null;
                }

                lastAccess = entry.DateTime;
                entry.DateTime = DateTime.UtcNow;
                m_Mutex.ReleaseMutex();
                return entry;
            }

            m_Mutex.ReleaseMutex();
            return null;
        } // GetValue

        private void CheckExpired()
        {
            if (Validity > TimeSpan.Zero) {
                // Remove items only after Validity has passed:
                TimeSpan elapsed = DateTime.UtcNow - m_LastExpiredCheck;
                if (elapsed >= Validity && elapsed.TotalMinutes >= 1) {                    
                    RemoveExpired();
                    m_LastExpiredCheck = DateTime.UtcNow;
                }
            }
        } // CheckExpired

        /// <summary>
        /// Remove all expired <see cref="CacheEntry"/> from the cache
        /// </summary>
        private void RemoveExpired()
        {
            if (Validity > TimeSpan.Zero) {
                List<CacheEntry> itemsToRemove = new List<CacheEntry>();
                foreach (CacheEntry entry in m_SortedCache) {
                    if (DateTime.UtcNow - entry.DateTime > Validity)
                        itemsToRemove.Add(entry);
                    else
                        break;
                }

                foreach (var item in itemsToRemove) {
                    m_Cache.Remove(item.Key);
                    m_SortedCache.Remove(item);
                }
            }
        } // RemoveExpired
    } // ObjectCache
}
