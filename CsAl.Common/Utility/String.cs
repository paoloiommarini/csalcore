﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCore.Common.Utility
{
    public class String
    {
        /// <summary>
        /// Encrypt a string using SHA1
        /// </summary>
        /// <param name="input">The string to encrypt</param>
        /// <returns>The encrypted string</returns>
        public static string GetSHA1Hash(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            using (SHA1Managed sha1 = new SHA1Managed()) {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                    sb.Append(b.ToString("x2"));
                return sb.ToString();
            }
        } // GetSHA1Hash

        /// <summary>
        /// Encrypt a string using MD5
        /// </summary>
        /// <param name="input">The string to encrypt</param>
        /// <returns>The encrypted string</returns>
        public static string GetMD5Hash(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++) {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        } // GetMD5Hash

        public static List<long> StringIdListToList(string idList)
        {
            if (string.IsNullOrEmpty(idList))
                return null;

            List<long> res = new List<long>();
            foreach (string strId in idList.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)) {
                long id = 0;
                if (long.TryParse(strId, out id)) {
                    res.Add(id);
                }
            }
            return res;
        } // StringIdListToList

        public static string EnumFlagsValuesToString(List<Schema.ObjectAttribute.EnumValue> values)
        {
            StringBuilder sb = new StringBuilder();

            if (values != null) {
                foreach (Schema.ObjectAttribute.EnumValue ev in values) {
                    if (sb.Length > 0)
                        sb.Append(", ");
                    sb.AppendFormat("'{0}'", ev.Name);
                }
            }
            return sb.ToString();
        } // EnumValuesToString
    }
}
