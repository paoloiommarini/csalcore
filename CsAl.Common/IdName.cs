﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CsAlCore.Common
{
    /// <summary>
    /// Id/name object
    /// </summary>
    public class IdName
    {
        /// <summary>
        /// Create a new <see cref="IdName"/>
        /// </summary>
        public IdName()
        {

        }

        /// <summary>
        /// Create a new <see cref="IdName"/>
        /// </summary>
        /// <param name="id">The id</param>
        /// <param name="name">The name</param>
        public IdName(long id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// The object Id
        /// </summary>
        [XmlAttribute]
        public long Id { get; set; }

        /// <summary>
        /// The object name
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        public new string ToString()
        {
            return string.Format("Id: {0} Name: {1}", Id, Name);
        }
    } // IdName
}
