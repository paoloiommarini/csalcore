﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Describes a date attribute
    /// </summary>
    public class DateAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="DateAttrType"/>
        /// </summary>
        public DateAttrType() :
          base(ObjectAttribute.Types.Date)
        {
            IsAttribute = true;
            IsTableField = true;
        }

        /// <summary>
        /// The default value 
        /// </summary>
        public DateTime? DefaultValue { get; set; }

        /// <summary>
        /// The minimum valid value 
        /// </summary>
        public DateTime? MinValue { get; set; }

        /// <summary>
        /// The maximum valid value
        /// </summary>
        public DateTime? MaxValue { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        } // GetDefaultValue

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is DateTime))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            if (MinValue.HasValue && (DateTime)value < MinValue.Value)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));

            if (MaxValue.HasValue && (DateTime)value > MaxValue.Value)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));

            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is long) {
                DateTime dt = DateTime.MinValue.AddSeconds((long)value);
                xmlWriter.WriteString(Common.Utility.Serialization.DateToXml(dt));
            } else if (value is DateTime)
                xmlWriter.WriteString(Common.Utility.Serialization.DateToXml((DateTime)value));
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            return Common.Utility.Serialization.DateFromXml(xmlReader.Value)?.Date;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            if (value is long) {
                DateTime dt = DateTime.MinValue.AddSeconds((long)value);
                jsonWriter.WriteValue(Common.Utility.Serialization.DateToXml(dt));
            } else if (value is DateTime)
                jsonWriter.WriteValue(Common.Utility.Serialization.DateToXml((DateTime)value));
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            return Common.Utility.Serialization.DateFromXml(jsonReader.ReadAsString())?.Date;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            return true;
        } // IsEqual
    } // DateAttrType
}
