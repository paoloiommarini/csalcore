﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// The String attribute type stores a string value
    /// </summary>
    public class StringAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="StringAttrType"/>
        /// </summary>
        public StringAttrType() :
          base(ObjectAttribute.Types.String)
        {
            IsAttribute = false;
            IsTableField = true;
        }

        /// <summary>
        /// The minimum valid length 
        /// </summary>
        public int MinLength { get; set; }

        /// <summary>
        /// The maximum valid length 
        /// </summary>
        public int MaxLength { get; set; }

        /// <summary>
        /// The default value 
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// The regular expression used to validate the value
        /// </summary>
        public string ValidationRegEx { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is string))
                throw new Common.Exceptions.InvalidAttributeValueError(attributeName, value);

            string v = value as string;
            if (MaxLength != 0 && v.Length > MaxLength)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("minimum length {0}, maximum length {1}", MinLength, MaxLength));

            if (MinLength != 0 && v.Length < MinLength)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("minimum length {0}, maximum length {1}", MinLength, MaxLength));

            if (!string.IsNullOrEmpty(ValidationRegEx)) {
                Match m = Regex.Match(v, ValidationRegEx);
                if (m == null || !m.Success)
                    return false;
            }
            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            xmlWriter.WriteString((string)value);
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            return xmlReader.ReadElementString();
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            jsonWriter.WriteValue((string)value);
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            return jsonReader.ReadAsString();
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            StringAttrType at = attr as StringAttrType;
            if (MaxLength != at.MaxLength || MinLength != at.MinLength || string.Compare(ValidationRegEx, at.ValidationRegEx) != 0)
                return false;

            return true;
        } // IsEqual
    } // StringAttrType
}
