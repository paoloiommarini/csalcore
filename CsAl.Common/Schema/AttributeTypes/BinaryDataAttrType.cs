﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Describes a binary attribute
    /// </summary>
    public class BinaryDataAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="BinaryDataAttrType"/>
        /// </summary>
        public BinaryDataAttrType() :
          base(ObjectAttribute.Types.BinaryData)
        {
            IsAttribute = false;
            IsTableField = true;
        }

        /// <summary>
        /// A list of allowed mime types
        /// </summary>
        public string[] AllowedMimeTypes { get; set; }

        /// <summary>
        /// The file maximum size (in bytes)
        /// </summary>
        public int MinLength { get; set; }
        /// <summary>
        /// The file minimum size (in bytes)
        /// </summary>
        public int MaxLength { get; set; }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is long) && !(value is BinaryDataValue))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            if (value is BinaryDataValue) {
                BinaryDataValue bdv = value as BinaryDataValue;
                if (AllowedMimeTypes != null && !AllowedMimeTypes.Contains(bdv.MimeType.ToLower()))
                    throw new Exceptions.InvalidAttributeValueError(attributeName, value);

                if (MaxLength != 0 && bdv.Size > MaxLength)
                    throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("minimum size {0}, maximum size {1}", MinLength, MaxLength));

                if (MinLength != 0 && bdv.Size < MinLength)
                    throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("minimum size {0}, maximum size {1}", MinLength, MaxLength));
            }
            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is BinaryDataValue) {
                BinaryDataValue bd = value as BinaryDataValue;
                xmlWriter.WriteStartElement(Schema.BinaryDataElementName);

                xmlWriter.WriteStartAttribute(Schema.ObjectIdName);
                xmlWriter.WriteString(bd.Id.ToString(CultureInfo.InvariantCulture));
                xmlWriter.WriteEndAttribute();

                xmlWriter.WriteStartAttribute("fileName");
                xmlWriter.WriteString(bd.FileName);
                xmlWriter.WriteEndAttribute();

                xmlWriter.WriteStartAttribute("mimeType");
                xmlWriter.WriteString(bd.MimeType);
                xmlWriter.WriteEndAttribute();

                xmlWriter.WriteStartAttribute("size");
                xmlWriter.WriteString(bd.Size.ToString(CultureInfo.InvariantCulture));
                xmlWriter.WriteEndAttribute();

                xmlWriter.WriteEndElement();
            }
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.HasAttributes) {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == Schema.BinaryDataElementName) {
                    BinaryDataValue res = new BinaryDataValue();
                    long tempLong = 0;
                    string strNum = xmlReader.GetAttribute(Schema.ObjectIdName);
                    if (long.TryParse(strNum, out tempLong))
                        res.Id = tempLong;

                    res.FileName = xmlReader.GetAttribute("fileName");
                    res.MimeType = xmlReader.GetAttribute("mimeType");
                    res.FileName = xmlReader.GetAttribute("fileName");

                    strNum = xmlReader.GetAttribute("size");
                    if (long.TryParse(strNum, out tempLong))
                        res.Size = tempLong;
                }
                return null;
            }
            return null;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            if (value is BinaryDataValue) {
                jsonWriter.WriteStartObject();
                BinaryDataValue bd = value as BinaryDataValue;

                jsonWriter.WritePropertyName(Schema.ObjectIdName);
                jsonWriter.WriteValue(bd.Id.ToString(CultureInfo.InvariantCulture));

                jsonWriter.WritePropertyName("fileName");
                jsonWriter.WriteValue(bd.FileName);

                jsonWriter.WritePropertyName("mimeType");
                jsonWriter.WriteValue(bd.MimeType);

                jsonWriter.WritePropertyName("size");
                jsonWriter.WriteValue(bd.Size.ToString(CultureInfo.InvariantCulture));

                jsonWriter.WriteEndObject();
            }
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            throw new NotImplementedException();
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))            
                return false;

            return true;
        } // IsEqual
    } // BinaryDataAttrType
}
