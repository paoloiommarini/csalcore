﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Describes a Boolean attribute
    /// </summary>
    public class BooleanAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="BooleanAttrType"/>
        /// </summary>
        public BooleanAttrType() :
          base(ObjectAttribute.Types.Boolean)
        {
            IsAttribute = true;
            IsTableField = true;
        }

        /// <summary>
        /// The default value 
        /// </summary>
        public bool DefaultValue { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is bool))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            xmlWriter.WriteString((bool)value ? "true" : "false");
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            return string.Compare(xmlReader.Value, "true", StringComparison.OrdinalIgnoreCase) == 0;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            jsonWriter.WriteValue((bool)value ? "true" : "false");
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            return string.Compare(jsonReader.ReadAsString(), "true", StringComparison.OrdinalIgnoreCase) == 0;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            return true;
        } // IsEqual
    } // BooleanAttrType
}
