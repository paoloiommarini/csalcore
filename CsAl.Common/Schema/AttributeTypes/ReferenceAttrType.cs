﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Describes a Reference attribute: reference to another object
    /// </summary>
    public class ReferenceAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="ReferenceAttrType"/>
        /// </summary>
        public ReferenceAttrType(string refObjectTypeName) :
          base(ObjectAttribute.Types.Reference)
        {
            RefObjectType = refObjectTypeName;
            IsAttribute = false;
            IsTableField = true;
        }

        /// <summary>
        /// The name of the referenced <see cref="ObjectType"/>
        /// </summary>
        public string RefObjectType { get; private set; }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is int) && !(value is long) && !(value is GenericObject)/* && !(value is SchemaObject)*/)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            GenericObject obj = null;
            if (value is GenericObject)
                obj = value as GenericObject;
            //else if (value is SchemaObject)
            //    obj = (value as SchemaObject).ToGenericObject();

            if (obj != null && string.Compare(obj.ObjectTypeName, RefObjectType, StringComparison.OrdinalIgnoreCase) != 0)
                throw new Exceptions.InvalidReferenceError(attributeName, obj.ObjectTypeName);

            return true;
        }

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is long && (long)value > 0) {
                xmlWriter.WriteStartAttribute(Schema.ReferenceIdName);
                xmlWriter.WriteString(((long)value).ToString(CultureInfo.InvariantCulture));
                xmlWriter.WriteEndAttribute();
            } else if (value is GenericObject) {
                GenericObject refObj = value as GenericObject;
                xmlWriter.WriteStartAttribute(Schema.ReferenceIdName);
                xmlWriter.WriteString(refObj.Id.ToString(CultureInfo.InvariantCulture));
                xmlWriter.WriteEndAttribute();
                refObj.Serialize(xmlWriter);
            }
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.HasAttributes) {
                if (!xmlReader.IsEmptyElement) {
                    GenericObject obj = GenericObject.Deserialize(schema, xmlReader.ReadInnerXml());
                    if (obj != null)
                        return obj;
                }
                string strRef = xmlReader.GetAttribute(Schema.ReferenceIdName);
                long refId = 0;
                if (long.TryParse(strRef, out refId))
                    return refId;
                return null;
            }
            return 0;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            jsonWriter.WriteStartObject();
            if (value is long && (long)value > 0) {
                jsonWriter.WritePropertyName(Schema.ReferenceIdName);
                jsonWriter.WriteValue(((long)value).ToString(CultureInfo.InvariantCulture));
            } else if (value is GenericObject) {
                GenericObject refObj = value as GenericObject;
                jsonWriter.WritePropertyName(Schema.ReferenceIdName);
                jsonWriter.WriteValue(refObj.Id.ToString(CultureInfo.InvariantCulture));

                jsonWriter.WritePropertyName(refObj.ObjectTypeName);
                refObj.Serialize(jsonWriter);
            }
            jsonWriter.WriteEndObject();
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            object res = null;
            if (jsonReader.TokenType == JsonToken.StartObject) {
                string property = string.Empty;
                while (jsonReader.Read() && jsonReader.TokenType != JsonToken.EndObject) {
                    if (jsonReader.TokenType == JsonToken.PropertyName) {
                        property = (string)jsonReader.Value;
                        if (string.Compare(property, Schema.ReferenceIdName, StringComparison.OrdinalIgnoreCase) == 0) {
                            long refId = 0;
                            if (long.TryParse(jsonReader.ReadAsString(), out refId))
                                res = refId;
                        } else if (string.Compare(property, RefObjectType, StringComparison.OrdinalIgnoreCase) == 0) {
                            res = GenericObject.Deserialize(schema, jsonReader);
                        }
                    }
                }
            }
            return res;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            ReferenceAttrType at = attr as ReferenceAttrType;
            if (RefObjectType != at.RefObjectType)
                return false;

            return true;
        } // IsEqual
    } // ReferenceAttrType

    /// <summary>
    /// The special <see cref="ReferenceAttrType"/> used in a child object to reference its parent
    /// </summary>
    public class ChildOfAttrType : ReferenceAttrType
    {
        public ChildOfAttrType(string refObjectTypeName) :
          base(refObjectTypeName)
        {
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            return true;
        } // IsEqual
    } // ChildOfAttrType
}
