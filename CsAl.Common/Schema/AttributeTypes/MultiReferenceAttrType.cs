﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Describes a MultiReference attribute: multiple reference to other objects
    /// </summary>
    public class MultiReferenceAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="MultiReferenceAttrType"/>
        /// </summary>
        public MultiReferenceAttrType(string refObjectTypeName, bool isChildrenReference, List<ObjectAttribute> subAttributes) :
          base(ObjectAttribute.Types.MultiReference)
        {
            IsChildrenReference = isChildrenReference;
            RefObjectTypeName = refObjectTypeName;
            IsAttribute = false;
            IsTableField = false;
            SubAttributes = subAttributes;
        }

        /// <summary>
        /// Gets if this is a children multi reference
        /// </summary>
        public bool IsChildrenReference { get; private set; }

        /// <summary>
        /// The referenced <see cref="ObjectType"/> name
        /// </summary>
        public string RefObjectTypeName { get; private set; }

        /// <summary>
        /// The sub attributes
        /// </summary>
        public List<ObjectAttribute> SubAttributes { get; private set; }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is List<MultiReferenceValue>))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);
            return true;
        }

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is List<MultiReferenceValue>) {
                List<MultiReferenceValue> mRefValue = value as List<MultiReferenceValue>;
                foreach (MultiReferenceValue mRef in mRefValue) {
                    xmlWriter.WriteStartElement(Schema.RefElementName);
                    xmlWriter.WriteStartAttribute(Schema.ReferenceIdName);
                    xmlWriter.WriteString(mRef.RefId.ToString(CultureInfo.InvariantCulture));
                    xmlWriter.WriteEndAttribute();

                    // Sub attributes
                    if (mRef.SubAttributes != null) {
                        // Attributes
                        foreach (GenericAttribute sAt in mRef.SubAttributes.GetAttributes(mRef.SubAttributes.ObjectTypeName)) {
                            if (sAt.Attribute.IsAttribute) {
                                xmlWriter.WriteStartAttribute(sAt.Name);
                                sAt.Attribute.Serialize(xmlWriter, sAt.Value);
                                xmlWriter.WriteEndAttribute();
                            }
                        }

                        // Elements
                        foreach (GenericAttribute sAt in mRef.SubAttributes.GetAttributes(mRef.SubAttributes.ObjectTypeName)) {
                            if (!sAt.Attribute.IsAttribute) {
                                xmlWriter.WriteStartElement(sAt.Name);
                                sAt.Attribute.Serialize(xmlWriter, sAt.Value);
                                xmlWriter.WriteEndElement();
                            }
                        }
                    }

                    // Referenced object
                    if (mRef.RefObject != null)
                        mRef.RefObject.Serialize(xmlWriter);
                    xmlWriter.WriteEndElement();
                }
            }
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            if (!xmlReader.IsEmptyElement) {
                List<MultiReferenceValue> mrefValue = new List<MultiReferenceValue>();
                while (xmlReader.NodeType != XmlNodeType.EndElement) {
                    xmlReader.Read();
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == Schema.RefElementName) {
                        MultiReferenceValue mrv = null;

                        // Attributes
                        if (xmlReader.HasAttributes) {
                            string strId = xmlReader.GetAttribute(Schema.ReferenceIdName);
                            long mRefId = 0;
                            if (long.TryParse(strId, out mRefId))
                                mrv = new MultiReferenceValue(mRefId);
                        }

                        Dictionary<string, ObjectAttribute> subAttrsDict = new Dictionary<string, ObjectAttribute>();
                        if (SubAttributes != null && SubAttributes.Count > 0) {
                            mrv.SubAttributes = GenericObject.Create(string.Empty, SubAttributes);
                            foreach (ObjectAttribute sAt in SubAttributes) {
                                if (sAt.IsAttribute)
                                    mrv.SubAttributes.SetAttributeValue(sAt.Name, sAt.Deserialize(schema, xmlReader));
                                else
                                    subAttrsDict[sAt.Name] = sAt;
                            }
                        }

                        // Elements
                        while (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != Schema.RefElementName) {
                            xmlReader.Read();

                            ObjectAttribute sAt = null;
                            if (subAttrsDict.TryGetValue(xmlReader.Name, out sAt)) {
                                mrv.SubAttributes.SetAttributeValue(sAt.Name, sAt.Deserialize(schema, xmlReader));
                            } else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == RefObjectTypeName) {
                                // Referenced object
                                GenericObject obj = GenericObject.Deserialize(schema, xmlReader.ReadOuterXml());
                                if (obj != null) {
                                    if (mrv != null)
                                        mrv.RefObject = obj;
                                    else
                                        mrv = new MultiReferenceValue(obj);
                                }
                            }
                        }
                        mrefValue.Add(mrv);
                    }
                }

                return mrefValue;
            }
            return null;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            jsonWriter.WriteStartArray();
            if (value is List<MultiReferenceValue>) {
                List<MultiReferenceValue> mRefValue = value as List<MultiReferenceValue>;
                foreach (MultiReferenceValue mRef in mRefValue) {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName(Schema.RefElementName);
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName(Schema.ReferenceIdName);
                    jsonWriter.WriteValue(mRef.RefId.ToString(CultureInfo.InvariantCulture));

                    // Sub attributes
                    if (mRef.SubAttributes != null) {
                        // Attributes
                        foreach (GenericAttribute sAt in mRef.SubAttributes.GetAttributes(mRef.SubAttributes.ObjectTypeName)) {
                            if (sAt.Attribute.IsAttribute) {
                                jsonWriter.WritePropertyName(sAt.Name);
                                sAt.Attribute.Serialize(jsonWriter, sAt.Value);
                            }
                        }

                        // Elements
                        foreach (GenericAttribute sAt in mRef.SubAttributes.GetAttributes(mRef.SubAttributes.ObjectTypeName)) {
                            if (!sAt.Attribute.IsAttribute) {
                                jsonWriter.WritePropertyName(sAt.Name);
                                sAt.Attribute.Serialize(jsonWriter, sAt.Value);
                            }
                        }
                    }

                    // Referenced object
                    if (mRef.RefObject != null) {
                        jsonWriter.WritePropertyName(mRef.RefObject.ObjectTypeName);
                        mRef.RefObject.Serialize(jsonWriter);
                    }
                    jsonWriter.WriteEndObject();
                    jsonWriter.WriteEndObject();
                }
            }
            jsonWriter.WriteEndArray();
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            if (jsonReader.TokenType == JsonToken.StartArray) {
                List<MultiReferenceValue> mrefValue = new List<MultiReferenceValue>();

                string property = string.Empty;
                while (jsonReader.Read() && jsonReader.TokenType != JsonToken.EndArray) {
                    if (jsonReader.TokenType == JsonToken.PropertyName) {
                        property = (string)jsonReader.Value;
                        if (string.Compare(property, Schema.RefElementName, StringComparison.OrdinalIgnoreCase) == 0) {
                            MultiReferenceValue mrv = null;
                            string objProperty = string.Empty;
                            while (jsonReader.Read() && jsonReader.TokenType != JsonToken.EndObject) {
                                if (jsonReader.TokenType == JsonToken.PropertyName) {
                                    objProperty = (string)jsonReader.Value;
                                    if (string.Compare(objProperty, Schema.ReferenceIdName, StringComparison.OrdinalIgnoreCase) == 0) {
                                        long refId = 0;
                                        if (long.TryParse(jsonReader.ReadAsString(), out refId)) {
                                            mrv = new MultiReferenceValue(refId);
                                            mrefValue.Add(mrv);
                                        }
                                    } else if (string.Compare(objProperty, RefObjectTypeName, StringComparison.OrdinalIgnoreCase) == 0) {
                                            GenericObject obj = GenericObject.Deserialize(schema, jsonReader);
                                            if (obj != null && mrv != null)
                                                mrv.RefObject = obj;
                                    }
                                }
                            }
                        }
                    }
                }

                return mrefValue;
            }
            return null;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            MultiReferenceAttrType at = attr as MultiReferenceAttrType;
            if (at.RefObjectTypeName != at.RefObjectTypeName || at.IsChildrenReference != at.IsChildrenReference)
                return false;

            if (SubAttributes == null && at.SubAttributes != null || SubAttributes != null && at.SubAttributes == null)
                return false;

            if (SubAttributes != null) {
                if (SubAttributes.Count != at.SubAttributes.Count)
                    return false;

                for (int i=0; i<SubAttributes.Count; i++) {
                    if (!SubAttributes[i].AttributeType.IsEqual(at.SubAttributes[i].AttributeType))
                        return false;
                }
            }
            return true;
        } // IsEqual
    } // MultiReferenceAttrType
}
