﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Describes a TimeSpan attribute
    /// </summary>
    public class TimeSpanAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="TimeSpanAttrType"/>
        /// </summary>
        public TimeSpanAttrType() :
          base(ObjectAttribute.Types.TimeSpan)
        {
            IsAttribute = true;
            IsTableField = true;
        }

        /// <summary>
        /// The minimum valid value 
        /// </summary>
        public TimeSpan? MinValue { get; set; }

        /// <summary>
        /// The maximum valid value
        /// </summary>
        public TimeSpan? MaxValue { get; set; }

        /// <summary>
        /// The default value 
        /// </summary>
        public TimeSpan? DefaultValue { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        } // GetDefaultValue

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is TimeSpan))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            if (MinValue.HasValue && (TimeSpan)value < MinValue.Value)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));

            if (MaxValue.HasValue && (TimeSpan)value > MaxValue.Value)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));

            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is long) {
                TimeSpan ts = TimeSpan.FromMilliseconds((long)value);
                xmlWriter.WriteString(Common.Utility.Serialization.TimeSpanToXml(ts));
            } else if (value is TimeSpan)
                xmlWriter.WriteString(Common.Utility.Serialization.TimeSpanToXml((TimeSpan)value));
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            return Common.Utility.Serialization.TimeSpanFromXml(xmlReader.Value);
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            if (value is long) {
                TimeSpan ts = TimeSpan.FromMilliseconds((long)value);
                jsonWriter.WriteValue(Common.Utility.Serialization.TimeSpanToXml(ts));
            } else if (value is TimeSpan)
                jsonWriter.WriteValue(Common.Utility.Serialization.TimeSpanToXml((TimeSpan)value));
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            return Common.Utility.Serialization.TimeSpanFromXml(jsonReader.ReadAsString());
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            return true;
        } // IsEqual

    } // TimeSpanAttrType
}
