﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// The Flags attribute type stores one or more values from a list of possible <see cref="Values"/>
    /// </summary>
    public class FlagsAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="FlagsAttrType"/>
        /// </summary>
        public FlagsAttrType(List<ObjectAttribute.EnumValue> values) :
          base(ObjectAttribute.Types.Flags)
        {
            Values = values;
            IsAttribute = false;
            IsTableField = true;
        }

        /// <summary>
        /// The default value 
        /// </summary>
        public long? DefaultValue { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        }

        /// <summary>
        /// The list of valid <see cref="ObjectAttribute.EnumValue"/>
        /// </summary>
        public List<ObjectAttribute.EnumValue> Values
        {
            get;
            private set;
        }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is long))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            long v = (long)value;
            foreach (ObjectAttribute.EnumValue ev in Values)
                if ((ev.Value & v) != 0)
                    v -= ev.Value;

            if (v != 0)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);
            return true;
        }

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is long) {
                long v = (long)value;
                foreach (ObjectAttribute.EnumValue ev in Values) {
                    if ((ev.Value & v) != 0) {
                        xmlWriter.WriteStartElement(Schema.FlagValueName);
                        xmlWriter.WriteStartAttribute(Schema.FlagValueAttributeName);
                        xmlWriter.WriteString(ev.Name);
                        xmlWriter.WriteEndAttribute();
                        xmlWriter.WriteEndElement();
                    }
                }
            }
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            if (!xmlReader.IsEmptyElement && Values != null) {
                long flagValue = 0;
                while (xmlReader.NodeType != XmlNodeType.EndElement) {
                    xmlReader.Read();
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == Schema.FlagValueName && xmlReader.HasAttributes) {
                        string strFlagVal = xmlReader.GetAttribute(Schema.FlagValueAttributeName);
                        foreach (ObjectAttribute.EnumValue ev in Values) {
                            if (string.Compare(ev.Name, strFlagVal, StringComparison.OrdinalIgnoreCase) == 0) {
                                flagValue |= ev.Value;
                                break;
                            }
                        }
                    }
                }
                return flagValue;
            }
            return null;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            jsonWriter.WriteStartArray();
            if (value is long) {
                long v = (long)value;
                foreach (ObjectAttribute.EnumValue ev in Values) {
                    if ((ev.Value & v) != 0) {
                        jsonWriter.WriteValue(ev.Name);
                    }
                }
            }
            jsonWriter.WriteEndArray();
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            throw new NotImplementedException();
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            FlagsAttrType at = attr as FlagsAttrType;
            if (Values.Count != at.Values.Count)
                return false;

            for (int i = 0; i < at.Values.Count; i++) {
                if (Values[i].Name != at.Values[i].Name || Values[i].Value != at.Values[i].Value)
                    return false;
            }

            return true;
        } // IsEqual
    } // FlagsAttrType
}
