﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Base class for attribute types
    /// </summary>
    public abstract class AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="AttrTypeBase"/>
        /// </summary>
        /// <param name="type">The <see cref="ObjectAttribute.Types"/></param>
        public AttrTypeBase(ObjectAttribute.Types type)
        {
            Type = type;
        }

        /// <summary>
        /// The <see cref="ObjectAttribute.Types"/>
        /// </summary>
        public ObjectAttribute.Types Type
        {
            get;
            private set;
        }

        /// <summary>
        /// Get if this attribute is rendered ad an XML attribute
        /// </summary>
        public bool IsAttribute
        {
            get;
            protected set;
        }

        /// <summary>
        /// Get if this attribute is saved to a database column
        /// </summary>
        public bool IsTableField
        {
            get;
            protected set;
        }

        /// <summary>
        /// Get the attribute's default value 
        /// </summary>
        public virtual object GetDefaultValue()
        {
            return null;
        } // GetDefaultValue

        /// <summary>
        /// Validate the given value
        /// </summary>
        /// <param name="attributeName">The attribute name</param>
        /// <param name="value">The value to validate</param>
        /// <returns></returns>
        public virtual bool ValidateValue(string attributeName, object value)
        {
            return true;
        } // ValidateValue

        /// <summary>
        /// Override in descending classes to alter a value (e.g. the Real attribute type rounds the value to its precision)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual object GetValue(object value)
        {
            return value;
        } // GetValue

        /// <summary>
        /// Check if an <see cref="AttrTypeBase"/> is equal to another one
        /// </summary>
        /// <param name="attr">The other <see cref="AttrTypeBase"/></param>
        /// <returns>True if the attributes are equal</returns>
        public virtual bool IsEqual(AttrTypeBase attr)
        {
            if (attr.GetType() != GetType())
                return false;

            if (Type != attr.Type || IsAttribute != attr.IsAttribute || IsTableField != attr.IsTableField)
                return false;

            object dv = GetDefaultValue();
            object aDv = attr.GetDefaultValue();
            if (dv == null && aDv != null || dv != null && aDv == null || dv != null && !dv.Equals(aDv))
                return false;
            return true;
        }

        /// <summary>
        /// Serialize the attribute
        /// </summary>
        /// <param name="xmlWriter">The <see cref="XmlWriter"/> to write to</param>
        /// <param name="value">The attribute value</param>
        public abstract void Serialize(XmlWriter xmlWriter, object value);

        /// <summary>
        /// Deserialize the attribute
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="xmlReader">The <see cref="XmlReader"/> to read from</param>
        /// <returns>The deserialized <see cref="AttrTypeBase"/></returns>
        public abstract object Deserialize(Schema schema, XmlReader xmlReader);

        /// <summary>
        /// Serialize the attribute to JSON
        /// </summary>
        /// <param name="jsonWriter">The <see cref="JsonWriter"/> to write to</param>
        /// <param name="value">The attribute value</param>
        public abstract void Serialize(JsonWriter jsonWriter, object value);

        /// <summary>
        /// Deserialize the attribute
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="jsonReader">The <see cref="JsonReader"/> to read from</param>
        /// <returns>The deserialized <see cref="AttrTypeBase"/></returns>
        public abstract object Deserialize(Schema schema, JsonReader jsonReader);
    } // AttrTypeBase
}
