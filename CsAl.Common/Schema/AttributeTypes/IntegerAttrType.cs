﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// The Integer attribute type stores a integer number
    /// </summary>
    public class IntegerAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="IntegerAttrType"/>
        /// </summary>
        public IntegerAttrType() :
          base(ObjectAttribute.Types.Integer)
        {
            IsAttribute = true;
            IsTableField = true;
        }

        /// <summary>
        /// The minimum valid value 
        /// </summary>
        public long MinValue { get; set; }

        /// <summary>
        /// The maximum valid value
        /// </summary>
        public long MaxValue { get; set; }

        /// <summary>
        /// The default value 
        /// </summary>
        public long? DefaultValue { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is long) && !(value is int))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            long v = 0;
            if (value is int)
                v = (int)value;
            else
                v = (long)value;

            if (MaxValue != 0 && v > MaxValue)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));
            if (MinValue != 0 && v < MinValue)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));

            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            xmlWriter.WriteString(((long)value).ToString(CultureInfo.InvariantCulture));
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            long iv = 0;
            long.TryParse(xmlReader.Value, out iv);
            return iv;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            jsonWriter.WriteValue(((long)value).ToString(CultureInfo.InvariantCulture));
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            long iv = 0;
            long.TryParse(jsonReader.ReadAsString(), out iv);
            return iv;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            IntegerAttrType at = attr as IntegerAttrType;
            if (MinValue != at.MinValue || MaxValue != at.MaxValue)
                return false;

            return true;
        } // IsEqual
    } // IntegerAttributeType
}
