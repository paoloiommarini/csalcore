﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    public class RealAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="RealAttrType"/>
        /// </summary>
        public RealAttrType() :
          base(ObjectAttribute.Types.Real)
        {
            IsAttribute = true;
            IsTableField = true;
        }

        /// <summary>
        /// The minimum valid value 
        /// </summary>
        public double MinValue { get; set; }

        /// <summary>
        /// The maximum valid value 
        /// </summary>
        public double MaxValue { get; set; }

        /// <summary>
        /// The default value 
        /// </summary>
        public double? DefaultValue { get; set; }

        /// <summary>
        /// The real number precision
        /// </summary>
        public int? Precision { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        }

        public override object GetValue(object value)
        {
            if (!Precision.HasValue || Precision.Value <= 0)
                return value;

            double? dv = (double?)value;
            if (dv.HasValue)
                return Math.Round(dv.Value, Precision.Value);
            return value;
        } // GetValue

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is double) && !(value is int) && !(value is long))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            double v = 0;
            if (value is int)
                v = (int)value;
            else if (value is long)
                v = (long)value;
            else
                v = (double)value;

            if (MaxValue != 0 && v > MaxValue)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));
            if (MinValue != 0 && v < MinValue)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("range {0}/{1}", MinValue, MaxValue));

            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            xmlWriter.WriteString(((double)value).ToString(CultureInfo.InvariantCulture));
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            double dv = 0;
            double.TryParse(xmlReader.Value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out dv);
            return dv;
        }

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            jsonWriter.WriteValue(((double)value).ToString(CultureInfo.InvariantCulture));
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            double dv = 0;
            double.TryParse(jsonReader.ReadAsString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out dv);
            return dv;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            RealAttrType at = attr as RealAttrType;
            if (MinValue != at.MinValue || MaxValue != at.MaxValue)
                return false;

            return true;
        } // IsEqual
    } // RealAttrType
}
