﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// The Enum attribute type stores a single value from a list of possible <see cref="Values"/>
    /// </summary>
    public class EnumAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="EnumAttrType"/>
        /// </summary>
        public EnumAttrType(List<ObjectAttribute.EnumValue> values) :
          base(ObjectAttribute.Types.Enum)
        {
            Values = values;
            IsAttribute = true;
            IsTableField = true;
        }

        /// <summary>
        /// The default value 
        /// </summary>
        public long? DefaultValue { get; set; }

        public override object GetDefaultValue()
        {
            return DefaultValue;
        }

        /// <summary>
        /// The list of valid <see cref="ObjectAttribute.EnumValue"/>
        /// </summary>
        public List<ObjectAttribute.EnumValue> Values
        {
            get;
            private set;
        }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is long))
                throw new Exceptions.InvalidAttributeValueError(attributeName, value);

            long v = (long)value;
            foreach (ObjectAttribute.EnumValue ev in Values) {
                if (ev.Value == v)
                    return true;
            }
            throw new Exceptions.InvalidAttributeValueError(attributeName, value);
        }

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is long) {
                long v = (long)value;
                foreach (ObjectAttribute.EnumValue ev in Values) {
                    if (ev.Value == v) {
                        xmlWriter.WriteString(ev.Name);
                        break;
                    }
                }
            }
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            if (Values != null) {
                string strVal = xmlReader.Value;
                foreach (ObjectAttribute.EnumValue ev in Values) {
                    if (string.Compare(ev.Name, strVal, StringComparison.OrdinalIgnoreCase) == 0)
                        return ev.Value;
                }
            }
            return null;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            if (value is long) {
                long v = (long)value;
                foreach (ObjectAttribute.EnumValue ev in Values) {
                    if (ev.Value == v) {
                        jsonWriter.WriteValue(ev.Name);
                        break;
                    }
                }
            }
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            if (Values != null) {
                string strVal = jsonReader.ReadAsString();
                foreach (ObjectAttribute.EnumValue ev in Values) {
                    if (string.Compare(ev.Name, strVal, StringComparison.OrdinalIgnoreCase) == 0)
                        return ev.Value;
                }
            }
            return null;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))
                return false;

            EnumAttrType at = attr as EnumAttrType;
            if (Values.Count != at.Values.Count)
                return false;

            for (int i=0; i<at.Values.Count; i++) {
                if (Values[i].Name != at.Values[i].Name || Values[i].Value != at.Values[i].Value)
                    return false;
            }
            return true;
        } // IsEqual
    } // EnumAttrType
}
