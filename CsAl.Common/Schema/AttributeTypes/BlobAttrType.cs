﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CsAlCore.Common.Schema.AttributeTypes
{
    /// <summary>
    /// Describes a blob attribute
    /// </summary>
    public class BlobAttrType : AttrTypeBase
    {
        /// <summary>
        /// Create a new <see cref="BlobAttrType"/>
        /// </summary>
        public BlobAttrType() :
          base(ObjectAttribute.Types.Blob)
        {
            IsAttribute = false;
            IsTableField = true;
        }

        /// <summary>
        /// The attribute maximum length (in bytes)
        /// </summary>
        public int MinLength { get; set; }
        /// <summary>
        /// The attribute minimum length (in bytes)
        /// </summary>
        public int MaxLength { get; set; }

        public override bool ValidateValue(string attributeName, object value)
        {
            if (value == null)
                return true;

            if (!(value is byte[]))
                throw new Common.Exceptions.InvalidAttributeValueError(attributeName, value);

            byte[] v = (byte[])value;
            if (MaxLength != 0 && v.Length > MaxLength)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("minimum length {0}, maximum length {1}", MinLength, MaxLength));

            if (MinLength != 0 && v.Length < MinLength)
                throw new Exceptions.InvalidAttributeValueError(attributeName, value, string.Format("minimum length {0}, maximum length {1}", MinLength, MaxLength));

            return true;
        } // ValidateValue

        public override void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value is byte[]) {
                xmlWriter.WriteString(Convert.ToBase64String((byte[])value));
            }
        } // Serialize

        public override object Deserialize(Schema schema, XmlReader xmlReader)
        {
            if (!string.IsNullOrEmpty(xmlReader.Value))
                return Convert.FromBase64String(xmlReader.Value);
            return null;
        } // Deserialize

        public override void Serialize(JsonWriter jsonWriter, object value)
        {
            if (value is byte[]) {
                jsonWriter.WriteValue(Convert.ToBase64String((byte[])value));
            }
        }

        public override object Deserialize(Schema schema, JsonReader jsonReader)
        {
            string value = jsonReader.ReadAsString();
            if (!string.IsNullOrEmpty(value))
                return Convert.FromBase64String(value);
            return null;
        }

        public override bool IsEqual(AttrTypeBase attr)
        {
            if (!base.IsEqual(attr))            
                return false;

            BlobAttrType at = attr as BlobAttrType;
            if (MaxLength != at.MaxLength || MinLength != at.MinLength)
                return false;

            return true;
        } // IsEqual
    } // BlobAttrType
}
