﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// Describes a multi reference attribute value.
    /// </summary>
    public class MultiReferenceValue
    {
        long m_RefId = 0;

        /// <summary>
        /// Create a new <see cref="MultiReferenceValue"/> from the referenced object id
        /// </summary>
        /// <param name="refId">The referenced object id</param>
        public MultiReferenceValue(long refId)
        {
            if (refId <= 0)
                throw new ArgumentOutOfRangeException("refId", "refId must be a positive number");
            m_RefId = refId;
        }

        /// <summary>
        /// Create a new <see cref="MultiReferenceValue"/> from the referenced object
        /// </summary>
        /// <param name="refObject">The referenced <see cref="GenericObject"/></param>
        public MultiReferenceValue(GenericObject refObject)
        {
            if (refObject == null)
                throw new ArgumentNullException("refObject");

            RefObject = refObject;
        }

        /// <summary>
        /// Create a new <see cref="MultiReferenceValue"/> from another one
        /// </summary>
        /// <param name="mRefValue">The other <see cref="MultiReferenceValue"/></param>
        public MultiReferenceValue(MultiReferenceValue mRefValue)
        {
            if (mRefValue == null)
                throw new ArgumentNullException("mRefValue");

            m_RefId = mRefValue.m_RefId;
            if (mRefValue.RefObject != null)
                RefObject = new GenericObject(mRefValue.RefObject);
            if (mRefValue.SubAttributes != null)
                SubAttributes = new GenericObject(mRefValue.SubAttributes);
        }

        /// <summary>
        /// The referenced object Id
        /// </summary>
        public long RefId
        {
            get {
                if (RefObject != null)
                    return RefObject.Id;
                return m_RefId;
            }
        }

        /// <summary>
        /// The referenced object
        /// </summary>
        public GenericObject RefObject { get; set; }

        /// <summary>
        /// The sub attributes
        /// </summary>
        public GenericObject SubAttributes { get; set; }
    } // MultiReferenceValue
}
