﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// Object validation error
    /// </summary>
    public class ValidationError
    {
        /// <summary>
        /// Create a new <see cref="ValidationError"/>
        /// </summary>
        public ValidationError()
        {

        }

        /// <summary>
        /// The error message
        /// </summary>
        [XmlAttribute]
        public string Message
        {
            get;
            set;
        }

        /// <summary>
        /// The name of the attribute the error refers to
        /// </summary>
        [XmlAttribute]
        public string AttributeName
        {
            get;
            set;
        }

        /// <summary>
        /// Get the string representation of the <see cref="ValidationError"/>
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return $"{ AttributeName }: {Message}";
        }
    } // ValidationError

    /// <summary>
    /// Collection of <see cref="ValidationError"/>
    /// </summary>
    [XmlRoot("ValidationErrors")]
    public class ValidationErrors
    {
        /// <summary>
        /// Create a new <see cref="ValidationErrors"/>
        /// </summary>
        public ValidationErrors()
        {

        }

        /// <summary>
        /// A list of <see cref="ValidationError"/>
        /// </summary>
        [XmlElement("ValidationError")]
        public List<ValidationError> Errors
        {
            get;
            set;
        }
    } // ValidationErrors
}
