﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// Defines an object attribute
    /// </summary>
    public class ObjectAttribute
    {
        /// <summary>
        /// Possible value for enum/flags attribute type
        /// </summary>
        public class EnumValue
        {
            public EnumValue()
            {

            }

            /// <summary>
            /// The value name
            /// </summary>
            [XmlAttribute]
            public string Name { get; set; }

            /// <summary>
            /// The numeric value
            /// </summary>
            [XmlAttribute]
            public long Value { get; set; }
        } // EnumValue

        /// <summary>
        /// Attribute types
        /// </summary>
        public enum Types
        {
            None = 0,
            Identity,
            Integer,
            Real,
            String,
            Password,
            Reference,
            MultiReference,
            Date,
            DateTime,
            TimeSpan,
            Blob,
            CreatedDateTime,
            LastModifiedDateTime,
            Enum,
            Boolean,
            Flags,
            BinaryData,
        }

        /// <summary>
        /// Attribute name
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Attribute description
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// The attribute <see cref="Types"/> name
        /// </summary>
        [XmlAttribute("Type")]
        public string TypeName { get; set; }

        /// <summary>
        /// The attribute <see cref="Types"/>
        /// </summary>
        [XmlIgnore]
        public Types Type
        {
            get {
                Types res = Types.None;
                if (Enum.TryParse<Types>(TypeName, out res))
                    return res;
                return Types.None;
            }
        }

        /// <summary>
        /// Applies to <see cref="Types.MultiReference"/>.
        /// If set to true the multireferenced object can be sorted.
        /// </summary>
        [XmlAttribute]
        public bool Sortable
        {
            get;
            set;
        }

        /// <summary>
        /// The <see cref="AttributeType"/>
        /// </summary>
        [XmlIgnore]
        public AttributeTypes.AttrTypeBase AttributeType
        {
            get;
            set;
        }

        /// <summary>
        /// Sub attribues, applies to <see cref="Types.MultiReference"/>
        /// </summary>
        [XmlArrayItem("Attribute")]
        public List<ObjectAttribute> SubAttributes
        {
            get;
            set;
        } // SubAttributes

        /// <summary>
        /// Attribute possible values, applies to <see cref="Types.Enum"/> and <see cref="Types.Flags"/>
        /// </summary>
        [XmlArrayItem("Value")]
        public List<EnumValue> Values { get; set; }

        /// <summary>
        /// Gets if this attribute is represented as a XML attribute
        /// </summary>
        [XmlIgnore]
        public bool IsAttribute
        {
            get {
                return AttributeType.IsAttribute;
            }
        }

        /// <summary>
        /// Gets if this attribute is represented as a XML element
        /// </summary>
        [XmlIgnore]
        public bool IsElement
        {
            get {
                return !AttributeType.IsAttribute;
            }
        }

        /// <summary>
        /// The attribute default value.
        /// For <see cref="Types.Enum"/> attribute set to the value name (e.g. 'News').
        /// For <see cref="Types.Flags"/> attribute set to a list of value names separated by a pipe '|' (e.g. 'News|Sport').
        /// </summary>
        [XmlAttribute]
        public string DefaultValue { get; set; }

        /// <summary>
        /// Regular expression to validate a string.
        /// Works wih <see cref="AttributeTypes.StringAttrType"/>
        /// </summary>
        [XmlAttribute]
        public string ValidationRegEx { get; set; }

        /// <summary>
        /// Get if the <see cref="ObjectAttribute"/> has a <see cref="DefaultValue"/> set
        /// </summary>
        [XmlIgnore]
        public bool HasDefaultValue
        {
            get { return !string.IsNullOrEmpty(DefaultValue); }
        }

        /// <summary>
        /// Get if the attribute is system managed (i.e. it is not exported or exposed to the user)
        /// </summary>
        [XmlIgnore]
        public bool SystemManaged
        {
            get {
                return string.Compare(Name, Schema.ObjectTypeRefName, StringComparison.OrdinalIgnoreCase) == 0 ||
                       string.Compare(Name, Schema.ObjectIsDeletedAttributeName, StringComparison.OrdinalIgnoreCase) == 0 ||
                       string.Compare(Name, Schema.ObjectDeletedAttributeName, StringComparison.OrdinalIgnoreCase) == 0 ||
                       string.Compare(Name, Schema.ChildProgressiveAttributeName, StringComparison.OrdinalIgnoreCase) == 0 ||
                       string.Compare(Name, Schema.ObjectVersionNumberAttributeName, StringComparison.OrdinalIgnoreCase) == 0;
            }
        }

        /// <summary>
        /// The real number precision.
        /// Applies to <see cref="Types.Real"/>
        /// </summary>
        [XmlAttribute]
        public int Precision { get; set; }

        /// <summary>
        /// The attribute minimum length.
        /// Applies to <see cref="Types.String"/> and <see cref="Types.Blob"/>
        /// </summary>
        [XmlAttribute]
        public int MinLength { get; set; }

        /// <summary>
        /// The attribute maximum length.
        /// Applies to <see cref="Types.String"/> and <see cref="Types.Blob"/>
        /// </summary>
        [XmlAttribute]
        public int MaxLength { get; set; }

        /// <summary>
        /// The attribute minimum value.
        /// Applies to <see cref="Types.Integer"/>, <see cref="Types.Real"/>, <see cref="Types.TimeSpan"/>
        /// </summary>
        [XmlAttribute]
        public string MinValue { get; set; }

        /// <summary>
        /// The attribute maximum value
        /// Applies to <see cref="Types.Integer"/>, <see cref="Types.Real"/>, <see cref="Types.TimeSpan"/>
        /// </summary>
        [XmlAttribute]
        public string MaxValue { get; set; }

        /// <summary>
        /// The referenced object type name.
        /// Applies to <see cref="Types.Reference"/> and <see cref="Types.MultiReference"/>
        /// </summary>
        [XmlAttribute]
        public string RefObjectType { get; set; }

        /// <summary>
        /// Defines if the <see cref="Types.MultiReference"/> is a father/children reference.
        /// Applies to <see cref="Types.MultiReference"/>
        /// </summary>
        [XmlAttribute]
        public bool ChildrenReference { get; set; }

        /// <summary>
        /// Defines is the attribute is required or not.
        /// </summary>
        [XmlAttribute]
        public bool Required { get; set; }

        /// <summary>
        /// The <see cref="ObjectType"/> name this attribute comes from
        /// </summary>
        [XmlAttribute]
        public string ObjectTypeName { get; set; }

        /// <summary>
        /// The <see cref="ObjectType"/> name this attribute has been inherited from
        /// </summary>
        [XmlAttribute]
        public string InheritedFrom { get; set; }

        /// <summary>
        /// A semicolon separated list of allowed mime types
        /// Applies to <see cref="Types.BinaryData"/>
        /// </summary>
        [XmlAttribute]
        public string AllowedMimeTypes { get; set; }

        /// <summary>
        /// Get if this <see cref="ObjectAttribute"/> is inherited
        /// </summary>
        [XmlIgnore]
        public bool IsInherited
        {
            get {
                return !string.IsNullOrEmpty(InheritedFrom);
            }
        }

        /// <summary>
        /// Get if this <see cref="ObjectAttribute"/> is an <see cref="ObjectAttribute.Types.Identity"/>
        /// </summary>
        [XmlIgnore]
        public bool IsIdentity
        {
            get {
                return Type == ObjectAttribute.Types.Identity;
            }
        }

        /// <summary>
        /// Get if this <see cref="ObjectAttribute"/> is the reference to the parent object
        /// </summary>
        [XmlIgnore]
        public bool IsParentObjRef
        {
            get { return Name == Schema.ParentObjElementName; }
        }

        /// <summary>
        /// Get if this <see cref="ObjectAttribute"/>'s value is saved in a database table's column
        /// </summary>
        public bool IsTableField
        {
            get {
                return AttributeType.IsTableField;
            }
        }

        /// <summary>
        /// Get the database table column's name
        /// </summary>
        [XmlIgnore]
        public string ColumnName
        {
            get {
                if (IsTableField)
                    return string.Format("a_{0}", Name.ToLower());
                else if (Type == Types.MultiReference)
                    return string.Format("a_{0}", ChildrenReference ? Schema.ObjectIdName.ToLower() : "RefId".ToLower());
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the <see cref="EnumValue"/> (from <see cref="Values"/>) with the given name
        /// </summary>
        /// <param name="name">The <see cref="EnumValue"/> name</param>
        /// <returns>A <see cref="EnumValue"/> or null</returns>
        public EnumValue GetValue(string name)
        {
            if (Values != null) {
                foreach (EnumValue ev in Values) {
                    if (string.Compare(ev.Name, name, StringComparison.OrdinalIgnoreCase) == 0)
                        return ev;
                }
            }
            return null;
        } // GetValue

        /// <summary>
        /// Return the sub attribute with the given name
        /// </summary>
        /// <param name="name">The sub attribute name</param>
        /// <returns>The <see cref="ObjectAttribute"/> or null</returns>
        public ObjectAttribute GetSubAttribute(string name)
        {
            if (SubAttributes != null) {
                foreach (ObjectAttribute sAt in SubAttributes) {
                    if (string.Compare(name, sAt.Name, StringComparison.OrdinalIgnoreCase) == 0)
                        return sAt;
                }
            }
            return null;
        } // GetSubAttribute

        /// <summary>
        /// Return a copy of the <see cref="ObjectAttribute"/>
        /// </summary>
        /// <returns>A new <see cref="ObjectAttribute"/></returns>
        public ObjectAttribute Copy()
        {
            string xml = Utility.Serialization.Serialize(this);
            return Utility.Serialization.Deserialize<ObjectAttribute>(xml);
        }

        public void Serialize(XmlWriter xmlWriter, object value)
        {
            if (value == null)
                return;

            AttributeType.Serialize(xmlWriter, value);
        } // Serialize

        public void Serialize(JsonWriter jsonWriter, object value)
        {
            if (value == null) {
                jsonWriter.WriteValue((object)null);
                return;
            }

            AttributeType.Serialize(jsonWriter, value);
        } // Serialize

        public object Deserialize(Schema schema, XmlReader xmlReader)
        {
            return AttributeType.Deserialize(schema, xmlReader);
        } // Deserialize


        public object Deserialize(Schema schema, JsonReader jsonReader)
        {
            return AttributeType.Deserialize(schema, jsonReader);
        } // Deserialize

        public new string ToString()
        {
            if (Type == Types.Reference || Type == Types.MultiReference)
                return string.Format("{0} [{1}] Ref: {2}", Name, Type, RefObjectType);
            return string.Format("{0} [{1}]", Name, Type);
        } // ToString
    }
}
