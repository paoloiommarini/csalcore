﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace CsAlCore.Common.Schema
{
    public class SchemaIncludes
    {
        [XmlAttribute]
        public string Path { get; set; }
    }

    /// <summary>
    /// Informations on a path
    /// </summary>
    public class PathInfo
    {
        /// <summary>
        /// The path result type
        /// </summary>
        public enum ResolveResult
        {
            /// <summary>
            /// No result
            /// </summary>
            None,
            /// <summary>
            /// An object
            /// </summary>
            Object,
            /// <summary>
            /// An attribute
            /// </summary>
            Attribute,
        } // ResolveResult

        public PathInfo()
        {
            ObjectTypes = new List<ObjectType>();
            AttributeTypes = new List<ObjectAttribute>();
        }

        public PathInfo(string path)
        {
            Path = path;
            ObjectTypes = new List<ObjectType>();
            AttributeTypes = new List<ObjectAttribute>();
        }

        /// <summary>
        /// The path
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// The path's <see cref="ResolveResult"/>
        /// </summary>
        public ResolveResult Result { get; set; }

        /// <summary>
        /// A list of the <see cref="ObjectType"/> used by tha path
        /// </summary>
        public List<ObjectType> ObjectTypes { get; set; }

        /// <summary>
        /// A list of the <see cref="ObjectAttribute"/> used by tha path
        /// </summary>
        public List<ObjectAttribute> AttributeTypes { get; set; }
    } // PathInfo

    /// <summary>
    /// The database schema
    /// </summary>
    public class Schema
    {
        #region Classes
        public class SchemaFile
        {
            public string FileName { get; set; }
            public string Content { get; set; }
        } // SchemaFile
        #endregion

        #region Constants
        public const string SystemTablePrefix = "sys_";
        public const string ObjectTablePrefix = "obj_";

        public const string ObjectObjectType = "object";
        public const string ObjectIdName = "id";
        public const string ObjectTypeRefName = "objectTypeRef";
        public const string ObjectVersionNumberAttributeName = "versionNumber";
        public const string ObjectIsDeletedAttributeName = "isDeleted";
        public const string ObjectDeletedAttributeName = "deleted";
        public const string ReferenceIdName = "idRef";
        public const string ParentObjElementName = "parentObjRef";
        public const string RefElementName = "ref";
        public const string ChildProgressiveAttributeName = "childProgressive";
        public const string FlagValueName = "Value";
        public const string FlagValueAttributeName = "Name";
        public const string ObjectsListElementName = "objects";
        public const string BinaryDataElementName = "binaryData";
        public const string JsonObjectTypeTokenName = "objectType";
        #endregion

        bool m_EnableForeignKeys = true;

        Dictionary<string, ObjectType> m_ObjectTypesDict = null;

        /// <summary>
        /// The schema name
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// The schema revision
        /// </summary>
        [XmlAttribute]
        public int Revision { get; set; }


        /// <summary>
        /// The system schema revision
        /// </summary>
        [XmlAttribute]
        public int SystemSchemaRevision { get; set; }

        /// <summary>
        /// Set if foreign keys should be used when creating tables in database
        /// </summary>
        [XmlAttribute]
        public bool EnableForeignKeys
        {
            get { return m_EnableForeignKeys; }
            set { m_EnableForeignKeys = value; }
        }

        /// <summary>
        /// The schema description
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// A list of the included schema
        /// </summary>
        [XmlArrayItem("File")]
        public List<SchemaIncludes> Includes { get; set; }

        /// <summary>
        /// The types defined by the schema
        /// </summary>
        [XmlArrayItem("Attribute")]
        public List<ObjectAttribute> Types { get; set; }

        /// <summary>
        /// The <see cref="ObjectType"/> defined by the schema
        /// </summary>
        public List<ObjectType> ObjectTypes { get; set; }

        /// <summary>
        /// The list of <see cref="SchemaFile"/> composing the schema
        /// </summary>
        [XmlIgnore]
        public List<SchemaFile> Files { get; set; }

        /// <summary>
        /// The list of object type names in the schema
        /// </summary>
        [XmlIgnore]
        public List<string> ObjectTypeNames
        {
            get {
                List<string> res = new List<string>();
                if (ObjectTypes != null) {
                    foreach (ObjectType ot in ObjectTypes) {
                        res.Add(ot.Name);
                    }
                }
                return res;
            }
        }

        /// <summary>
        /// Completes the Attribute types informations with a AttributeTypes class
        /// </summary>
        public void CompleteAttributeTypes()
        {
            if (ObjectTypes != null) {
                // First add childOf attributes
                foreach (ObjectType ot in ObjectTypes) {
                    if (ot.Attributes != null) {
                        for (int i = 0; i < ot.Attributes.Count; i++) {
                            ObjectAttribute at = ot.Attributes[i];
                            if (at.Type == ObjectAttribute.Types.MultiReference && at.ChildrenReference) {
                                // Mark the referenced object type as child
                                ObjectType rOt = GetObjectType(at.RefObjectType);
                                rOt.ParentObjectType = ot.Name;

                                List<ObjectIndex.IndexAttribute> indexAttrs = new List<ObjectIndex.IndexAttribute>();
                                bool indexUnique = false;
                                // Add parentObjRef attribute to the child objectType
                                ObjectAttribute oa = rOt.GetAttribute(Schema.ParentObjElementName);
                                if (oa == null) {
                                    rOt.Attributes.Add(new ObjectAttribute()
                                    {
                                        Name = Schema.ParentObjElementName,
                                        Description = "Reference to the parent object",
                                        TypeName = ObjectAttribute.Types.Reference.ToString(),
                                        RefObjectType = ot.Name,
                                        Required = true,
                                        AttributeType = new AttributeTypes.ChildOfAttrType(at.RefObjectType)
                                    });
                                } else if (oa.AttributeType == null) {
                                    oa.AttributeType = new AttributeTypes.ChildOfAttrType(at.RefObjectType);
                                }

                                indexAttrs.Add(new ObjectIndex.IndexAttribute() { Name = Schema.ParentObjElementName });

                                // Add childProgressiveAttributeName attribute to the child objectType
                                if (at.Sortable) {
                                    oa = rOt.GetAttribute(Schema.ChildProgressiveAttributeName);
                                    if (oa == null) {
                                        rOt.Attributes.Add(new ObjectAttribute()
                                        {
                                            Name = Schema.ChildProgressiveAttributeName,
                                            Description = "Progressive number used to sort children",
                                            TypeName = ObjectAttribute.Types.Integer.ToString(),
                                            Required = true,
                                            AttributeType = new AttributeTypes.IntegerAttrType()
                                        });
                                    }
                                    indexUnique = true;
                                    indexAttrs.Add(new ObjectIndex.IndexAttribute() { Name = Schema.ChildProgressiveAttributeName });
                                }

                                // Additional index on parentObjRef
                                string indexName = $"idx_{ rOt.Name }_parentRef";
                                if (rOt.GetIndex(indexName) == null) {
                                    rOt.Indexes.Add(new ObjectIndex()
                                    {
                                        Name = indexName,
                                        Unique = indexUnique,
                                        Attributes = indexAttrs
                                    });
                                }
                            }
                        }
                    }
                }

                foreach (ObjectType ot in ObjectTypes) {
                    if (ot.Attributes != null) {
                        for (int i = 0; i < ot.Attributes.Count; i++) {
                            ObjectAttribute at = ot.Attributes[i];
                            if (at.AttributeType != null)
                                continue;

                            ot.Attributes[i] = CompleteAttributeType(at);
                            if (at.SubAttributes != null) {
                                for (int j = 0; j < at.SubAttributes.Count; j++) {
                                    ObjectAttribute sAt = at.SubAttributes[j];
                                    at.SubAttributes[j] = CompleteAttributeType(sAt);
                                }
                            }
                        }

                        // Forse update dictionaries (if we used Types)
                        List<ObjectAttribute> attrs = ot.Attributes;
                        ot.Attributes = new List<ObjectAttribute>();
                        ot.Attributes = attrs;
                    }
                }
            }
        } // CompleteAttributeTypes

        private ObjectAttribute CompleteAttributeType(ObjectAttribute at)
        {
            object defaultValue = null;
            // Look for type:
            ObjectAttribute type = GetType(at.TypeName);
            if (type != null) {
                ObjectAttribute oAt = at;
                at = type.Copy();
                at.Description = oAt.Description;
                at.ObjectTypeName = oAt.ObjectTypeName;
                at.Name = oAt.Name;
                at.Required = oAt.Required;
                at.Sortable = oAt.Sortable;
                at.ValidationRegEx = oAt.ValidationRegEx;
                at.InheritedFrom = oAt.InheritedFrom;
            }

            switch (at.Type) {
                case ObjectAttribute.Types.BinaryData:
                    if (at.HasDefaultValue)
                        throw new Exceptions.AttributeCannotHaveDefaultValueError(at.Name);

                    at.AttributeType = new AttributeTypes.BinaryDataAttrType()
                    {
                        AllowedMimeTypes = at.AllowedMimeTypes?.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries),
                        MaxLength = at.MaxLength,
                        MinLength = at.MinLength
                    };
                    at.RefObjectType = "binaryData";
                    break;
                case ObjectAttribute.Types.Blob:
                    if (at.HasDefaultValue)
                        throw new Exceptions.AttributeCannotHaveDefaultValueError(at.Name);
                    at.AttributeType = new AttributeTypes.BlobAttrType()
                    {
                        MinLength = at.MinLength,
                        MaxLength = at.MaxLength
                    };
                    break;
                case ObjectAttribute.Types.Boolean:
                    defaultValue = string.Compare(at.DefaultValue, "true", StringComparison.OrdinalIgnoreCase) == 0;
                    at.AttributeType = new AttributeTypes.BooleanAttrType()
                    {
                        DefaultValue = (bool)defaultValue,
                    };
                    break;
                case ObjectAttribute.Types.Integer:
                    int iMin = 0;
                    int iMax = 0;
                    int iDef = 0;
                    if (!string.IsNullOrEmpty(at.MinValue) && !int.TryParse(at.MinValue, out iMin))
                        throw new Exceptions.InvalidAttributeRangeValueError(at.Name, at.MinValue, at.MaxValue);
                    if (!string.IsNullOrEmpty(at.MaxValue) && !int.TryParse(at.MaxValue, out iMax))
                        throw new Exceptions.InvalidAttributeRangeValueError(at.Name, at.MinValue, at.MaxValue);
                    if (iMin > iMax && iMax != 0)
                        throw new Exceptions.InvalidAttributeRangeValueError(at.Name, at.MinValue, at.MaxValue);
                    if (!string.IsNullOrEmpty(at.DefaultValue) && !int.TryParse(at.DefaultValue, out iDef))
                        throw new Exceptions.InvalidAttributeDefaultValueError(at.Name, at.DefaultValue);

                    defaultValue = at.HasDefaultValue ? (int?)iDef : null;
                    at.AttributeType = new AttributeTypes.IntegerAttrType()
                    {
                        MinValue = iMin,
                        MaxValue = iMax,
                        DefaultValue = (int?)defaultValue,
                    };
                    break;
                case ObjectAttribute.Types.Identity:
                    if (at.HasDefaultValue)
                        throw new Exceptions.AttributeCannotHaveDefaultValueError(at.Name);
                    at.AttributeType = new AttributeTypes.IntegerAttrType();
                    at.Required = true; // Force required for identity attributes
                    break;
                case ObjectAttribute.Types.Date:
                    DateTime? dateDef = Utility.Serialization.DateFromXml(at.DefaultValue);
                    DateTime? dateMin = Utility.Serialization.DateFromXml(at.MinValue);
                    DateTime? dateMax = Utility.Serialization.DateFromXml(at.MaxValue);

                    at.AttributeType = new AttributeTypes.DateAttrType()
                    {
                        DefaultValue = dateDef,
                        MaxValue = dateMax,
                        MinValue = dateMin
                    };
                    defaultValue = dateDef;
                    break;
                case ObjectAttribute.Types.DateTime:
                    DateTime? dtDef = Utility.Serialization.DateTimeFromXml(at.DefaultValue);
                    DateTime? dtMin = Utility.Serialization.DateFromXml(at.MinValue);
                    DateTime? dtMax = Utility.Serialization.DateFromXml(at.MaxValue);

                    at.AttributeType = new AttributeTypes.DateTimeAttrType()
                    {
                        DefaultValue = dtDef,
                        MaxValue = dtMax,
                        MinValue = dtMin
                    };
                    defaultValue = dtDef;
                    break;
                case ObjectAttribute.Types.LastModifiedDateTime:
                case ObjectAttribute.Types.CreatedDateTime:
                    if (at.HasDefaultValue)
                        throw new Exceptions.AttributeCannotHaveDefaultValueError(at.Name);
                    at.AttributeType = new AttributeTypes.DateTimeAttrType();

                    if (string.IsNullOrEmpty(at.Description)) {
                        if (at.Type == ObjectAttribute.Types.CreatedDateTime)
                            at.Description = "The creation date time";
                        else
                            at.Description = "The last modified date time";
                    }
                    break;
                case ObjectAttribute.Types.Enum:
                    if (!string.IsNullOrEmpty(at.DefaultValue) && at.GetValue(at.DefaultValue) == null)
                        throw new Exceptions.InvalidAttributeDefaultValueError(at.Name, at.DefaultValue);

                    defaultValue = at.HasDefaultValue ? (long?)at.GetValue(at.DefaultValue).Value : null;
                    at.AttributeType = new AttributeTypes.EnumAttrType(at.Values)
                    {
                        DefaultValue = (long?)defaultValue,
                    };
                    
                    break;
                case ObjectAttribute.Types.Flags:
                    long? fDef = null;
                    if (!string.IsNullOrEmpty(at.DefaultValue)) {
                        string[] flags = at.DefaultValue.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string flag in flags) {
                            ObjectAttribute.EnumValue fv = at.GetValue(flag.Trim());
                            if (fv == null)
                                throw new Exceptions.InvalidAttributeDefaultValueError(at.Name, at.DefaultValue);

                            if (fDef == null)
                                fDef = 0;
                            fDef |= fv.Value;
                        }

                        if (fDef == null)
                            throw new Exceptions.InvalidAttributeDefaultValueError(at.Name, at.DefaultValue);
                    }

                    at.AttributeType = new AttributeTypes.FlagsAttrType(at.Values)
                    {
                        DefaultValue = fDef,
                    };
                    defaultValue = fDef;
                    break;
                case ObjectAttribute.Types.MultiReference:
                    if (at.HasDefaultValue)
                        throw new Exceptions.AttributeCannotHaveDefaultValueError(at.Name);
                    at.AttributeType = new AttributeTypes.MultiReferenceAttrType(at.RefObjectType, at.ChildrenReference, at.SubAttributes);
                    break;
                case ObjectAttribute.Types.Real:
                    double dMin = 0;
                    double dMax = 0;
                    double dDef = 0;
                    if (!string.IsNullOrEmpty(at.MinValue) && !double.TryParse(at.MinValue, out dMin))
                        throw new Exceptions.InvalidAttributeRangeValueError(at.Name, at.MinValue, at.MaxValue);
                    if (!string.IsNullOrEmpty(at.MinValue) && !double.TryParse(at.MaxValue, out dMax))
                        throw new Exceptions.InvalidAttributeRangeValueError(at.Name, at.MinValue, at.MaxValue);
                    if (dMin > dMax)
                        throw new Exceptions.InvalidAttributeRangeValueError(at.Name, at.MinValue, at.MaxValue);
                    if (!string.IsNullOrEmpty(at.DefaultValue) && !double.TryParse(at.DefaultValue, out dDef))
                        throw new Exceptions.InvalidAttributeDefaultValueError(at.Name, at.DefaultValue);

                    at.AttributeType = new AttributeTypes.RealAttrType()
                    {
                        MinValue = dMin,
                        MaxValue = dMax,
                        DefaultValue = at.HasDefaultValue ? (double?)dDef : null,
                        Precision = at.Precision,
                    };
                    defaultValue = at.HasDefaultValue ? (double?)dDef : null;
                    break;
                case ObjectAttribute.Types.Reference:
                    if (at.HasDefaultValue)
                        throw new Exceptions.AttributeCannotHaveDefaultValueError(at.Name);
                    at.AttributeType = new AttributeTypes.ReferenceAttrType(at.RefObjectType);
                    break;
                case ObjectAttribute.Types.String:
                case ObjectAttribute.Types.Password:
                    if (at.MinLength > at.MaxLength)
                        throw new Exceptions.InvalidAttributeRangeValueError(at.Name, at.MinLength, at.MaxLength);

                    at.AttributeType = new AttributeTypes.StringAttrType()
                    {
                        MinLength = at.MinLength,
                        MaxLength = at.MaxLength,
                        DefaultValue = at.DefaultValue,
                        ValidationRegEx = at.ValidationRegEx,
                    };
                    defaultValue = at.DefaultValue;
                    break;
                case ObjectAttribute.Types.TimeSpan:
                    TimeSpan? tsDef = Utility.Serialization.TimeSpanFromXml(at.DefaultValue);
                    TimeSpan? tsMin = Utility.Serialization.TimeSpanFromXml(at.MinValue);
                    TimeSpan? tsMax = Utility.Serialization.TimeSpanFromXml(at.MaxValue);

                    at.AttributeType = new AttributeTypes.TimeSpanAttrType()
                    {
                        DefaultValue = tsDef,
                        MinValue = tsMin,
                        MaxValue = tsMax
                    };
                    defaultValue = tsDef;
                    break;
                default:
                    throw new Exceptions.UnknownAttributeType(at.TypeName);
            }

            if (at.HasDefaultValue) {
                try {
                    at.AttributeType.ValidateValue(at.Name, defaultValue);
                } catch (Exception) {
                    throw new Exceptions.InvalidAttributeDefaultValueError(at.Name, at.DefaultValue);
                }
            }
            return at;
        } // CompleteAttributeType

        /// <summary>
        /// Validate the schema
        /// </summary>
        /// <returns>True on success</returns>
        public bool Validate()
        {
            bool res = true;

            if (string.IsNullOrEmpty(Name))
                throw new Exceptions.MissingSchemaNameError();
            if (Revision <= 0)
                throw new Exceptions.InvalidSchemaRevisionError();

            // Types:
            if (Types != null) {
                foreach (ObjectAttribute ta in Types) {
                    // A user type cannot be named as a standard type
                    ObjectAttribute.Types tType = ObjectAttribute.Types.None;
                    if (Enum.TryParse<ObjectAttribute.Types>(ta.Name, out tType))
                        throw new Exceptions.InvalidTypeNameError(ta.Name);
                }
            }

            HashSet<string> reservedAttributeNames = new HashSet<string>()
            {
                Schema.ObjectTypeRefName.ToLower(),
                Schema.ChildProgressiveAttributeName.ToLower(),
                Schema.ObjectVersionNumberAttributeName.ToLower(),
                Schema.ObjectIsDeletedAttributeName.ToLower(),
                Schema.ObjectDeletedAttributeName.ToLower(),
            };

            // ObjectTypes:
            ObjectType objectOt = GetObjectType(ObjectObjectType);
            HashSet<string> oTypes = new HashSet<string>();
            HashSet<string> idxNames = new HashSet<string>();
            foreach (ObjectType ot in ObjectTypes) {
                if (string.IsNullOrEmpty(ot.Name) || ot.Name.Contains(" "))
                    throw new Exceptions.InvalidObjectOrAttributeNameError(ot.Name);

                if (oTypes.Contains(ot.Name.ToLower()))
                    throw new Exceptions.DuplicatedObjectTypeError(ot.Name);
                oTypes.Add(ot.Name);

                if (!ot.SystemObject) {
                    // Non system objects must have an identity attribute
                    if (ot.GetIdentity(true) == null)
                        throw new Exceptions.NonSystemObjectMustHaveIdentityError(ot.Name);
                }

                HashSet<string> attNames = new HashSet<string>();
                foreach (ObjectAttribute att in ot.Attributes) {
                    if (string.IsNullOrEmpty(att.Name) || att.Name.Contains(" ") ||
                        !att.SystemManaged && reservedAttributeNames.Contains(att.Name.ToLower()))
                        throw new Exceptions.InvalidObjectOrAttributeNameError(att.Name);
                    if (attNames.Contains(att.Name))
                        throw new Exceptions.DuplicatedAttributeError(att.Name);
                    if (att.Type == ObjectAttribute.Types.None)
                        throw new Exceptions.InvalidAtributeTypeError(att.Name, att.TypeName);

                    // Reference and multireference
                    ObjectType refOt = null;
                    if (att.Type == ObjectAttribute.Types.Reference || att.Type == ObjectAttribute.Types.MultiReference) {
                        if (string.IsNullOrEmpty(att.RefObjectType))
                            throw new Exceptions.UnknownObjectType(att.RefObjectType);

                        refOt = GetObjectType(att.RefObjectType);
                        if (refOt == null)
                            throw new Exceptions.UnknownObjectType(att.RefObjectType);

                        if (!refOt.SystemObject && string.Compare(GetObjectTypeInheritance(refOt.Name).FirstOrDefault().Name, Schema.ObjectObjectType, StringComparison.OrdinalIgnoreCase) != 0)
                            throw new Exceptions.ReferenceMustInheritFromObjectError(att.RefObjectType);                        
                    }

                    // Sub attributes
                    if (att.SubAttributes != null && att.SubAttributes.Count > 0) {
                        if (att.Type != ObjectAttribute.Types.MultiReference || att.ChildrenReference)
                            throw new Exceptions.InvalidSubAttributesError(att.Name);

                        if (refOt != null) {
                            foreach (ObjectAttribute sAt in att.SubAttributes) {
                                if (!sAt.IsTableField || sAt.Type == ObjectAttribute.Types.CreatedDateTime ||
                                  sAt.Type == ObjectAttribute.Types.LastModifiedDateTime || sAt.Type == ObjectAttribute.Types.Identity)
                                    throw new Exceptions.InvalidSubAttributesError(sAt.Name);
                                if (refOt.GetAttribute(sAt.Name) != null)
                                    throw new Exceptions.DuplicatedAttributeError(sAt.Name);
                            }
                        }
                    }

                    // Flags
                    if (att.Type == ObjectAttribute.Types.Flags) {
                        if (att.Values == null || att.Values.Count == 0)
                            throw new Exceptions.MissingFlagsEnumValuesError(att.Name);

                        HashSet<long> tv = new HashSet<long>();
                        long last = 0;
                        foreach (ObjectAttribute.EnumValue ev in att.Values.OrderBy(o => o.Value)) {
                            if (tv.Contains(ev.Value))
                                throw new Exceptions.DuplicatedFlagsEnumValuesError(att.Name, ev.Name);

                            if (last > long.MaxValue / 2)
                                throw new Exceptions.InvalidFlagsValueError(att.Name, ev.Name);
                            if (last != 0 && ev.Value != last * 2)
                                throw new Exceptions.InvalidFlagsValueError(att.Name, ev.Name);
                            if (string.IsNullOrEmpty(ev.Name) || ev.Name.Contains("|") || char.IsDigit(ev.Name[0]))
                                throw new Exceptions.InvalidFlagsNameError(att.Name, ev.Name);
                            last = ev.Value;
                            tv.Add(ev.Value);
                        }
                    }

                    // Enum
                    if (att.Type == ObjectAttribute.Types.Enum) {
                        if (att.Values == null || att.Values.Count == 0)
                            throw new Exceptions.MissingFlagsEnumValuesError(att.Name);

                        List<long> tv = new List<long>();
                        foreach (ObjectAttribute.EnumValue ev in att.Values) {
                            if (tv.Contains(ev.Value))
                                throw new Exceptions.DuplicatedFlagsEnumValuesError(att.Name, ev.Name);
                            tv.Add(ev.Value);
                        }
                    }
                    
                    attNames.Add(att.Name);
                }

                // Indexes
                if (ot.Indexes != null) {
                    foreach (ObjectIndex idx in ot.Indexes) {
                        if (string.IsNullOrEmpty((idx.Name))) {
                            Common.Utility.ShortGuid sg = Guid.NewGuid();
                            idx.Name = sg.Value.Replace("-", "_");
                        }

                        if (idxNames.Contains(idx.Name))
                            throw new Exceptions.DuplicatedIndexNameError(idx.Name);
                        
                        if (idx.Attributes == null || idx.Attributes.Count == 0)
                            throw new Exceptions.EmptyIndexError(idx.Name);
                        foreach (ObjectIndex.IndexAttribute iat in idx.Attributes) {
                            ObjectAttribute at = ot.GetAttribute(iat.Name);
                            if (at == null)
                                throw new Exceptions.UnknownAttributeType(iat.Name);
                        }

                        idxNames.Add(idx.Name);
                    }
                }

                // ValidationXslt
                if (!string.IsNullOrEmpty(ot.ValidationXslt)) {
                    XslCompiledTransform xslt = new XslCompiledTransform();
                    try {
                        using (StringReader sr = new StringReader(ot.ValidationXslt)) {
                            using (XmlReader xtr = XmlReader.Create(sr)) {
                                xslt.Load(xtr);
                            }
                        }
                    } catch (Exception ex) {
                        throw new Exceptions.InvalidXsltError(ot.Name, ex.Message);
                    }
                    ot.ValidationXsltCompiled = xslt;
                }
            }

            return res;
        } // Validate

        /// <summary>
        /// Get a type defined in the schema
        /// </summary>
        /// <param name="name">The type name</param>
        /// <returns>The corresponding ObjectAttribute or null</returns>
        public ObjectAttribute GetType(string name)
        {
            ObjectAttribute res = null;
            if (Types != null) {
                foreach (ObjectAttribute at in Types) {
                    if (string.Compare(at.Name, name, StringComparison.OrdinalIgnoreCase) == 0) {
                        res = at;
                        break;
                    }
                }
            }
            return res;
        } // GetType

        /// <summary>
        /// Get an object type from the schema
        /// </summary>
        /// <param name="name">The object type name</param>
        /// <returns>The corresponding ObjectType or null</returns>
        public ObjectType GetObjectType(string name)
        {
            ObjectType res = null;
            if (m_ObjectTypesDict == null)
                UpdateDictionaries();
            m_ObjectTypesDict.TryGetValue(name.ToLower(), out res);
            return res;
        } // GetObjectType

        /// <summary>
        /// Get the inheritance chain of the given object type
        /// </summary>
        /// <param name="objectTypeName">The object type name</param>
        /// <returns>A list ObjectType</returns>
        public List<ObjectType> GetObjectTypeInheritance(string objectTypeName)
        {
            List<ObjectType> res = new List<ObjectType>();
            ObjectType ot = GetObjectType(objectTypeName);
            if (ot != null)
                res.Add(ot);
            while (ot != null) {
                ot = string.IsNullOrEmpty(ot.Inherits) ? null : GetObjectType(ot.Inherits);
                if (ot != null)
                    res.Insert(0, ot);
            }
            return res;
        } // GetInheritance

        /// <summary>
        /// Get all the <see cref="ObjectType"/> inheriting from this object type
        /// </summary>
        /// <param name="objectTypeName"></param>
        /// <returns>A list of <see cref="ObjectType"/></returns>
        public List<ObjectType> GetObjectTypeDescendants(string objectTypeName)
        {
            List<ObjectType> res = new List<ObjectType>();
            foreach (ObjectType ot in ObjectTypes) {
                if (string.Compare(ot.Inherits, objectTypeName, StringComparison.OrdinalIgnoreCase) == 0)
                    res.Add(ot);
            }

            return res;
        } // GetObjectTypeDescenders

        /// <summary>
        /// Get the attributes referencing the given object type
        /// </summary>
        /// <param name="objectTypeName">The object type name</param>
        /// <returns>A list <see cref="ObjectAttribute"/></returns>
        public List<ObjectAttribute> GetReferencing(string objectTypeName)
        {
            List<ObjectAttribute> res = new List<ObjectAttribute>();
            ObjectType refOt = GetObjectType(objectTypeName);
            if (refOt != null) {
                foreach (ObjectType ot in ObjectTypes) {
                    foreach (ObjectAttribute oAt in ot.Attributes) {
                        if ((oAt.Type == ObjectAttribute.Types.Reference || oAt.Type == ObjectAttribute.Types.MultiReference) && string.Compare(oAt.RefObjectType, refOt.Name, StringComparison.OrdinalIgnoreCase) == 0)
                            res.Add(oAt);
                    }
                }
            }
            return res;
        } // GetReferencing

        /// <summary>
        /// Resolve the given path and return its <see cref="PathInfo"/> 
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>The path's <see cref="PathInfo"/></returns>
        public PathInfo ResolvePath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return null;

            PathInfo res = new PathInfo(path);
            string[] parts = path.Split('.');
            ObjectType ot = null;
            if (parts.Length > 0) {
                ot = GetObjectType(parts[0]);
                if (ot == null)
                    throw new Exceptions.UnknownObjectType(parts[0]);
                res.ObjectTypes.Add(ot);
                res.Result = PathInfo.ResolveResult.Object;
            }

            ObjectAttribute at = null;
            for (int i = 1; i < parts.Length; i++) {
                at = ot.GetAttribute(parts[i]);
                if (at == null)
                    throw new Exceptions.UnknownAttributeType(parts[i]);

                if (!res.ObjectTypes.Contains(ot))
                    res.ObjectTypes.Add(ot);

                res.AttributeTypes.Add(at);
                res.Result = PathInfo.ResolveResult.Attribute;

                // Add the referenced object type
                if (at.Type == ObjectAttribute.Types.Reference || at.Type == ObjectAttribute.Types.MultiReference) {
                    ot = GetObjectType(at.RefObjectType);
                    res.ObjectTypes.Add(ot);
                    res.Result = PathInfo.ResolveResult.Object;

                    if (at.Type == ObjectAttribute.Types.Reference) {
                        // Check reference virtual paths ("idRef", "objectTypeName", ...)
                        if (i < parts.Length - 1) {
                            string next = parts[i + 1];
                            if (string.Compare(next, Schema.ReferenceIdName, StringComparison.OrdinalIgnoreCase) == 0) {
                                // Remove the object type of the referenced object
                                res.ObjectTypes.RemoveAt(res.ObjectTypes.Count - 1);
                                i++;
                                res.Result = PathInfo.ResolveResult.Attribute;
                            } else if (string.Compare(next, ot.Name, StringComparison.OrdinalIgnoreCase) == 0) {
                                i++;
                                res.Result = PathInfo.ResolveResult.Object;
                            } else {
                                throw new Exceptions.InvalidAttributePathError(path, string.Format("expecting '{0}' or '{1}' after '{2}'", Schema.ReferenceIdName, ot.Name, parts[i]));
                            }
                        }
                    } else if (at.Type == ObjectAttribute.Types.MultiReference) {
                        // Check reference virtual paths ("ref.objectTypeName", "ref.SubAttribute"...)
                        if (i < parts.Length - 1) {
                            string next = parts[i + 1];
                            if (string.Compare(next, Schema.RefElementName, StringComparison.OrdinalIgnoreCase) == 0) {
                                i++;
                                res.Result = PathInfo.ResolveResult.Object;
                            } else {
                                throw new Exceptions.InvalidAttributePathError(path, string.Format("expecting '{0}' or '{1}' after '{2}'", Schema.RefElementName, ot.Name, parts[i]));
                            }
                        }

                        // Check sub attributes and "objectTypeName"
                        if (i < parts.Length - 1) {
                            string next = parts[i + 1];
                            ObjectAttribute sAt = at.GetSubAttribute(next);
                            if (string.Compare(next, ot.Name, StringComparison.OrdinalIgnoreCase) == 0) {
                                i++;
                            } else if (string.Compare(next, Schema.ReferenceIdName, StringComparison.OrdinalIgnoreCase) == 0) {
                                // Remove the object type of the referenced object
                                //res.ObjectTypes.RemoveAt(res.ObjectTypes.Count - 1);
                                i++;
                                res.Result = PathInfo.ResolveResult.Attribute;
                            } else if (sAt != null) {
                                // Sub attribute:
                                i++;
                                res.AttributeTypes.Add(sAt);
                                res.Result = PathInfo.ResolveResult.Attribute;
                            } else {
                                throw new Exceptions.InvalidAttributePathError(path, string.Format("expecting '{0}' or '{1}' after '{2}'", Schema.ReferenceIdName, ot.Name, parts[i]));
                            }
                        }
                    }
                } else if (at.Type == ObjectAttribute.Types.BinaryData) {
                    // Check binary data virtual paths ("binaryData.fileName", "binaryData.size"...)
                    if (i < parts.Length - 1) {
                        string next = parts[i + 1];
                        if (string.Compare(next, Schema.BinaryDataElementName, StringComparison.OrdinalIgnoreCase) == 0) {
                            i++;
                            res.Result = PathInfo.ResolveResult.Object;
                        } else {
                            throw new Exceptions.InvalidAttributePathError(path, string.Format("expecting '{0}' after '{1}'", Schema.BinaryDataElementName, parts[i]));
                        }
                    }

                    if (i < parts.Length - 1) {
                        string next = parts[i + 1];
                        ObjectType bdOt = new ObjectType()
                        {
                            Name = "binaryData",
                            SystemObject = true
                        };

                        if (string.Compare(next, "id", StringComparison.OrdinalIgnoreCase) == 0) {
                            i++;
                            res.Result = PathInfo.ResolveResult.Attribute;
                        } else if (string.Compare(next, "fileName", StringComparison.OrdinalIgnoreCase) == 0) {
                            i++;
                            res.ObjectTypes.Add(bdOt);
                            res.AttributeTypes.Add(new ObjectAttribute()
                            {
                                Name = "fileName",
                                ObjectTypeName = bdOt.Name,
                                TypeName = "String",
                                AttributeType = new AttributeTypes.StringAttrType()
                            });
                            res.Result = PathInfo.ResolveResult.Attribute;
                        } else if (string.Compare(next, "mimeType", StringComparison.OrdinalIgnoreCase) == 0) {
                            i++;
                            res.ObjectTypes.Add(bdOt);
                            res.AttributeTypes.Add(new ObjectAttribute()
                            {
                                Name = "mimeType",
                                ObjectTypeName = bdOt.Name,
                                TypeName = "String",
                                AttributeType = new AttributeTypes.StringAttrType()
                            });
                            res.Result = PathInfo.ResolveResult.Attribute;
                        } else if (string.Compare(next, "size", StringComparison.OrdinalIgnoreCase) == 0) {
                            i++;
                            res.ObjectTypes.Add(bdOt);
                            res.AttributeTypes.Add(new ObjectAttribute()
                            {
                                Name = "fileSize",
                                ObjectTypeName = bdOt.Name,
                                TypeName = "Integer",
                                AttributeType = new AttributeTypes.IntegerAttrType()
                            });
                            res.Result = PathInfo.ResolveResult.Attribute;
                        }
                    }
                }

            }
            return res;
        } // ResolvePath

        /// <summary>
        /// Merge the given schema to this one
        /// </summary>
        /// <param name="schema">The schema to merge</param>
        /// <returns>True on success</returns>
        public bool Merge(Schema schema)
        {
            int idx = 0;
            // Types
            if (schema.Types != null) {
                foreach (ObjectAttribute oAt in schema.Types) {
                    if (GetType(oAt.Name) != null)
                        throw new Exceptions.DuplicatedTypeError(oAt.Name);
                    if (Types == null)
                        Types = new List<ObjectAttribute>();
                    Types.Add(oAt);
                }
            }

            // Object types
            foreach (ObjectType ot in schema.ObjectTypes) {
                ObjectType tOt = GetObjectType(ot.Name);
                if (tOt == null) {
                    if (ot.Extension)
                        throw new Exceptions.UnknownObjectType(ot.Name);
                    ObjectTypes.Insert(idx++, ot);
                } else if (tOt.Extension) {
                    if (ot.SystemObject)
                        throw new Exceptions.CannotExtendSystemObjectError(ot.Name);
                    ot.Merge(tOt);
                    ObjectTypes.Remove(tOt);
                    ObjectTypes.Insert(idx++, ot);
                } else
                    throw new Exceptions.DuplicatedObjectTypeError(ot.Name);
            }
            return true;
        } // Merge

        private void UpdateDictionaries()
        {
            m_ObjectTypesDict = new Dictionary<string, ObjectType>();
            foreach (ObjectType ot in ObjectTypes) {
                m_ObjectTypesDict[ot.Name.ToLower()] = ot;
            }
        } // UpdateDictionaries

        /// <summary>
        /// Load a schema from file
        /// </summary>
        /// <param name="fileName">The schema file name</param>
        /// <returns>The loaded schema</returns>
        public static Schema Load(string fileName)
        {
            return Load(fileName, false);
        } // Load

        private static Schema Load(string fileName, bool isInclude)
        {
            string fPath = Path.GetFullPath(fileName);
            string folder = Path.GetDirectoryName(fPath);

            Schema res = null;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read)) {
                res = Utility.Serialization.Deserialize<Schema>(fs);
                if (res != null) {
                    FileInfo fi = new FileInfo(fileName);
                    res.Files = new List<SchemaFile>() { new SchemaFile() { FileName = fi.Name, Content = Utility.Serialization.Serialize(res) } };
                }
            }

            // Includes
            if (res != null && res.Includes != null) {
                List<Schema> includes = new List<Schema>();
                foreach (SchemaIncludes inc in res.Includes) {
                    string fullName = Path.Combine(folder, inc.Path);
                    Schema include = Schema.Load(fullName, true);
                    if (include != null) {
                        FileInfo fi = new FileInfo(fullName);
                        includes.Add(include);
                        res.Files.Add(new SchemaFile() { FileName = fi.Name, Content = Utility.Serialization.Serialize(include) });
                    }
                }

                foreach (Schema include in includes) {
                    res.Merge(include);
                }
            }

            // Include system.xml
            if (!isInclude) {
                Schema systemSchema = null;
                using (Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("CsAlCore.Common.Resources.system.xml")) {
                    systemSchema = Utility.Serialization.Deserialize<Schema>(stream);
                    if (systemSchema != null) {
                        res.Merge(systemSchema);
                        res.Files.Add(new SchemaFile() { FileName = "system.xml", Content = Utility.Serialization.Serialize(systemSchema) });
                        res.SystemSchemaRevision = systemSchema.Revision;
                    }
                }
            }

            // Check flags attributes and set values
            foreach (ObjectType ot in res.ObjectTypes) {
                if (ot.Extension)
                    throw new Exceptions.UnknownObjectType(ot.Name);

                foreach (ObjectAttribute at in ot.Attributes) {
                    at.ObjectTypeName = ot.Name;
                    if (at.Type == ObjectAttribute.Types.Flags) {
                        if (at.Values != null) {
                            long flagValue = 1;
                            foreach (ObjectAttribute.EnumValue fv in at.Values) {
                                if (fv.Value != 0)
                                    throw new Exceptions.CannotSetFlagsValueInSchemaError(at.Name);
                                fv.Value = flagValue;
                                flagValue *= 2;
                            }
                        }
                    }
                }
            }

            if (!isInclude) {
                // Build dictionaries:
                res.UpdateDictionaries();

                // Complete inheriting objects:
                foreach (ObjectType ot in res.ObjectTypes) {
                    ObjectType iOt = ot;
                    while (iOt != null) {
                        if (!string.IsNullOrEmpty(iOt.Inherits)) {
                            string inheritedType = iOt.Inherits;
                            iOt = res.GetObjectType(inheritedType);
                            if (iOt == null)
                                throw new Exceptions.UnknownObjectType(inheritedType);
                            ot.Merge(iOt);
                        } else
                            iOt = null;
                    }
                }

                res.Files.Reverse();

                // Put child object AFTER its parent (else the reference key creation would fail)
                for (int i = 0; i < res.ObjectTypes.Count; i++) {
                    ObjectType ot = res.ObjectTypes[i];
                    if (ot.Attributes != null) {
                        foreach (ObjectAttribute at in ot.Attributes) {
                            if (at.Type == ObjectAttribute.Types.MultiReference && at.ChildrenReference) {
                                ObjectType rOt = res.GetObjectType(at.RefObjectType);
                                res.ObjectTypes.Remove(rOt);
                                res.ObjectTypes.Insert(res.ObjectTypes.IndexOf(ot) + 1, rOt);
                            }
                        }
                    }
                }

                // Remove includes:
                res.Includes.Clear();
                res.CompleteAttributeTypes();
                res.Validate();
            }

            return res;
        } // Load
    }
}
