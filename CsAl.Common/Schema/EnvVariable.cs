﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// Env variable
    /// </summary>
    public class EnvVariable
    {
        static Dictionary<Names, DataTypes> m_DataTypes = new Dictionary<Names, DataTypes>()
        {
            { Names.BinaryDataRoot, DataTypes.String },
            { Names.BinaryDataMaxFiles, DataTypes.Int }
        };

        /// <summary>
        /// List of env variable names
        /// </summary>
        public enum Names
        {
            /// <summary>
            /// The root path where binary data are saved on filestystem
            /// </summary>
            BinaryDataRoot,
            /// <summary>
            /// The maximum number of files to keep in a single folder
            /// </summary>
            BinaryDataMaxFiles,
        } // EnvVariables

        public enum DataTypes
        {
            String,
            Boolean,
            Int,
            Real
        }

        /// <summary>
        /// Create a new <see cref="EnvVariable"/>
        /// </summary>
        /// <param name="name">The variable <see cref="Names"/></param>
        /// <param name="value">The value</param>
        public EnvVariable(Names name, object value)
        {
            Name = name;
            DataType = m_DataTypes[Name];
            SetValue(value);
        }

        /// <summary>
        /// Name
        /// </summary>
        public Names Name { get; private set; }

        /// <summary>
        /// Data type
        /// </summary>
        public DataTypes DataType { get; private set; }

        /// <summary>
        /// The creation date time
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// The last modified date time
        /// </summary>
        public DateTime? Modified { get; set; }

        /// <summary>
        /// The value expressed as a string
        /// </summary>
        public string StringValue { get; private set; }

        private void SetValue(object value)
        {
            switch (DataType) {
                case DataTypes.Boolean:
                    if (value is bool) {
                        StringValue = ((bool)value) ? "1" : "0";
                        return;
                    } else if (value is string) {
                        StringValue = (string)value == "1" ? "1" : "0";
                    }
                    return;
                case DataTypes.Int:
                    if (value is int) {
                        StringValue = ((int)value).ToString(CultureInfo.InvariantCulture);
                        return;
                    } else if (value is long) {
                        StringValue = ((long)value).ToString(CultureInfo.InvariantCulture);
                        return;
                    } else if (value is string) {
                        long longValue = 0;
                        if (long.TryParse(value as string, NumberStyles.Integer, CultureInfo.InvariantCulture, out longValue)) {
                            StringValue = longValue.ToString(CultureInfo.InvariantCulture);
                            return;
                        }
                    }
                    break;
                case DataTypes.Real:
                    if (value is float) {
                        StringValue = ((float)value).ToString(CultureInfo.InvariantCulture);
                        return;
                    } else if (value is decimal) {
                        StringValue = ((decimal)value).ToString(CultureInfo.InvariantCulture);
                        return;
                    } else if (value is double) {
                        StringValue = ((double)value).ToString(CultureInfo.InvariantCulture);
                        return;
                    } else if (value is string) {
                        double dValue = 0;
                        if (double.TryParse(value as string, NumberStyles.Integer, CultureInfo.InvariantCulture, out dValue)) {
                            StringValue = dValue.ToString(CultureInfo.InvariantCulture);
                            return;
                        }
                    }
                    break;
                case DataTypes.String:
                    if (value is string) {
                        StringValue = value as string;
                        return;
                    }
                    return;
            }
            throw new ArgumentException($"Invalid data type for { Name }, expecting { DataType }");
        } // SetValue

        /// <summary>
        /// Get the value of the correct data type
        /// </summary>
        /// <returns></returns>
        public T GetValue<T>()
        {
            switch (DataType) {
                case DataTypes.Boolean:
                    return (T)(object)(StringValue == "1");
                case DataTypes.Int:
                    long intValue = 0;
                    if (long.TryParse(StringValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out intValue))
                        return (T)(object)intValue;
                    break;
                case DataTypes.Real:
                    double realValue = 0;
                    if (double.TryParse(StringValue, NumberStyles.Float, CultureInfo.InvariantCulture, out realValue))
                        return (T)(object)realValue;
                    break;
                case DataTypes.String:
                    return (T)(object)StringValue;
            }
            return default(T);
        } // GetValue
    }
}
