﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// Defines an index
    /// </summary>
    [XmlRoot("Index")]
    public class ObjectIndex
    {
        /// <summary>
        /// Attribute used for indexes
        /// </summary>
        public class IndexAttribute
        {
            /// <summary>
            /// The attribute name
            /// </summary>
            [XmlAttribute]
            public string Name { get; set; }

            /// <summary>
            /// Get or set if the index on the attribute should be descending
            /// </summary>
            [XmlAttribute]
            public bool IsDescending { get; set; }
        } // IndexAttribute

        /// <summary>
        /// The index name
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        
        /// <summary>
        /// Get or set if this index is unique
        /// </summary>
        [XmlAttribute]
        public bool Unique { get; set; }

        /// <summary>
        /// A list of <see cref="IndexAttribute"/> composing the index
        /// </summary>
        [XmlArrayItem("Attribute")]
        public List<IndexAttribute> Attributes { get; set; }

        /// <summary>
        /// Get the index name to use in the database
        /// </summary>
        /// <param name="objectType">The <see cref="ObjectType"/></param>
        /// <returns>The index name</returns>
        public string GetDbName(ObjectType objectType)
        {
            string md5 = Utility.String.GetMD5Hash($"{objectType.Name}_{Name}");
            return $"idx_{ md5 }";
        } // GetDbName
        
        /// <summary>
        /// Compare this <see cref="ObjectIndex"/> with the given one
        /// </summary>
        /// <param name="idx">The other index</param>
        /// <returns>True if the indexes are equal</returns>
        public bool IsEqual(ObjectIndex idx)
        {
            bool different = false;
            if (Unique != idx.Unique || Attributes.Count != idx.Attributes.Count) {
                different = true;
            } else {
                for (int i = 0; i < idx.Attributes.Count; i++) {
                    if (Attributes[i].Name != idx.Attributes[i].Name ||
                        Attributes[i].IsDescending != idx.Attributes[i].IsDescending) {
                        different = true;
                        break;
                    }
                }
            }
            return !different;
        } // IsEqual
    }
}
