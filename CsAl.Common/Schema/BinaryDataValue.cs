﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// Describes a binary data attribute value.
    /// </summary>
    public class BinaryDataValue
    {
        /// <summary>
        /// The binary data Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The file name
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// The mime type
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// The data size
        /// </summary>
        public long Size { get; set; }

        /// <summary>
        /// The stream containing the binary data
        /// </summary>
        public Stream Stream { get; set; }

        /// <summary>
        /// Get if the <see cref="BinaryDataValue"/> has data
        /// </summary>
        public bool HasData
        {
            get { return Stream != null && Stream.CanRead; }
        }
    } // BinaryDataValue
}
