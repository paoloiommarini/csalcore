﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// Defines an object type
    /// </summary>
    public class ObjectType
    {
        List<ObjectAttribute> m_Attributes = null;
        List<string> m_AttrNames = null;
        List<ObjectAttribute> m_RefAttributes = null;
        List<ObjectAttribute> m_MultiRefAttributes = null;
        List<ObjectAttribute> m_ChildrenRefAttributes = null;
        ObjectAttribute m_IdentityAttr = null;
        ObjectAttribute m_CreatedAttr = null;
        ObjectAttribute m_LastModifiedAttr = null;

        Dictionary<string, ObjectAttribute> m_AttributesDict = null;

        /// <summary>
        /// Create a new <see cref="ObjectType"/>
        /// </summary>
        public ObjectType()
        {

        }

        /// <summary>
        /// The object type name
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Th eobject type description
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// Define if this is an abstract object type
        /// </summary>
        [XmlAttribute]
        public bool Abstract { get; set; }

        /// <summary>
        /// Get if the object type is child of another object type (see <see cref="ParentObjectType"/>)
        /// </summary>
        [XmlIgnore]
        public bool IsChild
        {
            get {
                return !string.IsNullOrEmpty(ParentObjectType);
            }
        }

        /// <summary>
        /// The parent object type name
        /// </summary>
        [XmlIgnore]
        public string ParentObjectType { get; set; }

        /// <summary>
        /// DEfines if this in a system object type
        /// </summary>
        [XmlAttribute]
        public bool SystemObject { get; set; }

        /// <summary>
        /// The base object type name
        /// </summary>
        [XmlAttribute]
        public string Inherits { get; set; }

        /// <summary>
        /// Defines if this object type extends an existing one
        /// </summary>
        [XmlAttribute]
        public bool Extension { get; set; }

        /// <summary>
        /// The object type attributes
        /// </summary>
        [XmlArrayItem("Attribute")]
        public List<ObjectAttribute> Attributes
        {
            get { return m_Attributes; }
            set {
                m_Attributes = value;
                UpdateDictionaries();
            }
        }

        /// <summary>
        /// A list of all the object's attribe names
        /// </summary>
        [XmlIgnore]
        public List<string> AttributeNames
        {
            get {
                if (m_AttrNames != null)
                    return new List<string>(m_AttrNames);
                return null;
            }
        }

        /// <summary>
        /// The XSLT used to validate the object
        /// </summary>
        [XmlElement]
        public string ValidationXslt { get; set; }

        /// <summary>
        /// The compiled XSLT used to validate the object
        /// </summary>
        [XmlIgnore]
        public XslCompiledTransform ValidationXsltCompiled { get; set; }

        /// <summary>
        /// List of indexes 
        /// </summary>
        [XmlArrayItem("Index")]
        public List<ObjectIndex> Indexes { get; set; }

        /// <summary>
        /// Get the database table name
        /// </summary>
        [XmlIgnore]
        public string TableName
        {
            get {
                if (SystemObject)
                    return string.Format("{0}{1}", Schema.SystemTablePrefix, Name.ToLower());
                return string.Format("{0}{1}", Schema.ObjectTablePrefix, Name.ToLower());
            }
        }

        #region public operations
        /// <summary>
        /// Get the identity attribute
        /// </summary>
        /// <param name="includeInherited">If false only a non inherited attribute is returned</param>
        /// <returns>An identity <see cref="ObjectAttribute"/> or null</returns>
        public ObjectAttribute GetIdentity(bool includeInherited)
        {
            if (includeInherited && m_IdentityAttr != null)
                return m_IdentityAttr;

            foreach (ObjectAttribute at in Attributes) {
                if ((includeInherited || !at.IsInherited) && at.Type == ObjectAttribute.Types.Identity) {
                    if (includeInherited)
                        m_IdentityAttr = at;
                    return at;
                }
            }
            return null;
        } // GetIdentity

        /// <summary>
        /// Get the created date attribute
        /// </summary>
        /// <returns></returns>
        public ObjectAttribute GetCreatedDate()
        {
            return m_CreatedAttr;
        } // GetLastModifiedDate


        /// <summary>
        /// Get the last modified date attribute
        /// </summary>
        /// <returns></returns>
        public ObjectAttribute GetLastModifiedDate()
        {
            return m_LastModifiedAttr;
        } // GetLastModifiedDate

        /// <summary>
        /// Get a list of all <see cref="AttributeTypes.ReferenceAttrType"/> attributes
        /// </summary>
        /// <returns>A list of <see cref="ObjectAttribute"/></returns>
        public List<ObjectAttribute> GetReferences()
        {
            if (m_RefAttributes != null)
                return new List<ObjectAttribute>(m_RefAttributes);
            return null;
        } // GetReferences

        /// <summary>
        /// Get the parent object attribute
        /// </summary>
        /// <returns>A <see cref="ObjectAttribute"/> or null</returns>
        public ObjectAttribute GetParentObjectAttribute()
        {
            return GetAttribute(Schema.ParentObjElementName);
        } // GetParentObjectAttribute

        /// <summary>
        /// Get the list of multi reference attributes
        /// </summary>
        /// <returns>A list of <see cref="ObjectAttribute"/> or null</returns>
        public List<ObjectAttribute> GetMultiReferences()
        {
            if (m_MultiRefAttributes != null)
                return new List<ObjectAttribute>(m_MultiRefAttributes);
            return null;
        } // GetMultiReferences

        /// <summary>
        /// Get the list of children reference attributes
        /// </summary>
        /// <returns>A list of <see cref="ObjectAttribute"/> or null</returns>
        public List<ObjectAttribute> GetChildrenReferences()
        {
            if (m_ChildrenRefAttributes != null)
                return new List<ObjectAttribute>(m_ChildrenRefAttributes);
            return null;
        } // GetChildrenReferences

        /// <summary>
        /// Get the children reference attribute to the given object type
        /// </summary>
        /// <param name="objectTypeName">The children object type name</param>
        /// <returns>A <see cref="ObjectAttribute"/> or null</returns>
        public ObjectAttribute GetChildrenReference(string objectTypeName)
        {
            foreach (ObjectAttribute at in GetChildrenReferences()) {
                if (string.Compare(at.RefObjectType, objectTypeName, StringComparison.OrdinalIgnoreCase) == 0)
                    return at;
            }
            return null;
        } // GetChildrenReferences

        /// <summary>
        /// Get an attribute
        /// </summary>
        /// <param name="name">The attribute name</param>
        /// <returns>A <see cref="ObjectAttribute"/> or null</returns>
        public ObjectAttribute GetAttribute(string name)
        {
            UpdateDictionaries();
            ObjectAttribute res = null;
            m_AttributesDict.TryGetValue(name.ToLower(), out res);
            return res;
        } // GetAttribute

        /// <summary>
        /// Get a <see cref="ObjectIndex"/>
        /// </summary>
        /// <param name="name">The index name</param>
        /// <returns>A <see cref="ObjectIndex"/> or null</returns>
        public ObjectIndex GetIndex(string name)
        {
            if (Indexes != null) {
                foreach (ObjectIndex idx in Indexes) {
                    if (string.Compare(idx.Name, name, StringComparison.OrdinalIgnoreCase) == 0)
                        return idx;
                }
            }
            return null;
        }

        /// <summary>
        /// Merge an object type
        /// </summary>
        /// <param name="objectType">The object type to merge</param>
        /// <returns>True on success</returns>
        public bool Merge(ObjectType objectType)
        {
            int idx = 0;
            foreach (ObjectAttribute at in objectType.Attributes) {
                if (!at.IsInherited) {
                    ObjectAttribute tAt = GetAttribute(at.Name);
                    if (tAt == null) {
                        ObjectAttribute nAt = at.Copy();
                        if (objectType.Name != Name)
                            nAt.InheritedFrom = objectType.Name;
                        Attributes.Insert(idx++, nAt);
                    } else if (!tAt.IsInherited) {
                        throw new Exceptions.DuplicatedAttributeError(at.Name);
                    }
                }
            }
            UpdateDictionaries();
            return true;
        } // Merge


        /// <summary>
        /// Get the string representation of the <see cref="ObjectType"/>
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return string.Format("{0}", Name);
        }
        #endregion

        /// <summary>
        /// Build dictionaries and caches for special attribute types
        /// </summary>
        private void UpdateDictionaries()
        {
            if (m_AttributesDict == null || m_AttributesDict.Count != m_Attributes.Count) {
                m_IdentityAttr = null;
                m_CreatedAttr = null;
                m_LastModifiedAttr = null;
                m_AttrNames = new List<string>();
                m_AttributesDict = new Dictionary<string, ObjectAttribute>();
                m_RefAttributes = new List<ObjectAttribute>();
                m_MultiRefAttributes = new List<ObjectAttribute>();
                m_ChildrenRefAttributes = new List<ObjectAttribute>();

                if (Attributes != null) {
                    foreach (ObjectAttribute att in Attributes) {
                        m_AttributesDict[att.Name.ToLower()] = att;

                        if (att.Type == ObjectAttribute.Types.CreatedDateTime)
                            m_CreatedAttr = att;

                        if (att.Type == ObjectAttribute.Types.LastModifiedDateTime)
                            m_LastModifiedAttr = att;

                        if (!att.SystemManaged || string.Compare(att.Name, Schema.ObjectVersionNumberAttributeName, StringComparison.OrdinalIgnoreCase) == 0)
                            m_AttrNames.Add(att.Name);

                        if (att.Type == ObjectAttribute.Types.Reference && !(att.AttributeType is AttributeTypes.ChildOfAttrType))
                            m_RefAttributes.Add(att);

                        if (att.Type == ObjectAttribute.Types.MultiReference && !att.ChildrenReference)
                            m_MultiRefAttributes.Add(att);

                        if (att.Type == ObjectAttribute.Types.MultiReference && att.ChildrenReference)
                            m_ChildrenRefAttributes.Add(att);
                    }
                }
            }
        } // UpdateDictionaries
    }
}
