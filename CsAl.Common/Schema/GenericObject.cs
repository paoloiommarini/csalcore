﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CsAlCore.Common.Schema
{
    /// <summary>
    /// A <see cref="GenericObject"/>'s attribute
    /// </summary>
    public class GenericAttribute
    {
        object m_Value = null;

        /// <summary>
        /// Create a new <see cref="GenericAttribute"/>
        /// </summary>
        public GenericAttribute()
        {
            Changed = false;
        }

        /// <summary>
        /// Create a new <see cref="ObjectAttribute"/>
        /// </summary>
        /// <param name="attribute">The <see cref="ObjectAttribute"/></param>
        /// <param name="value">The value</param>
        public GenericAttribute(ObjectAttribute attribute, object value)
        {
            Attribute = attribute;
            m_Value = value;
            Changed = true;
        }

        /// <summary>
        /// Create a new <see cref="ObjectAttribute"/>
        /// </summary>
        /// <param name="attribute">The <see cref="ObjectAttribute"/></param>
        /// <param name="value">The value</param>
        /// <param name="changed">The <see cref="Changed"/> value</param>
        public GenericAttribute(ObjectAttribute attribute, object value, bool changed)
        {
            Attribute = attribute;
            m_Value = value;
            Changed = changed;
        }

        /// <summary>
        /// The attribute name
        /// </summary>
        public string Name
        {
            get {
                if (Attribute != null)
                    return Attribute.Name;
                return string.Empty;
            }
        }

        /// <summary>
        /// The <see cref="ObjectAttribute"/>
        /// </summary>
        public ObjectAttribute Attribute { get; set; }

        /// <summary>
        /// The attribute value
        /// </summary>
        public object Value
        {
            get { return m_Value; }
            set {
                if (value != m_Value)
                    m_Value = value;
                Changed = true;
            }
        }

        /// <summary>
        /// The attribute's changed flag
        /// </summary>
        public bool Changed { get; private set; }

        /// <summary>
        /// Reset the <see cref="Changed"/> flag
        /// </summary>
        public void ResetChanged()
        {
            Changed = false;
        }

        public new string ToString()
        {
            if (Attribute == null)
                return string.Format("No Attribute: {0}", Value);
            return string.Format("{0}: {1} {2}", Attribute.Name, Value, Changed ? "CHANGED" : string.Empty).TrimEnd();
        }
    } // GenericAttribute

    /// <summary>
    /// An object with its <see cref="GenericAttribute"/>s
    /// </summary>
    public class GenericObject
    {
        Dictionary<string, GenericAttribute> m_AttributesDict = null;

        /// <summary>
        /// Create a new <see cref="GenericObject"/>
        /// </summary>
        public GenericObject()
        {
            Attributes = new List<GenericAttribute>();
        }

        /// <summary>
        /// Create a new <see cref="GenericObject"/> from an existing one
        /// </summary>
        /// <param name="obj">The source object</param>
        public GenericObject(GenericObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            ObjectType = obj.ObjectType;

            Attributes = new List<GenericAttribute>();
            if (obj.Attributes != null) {
                foreach (GenericAttribute att in obj.Attributes)
                    Attributes.Add(new GenericAttribute(att.Attribute, att.Value, att.Changed));
            }
            UpdateDictionaries();
        }

        /// <summary>
        /// The object id
        /// </summary>
        [XmlIgnore]
        public long Id
        {
            get {
                ObjectAttribute idAt = ObjectType.GetIdentity(true);
                if (idAt != null && GetAttribute(idAt.Name).Value != null) {
                    return (long)GetAttribute(idAt.Name).Value;
                }
                return 0;
            }
        }

        /// <summary>
        /// Get the object creation date time
        /// </summary>
        [XmlIgnore]
        public DateTime CreatedDateTime
        {
            get {
                ObjectAttribute cAt = ObjectType.GetCreatedDate();
                if (cAt != null)
                    return (DateTime)GetAttribute(cAt.Name).Value;
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Get the object last modified date time
        /// </summary>
        [XmlIgnore]
        public DateTime? LastModifiedDateTime
        {
            get {
                ObjectAttribute lmdAt = ObjectType.GetLastModifiedDate();
                if (lmdAt != null) 
                    return (DateTime?)GetAttribute(lmdAt.Name).Value;
                return null;
            }
        }

        /// <summary>
        /// Get the object version number
        /// </summary>
        [XmlIgnore]
        public long VersionNumber
        {
            get {
                ObjectAttribute vnAt = ObjectType.GetAttribute(Schema.ObjectVersionNumberAttributeName);
                if (vnAt != null && GetAttribute(vnAt.Name).Value != null)
                    return (long)GetAttribute(vnAt.Name).Value;
                return 0;
            }
        }

        /// <summary>
        /// The <see cref="ObjectType"/>
        /// </summary>
        [XmlIgnore]
        public ObjectType ObjectType { get; set; }

        /// <summary>
        /// The object type name
        /// </summary>
        [XmlIgnore]
        public string ObjectTypeName
        {
            get {
                if (ObjectType != null)
                    return ObjectType.Name;
                return string.Empty;
            }
        }

        private List<GenericAttribute> Attributes { get; set; }

        /// <summary>
        /// Get a <see cref="GenericAttribute"/>
        /// </summary>
        /// <param name="name">The attribute name</param>
        /// <returns>A <see cref="GenericAttribute"/> or null</returns>
        public GenericAttribute GetAttribute(string name)
        {
            if (m_AttributesDict != null) {
                GenericAttribute ga = null;
                if (m_AttributesDict.TryGetValue(name.ToLower(), out ga))
                    return ga;
            }
            return null;
        } // GetAttribute

        /// <summary>
        /// Get an attribute's value
        /// </summary>
        /// <param name="attributeName">The attribute name</param>
        /// <returns>The attribute value</returns>
        public object GetAttributeValue(string attributeName)
        {
            GenericAttribute gat = GetAttribute(attributeName);
            if (gat == null)
                throw new Exceptions.UnknownAttributeType(attributeName);
            return gat.Value;
        } // GetAttributeValue

        /// <summary>
        /// Set an attribute's value
        /// </summary>
        /// <param name="attributeName">The attribute name</param>
        /// <param name="value">The value</param>
        /// <returns>True on success</returns>
        public bool SetAttributeValue(string attributeName, object value)
        {
            GenericAttribute att = GetAttribute(attributeName);
            if (att != null) {
                if (!att.Attribute.AttributeType.ValidateValue(att.Name, value))
                    return false;
                att.Value = value;
                return true;
            } else {
                ObjectAttribute at = ObjectType.GetAttribute(attributeName);
                if (at != null) {
                    if (!at.AttributeType.ValidateValue(at.Name, value))
                        return false;
                    value = at.AttributeType.GetValue(value);
                    GenericAttribute ga = new GenericAttribute(at, value);
                    Attributes.Add(ga);
                    if (m_AttributesDict != null)
                        m_AttributesDict[ga.Name.ToLower()] = ga;
                    return true;
                } else {
                    throw new Exceptions.UnknownAttributeType(attributeName);
                }
            }
        } // SetAttributeValue

        /// <summary>
        /// Get the <see cref="GenericAttribute"/> of the given object type
        /// </summary>
        /// <param name="objectTypeName">The object type name</param>
        /// <returns>A list of <see cref="GenericAttribute"/></returns>
        public List<GenericAttribute> GetAttributes(string objectTypeName)
        {
            List<GenericAttribute> res = new List<GenericAttribute>();

            foreach (GenericAttribute at in Attributes) {
                if (!at.Attribute.IsInherited && string.Compare(ObjectType.Name, objectTypeName, StringComparison.OrdinalIgnoreCase) == 0)
                    res.Add(at);
                else if (string.Compare(at.Attribute.InheritedFrom, objectTypeName, StringComparison.OrdinalIgnoreCase) == 0)
                    res.Add(at);
            }
            return res;
        } // GetAttributes

        /// <summary>
        /// Reset the changed flag of all the attributes
        /// </summary>
        public void ResetChanged()
        {
            if (Attributes != null) {
                foreach (GenericAttribute at in Attributes)
                    at.ResetChanged();
            }
        } // ResetChanged

        #region XML serialization
        /// <summary>
        /// Serialize this object to a string
        /// </summary>
        /// <returns>The serialized object</returns>
        public string Serialize()
        {
            return Serialize(true);
        } // Serialize

        /// <summary>
        /// Serialize this object to a string
        /// </summary>
        /// <param name="writeStartDocument">If false the xml declaration is omitted</param>
        /// <returns>The serialized object</returns>
        public string Serialize(bool writeStartDocument)
        {
            using (MemoryStream output = new MemoryStream()) {
                XmlWriterSettings xSettings = new XmlWriterSettings()
                {
                    Encoding = Encoding.UTF8,
                    Indent = true,
                    NewLineOnAttributes = true,
                    OmitXmlDeclaration = !writeStartDocument
                };
                using (XmlWriter xw = XmlWriter.Create(output, xSettings)) {
                    Serialize(xw);
                }
                return Encoding.UTF8.GetString(output.ToArray());
            }
        } // Serialize

        /// <summary>
        /// Serialize this object to a the given <see cref="XmlWriter"/>
        /// </summary>
        /// <returns>True on success</returns>
        public bool Serialize(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement(ObjectTypeName);

            // Attributes
            foreach (GenericAttribute gAt in Attributes) {
                if (gAt.Value == null)
                    continue;

                if (string.Compare(gAt.Name, Schema.ObjectVersionNumberAttributeName, StringComparison.OrdinalIgnoreCase) == 0 || !gAt.Attribute.SystemManaged && gAt.Attribute.IsAttribute) {
                    xmlWriter.WriteStartAttribute(gAt.Name);
                    gAt.Attribute.Serialize(xmlWriter, gAt.Value);
                    xmlWriter.WriteEndAttribute();
                }
            }

            // Elements
            foreach (GenericAttribute gAt in Attributes) {
                if (gAt.Value == null)
                    continue;

                if (gAt.Attribute.IsElement) {
                    // Do not serialize objectType reference:
                    if (gAt.Attribute.Type == ObjectAttribute.Types.Reference) {
                        if (string.Compare(gAt.Attribute.RefObjectType, "objectType", StringComparison.OrdinalIgnoreCase) == 0)
                            continue;
                    }

                    xmlWriter.WriteStartElement(gAt.Name);
                    gAt.Attribute.Serialize(xmlWriter, gAt.Value);
                    xmlWriter.WriteEndElement();
                }
            }

            xmlWriter.WriteEndElement();
            return true;
        } // Serialize

        /// <summary>
        /// Deserialize a <see cref="GenericObject"/> from a <see cref="XmlReader"/>
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="xmlReader">The <see cref="XmlReader"/></param>
        /// <returns>A <see cref="GenericObject"/> or null</returns>
        public static GenericObject Deserialize(Schema schema, XmlReader xmlReader)
        {
            ObjectType ot = null;
            GenericObject res = null;

            xmlReader.MoveToContent();
            string objectTypeName = xmlReader.Name;
            ot = schema.GetObjectType(objectTypeName);
            if (ot == null)
                throw new Exceptions.UnknownObjectType(objectTypeName);

            res = Create(ot);
            int depth = xmlReader.Depth;
            // Attributes:
            if (xmlReader.HasAttributes) {
                while (xmlReader.MoveToNextAttribute()) {
                    string attName = xmlReader.Name;
                    ObjectAttribute at = ot.GetAttribute(attName);
                    if (at != null) {
                        res.SetAttributeValue(at.Name, at.Deserialize(schema, xmlReader));
                    }
                }
            }

            // Elements:
            while (xmlReader.Read()) {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Depth == depth + 1) {
                    string attName = xmlReader.Name;
                    ObjectAttribute at = ot.GetAttribute(attName);
                    if (at != null) {
                        res.SetAttributeValue(at.Name, at.Deserialize(schema, xmlReader));
                    }
                } else if (xmlReader.NodeType == XmlNodeType.EndElement) {
                    if (xmlReader.Name == objectTypeName)
                        break;
                }
            }

            res.ResetChanged();
            return res;
        } // Deserialize

        /// <summary>
        /// Deserialize a <see cref="GenericObject"/> from a string
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="xml">The xml string</param>
        /// <returns>A <see cref="GenericObject"/> or null</returns>
        public static GenericObject Deserialize(Schema schema, string xml)
        {
            XmlReaderSettings xSettings = new XmlReaderSettings();
            using (StringReader sr = new StringReader(xml)) {
                using (XmlReader xr = XmlReader.Create(sr, xSettings)) {
                    return Deserialize(schema, xr);
                }
            }
        } // Deserialize
        #endregion

        #region JSON serialization
        public string SerializeToJSON()
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb)) {
                using (JsonWriter jw = new JsonTextWriter(sw)) {
#if DEBUG
                    jw.Formatting = Newtonsoft.Json.Formatting.Indented;
#endif
                    Serialize(jw);
                }
                return sb.ToString();
            }
        } // SerializeToJSON

        /// <summary>
        /// Serialize this object to a the given <see cref="JsonWriter"/>
        /// </summary>
        /// <returns>True on success</returns>
        public bool Serialize(JsonWriter jsonWriter)
        {
            jsonWriter.WriteStartObject();
            // Object type:
            jsonWriter.WritePropertyName(Schema.JsonObjectTypeTokenName);
            jsonWriter.WriteValue(ObjectTypeName);

            foreach (GenericAttribute gAt in Attributes) {
                if (gAt.Value != null) {
                    jsonWriter.WritePropertyName(gAt.Name);
                    gAt.Attribute.Serialize(jsonWriter, gAt.Value);
                }
            }
            jsonWriter.WriteEndObject();
            return true;
        } // Serialize

        /// <summary>
        /// Deserialize a <see cref="GenericObject"/> from a <see cref="JsonReader"/>
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="jsonReader">The <see cref="JsonReader"/></param>
        /// <returns>A <see cref="GenericObject"/> or null</returns>
        public static GenericObject Deserialize(Schema schema, JsonReader jsonReader)
        {
            ObjectType ot = null;
            GenericObject res = null;

            jsonReader.Read();
            int depth = jsonReader.Depth;
            if (jsonReader.TokenType == JsonToken.StartObject) {
                jsonReader.Read();
                if (jsonReader.TokenType == JsonToken.PropertyName && (string)jsonReader.Value == Schema.JsonObjectTypeTokenName) {
                    string objectTypeName = jsonReader.ReadAsString();
                    ot = schema.GetObjectType(objectTypeName);
                    if (ot == null)
                        throw new Exceptions.UnknownObjectType(objectTypeName);

                    res = Create(ot);

                    // Read properties:
                    string property = string.Empty;
                    while (jsonReader.Read()) {
                        if (jsonReader.TokenType == JsonToken.EndObject && jsonReader.Depth == depth)
                            break;
                        if (jsonReader.TokenType == JsonToken.PropertyName) {
                            property = (string)jsonReader.Value;
                            ObjectAttribute at = ot.GetAttribute(property);
                            if (at != null) {
                                if (at.AttributeType.Type == ObjectAttribute.Types.Reference || at.AttributeType.Type == ObjectAttribute.Types.MultiReference)
                                    jsonReader.Read();
                                object value = at.Deserialize(schema, jsonReader);
                                res.SetAttributeValue(property, value);
                            } else {
                                throw new Common.Exceptions.UnknownAttributeType(property);
                            }
                        }
                    }

                    res.ResetChanged();
                }
            }
            return res;
        } // Deserialize

        /// <summary>
        /// Deserialize a <see cref="GenericObject"/> from a string
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="json">The JSON string</param>
        /// <returns>A <see cref="GenericObject"/> or null</returns>
        public static GenericObject DeserializeFromJSON(Schema schema, string json)
        {
            XmlReaderSettings xSettings = new XmlReaderSettings();
            using (StringReader sr = new StringReader(json)) {
                using (JsonTextReader jr = new JsonTextReader(sr)) {
                    return Deserialize(schema, jr);
                }
            }
        } // Deserialize
        #endregion

        /// <summary>
        /// Create a <see cref="GenericObject"/> for the given <see cref="ObjectType"/>
        /// </summary>
        /// <param name="schema">The <see cref="Schema"/></param>
        /// <param name="objectTypeName">The <see cref="ObjectType"/> name</param>
        /// <returns></returns>
        public static GenericObject Create(Schema schema, string objectTypeName)
        {
            ObjectType ot = schema.GetObjectType(objectTypeName);
            if (ot != null)
                return Create(ot);
            return null;
        } // Create

        /// <summary>
        /// Create a <see cref="GenericObject"/> for the given <see cref="ObjectType"/>
        /// </summary>
        /// <param name="objectType">The object type </param>
        /// <returns>A new <see cref="GenericObject"/></returns>
        public static GenericObject Create(ObjectType objectType)
        {
            GenericObject res = new GenericObject() { ObjectType = objectType };
            foreach (ObjectAttribute at in objectType.Attributes)
                res.Attributes.Add(new GenericAttribute(at, at.AttributeType.GetDefaultValue()));
            res.UpdateDictionaries();
            res.ResetChanged();
            return res;
        } // Create

        /// <summary>
        /// Create a <see cref="GenericObject"/> with the given name and attributes
        /// </summary>
        /// <param name="objectTypeName">The object type name</param>
        /// <param name="attributes">The attributes</param>
        /// <returns>A new <see cref="GenericObject"/></returns>
        public static GenericObject Create(string objectTypeName, List<ObjectAttribute> attributes)
        {
            GenericObject res = new GenericObject() { ObjectType = new ObjectType() { Name = objectTypeName } };
            foreach (ObjectAttribute at in attributes)
                res.Attributes.Add(new GenericAttribute(at, at.AttributeType.GetDefaultValue()));
            res.UpdateDictionaries();
            res.ResetChanged();
            return res;
        } // Create

        #region private operations
        private void UpdateDictionaries()
        {
            m_AttributesDict = new Dictionary<string, GenericAttribute>();
            if (Attributes != null) {
                foreach (GenericAttribute att in Attributes) {
                    m_AttributesDict[att.Name.ToLower()] = att;
                }
            }
        } // UpdateDictionaries
        #endregion
    }
}
