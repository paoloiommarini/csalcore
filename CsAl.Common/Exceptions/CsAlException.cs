﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsAlCore.Common.Exceptions
{
    /// <summary>
    /// Defines a CsAl exception
    /// </summary>
    public class CsAlException : Exception
    {
        /// <summary>
        /// Error codes
        /// </summary>
        public enum ErrorCodes
        {
            /// <summary>
            /// No error
            /// </summary>
            None = 0,

            /* Schema exceptions */
            UnknownObjectType = 1,
            UnknownAttributeType = 2,
            WrongSchemaName = 3,
            WrongSchemaRevision = 4,
            DuplicatedAttribute = 5,
            MissingSchemaName = 6,
            InvalidSchemaRevision = 7,
            InvalidObjectOrAttributeName = 8,
            InvalidAtributeType = 9,
            MissingFlagsEnumValues = 10,
            DuplicatedFlagsEnumValues = 11,
            InvalidFlagsValue = 12,
            CannotSetFlagsValueInSchema = 13,
            DuplicatedObjectType = 14,
            EmptyIndex = 15,            
            NonSystemObjectMustHaveIdentity = 16,
            InvalidAttributePath = 17,
            InvalidAttributeRangeValue = 18,
            InvalidAttributeDefaultValue = 19,
            CannotExtendSystemObject = 20,
            DuplicatedType = 21,
            InvalidFlagsValueName = 22,
            InvalidTypeName = 23,
            InvalidSubAttributes = 24,
            InvalidUnrefAttribute = 25,
            InvalidExtendAttribute = 25,
            NoSchemaFound = 26,
            CannotModifyAttributeType = 27,
            CannotChangeReferencedObjectType = 28,
            AttributeCannotHaveDefaultValue = 29,
            InvalidXslt = 30,
            ReferenceMustInheritFromObject = 31,
            DuplicatedIndexName = 32,

            /* Query exceptions */
            UnknownQueryParameter = 1000,
            QuerySyntaxError = 1001,
            QueryDataTypesMismatch = 1002,
            CannotQuerySystemObjects = 1003,
            InvalidEnumValue = 1004,
            ExistsConditionOnNonObject = 1005,            
            UnnamedParameter = 1006,
            DuplicatedParameterName = 1007,
            InvalidDateTimeValue = 1008,
            InvalidTimeSpanValue = 1009,
            ExpectingAttributeInPath = 1010,
            ExpectingNumericValue = 1011,
            InvalidDateValue = 1012,
            InvalidLimitValue = 1013,
            InvalidOffsetValue = 1014,
            OffsetLimitWithoutOrderBy = 1015,
            CountOnNonObject = 1016,
            WrongArgumentDataType = 1017,
            CannotQueryObjectWithoutIdentity = 1018,

            /* Object exception */
            CannotCreateAbstractObject = 2000,
            UpdateObjectWithoutId = 2001,
            ObjectTypeMismatch = 2002,
            CreateObjectWithId = 2003,
            ObjectDoesNotExists = 2004,
            InvalidAttributeValue = 2005,
            MissingRequiredAttribute = 2006,
            InvalidReference = 2007,
            CannotDeleteReferencedObject = 2008,
            ObjectIsSoftDeleted = 2009,
            ObjectIsNotSoftDeleted = 2010,
            CannotRestoreObjectReferencedBydeletedObject = 2011,
            WaitOnLockFileTimeout = 2012,
            BinaryDataDoesNotExists = 2013,
            ObjectValidationFailed = 2014,
            CannotSoftDeleteObjectNonInheritingObject = 2015,
            UniqueIndexDuplicateValue = 2016,
            InvalidMimeType = 2017,
            CannotUpdateSystemManagedAttributes = 2018,

            /* Database errors */
            MinimumDbVersionNotSatisfied = 3000,

            /* Configuration errors */
            MissingBinarySavePath = 4000,
        } // ErrorCodes

        public CsAlException(ErrorCodes errorCode, string message) :
          base(message)
        {
            ErrorCode = errorCode;
        }

        public ErrorCodes ErrorCode { get; set; }
    } // CsAlException

    #region Schema exceptions
    /// <summary>
    /// Unknown object type
    /// </summary>
    public class UnknownObjectType : CsAlException
    {
        public UnknownObjectType(string objectTypeName) :
          base(ErrorCodes.UnknownObjectType, string.Format("Unknown object type: {0}", objectTypeName))
        {

        }
    } // UnknownObjectType

    /// <summary>
    /// Unknown attribute type
    /// </summary>
    public class UnknownAttributeType : CsAlException
    {
        public UnknownAttributeType(string attributeTypeName) :
          base(ErrorCodes.UnknownAttributeType, string.Format("Unknown attribute type: {0}", attributeTypeName))
        {

        }
    } // UnknownAttributeType

    /// <summary>
    /// Invalid schema name
    /// </summary>
    public class WrongSchemaName : CsAlException
    {
        public WrongSchemaName(string schemaName) :
          base(ErrorCodes.WrongSchemaName, string.Format("Wrong schema name: {0}", schemaName))
        {

        }
    } // WrongSchemaName

    /// <summary>
    /// Invalid schema revision
    /// </summary>
    public class WrongSchemaRevision : CsAlException
    {
        public WrongSchemaRevision(int revision) :
          base(ErrorCodes.WrongSchemaName, string.Format("Wrong schema revision: {0}", revision))
        {

        }
    } // WrongSchemaRevision

    /// <summary>
    /// Duplicated type
    /// </summary>
    public class DuplicatedTypeError : CsAlException
    {
        public DuplicatedTypeError(string typeName) :
          base(ErrorCodes.DuplicatedType, string.Format("Duplicated type: {0}", typeName))
        {

        }
    } // DuplicatedTypeError

    /// <summary>
    /// Duplicated object type
    /// </summary>
    public class DuplicatedObjectTypeError : CsAlException
    {
        public DuplicatedObjectTypeError(string objectTypeName) :
          base(ErrorCodes.DuplicatedObjectType, string.Format("Duplicated object type: {0}", objectTypeName))
        {

        }
    } // DuplicatedObjectTypeError

    /// <summary>
    /// Duplicated attribute
    /// </summary>
    public class DuplicatedAttributeError : CsAlException
    {
        public DuplicatedAttributeError(string attributeName) :
          base(ErrorCodes.DuplicatedAttribute, string.Format("Duplicated attribute: {0}", attributeName))
        {

        }
    } // DuplicatedAttributeError

    /// <summary>
    /// Missing schema name
    /// </summary>
    public class MissingSchemaNameError : CsAlException
    {
        public MissingSchemaNameError() :
          base(ErrorCodes.MissingSchemaName, "Missing schema name")
        {

        }
    } // MissingSchemaNameError

    /// <summary>
    /// Invalid schema revision
    /// </summary>
    public class InvalidSchemaRevisionError : CsAlException
    {
        public InvalidSchemaRevisionError() :
          base(ErrorCodes.InvalidSchemaRevision, "Missing or invalid schema revision")
        {

        }
    } // InvalidSchemaRevisionError

    /// <summary>
    /// Invalid type name
    /// </summary>
    public class InvalidTypeNameError : CsAlException
    {
        public InvalidTypeNameError(string name) :
          base(ErrorCodes.InvalidTypeName, string.Format("Invalid type name: {0}", name))
        {

        }
    } // InvalidTypeNameError

    /// <summary>
    /// Invalid su abttributes
    /// </summary>
    public class InvalidSubAttributesError : CsAlException
    {
        public InvalidSubAttributesError(string attributeName) :
          base(ErrorCodes.InvalidSubAttributes, string.Format("Atribute {0} does not support sub attributes", attributeName))
        {

        }
    } // InvalidSubAttributesError

    public class InvalidObjectOrAttributeNameError : CsAlException
    {
        public InvalidObjectOrAttributeNameError(string name) :
          base(ErrorCodes.InvalidObjectOrAttributeName, string.Format("Invalid object/attribute name: {0}", name))
        {

        }
    } // InvalidObjectOrAttributeNameError

    public class InvalidAtributeTypeError : CsAlException
    {
        public InvalidAtributeTypeError(string attributeName, string typeName) :
          base(ErrorCodes.InvalidAtributeType, string.Format("Invalid attribute type for attribute {0}: {1}", attributeName, typeName))
        {

        }
    } // InvalidAtributeTypeError

    public class MissingFlagsEnumValuesError : CsAlException
    {
        public MissingFlagsEnumValuesError(string attributeName) :
          base(ErrorCodes.MissingFlagsEnumValues, string.Format("Missing flags/enum values for attribute {0}", attributeName))
        {

        }
    } // MissingFlagsEnumValuesError

    public class DuplicatedFlagsEnumValuesError : CsAlException
    {
        public DuplicatedFlagsEnumValuesError(string attributeName, string flagName) :
          base(ErrorCodes.DuplicatedFlagsEnumValues, string.Format("Duplicated flags/enum value for attribute {0}: '{1}'", attributeName, flagName))
        {

        }
    } // DuplicatedFlagsEnumValuesError

    public class InvalidFlagsNameError : CsAlException
    {
        public InvalidFlagsNameError(string attributeName, string flagName) :
          base(ErrorCodes.InvalidFlagsValueName, string.Format("Invalid flag value for attribute {0}: '{1}'", attributeName, flagName))
        {

        }
    } // InvalidFlagsNameError

    public class InvalidFlagsValueError : CsAlException
    {
        public InvalidFlagsValueError(string attributeName, string flagName) :
          base(ErrorCodes.InvalidFlagsValue, string.Format("Invalid flag value for attribute {0}: '{1}'", attributeName, flagName))
        {

        }
    } // InvalidFlagsValueError

    public class CannotSetFlagsValueInSchemaError : CsAlException
    {
        public CannotSetFlagsValueInSchemaError(string attributeName) :
          base(ErrorCodes.CannotSetFlagsValueInSchema, string.Format("Cannot set Flags value in schema for attribute {0}", attributeName))
        {

        }
    } // CannotSetFlagsValueInSchemaError

    public class EmptyIndexError : CsAlException
    {
        public EmptyIndexError(string indexName) :
          base(ErrorCodes.EmptyIndex, string.Format("Index without attributes {0}", indexName))
        {

        }
    } // EmptyIndexError

    public class DuplicatedIndexNameError : CsAlException
    {
        public DuplicatedIndexNameError(string indexName) :
            base(ErrorCodes.DuplicatedIndexName, string.Format("Duplicated index name {0}", indexName))
        {

        }
    } // DuplicatedIndexNameError
    
    public class NonSystemObjectMustHaveIdentityError : CsAlException
    {
        public NonSystemObjectMustHaveIdentityError(string objectTypeName) :
          base(ErrorCodes.NonSystemObjectMustHaveIdentity, $"Non system object {objectTypeName} must have an identity attribute")
        {

        }
    } // NonSystemObjectMustHaveIdentityError

    public class CannotQueryObjectWithoutIdentityError : CsAlException
    {
        public CannotQueryObjectWithoutIdentityError(string objectTypeName) :
          base(ErrorCodes.CannotQueryObjectWithoutIdentity, string.Format("Cannot query object type {0} without an identity attribute", objectTypeName))
        {

        }
    } // CannotQueryObjectWithoutIdentityError

    public class InvalidAttributePathError : CsAlException
    {
        public InvalidAttributePathError(string path, string message) :
          base(ErrorCodes.InvalidAttributePath, string.Format("Invalid attribute path '{0}' {1}", path, message))
        {

        }
    } // InvalidAttributePathError

    public class InvalidAttributeRangeValueError : CsAlException
    {
        public InvalidAttributeRangeValueError(string attributeName, object minValue, object maxValue) :
          base(ErrorCodes.InvalidAttributeRangeValue, string.Format("Invalid range for attribute {0} {1}/{2}", attributeName, minValue, maxValue))
        {

        }
    } // InvalidAttributeRangeValueError

    public class InvalidAttributeDefaultValueError : CsAlException
    {
        public InvalidAttributeDefaultValueError(string attributeName, object defValue) :
          base(ErrorCodes.InvalidAttributeDefaultValue, string.Format("Invalid default value for attribute {0} {1}", attributeName, defValue))
        {

        }
    } // InvalidAttributeDefaultValueError

    public class AttributeCannotHaveDefaultValueError : CsAlException
    {
        public AttributeCannotHaveDefaultValueError(string attributeName) :
          base(ErrorCodes.AttributeCannotHaveDefaultValue, string.Format("Attribute {0} cannot have a default value", attributeName))
        {

        }
    } // AttributeCannotHaveDefaultValueError

    public class InvalidXsltError : CsAlException
    {
        public InvalidXsltError(string objectTypeName, string error) :
          base(ErrorCodes.InvalidXslt, string.Format("The XSLT for object type {0} is invalid: {1}", objectTypeName, error))
        {

        }
    } // InvalidXsltError


    public class ReferenceMustInheritFromObjectError : CsAlException
    {
        public ReferenceMustInheritFromObjectError(string objectTypeName) :
          base(ErrorCodes.ReferenceMustInheritFromObject, string.Format("The referenced object type {0} must inherit from {1}", objectTypeName, Schema.Schema.ObjectObjectType))
        {

        }
    } // ReferenceMustInheritFromObjectError

    /// <summary>
    /// The <see cref="Common.Schema.ObjectAttribute"/> specified in an extend option is invalid because it is referencing a system object
    /// </summary>
    public class CannotExtendSystemObjectError : CsAlException
    {
        public CannotExtendSystemObjectError(string objectTypeName) :
          base(ErrorCodes.CannotExtendSystemObject, string.Format("Cannot extend system object type {0}", objectTypeName))
        {

        }
    } // CannotExtendSystemObjectError

    /// <summary>
    /// The <see cref="Common.Schema.ObjectAttribute"/> specified in an unref option is invalid
    /// </summary>
    public class InvalidUnrefAttributeError : CsAlException
    {
        public InvalidUnrefAttributeError(string attributePath) :
          base(ErrorCodes.InvalidUnrefAttribute, string.Format("Invalid unref attribute {0}", attributePath))
        {

        }
    } // InvalidUnrefAttributeError

    /// <summary>
    /// The <see cref="Common.Schema.ObjectAttribute"/> specified in an extend option is invalid
    /// </summary>
    public class InvalidExtendAttributeError : CsAlException
    {
        public InvalidExtendAttributeError(string attributePath) :
          base(ErrorCodes.InvalidExtendAttribute, string.Format("Invalid extend attribute {0}", attributePath))
        {

        }
    } // InvalidExtendAttributeError

    /// <summary>
    /// No schema has been found in the database
    /// </summary>
    public class NoSchemaFoundError : CsAlException
    {
        public NoSchemaFoundError() :
          base(ErrorCodes.NoSchemaFound, "No schema found")
        {

        }
    } // NoSchemaFoundError

    /// <summary>
    /// It is not possible to modify the attribute type
    /// </summary>
    public class CannotModifyAttributeTypeError : CsAlException
    {
        public CannotModifyAttributeTypeError(string attributeName) :
          base(ErrorCodes.CannotModifyAttributeType, $"Cannot modify attribute type of { attributeName }")
        {

        }
    } // CannotModifyAttributeTypeError

    /// <summary>
    /// It is not possible to modify the referenced object type
    /// </summary>
    public class CannotChangeReferencedObjectTypeError : CsAlException
    {
        public CannotChangeReferencedObjectTypeError(string attributeName) :
          base(ErrorCodes.CannotChangeReferencedObjectType, $"Cannot modify referenced object type of { attributeName }")
        {

        }
    } // CannotChangeReferencedObjectTypeError
    #endregion

    #region Query Exceptions
    public class CsAlQueryException : CsAlException
    {
        public CsAlQueryException(ErrorCodes errorCode, string message) :
          base(errorCode, message)
        {

        }
    } // CsAlQueryException

    public class UnknownQueryParameter : CsAlQueryException
    {
        public UnknownQueryParameter(string parameterName) :
          base(ErrorCodes.UnknownQueryParameter, string.Format("Unknown query parameter: {0}", parameterName))
        {

        }
    } // UnknownQueryParameter

    public class QuerySyntaxError : CsAlQueryException
    {
        public QuerySyntaxError(string msg) :
          base(ErrorCodes.QuerySyntaxError, string.Format("Query syntax error: {0}", msg))
        {

        }
    } // QuerySyntaxError

    public class QueryDataTypesMismatch : CsAlQueryException
    {
        public QueryDataTypesMismatch(string expr, string type1, string type2) :
          base(ErrorCodes.QueryDataTypesMismatch, string.Format("Cannot use {0} data type and {1} data type in expression '{2}'", type1, type2, expr))
        {

        }
    } // QueryDataTypesMismatch

    public class QueryOnSystemObjectError : CsAlQueryException
    {
        public QueryOnSystemObjectError(string objectTypeName) :
          base(ErrorCodes.CannotQuerySystemObjects, string.Format("Cannot query system object: {0}", objectTypeName))
        {

        }
    } // QueryOnSystemObjectError

    public class InvalidEnumValueError : CsAlQueryException
    {
        public InvalidEnumValueError(string value, string values) :
          base(ErrorCodes.InvalidEnumValue, string.Format("Invalid Enum value '{0}'. Possible values: {1}", value, values))
        {

        }
    } // InvalidEnumValueError

    public class ExistConditionOnNonObjectError : CsAlQueryException
    {
        public ExistConditionOnNonObjectError(string attributeName) :
          base(ErrorCodes.ExistsConditionOnNonObject, string.Format("Cannot evaluate exists condition on attribute {0}", attributeName))
        {

        }
    } // ExistConditionOnNonObjectError

    public class CountOnNonObjectError : CsAlQueryException
    {
        public CountOnNonObjectError(string attributeName) :
          base(ErrorCodes.CountOnNonObject, string.Format("Cannot evaluate count on attribute {0}", attributeName))
        {

        }
    } // CountOnNonObjectError

    public class WrongArgumentDataTypeError : CsAlQueryException
    {
        public WrongArgumentDataTypeError(string wrongDataType, string expectedDataType, string expr) :
          base(ErrorCodes.WrongArgumentDataType, string.Format("Wrong data type for argument. Expecting {0}, got {1} in expression {2}", wrongDataType, expectedDataType, expr))
        {

        }
    } // WrongArgumentDataTypeError

    public class UnnamedParameterError : CsAlQueryException
    {
        public UnnamedParameterError() :
          base(ErrorCodes.UnnamedParameter, "Unnamed parameter")
        {

        }
    } // UnnamedParameterError

    public class DuplicatedParameterNameError : CsAlQueryException
    {
        public DuplicatedParameterNameError(string parameterName) :
          base(ErrorCodes.DuplicatedParameterName, string.Format("Duplicated parameter name {0}", parameterName))
        {

        }
    } // DuplicatedParameterNameError

    public class InvalidDateTimeValueError : CsAlQueryException
    {
        public InvalidDateTimeValueError(string value) :
          base(ErrorCodes.InvalidDateTimeValue, string.Format("Invalid date time value '{0}'", value))
        {

        }
    } // InvalidDateTimeValueError

    public class InvalidDateValueError : CsAlQueryException
    {
        public InvalidDateValueError(string value) :
          base(ErrorCodes.InvalidDateValue, string.Format("Invalid date value '{0}'", value))
        {

        }
    } // InvalidDateValueError

    public class InvalidTimeSpanValueError : CsAlQueryException
    {
        public InvalidTimeSpanValueError(string value) :
          base(ErrorCodes.InvalidTimeSpanValue, string.Format("Invalid time span value '{0}'", value))
        {

        }
    } // InvalidTimeSpanValueError

    public class InvalidLimitValueError : CsAlQueryException
    {
        public InvalidLimitValueError(string value) :
          base(ErrorCodes.InvalidLimitValue, string.Format("Invalid limit value '{0}'", value))
        {

        }
    } // InvalidLimitValueError

    public class InvalidOffsetValueError : CsAlQueryException
    {
        public InvalidOffsetValueError(string value) :
          base(ErrorCodes.InvalidLimitValue, string.Format("Invalid offset value '{0}'", value))
        {

        }
    } // InvalidOffsetValueError

    public class OffsetLimitWithoutOrderByError : CsAlQueryException
    {
        public OffsetLimitWithoutOrderByError() :
          base(ErrorCodes.OffsetLimitWithoutOrderBy, string.Format("Cannot use offset or limit without order by"))
        {

        }
    } // OffsetLimitWithoutOrderByError

    public class ExpectingAttributeInPathError : CsAlQueryException
    {
        public ExpectingAttributeInPathError(string path) :
          base(ErrorCodes.ExpectingAttributeInPath, string.Format("Expecting attribute in path  '{0}'", path))
        {

        }
    } // ExpectingAttributeInPathError

    public class ExpectingNumericValueError : CsAlQueryException
    {
        public ExpectingNumericValueError(string path) :
          base(ErrorCodes.ExpectingNumericValue, string.Format("Expecting numeric value for expression '{0}'", path))
        {

        }
    } // ExpectingNumericValueError
    #endregion

    #region Object Exceptions
    public class CannotCreateAbstractObjectError : CsAlException
    {
        public CannotCreateAbstractObjectError(string objectTypeName) :
          base(ErrorCodes.CannotCreateAbstractObject, string.Format("Cannot create abstract object: {0}", objectTypeName))
        {

        }
    } // CannotCreateAbstractObjectError

    public class UpdateObjectWithoutIdError : CsAlException
    {
        public UpdateObjectWithoutIdError() :
          base(ErrorCodes.UpdateObjectWithoutId, "Cannot update an object without Id")
        {

        }
    } // UpdateObjectWithoutIdError

    public class CreateObjectWithIdError : CsAlException
    {
        public CreateObjectWithIdError() :
          base(ErrorCodes.CreateObjectWithId, "Cannot create new object with Id set")
        {

        }
    } // CreateObjectWithIdError

    public class ObjectTypeMismatchError : CsAlException
    {
        public ObjectTypeMismatchError(string expectedType, string receivedType) :
          base(ErrorCodes.ObjectTypeMismatch, string.Format("Object type expected {0} received {1}", expectedType, receivedType))
        {

        }
    } // ObjectTypeMismatchError

    public class ObjectDoesNotExistsError : CsAlException
    {
        public ObjectDoesNotExistsError(long id) :
          base(ErrorCodes.ObjectDoesNotExists, string.Format("The object with id {0} does not exists", id))
        {

        }
    } // ObjectDoesNotExistsError

    public class BinaryDataDoesNotExistsError : CsAlException
    {
        public BinaryDataDoesNotExistsError(long id) :
          base(ErrorCodes.BinaryDataDoesNotExists, string.Format("The binary data with id {0} does not exists", id))
        {

        }
    } // BinaryDataDoesNotExistsError

    public class InvalidAttributeValueError : CsAlException
    {
        public InvalidAttributeValueError(string attributeName, object value) :
          base(ErrorCodes.InvalidAttributeValue, string.Format("Invalid value for attribute {0}: {1}", attributeName, value))
        {

        }

        public InvalidAttributeValueError(string attributeName, object value, string message) :
          base(ErrorCodes.InvalidAttributeValue, string.Format("Invalid value for attribute {0}: {1} ({2})", attributeName, value, message))
        {

        }
    } // InvalidAttributeValueError

    public class MissingRequiredAttributeError : CsAlException
    {
        public MissingRequiredAttributeError(string attributeName) :
          base(ErrorCodes.MissingRequiredAttribute, string.Format("Required attribute {0} cannot be null", attributeName))
        {

        }
    } // MissingRequiredAttributeError

    public class InvalidReferenceError : CsAlException
    {
        public InvalidReferenceError(string attributeName, string referencedObjectType) :
          base(ErrorCodes.InvalidReference, string.Format("Invalid reference to object type {0} for attribute {1}", referencedObjectType, attributeName))
        {

        }
    } // InvalidReferenceError

    public class CannotDeleteReferencedObjectError : CsAlException
    {
        public CannotDeleteReferencedObjectError(long objId, long refObjId) :
          base(ErrorCodes.CannotDeleteReferencedObject, string.Format("Cannot delete object with id {0}, it is referenced by the object with id {1}", objId, refObjId))
        {

        }
    } // CannotDeleteReferencedObjectError

    public class ObjectIsSoftDeletedError : CsAlException
    {
        public ObjectIsSoftDeletedError(long objId) :
          base(ErrorCodes.ObjectIsSoftDeleted, string.Format("The object with id {0} is soft deleted", objId))
        {

        }
    } // ObjectIsSoftDeletedError

    public class ObjectIsNotSoftDeletedError : CsAlException
    {
        public ObjectIsNotSoftDeletedError(long objId) :
          base(ErrorCodes.ObjectIsNotSoftDeleted, string.Format("The object with id {0} is not soft deleted", objId))
        {

        }
    } // ObjectIsSoftDeletedError

    public class CannotRestoreObjectReferencedBydeletedObjectError : CsAlException
    {
        public CannotRestoreObjectReferencedBydeletedObjectError(long objId, long refObjId) :
          base(ErrorCodes.CannotRestoreObjectReferencedBydeletedObject, string.Format("The object with id {0} is referenced by deleted object with id {1}", objId, refObjId))
        {

        }
    } // CannotRestoreObjectReferencedBydeletedObjectError

    public class CannotSoftDeleteObjectNonInheritingObjectError : CsAlException
    {
        public CannotSoftDeleteObjectNonInheritingObjectError(string objectType) :
          base(ErrorCodes.CannotSoftDeleteObjectNonInheritingObject, string.Format("Cannot soft delete {0}. The object type does not inherit object.", objectType))
        {

        }
    } // CannotSoftDeleteObjectNonInheritingObjectError


    public class UniqueIndexDuplicateValueError : CsAlException
    {
        public UniqueIndexDuplicateValueError(string indexName) :
          base(ErrorCodes.UniqueIndexDuplicateValue, string.Format("Duplicate value for index {0}", indexName))
        {

        }
    } // UniqueIndexDuplicateValueError

    public class InvalidMimeTypeError : CsAlException
    {
        public InvalidMimeTypeError(string attributeName) :
            base(ErrorCodes.InvalidMimeType, string.Format("Invalid mime type for attribute {0}", attributeName))
        {

        }
    } // InvalidMimeTypeError
    
    /// <summary>
    /// Cannot update system managed attribute
    /// </summary>
    public class CannotUpdateSystemManagedAttributesError : CsAlException
    {
        public CannotUpdateSystemManagedAttributesError(string attributeName) :
            base(ErrorCodes.CannotUpdateSystemManagedAttributes, string.Format("Cannot update systema managed attribute {0}", attributeName))
        {

        }
    } // CannotUpdateSystemManagedAttributesError
    
    /// <summary>
    /// Wait on lock file timeout.
    /// </summary>
    public class WaitOnLockFileTimeoutError : CsAlException
    {
        public WaitOnLockFileTimeoutError(string lockFile) :
          base(ErrorCodes.WaitOnLockFileTimeout, $"Timeout waiting for lockfile { lockFile }")
        {

        }
    } // WaitOnLockFileTimeoutError

    /// <summary>
    /// Object validation error(s)
    /// </summary>
    public class ObjectValidationError : CsAlException
    {
        public List<Common.Schema.ValidationError> Errors
        {
            get;
            private set;
        }

        public ObjectValidationError(string objectTypeName, List<Common.Schema.ValidationError> errors) :
          base(ErrorCodes.ObjectValidationFailed, $"Validation failed for object of type { objectTypeName }")
        {
            Errors = errors;
        }
    } // WaitOnLockFileTimeoutError
    #endregion

    #region Database Exceptions
    public class MinimumDbVersionNotSatisfiedError : CsAlException
    {
        public MinimumDbVersionNotSatisfiedError(string version, string minimumVersion) :
          base(ErrorCodes.MinimumDbVersionNotSatisfied, string.Format("The database version {0} does not satisfy the minimum required {1}", version, minimumVersion))
        {

        }
    } // MinimumDbVersionNotSatisfiedError
    #endregion

    #region Configuration Exceptions
    public class MissingBinarySavePathError : CsAlException
    {
        public MissingBinarySavePathError() :
          base(ErrorCodes.MissingBinarySavePath, "Missing BinarySavePath")
        {

        }
    } // MissingBinarySavePathError
    #endregion
}
