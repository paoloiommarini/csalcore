﻿using CsAlCore.Common.Schema;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests
{
    [TestClass]
    public class DeleteAndRestoreTests : TestsBase
    {
        public DeleteAndRestoreTests() :
          base(CsAlCore.CsAlCore.DbType.SQLite, "unitTests.sqlite", true)
        {

        } // DeleteAndRestoreTests

        #region Tests
        [TestMethod]
        public void SoftDeleteObjs()
        {
            ObjectType objectType = null;
            List<CsAlCore.Common.IdName> objs = CsAlInstance.SearchObjs("select orderObj where name == 'Order number 1'", out objectType);
            List<long> ids = CsAlInstance.SoftDeleteObjects(new List<long>() { objs[0].Id }, new CsAlCore.CsAlCore.DeleteOptions(string.Empty, string.Empty));
            if (ids.Count != 2 || ids[0] != objs[0].Id) {
                throw new Exception(string.Format("SoftDelete ids error: {0}", string.Join(',', ids)));
            }
        } // SoftDeleteObjs

        [TestMethod]
        public void HardDeleteObjs()
        {
            ObjectType objectType = null;
            List<CsAlCore.Common.IdName> objs = CsAlInstance.SearchObjs("select orderObj where name == 'Order number 2'", out objectType);
            List<long> ids = CsAlInstance.DeleteObjects(new List<long>() { objs[0].Id }, new CsAlCore.CsAlCore.DeleteOptions(string.Empty, string.Empty));
            if (ids.Count != 2 || ids[0] != objs[0].Id) {
                throw new Exception(string.Format("HardDelete ids error: {0}", string.Join(',', ids)));
            }
        } // HardDeleteObjs

        [TestMethod]
        public void RestoreObjs()
        {
            ObjectType objectType = null;
            List<CsAlCore.Common.IdName> objs = CsAlInstance.SearchObjs("select orderObj where name == 'Order number 3'", out objectType);
            CsAlInstance.SoftDeleteObjects(new List<long>() { objs[0].Id }, new CsAlCore.CsAlCore.DeleteOptions(string.Empty, string.Empty));
            List<long> ids = CsAlInstance.RestoreObjects(new List<long>() { objs[0].Id }, new CsAlCore.CsAlCore.RestoreOptions(string.Empty));
            if (ids.Count != 2 || ids[0] != objs[0].Id) {
                throw new Exception(string.Format("Restore ids error: {0}", string.Join(',', ids)));
            }
        } // RestoreObjs
        #endregion
    }
}
