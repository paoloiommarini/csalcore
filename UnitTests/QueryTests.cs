﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using CsAlCore;
using CsAlCore.Common.Schema;
using CsAlCore.Query;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class QueryTests
    {
        #region classes
        public class Groups
        {
            [XmlElement("Group")]
            public List<Group> Group { get; set; }
        }

        public class Group
        {
            [XmlAttribute]
            public string Name { get; set; }
            
            [XmlElement("Test")]
            public List<Test> Test { get; set; }
        }
        
        public class Test
        {
            public class Par
            {
                public string Name { get; set; }
                public string Value { get; set; }
            }

            public class Result
            {
                [XmlAttribute]
                public CsAlCore.CsAlCore.DbType DbType { get; set; }
                public string SqlQuery { get; set; }                
            }
            
            public Test()
            {
                Deleted = CsAlCore.CsAlCore.DeletedObjectsStrategy.All;
            }
            
            public string Query { get; set; }
            public CsAlCore.CsAlCore.DeletedObjectsStrategy Deleted { get; set; }
            [XmlElement("Par")]
            public List<Par> Pars { get; set; } 
            public List<Result> Results { get; set; }
        }
        #endregion
        
        private Groups m_Groups = null;        
        Schema m_Schema = null;
        List<CsAlCore.Common.IdName> m_ObjectTypeIds = null;
        
        #region constructor

        public QueryTests()
        {
            // Load quesries:
            using (Stream sr = Assembly.GetExecutingAssembly()
                .GetManifestResourceStream("UnitTests.Data.Queries.xml")) {
                m_Groups = CsAlCore.Common.Utility.Serialization.Deserialize<Groups>(sr);
            }        
        }
        
        #endregion
        
        [TestMethod]
        public void Query()
        {
            // List of databases
            List<CsAlCore.CsAlCore.DbType> dbtypes = new List<CsAlCore.CsAlCore.DbType>()
            {
                CsAlCore.CsAlCore.DbType.MariaDB,
                CsAlCore.CsAlCore.DbType.MySQL,
                CsAlCore.CsAlCore.DbType.Oracle,
                CsAlCore.CsAlCore.DbType.PostgreSQL,
                CsAlCore.CsAlCore.DbType.SQLite,
                CsAlCore.CsAlCore.DbType.SQLServer,
            };
            
            // Load the schema
            m_Schema = Schema.Load($"..{ Path.DirectorySeparatorChar }..{ Path.DirectorySeparatorChar }..{ Path.DirectorySeparatorChar }Schema{ Path.DirectorySeparatorChar }test.xml");
            
            // Build object type ids:
            m_ObjectTypeIds = new List<CsAlCore.Common.IdName>();
            int i = 0;
            foreach (ObjectType ot in m_Schema.ObjectTypes) {
                m_ObjectTypeIds.Add(new CsAlCore.Common.IdName(++i, ot.Name));
            }
            
            // Execute tests
            foreach (Group g in m_Groups.Group) {
                foreach (Test t in g.Test) {
                    List<CsAlCore.Query.QueryParameter> pars = new List<QueryParameter>();
                    if (t.Pars != null) {
                        foreach (Test.Par p in t.Pars) {
                            pars.Add(new QueryParameter(p.Name, p.Value));
                        }
                    }

                    foreach (Test.Result res in t.Results) {
                        if (res.DbType != CsAlCore.CsAlCore.DbType.None) {
                            DoQueryTest(res.DbType, g.Name, t.Query, pars, t.Deleted, res.SqlQuery);
                        } else {
                            foreach (CsAlCore.CsAlCore.DbType dbType in dbtypes) {
                                if (t.Results.FirstOrDefault(r => r.DbType == dbType) == null)
                                    DoQueryTest(dbType, g.Name, t.Query, pars, t.Deleted, res.SqlQuery);
                            }
                        }
                    }
                }
            }
        }
        
        #region private operations
        private bool DoQueryTest(CsAlCore.CsAlCore.DbType dbType, string category, string query, List<CsAlCore.Query.QueryParameter> pars, CsAlCore.CsAlCore.DeletedObjectsStrategy deleted, string expectedResult)
        {
            expectedResult = NormalizeString(expectedResult);
            using (CsAlCore.DB.DbConnectionBase conn = CsAlCore.DB.DbConnectionFactory.Create(dbType, "fake connString")) {
                string res = NormalizeString(CsAlCore.CsAlCore.EvaluateQuery(m_Schema, m_ObjectTypeIds, conn, query, pars, deleted));
                if (res != expectedResult)
                    throw new Exception(string.Format("Category: {0}\r\nDbType: {1}\r\nQuery: {2}\r\nExpected:\r\n{3}\r\nResult:\r\n{4}", category, dbType, query, expectedResult, res));
                return true;
            }
        } // DoQueryTest        

        private string NormalizeString(string str)
        {
            StringBuilder sb = new StringBuilder();
            string[] resLines = str.Replace("\r", string.Empty).Split('\n');
            foreach (string line in resLines) {
                string trimmed = line.Trim();
                if (!string.IsNullOrEmpty(trimmed))
                    sb.AppendLine(trimmed);
            }

            return sb.ToString();
        }
        #endregion
    }
}