﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Xml;
using System.Reflection;

namespace UnitTests
{
    public abstract class TestsBase
    {
        static object m_Lock = new object();

        CsAlCore.CsAlCore.DbType m_DbType = CsAlCore.CsAlCore.DbType.None;
        string m_ConnString = string.Empty;
        CsAlCore.CsAlCore m_CsAl = null;
        Schema m_Schema = null;
        List<CsAlCore.Common.IdName> m_ObjectTypeIds = null;

        public TestsBase(CsAlCore.CsAlCore.DbType dbType, string connString, bool loadInitialData)
        {
            var appsettings = new ConfigurationBuilder()
                                  .SetBasePath(Directory.GetCurrentDirectory())
                                  .AddJsonFile("UnitTests.json")
                                  .Build();

            m_DbType = dbType;
            m_ConnString = connString;

            lock (m_Lock) {
                if (m_CsAl == null) {
                    string schemaFile = appsettings["SchemaFile"] as string;

                    if (loadInitialData && m_DbType == CsAlCore.CsAlCore.DbType.SQLite) {
                        if (File.Exists(m_ConnString))
                            File.Delete(m_ConnString);

                        m_ConnString = string.Format("Data Source={0};", m_ConnString);
                    }

                    CsAlCore.Configuration config = new CsAlCore.Configuration()
                    {
                        DbType = m_DbType,
                        DbConnectionString = m_ConnString
                    };
                    m_CsAl = new CsAlCore.CsAlCore(config);

                    Schema schema = Schema.Load($"..{ Path.DirectorySeparatorChar }..{ Path.DirectorySeparatorChar }..{ Path.DirectorySeparatorChar }Schema{ Path.DirectorySeparatorChar }test.xml");
                    if (loadInitialData) {
                        if (!m_CsAl.Open())
                            throw new Exception("Fail to open database");

                        if (schema != null) {
                            m_CsAl.UpgradeSchema(schema);
                            SeedInitialData();
                            m_Schema = m_CsAl.DbSchema;
                            m_ObjectTypeIds = m_CsAl.ObjectTypes;
                        }
                    } else {
                        m_Schema = schema;
                        // Build object type ids:
                        m_ObjectTypeIds = new List<CsAlCore.Common.IdName>();
                        int i = 0;
                        foreach (ObjectType ot in m_Schema.ObjectTypes) {
                            m_ObjectTypeIds.Add(new CsAlCore.Common.IdName(++i, ot.Name));
                        }
                    }
                }
            }
        }

        public CsAlCore.CsAlCore CsAlInstance
        {
            get { return m_CsAl; }
        }


        #region private operations
        private long SeedInitialData()
        {
            long count = 0;
            using (StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("UnitTests.Data.InitialData.xml"))) {
                using (XmlReader xr = XmlReader.Create(sr)) {
                    xr.MoveToContent();
                    if (xr.NodeType == XmlNodeType.Element && xr.Name == Schema.ObjectsListElementName) {
                        while (xr.Read()) {
                            if (xr.NodeType == XmlNodeType.Element) {
                                string nodeName = xr.Name;
                                GenericObject obj = GenericObject.Deserialize(m_CsAl.DbSchema, xr);
                                if (obj != null) {
                                    if (obj.Id != 0) {
                                        throw new Exception("Cannot update object in intial data");
                                    } else {
                                        if (m_CsAl.CreateObject(obj) != 0)
                                            count++;
                                    }
                                } else {
                                    return count;
                                }
                            }
                        }
                    } else {
                        return count;
                    }
                }
            }

            return count;
        } // SeedInitialData

        protected bool DoQueryTest(string query, List<CsAlCore.Query.QueryParameter> pars, string expectedResult)
        {
            return DoQueryTest(query, pars, CsAlCore.CsAlCore.DeletedObjectsStrategy.All, expectedResult);
        }

        protected bool DoQueryTest(string query, List<CsAlCore.Query.QueryParameter> pars, CsAlCore.CsAlCore.DeletedObjectsStrategy deleted, string expectedResult)
        {
            using (CsAlCore.DB.DbConnectionBase conn = CsAlCore.DB.DbConnectionFactory.Create(m_DbType, m_ConnString)) {
                string res = CsAlCore.CsAlCore.EvaluateQuery(m_Schema, m_ObjectTypeIds, conn, query, pars, deleted);
                if (res.Trim() != expectedResult.Trim())
                    throw new Exception(string.Format("Query: {0}\r\nExpected:\r\n{1}\r\nResult:\r\n{2}", query, expectedResult, res));
                return true;
            }
        } // DoQueryTest
        #endregion
    }
}
