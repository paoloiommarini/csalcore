﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    public class QueryErrorTests : TestsBase
    {
        public QueryErrorTests() :
          base(CsAlCore.CsAlCore.DbType.SQLite, "unitTests.sqlite", false)
        {

        } // QueryErrorTests

        private void DoErrorTest(string sql, List<CsAlCore.Query.QueryParameter> pars, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes expectedErrorCode)
        {
            try {
                DoQueryTest(sql, pars, string.Empty);
            } catch (CsAlCore.Common.Exceptions.CsAlException ex) {
                if (ex.ErrorCode != expectedErrorCode)
                    throw ex;
            }
        }

        [TestMethod]
        public void QueryErrors()
        {
            DoErrorTest("select unknownObject", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.UnknownObjectType);
            DoErrorTest("select folderElementObj where unknownAttribute == 'a'", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.UnknownAttributeType);
            DoErrorTest("select folderElementObj where name == @name", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.UnknownQueryParameter);
            DoErrorTest("select folderElementObj where name + 3 == 'a'", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.QueryDataTypesMismatch);
            DoErrorTest("select folderElementObj where name == 1", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.QueryDataTypesMismatch);
            DoErrorTest("select folderElementObj where id >= 'a'", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.QueryDataTypesMismatch);
            DoErrorTest("select object", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.CannotQuerySystemObjects);
            DoErrorTest("select schema", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.CannotQuerySystemObjects);

            DoErrorTest("select folderObj where name==@name",
                        new List<CsAlCore.Query.QueryParameter>()
                        {
                            new CsAlCore.Query.QueryParameter("name", "test"),
                            new CsAlCore.Query.QueryParameter("name", "test2"),
                        },
                        CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.DuplicatedParameterName);

            DoErrorTest("select articleObj where name in ('a', 1, 'b')", null, CsAlCore.Common.Exceptions.CsAlException.ErrorCodes.QueryDataTypesMismatch);
        } // QueryErrors
    }
}
