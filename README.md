# CsAlCore

CsAlCore is a database abstraction layer library.

It supports

- MariaDb

- MySQL
- Oracle
- PostgreSQL
- SQLite
- Sql Server

CsAlCore is developed in C# and it is compatible with the .NET framework 4.6.2 and .NET core 2.0

The data model is defined in an XML file, objects can be queried using a SQL-like language.

For a quick guide read the [getting started](https://bitbucket.org/paoloiommarini/csalcore/wiki/Getting%20started) guide

For further informations read the [CsAl Core concepts](https://bitbucket.org/paoloiommarini/csalcore/wiki/Concepts) documentation.