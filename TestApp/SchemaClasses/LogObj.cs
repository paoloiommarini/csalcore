﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Newtonsoft.Json;

namespace SchemaClasses
{
  // Autogenerated class
  // Schema name    : TestSchema
  // Schema revision: 1
  /// <summary>
  /// Application log
  /// </summary>
  public partial class LogObj : CsAlCore.Common.SchemaObject
  {
    #region Attribute names
    public const string IdAttrName = "Id";
    public const string ProcedureNameAttrName = "ProcedureName";
    public const string UserRefAttrName = "UserRef";
    public const string MessageAttrName = "Message";
    public const string DataAttrName = "Data";
    public const string CreatedAttrName = "Created";
    #endregion

    #region Constructors
    public LogObj(GenericObject obj)
    {
      if (obj == null)
        throw new ArgumentNullException("obj");
      if (obj.ObjectTypeName != ObjectTypeName)
        throw new Exception(string.Format("Cannot create logObj from {0}", obj.ObjectTypeName));
      m_GenericObject = obj;
    }

    public LogObj(Schema schema)
    {
      if (schema == null)
        throw new ArgumentNullException("schema");
      m_GenericObject = GenericObject.Create(schema, ObjectTypeName);
      if (m_GenericObject == null)
        throw new Exception(string.Format("Cannot create GenericObject of type {0}", ObjectTypeName));
    }

    #endregion

    public override string ObjectTypeName {
      get { return "logObj"; }
    }

    #region Attribute types
    public ObjectAttribute IdAttrType {
      get { return m_GenericObject.GetAttribute(IdAttrName).Attribute; }
    }

    public ObjectAttribute ProcedureNameAttrType {
      get { return m_GenericObject.GetAttribute(ProcedureNameAttrName).Attribute; }
    }

    public ObjectAttribute UserRefAttrType {
      get { return m_GenericObject.GetAttribute(UserRefAttrName).Attribute; }
    }

    public ObjectAttribute MessageAttrType {
      get { return m_GenericObject.GetAttribute(MessageAttrName).Attribute; }
    }

    public ObjectAttribute DataAttrType {
      get { return m_GenericObject.GetAttribute(DataAttrName).Attribute; }
    }

    public ObjectAttribute CreatedAttrType {
      get { return m_GenericObject.GetAttribute(CreatedAttrName).Attribute; }
    }

    #endregion

    #region Public properties
    public ObjectType ObjectType {
      get { return m_GenericObject.ObjectType; }
    }

    public long VersionNumber {
      get { return m_GenericObject.VersionNumber; }
    }

    public long Id {
      get { return m_GenericObject.Id; }
    }

    public string ProcedureName {
      get { return (string)m_GenericObject.GetAttributeValue(ProcedureNameAttrName); }
      set { m_GenericObject.SetAttributeValue(ProcedureNameAttrName, value); }
    }

    public UserObj UserRef {
      get {
        GenericObject obj = m_GenericObject.GetAttributeValue(UserRefAttrName) as GenericObject;
        if (obj == null)
          return null;
        return new UserObj(obj);
      }
      set {
        GenericObject obj = value?.ToGenericObject();
        m_GenericObject.SetAttributeValue(UserRefAttrName, obj);
      }
    }

    public long? UserRefId {
      get {
        object gObj = m_GenericObject.GetAttributeValue(UserRefAttrName);
        if (gObj is GenericObject)
          return ((GenericObject)gObj).Id;
        if (gObj is int)
          return (long)(int)gObj;
        if (gObj is long)
          return (long)gObj;
        return null;
      }
      set {
        if (m_GenericObject.GetAttributeValue(UserRefAttrName) as GenericObject == null)
            m_GenericObject.SetAttributeValue(UserRefAttrName, value);
        else
            throw new NotSupportedException("Cannot set Id of a reference set to an object");
      }
    }

    public string Message {
      get { return (string)m_GenericObject.GetAttributeValue(MessageAttrName); }
      set { m_GenericObject.SetAttributeValue(MessageAttrName, value); }
    }

    public byte[] Data {
      get { return (byte[])m_GenericObject.GetAttributeValue(DataAttrName); }
      set { m_GenericObject.SetAttributeValue(DataAttrName, value); }
    }

    /// <summary>
    /// The creation date time
    /// </summary>
    public DateTime? Created {
      get { return (DateTime?)m_GenericObject.GetAttributeValue(CreatedAttrName); }
    }

    #endregion

    #region Public operations
    public static LogObj Deserialize(Schema schema, Stream stream) {
      using (XmlReader xr = XmlReader.Create(stream)) {
      return new LogObj(GenericObject.Deserialize(schema, xr));
      }
    }

    public string SerializeToJSON() {
      return m_GenericObject.SerializeToJSON();
    }

    public static LogObj DeserializeFromJSON(Schema schema, Stream stream) {
      using (StreamReader sr = new StreamReader(stream)) {
        using (JsonTextReader jr = new JsonTextReader(sr)) {
          return new LogObj(GenericObject.Deserialize(schema, jr));
        }
      }
    }
    #endregion
  }
}
