﻿using CsAlCore.Common.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Newtonsoft.Json;

namespace SchemaClasses
{
  // Autogenerated class
  // Schema name    : TestSchema
  // Schema revision: 1
  public partial class OrderDetailObj : CsAlCore.Common.SchemaObject
  {
    #region Attribute names
    public const string IdAttrName = "Id";
    public const string CreatedAttrName = "Created";
    public const string ModifiedAttrName = "Modified";
    public const string ArticleRefAttrName = "ArticleRef";
    public const string QuantityAttrName = "Quantity";
    public const string ParentObjRefAttrName = "ParentObjRef";
    #endregion

    #region Constructors
    public OrderDetailObj(GenericObject obj)
    {
      if (obj == null)
        throw new ArgumentNullException("obj");
      if (obj.ObjectTypeName != ObjectTypeName)
        throw new Exception(string.Format("Cannot create orderDetailObj from {0}", obj.ObjectTypeName));
      m_GenericObject = obj;
    }

    public OrderDetailObj(Schema schema)
    {
      if (schema == null)
        throw new ArgumentNullException("schema");
      m_GenericObject = GenericObject.Create(schema, ObjectTypeName);
      if (m_GenericObject == null)
        throw new Exception(string.Format("Cannot create GenericObject of type {0}", ObjectTypeName));
    }

    #endregion

    public override string ObjectTypeName {
      get { return "orderDetailObj"; }
    }

    #region Attribute types
    public ObjectAttribute IdAttrType {
      get { return m_GenericObject.GetAttribute(IdAttrName).Attribute; }
    }

    public ObjectAttribute CreatedAttrType {
      get { return m_GenericObject.GetAttribute(CreatedAttrName).Attribute; }
    }

    public ObjectAttribute ModifiedAttrType {
      get { return m_GenericObject.GetAttribute(ModifiedAttrName).Attribute; }
    }

    public ObjectAttribute ArticleRefAttrType {
      get { return m_GenericObject.GetAttribute(ArticleRefAttrName).Attribute; }
    }

    public ObjectAttribute QuantityAttrType {
      get { return m_GenericObject.GetAttribute(QuantityAttrName).Attribute; }
    }

    public ObjectAttribute ParentObjRefAttrType {
      get { return m_GenericObject.GetAttribute(ParentObjRefAttrName).Attribute; }
    }

    #endregion

    #region Public properties
    public ObjectType ObjectType {
      get { return m_GenericObject.ObjectType; }
    }

    public long VersionNumber {
      get { return m_GenericObject.VersionNumber; }
    }

    /// <summary>
    /// The object unique identifier
    /// </summary>
    public long Id {
      get { return m_GenericObject.Id; }
    }

    /// <summary>
    /// The object creation date time
    /// </summary>
    public DateTime? Created {
      get { return (DateTime?)m_GenericObject.GetAttributeValue(CreatedAttrName); }
    }

    /// <summary>
    /// The object last modified date time
    /// </summary>
    public DateTime? Modified {
      get { return (DateTime?)m_GenericObject.GetAttributeValue(ModifiedAttrName); }
    }

    public ArticleObj ArticleRef {
      get {
        GenericObject obj = m_GenericObject.GetAttributeValue(ArticleRefAttrName) as GenericObject;
        if (obj == null)
          return null;
        return new ArticleObj(obj);
      }
      set {
        GenericObject obj = value?.ToGenericObject();
        m_GenericObject.SetAttributeValue(ArticleRefAttrName, obj);
      }
    }

    public long? ArticleRefId {
      get {
        object gObj = m_GenericObject.GetAttributeValue(ArticleRefAttrName);
        if (gObj is GenericObject)
          return ((GenericObject)gObj).Id;
        if (gObj is int)
          return (long)(int)gObj;
        if (gObj is long)
          return (long)gObj;
        return null;
      }
      set {
        if (m_GenericObject.GetAttributeValue(ArticleRefAttrName) as GenericObject == null)
            m_GenericObject.SetAttributeValue(ArticleRefAttrName, value);
        else
            throw new NotSupportedException("Cannot set Id of a reference set to an object");
      }
    }

    public long Quantity {
      get { return (long)m_GenericObject.GetAttributeValue(QuantityAttrName); }
      set { m_GenericObject.SetAttributeValue(QuantityAttrName, value); }
    }

    /// <summary>
    /// Reference to the parent object
    /// </summary>
    public OrderObj ParentObjRef {
      get {
        GenericObject obj = m_GenericObject.GetAttributeValue(ParentObjRefAttrName) as GenericObject;
        if (obj == null)
          return null;
        return new OrderObj(obj);
      }
      set {
        GenericObject obj = value?.ToGenericObject();
        m_GenericObject.SetAttributeValue(ParentObjRefAttrName, obj);
      }
    }

    public long? ParentObjRefId {
      get {
        object gObj = m_GenericObject.GetAttributeValue(ParentObjRefAttrName);
        if (gObj is GenericObject)
          return ((GenericObject)gObj).Id;
        if (gObj is int)
          return (long)(int)gObj;
        if (gObj is long)
          return (long)gObj;
        return null;
      }
      set {
        if (m_GenericObject.GetAttributeValue(ParentObjRefAttrName) as GenericObject == null)
            m_GenericObject.SetAttributeValue(ParentObjRefAttrName, value);
        else
            throw new NotSupportedException("Cannot set Id of a reference set to an object");
      }
    }

    #endregion

    #region Public operations
    public static OrderDetailObj Deserialize(Schema schema, Stream stream) {
      using (XmlReader xr = XmlReader.Create(stream)) {
      return new OrderDetailObj(GenericObject.Deserialize(schema, xr));
      }
    }

    public string SerializeToJSON() {
      return m_GenericObject.SerializeToJSON();
    }

    public static OrderDetailObj DeserializeFromJSON(Schema schema, Stream stream) {
      using (StreamReader sr = new StreamReader(stream)) {
        using (JsonTextReader jr = new JsonTextReader(sr)) {
          return new OrderDetailObj(GenericObject.Deserialize(schema, jr));
        }
      }
    }
    #endregion
  }
}
