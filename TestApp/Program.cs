﻿using CsAlCore.Common.Schema;
using Microsoft.Extensions.Configuration;
using SchemaClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("CsAl Test application");
            Console.ResetColor();
            CsAlCore.CsAlCore.VersionInfo ver = CsAlCore.CsAlCore.Version;
            Console.WriteLine("Using CsAl v.{0} [{1}]", ver.Version, ver.BuildDate.ToShortDateString());
            Console.WriteLine();

            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            var appSettings = new ConfigurationBuilder()
                                  .SetBasePath(Directory.GetCurrentDirectory())
                                  .AddJsonFile("TestApp.json")
                                  .Build();

            string sqLiteFile = string.Empty;
            CsAlCore.CsAlCore.DbType dbtype = CsAlCore.CsAlCore.DbType.None;
            string dbConn = appSettings["DbConnString"];
            if (!Enum.TryParse(appSettings["DbType"], true, out dbtype)) {
                Console.WriteLine($"Unknown database type: { appSettings["DbType"] }");
                return;
            }

            if (dbtype == CsAlCore.CsAlCore.DbType.SQLite) {
                int start = dbConn.IndexOf("Data Source=", 0, StringComparison.InvariantCultureIgnoreCase);
                if (start >= 0) {
                    start += 12;
                    int end = dbConn.IndexOf(";", start, StringComparison.InvariantCultureIgnoreCase);
                    if (end >= 0)
                        sqLiteFile = dbConn.Substring(start, end - start);
                }
                if (File.Exists(sqLiteFile)) {
                    int row = Console.CursorTop;
                    while (true) {
                        Console.Write("Delete existing file {0} (y/N)? ", sqLiteFile);
                        string res = Console.ReadLine()?.Trim();
                        if (string.Compare(res, "y", StringComparison.OrdinalIgnoreCase) == 0) {
                            File.Delete(sqLiteFile);
                            break;
                        } else if (string.IsNullOrEmpty(res) || string.Compare(res, "n", StringComparison.OrdinalIgnoreCase) == 0) {
                            break;
                        }
                    }
                    Console.CursorTop = row;
                    Console.Write(new string(' ', Console.WindowWidth - 1));
                    Console.CursorLeft = 0;
                }
            }

            CsAlCore.Common.Utility.LogFile.LogLevels logLevel = CsAlCore.Common.Utility.LogFile.LogLevels.Info;
            Enum.TryParse(appSettings["LogLevel"], out logLevel);

            CsAlCore.Configuration config = new CsAlCore.Configuration()
            {
                DbType = dbtype,
                DbConnectionString = dbConn,
                LogLevel = logLevel,
                LogPath = ".\\logs",
            };
            CsAlCore.CsAlCore csal = new CsAlCore.CsAlCore(config);
            if (!csal.Open()) {
                Console.WriteLine("Failed to open database");
                return;
            }            
            Console.WriteLine($"Database { csal.DatabaseType } v.{ csal.DatabaseVersion} opened");

            if (csal.DbSchema == null) {
                Schema schema = Schema.Load($"..{ Path.DirectorySeparatorChar }..{ Path.DirectorySeparatorChar }..{ Path.DirectorySeparatorChar }Schema{ Path.DirectorySeparatorChar }test.xml");
                if (schema != null) {
                    Console.Write("Creating database tables...");
                    csal.UpgradeSchema(schema);
                    Console.WriteLine("DONE");

                    Console.Write("Setting env variables...");
                    string dataPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Binaries");
                    if (Directory.Exists(dataPath))
                        Directory.Delete(dataPath, true);
                    csal.SetEnvVariable(EnvVariable.Names.BinaryDataRoot, dataPath);
                    csal.SetEnvVariable(EnvVariable.Names.BinaryDataMaxFiles, 1000);
                    Console.WriteLine("DONE");
                    CreateInitialData(csal, 2000);
                }
            } else {
                Console.WriteLine("Schema found: {0} rev. {1}", csal.DbSchema.Name, csal.DbSchema.Revision);
                Console.WriteLine("Object types:");
                foreach (ObjectType ot in csal.DbSchema.ObjectTypes.OrderBy(ot => ot.Name)) {
                    if (!ot.SystemObject)
                        Console.WriteLine("  {0}", ot.Name);
                }
            }
            Console.WriteLine();

            // TEST 
            try {
                // Query objects non inheriting from object
                List<GenericObject> logs = csal.LoadObjects("select logObj where created >= '2016-01-01T00:00:00.000Z'", null, CsAlCore.CsAlCore.DeletedObjectsStrategy.OnlyLive, 0).ToList();

                // Update a subject:
                GenericObject subject = csal.LoadObjects("select subjectObj order by id limit 1").FirstOrDefault();
                if (subject != null) {
                    SubjectObj s = new SubjectObj(subject);
                    s.Name = $"{s.Name} modified";
                    csal.UpdateObject(s);
                }

                List<long> res = null;
                // Soft delete articleObj, extending to orderDetailObj
                Console.Write("Soft deleting articleObj: ");
                res = csal.SoftDeleteObject(20015, new CsAlCore.CsAlCore.DeleteOptions(string.Empty, "orderDetailObj.articleRef"));
                Console.WriteLine(string.Join(",", res));
                // Restore articleObj
                Console.Write("Restoring articleObj: ");
                res = csal.RestoreObject(20015, new CsAlCore.CsAlCore.RestoreOptions("orderDetailObj.articleRef"));
                Console.WriteLine(string.Join(",", res));
                // Hard delete articleObj, extending to orderDetailObj
                //Console.Write("Hard deleting articleObj: ");
                //res = csal.DeleteObject(20015, new CsAl.CsAl.DeleteOptions(string.Empty, "orderDetailObj.articleRef"));
                //Console.WriteLine(string.Join(',', res));

                // Soft delete orderObj, unreferencing it from shipmentObj
                Console.Write("Soft deleting orderObj: ");
                res = csal.SoftDeleteObject(10015, new CsAlCore.CsAlCore.DeleteOptions("shipmentObj.orders", string.Empty));
                Console.WriteLine(string.Join(",", res));
                // Restore orderObj
                Console.Write("Restoring orderObj: ");
                res = csal.RestoreObject(10015, new CsAlCore.CsAlCore.RestoreOptions(string.Empty));
                Console.WriteLine(string.Join(",", res));              
            } catch (CsAlCore.Common.Exceptions.CsAlException ex) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Test error: { ex.Message }");
                Console.ResetColor();
            }
            // END TEST

            List <CsAlCore.Common.IdName> searchRes = null;
            while (true) {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Search:");
                Console.ResetColor();
                string query = Console.ReadLine().Trim();
                if (string.IsNullOrEmpty(query))
                    break;
                try {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    ObjectType objectType = null;
                    searchRes = csal.SearchObjs(query, out objectType);
                    sw.Stop();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Objects  : {0} {1} [{2} ms]", searchRes.Count.ToString("###,###,##0", CultureInfo.CurrentCulture), objectType?.Name, sw.ElapsedMilliseconds.ToString("###,###,###"));

                    Console.Write("Load time: ");
                    int left = Console.CursorLeft;
                    Console.Write("0%");
                    sw.Restart();

                    int count = 0;
                    int? oldPerc = null;
                    Console.CursorVisible = false;
                    foreach (GenericObject obj in csal.LoadObjects(searchRes, objectType, maxDepth: -1)) {
                        //string json = obj.SerializeToJSON();
                        //GenericObject deserialize = GenericObject.DeserializeFromJSON(csal.DbSchema, json);

                        int perc = (int)Math.Round((double)++count / (double)searchRes.Count * 100.0);
                        if (oldPerc == null || perc != oldPerc.Value) {
                            Console.SetCursorPosition(left, Console.CursorTop);
                            Console.Write($"{ perc.ToString("##0", CultureInfo.CurrentCulture) }%");
                            oldPerc = perc;
                        }
                    }
                    //List<GenericObject> objs = csal.BulkLoadObjects(searchRes);
                    sw.Stop();
                    Console.CursorVisible = true;
                    Console.WriteLine(" [{0} ms]", sw.ElapsedMilliseconds.ToString("###,###,##0"));

                    Console.ResetColor();
                } catch (Exception ex) {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception: {0}", ex.Message);
                    Console.ResetColor();
                } finally {
                    Console.WriteLine();
                }
            }

            csal.Dispose();
        }

        private static void CreateInitialData(CsAlCore.CsAlCore csal, int objectsNumber)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Random rnd = new Random();
            List<CsAlCore.Common.SchemaObject> objs = new List<CsAlCore.Common.SchemaObject>();

            Console.Write("Creating default users...");
            // Create default user groups
            UserGroupObj adminUg = new UserGroupObj(csal.DbSchema);
            adminUg.Name = "Administrators";
            long adminGroupId = csal.CreateObject(adminUg);

            UserGroupObj ug = new UserGroupObj(csal.DbSchema);
            ug.Name = "Users";
            csal.CreateObject(ug);

            // Create default user
            UserObj user = new UserObj(csal.DbSchema);
            user.Name = "Administrator";
            user.UserName = "Administrator";
            user.Password = CsAlCore.Common.Utility.String.GetMD5Hash("csal");
            user.Groups.Add(new UserObj.GroupsValue(user.CreateGroupsMultiRefValue(adminUg.ToGenericObject())) { Notes = "ciccio pasticcio" });
            csal.CreateObject(user);
            Console.WriteLine("DONE");

            Console.Write("Creating folders...");
            FolderObj fo = new FolderObj(csal.DbSchema);
            fo.Name = "root";
            fo.Description = "Root folder";
            fo.ParentRef = null;
            long rootId = csal.CreateObject(fo);
            for (int i = 0; i < 10; i++) {
                fo = new FolderObj(csal.DbSchema);
                fo.Name = string.Format("Folder number {0}", i);
                fo.Description = string.Format("Description for folder number {0}", i);
                fo.ParentRefId = rootId;
                objs.Add(fo);
            }
            List<long> folderIds = CreateObjs(csal, objs);
            objs.Clear();
            Console.WriteLine("DONE");

            // Create textFolderElements
            Console.Write("Creating textFolderElements");
            for (int i = 0; i < objectsNumber; i++) {
                TextFolderElementObj tfe = new TextFolderElementObj(csal.DbSchema);
                tfe.Name = string.Format("TextFolderElement number {0}", i);
                tfe.ReadTime = TimeSpan.FromSeconds(90);
                tfe.Category = TextFolderElementObj.CategoryEnum.News;
                tfe.Categories = TextFolderElementObj.CategoriesEnum.Economy | TextFolderElementObj.CategoriesEnum.Local;
                tfe.FolderRefId = folderIds[rnd.Next(0, folderIds.Count)];
                tfe.Text = "test";
                objs.Add(tfe);
            }
            CreateObjs(csal, objs);
            objs.Clear();
            Console.WriteLine("DONE");

            // Create articles:
            Console.Write("Creating articles");
            List<long> artIds = null;
            FileInfo fi = new FileInfo("article.jpg");
            for (int i = 0; i < objectsNumber; i++) {
                ArticleObj art = new ArticleObj(csal.DbSchema);
                art.Name = string.Format("Article number {0}", i);
                art.Weight = rnd.Next(200, 2000);
                art.Volume = rnd.Next(200, 2000);
                //art.Image = new BinaryDataValue()
                //{
                //    FileName = fi.Name,
                //    Size = fi.Length,
                //    MimeType = "image/jpeg",
                //    Stream = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read, FileShare.Read)
                //};
                objs.Add(art);
            }
            artIds = CreateObjs(csal, objs);
            objs.Clear();
            Console.WriteLine("DONE");

            // Create subjects:
            Console.Write("Creating subjects");
            List<long> subjIds = null;
            for (int i = 0; i < objectsNumber; i++) {
                SubjectObj subj = new SubjectObj(csal.DbSchema);
                subj.Name = string.Format("Subject number {0}", i);

                for (int j = 0; j < 2; j++) {
                    AddressObj adr = new AddressObj(csal.DbSchema);
                    adr.Address = string.Format("Address number {0}", j);
                    adr.City = "Some City";
                    adr.ZipCode = "12345";

                    subj.Addresses.Add(new SubjectObj.AddressesValue(csal.DbSchema, adr.ToGenericObject()));
                }
                objs.Add(subj);
            }
            subjIds = CreateObjs(csal, objs);
            objs.Clear();
            Console.WriteLine("DONE");

            // Create orders:
            Console.Write("Creating orders");
            for (int i = 0; i < objectsNumber; i++) {
                OrderObj ord = new OrderObj(csal.DbSchema);
                ord.Name = string.Format("Order number {0}", i);
                ord.Priority = OrderObj.PriorityEnum.Normal;
                ord.Status = OrderObj.StatusEnum.Pending;
                ord.SubjectRefId = subjIds[rnd.Next(0, subjIds.Count)];

                int rows = 5; // rnd.Next(2, 10);
                for (int r = 0; r < rows; r++) {
                    OrderDetailObj ordDet = new OrderDetailObj(csal.DbSchema);
                    ordDet.ArticleRefId = artIds[rnd.Next(0, artIds.Count)];
                    ordDet.Quantity = rnd.Next(1, 100);

                    ord.Detail.Add(new OrderObj.DetailValue(ord.CreateDetailMultiRefValue(ordDet.ToGenericObject())));
                }
                objs.Add(ord);
            }
            List<long> ordIds = CreateObjs(csal, objs);
            objs.Clear();
            Console.WriteLine("DONE");

            // Shipment
            Console.Write("Creating shipment...");
            ShipmentObj shp = new ShipmentObj(csal.DbSchema);
            shp.Name = "Test shipment";
            shp.RecipientRefId = subjIds[rnd.Next(0, subjIds.Count)];
            foreach (long orderId in ordIds)
                shp.Orders.Add(new ShipmentObj.OrdersValue(csal.DbSchema, orderId) { Notes = "order notes", IntValue = 0 });
            csal.CreateObject(shp);
            Console.WriteLine("DONE");

            // Create some log
            Console.Write("Creating logObj");
            for (int i = 0; i < objectsNumber; i++) {
                LogObj log = new LogObj(csal.DbSchema);
                log.Message = $"Log message number {i}";
                objs.Add(log);
            }
            CreateObjs(csal, objs);
            objs.Clear();
            Console.WriteLine("DONE");

            // TEST
            OrderObj tOrd = new OrderObj(csal.LoadObject(ordIds[0], maxDepth: -1));
            if (tOrd.Detail != null) {
                tOrd.Detail.RemoveAt(0);
                OrderDetailObj dObj = new OrderDetailObj(tOrd.Detail[0].RefObject);
                dObj.Quantity = 150;
            }
            csal.UpdateObject(tOrd);

            shp.Orders[10].Notes = "note updated";
            csal.UpdateObject(shp);
            // END TEST

            sw.Stop();
            Console.WriteLine("CreateInitialData: {0}", sw.Elapsed);
        } // CreateInitialData

        private static List<long> CreateObjs(CsAlCore.CsAlCore csal, List<CsAlCore.Common.SchemaObject> objs)
        {
            List<long> res = new List<long>();
            while (objs.Count > 0) {
                int count = 100;
                if (count > objs.Count)
                    count = objs.Count;
                List<CsAlCore.Common.SchemaObject> tObjs = objs.Take(count).ToList();
                objs.RemoveRange(0, count);
                res.AddRange(csal.CreateObjects(tObjs));
                Console.Write(".");
            }

            return res;
        } // CreateObjs
    }
}
